<?php

use Phalcon\Acl;
use Phalcon\Acl\Role;
use Phalcon\Acl\Resource;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl\Adapter\Memory as AclList;

/**
 * SecurityPlugin
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class SecurityPlugin2 extends Plugin
{

	public function initialize()
	{
		$this->persistent->remove('acl');
	}

	/**
	 * Returns an existing or new access control list
	 *
	 * @returns AclList
	 */
	public function getAcl()
	{

		//throw new \Exception("something");

		//$this->persistent->remove('acl');

		//if (!isset($this->persistent->acl)) {

			$acl = new AclList();


			$acl->setDefaultAction(Acl::ALLOW);

			//Register roles
			$roles = array(
				'users'  => new Role('Users'),
				'userAdmins' => new Role('userAdmins'),
				'ABstaff' => new Role('ABstaff'),
				'superAdmins' => new Role('superAdmins'),
				'guests' => new Role('Guests')
			);
			foreach ($roles as $role) {
				$acl->addRole($role);
			}

			//Private area resources
			$privateBoardResources = array(
				'users'	   => array('index')
			);

			$privateBoardAdminsResources = array(
				0 => array(0)
			);

			$privateAbResources = array(
				0 => array(0)
			);

			$privateSuperResources = array(
				0 => array(0)
			);


			foreach ($privateBoardResources as $resource => $actions) {
				$acl->addResource(new Resource($resource), $actions);
			}

			//Public area resources
			$publicResources = array(
				'index'      => array('index'),
				'api'   	 => array('index','updateUser','createUser','authUser'),
				'register'   => array('index','addUsers'),
				'signup'	 => array('index','registerUser'),
				'errors'     => array('show404', 'show500', 'show401')
			);
			foreach ($publicResources as $resource => $actions) {
				$acl->addResource(new Resource($resource), $actions);
			}

			//Grant access to public areas to both users and guests
			foreach ($roles as $role) {
				foreach ($publicResources as $resource => $actions) {
					foreach ($actions as $action){
						$acl->allow($role->getName(), $resource, $action);
					}
				}
			}

			//Grant acess to private area to role Users
			foreach ($privateBoardResources as $resource => $actions) {
				foreach ($actions as $action){
					$acl->allow('Users', $resource, $action);
					$acl->allow('userAdmins', $resource, $action);
					$acl->allow('superAdmins', $resource, $action);
					$acl->allow('ABstaff', $resource, $action);
				}
			}

			//Grant acess to private area to role Users
			/*foreach ($privateBoadAdminsResources as $resource => $actions) {
				foreach ($actions as $action){
					$acl->allow('userAdmins', $resource, $action);
					$acl->allow('superAdmins', $resource, $action);
				}
			}

			//Grant acess to private area to role Users
			foreach ($privateAbResources as $resource => $actions) {
				foreach ($actions as $action){
					$acl->allow('ABstaff', $resource, $action);
					$acl->allow('superAdmins', $resource, $action);
				}
			}

			//Grant acess to private area to role Users
			foreach ($privateSuperResources as $resource => $actions) {
				foreach ($actions as $action){
					$acl->allow('superAdmins', $resource, $action);
				}
			}

			//The acl is stored in session, APC would be useful here too*/
			$this->persistent->acl = $acl;
		//}

		//return $acl;

		//$this->persistent->remove('acl');

		return $this->persistent->acl;
	}

	/**
	 * This action is executed before execute any action in the application
	 *
	 * @param Event $event
	 * @param Dispatcher $dispatcher
	 */
	public function beforeDispatch(Event $event, Dispatcher $dispatcher)
	{

		$this->persistent->remove('acl');

		$auth = $this->session->get('auth');
		if (!$auth){
			$role = 'Guests';
		} else {
			$role = 'Users';
		}

		$controller = $dispatcher->getControllerName();
		$action = $dispatcher->getActionName();

		$acl = $this->getAcl();

		//print_r($acl);

		echo $role.'<br/>';
		echo $controller.'<br/>';
		echo $action.'<br/>';

		$allowed = $acl->isAllowed($role, $controller, $action);

		if ($allowed != Acl::ALLOW) {
			$dispatcher->forward(array(
				'controller' => 'errors',
				'action'     => 'show401'
			));
			return false;
		}
	}
}
