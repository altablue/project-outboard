
{{ content() }}


<div class="boxTop row">
	<div class="row">
		<h2 class="col-lg-6">Your Recruiter Companies</h2>
		<form class="col-lg-4 offset-col-lg-2">
	        <div class="row">
	          <div class="col-lg-12">
	            <div class="input-group">
	              <span class="input-group-btn">
	                <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
	              </span>
	              <input type="text" class="form-control" name="keyword" placeholder="Search for...">
	            </div><!-- /input-group -->
	          </div><!-- /.col-lg-6 -->
	        </div><!-- /.row -->
		</form>
	</div>
	<hr/>
</div>

<div class="getAllWrapper">

	<table class="table table-striped footable">

	<thead>
		<tr>
			<th data-sort-ignore="true"><strong>Company Name</strong></th>
			<th data-sort-ignore="true" data-hide="phone,tablet"><strong>Company Location</strong></th>
			<th data-sort-ignore="true" data-hide="phone,tablet"><strong>Company Type</strong></th>
			<th data-sort-ignore="true" data-hide="phone,tablet"></th>
			<th data-sort-ignore="true" data-hide="phone,tablet"></th>
		</tr>
	</thead>

	{% for comps in companies %}

		<tr>
			<td>{{ comps.name }}</td>
			<td>{{ comps.location }}</td>
			<td>{{ comps.type }}</td>
			<td>
				<a href="/dashboard/board/users/{{ comps.id }}/" class="btn btn-primary">view company users</a>
			</td>
			<td>
				<a href="/dashboard/board/candidates/{{ comps.id }}/" class="btn btn-primary">view company candidates</a>
				<a href="/dashboard/board/jobs/{{ comps.id }}/" class="btn btn-primary">view company job adverts</a>
				<a href="/dashboard/board/uncomplete/{{ comps.id }}/" class="btn btn-warning">view uncomplete candidates</a>
			</td>
		</tr>

	{% endfor %}

		<tfoot>
			<tr>
				<td colspan="5">
					<div class="pagination pagination-centered hide-if-no-paging pager col-xs-12"></div>
				</td>
			</tr>
		</tfoot>

	</table>

</div>