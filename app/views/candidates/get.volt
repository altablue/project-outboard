{{ content() }}


<div id="profiles" class="clearfix" ng-controller="theProfiles">

    <div ng-bind-html="flashStatus"></div>

    <script type="text/ng-template" id="contactModal.html">
      <div class="modal-header">
        <h3>Contact Request:</h3>
        <button type="button" class="close" ng-click="cancel()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        
        <fieldset>
            <form>

                <input class="form-control" ng-model="emailApi.boardId" ng-init="emailApi.boardId='{{board}}'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="emailApi.candId" ng-init="emailApi.candId='{{id}}'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="emailApi.candComp" ng-init="emailApi.candComp='{{candComp}}'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="emailApi.type" ng-init="emailApi.type='cand'" type="hidden" disabled="disabled" />

                <div class="form-group">
                    <label>Your Name</label>
                    <input class="form-control" ng-model="emailApi.name" ng-init="emailApi.name='{{name}}'" type="text" disabled="disabled" value="{{name}}" />
                </div>

                <div class="form-group">
                    <label>Company Name</label>
                    <input class="form-control" ng-model="emailApi.boardName"  ng-init="emailApi.boardName='{{boardName}}'"  type="text" disabled="disabled" value="{{boardName}}" />
                </div>

                <p>Please confirm you would like to register your intention of contacting the following candidate:</p>

                <div class="form-group">
                    <label class="col-xs-12">Candidate Overview:</label>
                    <br/><br/>
                    <div class="row col-xs-6">
                        <div class="left col-xs-4">
                            <figure>
                                <img style="max-width:100%" alt="User Pic" src="/imgs/avatar/default.png" class="img-circle"> 
                            </figure>
                        </div>
                        <div class="right col-xs-6">
                            <p>Candidate #{{ id }}</p>
                            <p>{{discipline}}</p>
                            <p>{{locationnice}}</p>
                            <p>{{availability}} hrs/pw</p>
                        </div>
                    </div>
                </div>
            </form>
        </fieldset>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" ng-click="cancel()">Close</button>
        <button type="button" class="btn btn-primary" ng-click="ok()">Send Contact Request</button>
      </div>
    </script>

    <?php if($userLevel >= 99): ?>
        <script type="text/ng-template" id="fillModal.html">
          <div class="modal-header">
            <h3>Contact Request:</h3>
            <button type="button" class="close" ng-click="cancel()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            
            <fieldset>
                <form>

                    <input class="form-control" ng-model="fillApi.boardId" ng-init="fillApi.boardId='{{board}}'" type="hidden" disabled="disabled" />
                    <input class="form-control" ng-model="fillApi.candId" ng-init="fillApi.candId='{{id}}'" type="hidden" disabled="disabled" />
                    <input class="form-control" ng-model="fillApi.candComp" ng-init="fillApi.candComp='{{candComp}}'" type="hidden" disabled="disabled" />
                    <input class="form-control" ng-model="fillApi.type" ng-init="fillApi.type='cand'" type="hidden" disabled="disabled" />
                    <div class="form-group" ng-init='refreshJobs(<?php echo json_encode($jobs); ?>);'>
                        <label>Assign to job:</label>
                        <div ng-if="browserDect.browser != 'Explorer' && browserDect.version > 8">
                            <ui-select ng-model="fillApi.job" theme="bootstrap" reset-search-input="true" search-enabled="true">
                                <ui-select-match placeholder="Please Select a Job from the list below...">[[$select.selected.title]]</ui-select-match>
                                <ui-select-choices repeat="j in job | propsFilter: {title: $select.search, comp: $select.search}">
                                  [[j.title]] - <em>([[j.comp]])</em>
                                </ui-select-choices>
                            </ui-select>
                        </div>
                    </div>

                    <p>Please confirm you would like to assign the following candidate to the vancy selected above:</p>

                    <div class="form-group">
                        <label class="col-xs-12">Candidate Overview:</label>
                        <br/><br/>
                        <div class="row col-xs-6">
                            <div class="left col-xs-4">
                                <figure>
                                    <img style="max-width:100%" alt="User Pic" src="/imgs/avatar/default.png" class="img-circle"> 
                                </figure>
                            </div>
                            <div class="right col-xs-6">
                                <p>Candidate #{{ id }}</p>
                                <p>{{discipline}}</p>
                                <p>{{locationnice}}</p>
                                <p>{{availability}} hrs/pw</p>
                            </div>
                        </div>
                    </div>
                </form>
            </fieldset>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" ng-click="cancel()">Close</button>
            <button type="button" class="btn btn-primary" ng-click="ok()">Send Contact Request</button>
          </div>
        </script>
    <?php endif; ?>

    <div class="boxTop" ng-init="userid='{{ recId }}'">

        <a href="javascript:history.back()" class="btn btn-primary ladda-button btn-primary" data-style="expand-right"><span class="ladda-label"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;&nbsp;Return to Search Results</span></a>
        <br/><br/>

        <h2 ng-init="candid='{{ id }}'">view profile: candidate #{{ id }}</h2>
        <hr/>
    </div>

    <div class="theDeets col-xs-12" ng-init="recName='{{name}}'">
        
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" id="avatar">
            <figure>
                <img alt="User Pic" src="/imgs/avatar/default.png" class="img-circle"> 
            </figure>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="profile">
           <article class="details">
                <?php if($userLevel >= 99): ?>
                    <p><strong>name: </strong> {{ candName }}</p>
                    <p><strong>email: </strong> {{ email }}</p>
                    <p><strong>phone #:</strong> {{phone}}</p>
                    <hr/>
                <?php endif; ?>
                <p>
                <p>{{title}}</p>
                <p>{{discipline}}</p>
                <p>{{locationnice}}</p>
                <p>Available for <span>{{availability}}</span><em> hrs/pw</em></p>
                <hr/>
                <p class="bio">{{profile}}</p>
                <hr/>
                <div class="skills">
                    {% for skill in skills %}
                        <span>{{skill}}</span>
                    {% endfor %}
                </div>
                <hr/>

                <?php if(!empty($qualifications) && is_array($qualifications)): ?>

                    <table class="table table-striped footable">
                        <thead class="hidden-lg hidden-md hidden-sm">
                            <th></th>
                            <th></th>
                            <th data-hide="phone"></th>
                            <th data-hide="phone"></th>
                            <th data-hide="phone"></th>
                        </thead>
                        <tbody>
                            {% for qual in qualifications %}
                                <tr>
                                    <td><img src="/imgs/svgs/qual-grey.svg" /></td>
                                    <td class="hidden-lg hidden-md hidden-sm">Degree #{{loop.index}}</td>
                                    <td>{% if qual.candQualTitle is defined %}{{qual.candQualTitle}}{% endif %}</td>
                                    <td>{% if qual.candQualDegree is defined %}{{qual.candQualDegree}}{% endif %}</td>
                                    <td>{% if qual.candQualField is defined %}{{qual.candQualField}}{% endif %}</td>
                                </tr>
                            {% endfor %}
                        </tbody>
                    </table>

                <?php else: ?>
                    <p>No qualifications</p>
                <?php endif; ?>

                <button  ng-if="global.isOnBoardComp() && global.isBoardUser()"  ng-click="contactThis()" onClick="return false;" ladda="emailLoading" data-style="expand-right" data-toggle="modal" data-target=".bs-example-modal-lg"  class="btn btn-warning ladda-button"><span class="ladda-label">contact candidate</span></button>

                <?php if($userLevel >= 99): ?>

                    <button  ng-if="global.isABStaff()"  ng-click="assignVac()" onClick="return false;" ladda="vacLoading" data-style="expand-right" data-toggle="modal" data-target=".bs-example-modal-lg"  class="btn btn-warning ladda-button"><span class="ladda-label">assign to vancancy</span></button>

                    <hr/>
                    <div ng-bind-html="noteFlashStatus"></div>

                    <form class="tables" onsubmit="return false;" ng-init='noteSetup("<?php echo (!empty($notes)? addslashes(json_encode($notes)) : ''); ?>")'>
                        <h4>Candidate Notes:</h4>
                        <br/>
                        <table class="table table-striped footable">
                            <thead>
                                <th data-sort-ignore="true"></th>
                            </thead>
                            <tbody>
                                <tr ng-repeat="note in theNotes track by $index">
                                    <td>
                                        <small><em>[[note.created ]]</em> created by <strong>[[note.redid]]</strong></small>
                                        <br/><br/>
                                        <div ng-bind-html="note.note"></div>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <div class="pagination pagination-centered hide-if-no-paging pager col-xs-12"></div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>

                        <textarea id="note" class='form-control' data-ck-editor ng-model='note' placeholder="Enter New Note..."></textarea>
                        <br/>
                        <button type="submit" class="btn btn-primary btn-lg ladda-button" ladda="noteCreateLoading" ng-click="createNote(note)" data-style="expand-right"><span class="ladda-label">save Note</span></button>

                    </form>
                    <hr/>

                <?php endif; ?>

            </article>
        </div>
    </div>
</div>