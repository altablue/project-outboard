
{{ content() }}

<div class="boxTop">
	<h2>Jobs Candidate #{{ candId }} Applied for</h2>
	<hr/>
</div>

<div class="getAllWrapper" ng-controller="recruitEdit">

	<div ng-bind-html="flashStatus"></div>
	
	<table class="table table-striped footable">

	<thead>
		<tr>
			<th data-sort-ignore="true" data-toggle="true"><strong>job id</strong></th>
			<th data-hide="phone"><strong>job title</strong></th>
			<th data-hide="phone"><strong>company</strong></th>
			<th data-hide="phone"><strong>location</strong></th>
			<th data-hide="phone"><strong>application status</strong></th>
			<th data-hide="phone" data-sort-ignore="true" data-toggle="true"></th>
		</tr>
	</thead>

	<tbody>

	{% for appJob in jobs %}
		
		<tr ng-init="showList[{{ loop.index }}] = false; cand[{{ loop.index }}] = {{ candId }}">
			<td>{{ appJob.job.ID }}</td>
			<td>{{ appJob.job.title }}</td>
			<td>{{ appJob.job.boards.name }}</td>
			<td>{{ appJob.job.locationnice }}</td>
			<td>
				<div ng-hide="!showList[{{ loop.index }}]">
					<ui-select ng-model="updateStatus[{{ loop.index }}]" theme="bootstrap" on-select="CandStatusChange($item, updateStatus, add, {{ loop.index }}, 'Fill', {{ appJob.job.ID }})"  ng-disabled="disabled">
	                    <ui-select-match placeholder="Please Select a Status...">[[$select.selected]]</ui-select-match>
	                    <ui-select-choices repeat="stat in applyStats">
	                      <div ng-bind-html="stat"></div>
	                    </ui-select-choices>
	                </ui-select>
				</div>
				{% if(appJob.status == 1) %}
					{% set status = 'Applied' %}
				{% elseif(appJob.status == 2) %}
					{% set status = 'Unsuccessful' %}
				{% elseif(appJob.status == 3) %}
					{% set status = 'Interview Stage' %}
				{% else %}
					{% set status = 'Successful' %}
				{% endif %}
				<span ng-init="updateStatus[{{ loop.index }}]='{{ status }}'" ng-hide="showList[{{ loop.index }}]" ng-bind="updateStatus[{{ loop.index }}]"></span>
			</td>
			<td>
				<a class="btn btn-primary" href="/dashboard/jobs/{{ appJob.job.ID }}">view advert</a>
				<a class="btn btn-warning" ng-click="showList[{{ loop.index }}] = true" onclick="return false;" href="">update application status</a>
			</td>
		</tr>

	{% endfor %}

<<<<<<< HEAD
	</tbody>

		<tfoot>
		<tr>
			<td colspan="5">
				<div class="pagination pagination-centered hide-if-no-paging pagination-sm pagination  pager col-sm-12"></div>
			</td>
		</tr>
	</tfoot>
=======
		<tfoot>
			<tr>
				<td colspan="5">
					<div class="pagination pagination-centered hide-if-no-paging pager col-xs-12"></div>
				</td>
			</tr>
		</tfoot>

>>>>>>> live-bugs

	</table>

</div>