
{{ content() }}


<div class="boxTop">
    <h2>Create Candidate</h2>
    <hr/>
</div>

<div class="regWrapper" ng-controller="candCreate">

    <div ng-bind-html="flashStatus"></div>

    {{ form('#', 'id': 'UserRegisterForm', 'onbeforesubmit': 'return false', 'onClick':'return false;', 'role':'form' ) }}

        <fieldset>

            {{ form.render('apiController', ['ng-model':'formData.apiController', 'ng-init':'formData.apiController="candidate"']) }}

            <input type="hidden" name="boardNum" ng-value="{{ boardNum }}" ng-model="formData.boardNum" ng-init="formData.boardNum={{ boardNum }}" />

            <div class="form-group">
                {{ form.label('candidateName', ['class': 'control-label']) }}
                {{ form.render('candidateName', ['class': 'form-control','required':'','ng-model':'formData.candidateName']) }}
                <p class="help-block">(required)</p>
            </div>    

            <div class="form-group">
                {{ form.label('candidateEmail', ['class': 'control-label']) }}
                {{ form.render('candidateEmail', ['class': 'form-control','required':'','ng-model':'formData.candidateEmail']) }}
                <p class="help-block">(required)</p>

            <div class="form-actions">
            <button class="btn btn-primary ladda-button" ladda="loading" ng-click="submitForm()" data-style="expand-right"><span class="ladda-label">Create Candidate</span></button>

            </div>

        </fieldset>

    </form>

    </form>

</div>