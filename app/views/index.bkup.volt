
<!DOCTYPE>
<!--[if lt IE 7 ]> <html class="ie6" id="root" lang="en"> <![endif]-->
<!--[if IE 7 ]> <html class="ie7" id="root" lang="en"> <![endif]-->
<!--[if IE 8 ]> <html class="ie8" id="root" lang="en"> <![endif]-->
<!--[if gt IE 8 ]> <html class="ie9" id="root" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html id="root" lang="en"> <!--<![endif]-->
    <head>
        <base href="https://www.matchpeopleskills.com/">
        {{ get_title() }}
        {{ stylesheet_link('css/main.min.css') }}
        {% if router.getRewriteUri() == '/index.html' %}
            {% include 'index/meta.volt' %}            
        {% endif %}
        {{ assets.outputCss() }}
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/es5-shim/4.0.5/es5-shim.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <style>
                .ng-hide {
                    display: none !important;
                }
            </style>
        <![endif]-->
        <style>
            .animate-fadein
            {
                -webkit-transition:all ease-in 0.4s;
                -moz-transition:all ease-in 0.4s;
                -ms-transition:all ease-in 0.4s;
                -o-transition:all ease-in 0.4s;
                transition:all ease-in 0.4s;
                opacity:1;
                display:block;
            }
            div.animate-fadein.hidden
            { 
                opacity:0;
                display:block !important;
                visibility: visible !important;
            }
            span.select2-container{
                width:100% !important;
            }
            .alert{
                margin-bottom:0;
            }
            span.select2-container{
                width:100% !important;
            }
        </style>
        <!--[if lte IE 8]>
        <script>
            document.createElement('ng-include');
            document.createElement('ng-pluralize');
            document.createElement('ng-view');
            // Optionally these for CSS
            document.createElement('ng:include');
            document.createElement('ng:pluralize');
            document.createElement('ng:view');
        </script>
        <![endif]-->
        <script type="text/javascript" src="/js/libs/modernizr.min.js"></script>
        <meta name="google-site-verification" content="JbX6LEpaZrccwOBJrUXWQKhoZAFQ_RPkOH7YjL1FzvE" />
    </head>
    <body id="ng-app">
        <div remove-class="hidden" class="hidden animate-fadein" ng-controller="accessControl">
            {{ content() }}
        </div>
        {{ javascript_include('js/libs/jquery/jquery.min.js') }}
        {{ javascript_include('js/libs/hmac-sha512.js') }}
        <!--[if lt IE 9]>
            <script src="//cdnjs.cloudflare.com/ajax/libs/es5-shim/4.0.5/es5-shim.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/json2/20140204/json2.min.js"></script>
            <script src="https://www.matchpeopleskills.com/js/libs/angular/angular.ie8.js"></script>
            <script src="https://code.angularjs.org/1.2.27/angular.js"></script>
        <![endif]-->
        {{ javascript_include('js/libs/angular/angular/angular.min.js') }}
        {{ javascript_include('js/libs/angular/angular-resource/angular-resource.min.js') }}
        {{ javascript_include('js/libs/angular/angular-cookies/angular-cookies.min.js') }}
        {{ javascript_include('js/libs/angular/angular-animate/angular-animate.min.js') }}
        {{ javascript_include('js/libs/angular/angular-touch/angular-touch.min.js') }}
        {{ javascript_include('js/libs/angular/angular-route/angular-route.min.js') }}
        {{ javascript_include('js/libs/angular/angular-sanitize/angular-sanitize.min.js') }}
        {{ javascript_include('js/libs/angular/angular-ui-router/release/angular-ui-router.min.js') }}
        {{ javascript_include('js/libs/angular/angular-ui-utils/ui-utils.min.js') }}
        {{ javascript_include('js/libs/angular/angular-bootstrap/ui-bootstrap-tpls.min.js') }}
        {{ javascript_include('js/libs/angular/angular-ui-select/dist/select.min.js') }}
        {{ javascript_include('js/libs/angular/ladda/dist/spin.min.js') }}
        {{ javascript_include('js/libs/angular/angularjs-placeholder/src/angularjs-placeholder.js') }}
        {{ javascript_include('js/libs/angular/ladda/js/ladda.js') }}
        {{ javascript_include('js/libs/angular/angular-ladda/dist/angular-ladda.min.js') }}
        {{ javascript_include('js/libs/angular/footable/dist/footable.all.min.js') }}
        {{ javascript_include('js/libs/angular/angular-footable/dist/angular-footable.min.js') }}
        {{ javascript_include('js/libs/angular/angular-media-queries/match-media.js') }}
        {{ javascript_include('js/config.js') }}
        {{ javascript_include('js/application.js') }}
        {{ assets.outputJs() }}
        {% for files in jsFiles %}{{ javascript_include(files) }}{% endfor %}
        <?php /*{{ javascript_include('js/libs//bootstrap/collapse.js') }}*/ ?>
    </body>
</html>