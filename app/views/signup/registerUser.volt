{{ content() }}


<div id="main_content" class="login col-xs-12 col-md-11 col-lg-11 col-centered" ng-controller="userLogin">

<div id="content_box">

<div class="boxTop">
    <h2>Register Your Account</h2>
    <hr/>
</div>

<div>

<div ng-controller="signUp">

	<div ng-hide="tc.Accept" class="regWrapper">
{% if candidateReg != false %}
		<div class="jumbotron">
			<div class="scrolly-box">
				<h1>Terms and Conditions</h1>
					
				<p><strong>1. pre-assignment candidate terms</strong></p>
				<p>1.1. These terms govern the basis on which the Agency shall Introduce the Candidate to Clients and search for jobs for the Candidate .</p>
				<p><strong>2. definitions and interpretation</strong></p>
				<p>2.1. The definitions and rules of interpretation in this Clause apply to any offer by Altablue to propose any Candidate for any role with any Client.</p>
				<p><strong>"Agency"</strong> means Altablue Limited a private company limited by shares registered in Jersey with Company Number 15271 and its head office at New Telecoms House, 5th Floor, 73-77 College Street, Aberdeen UK.</p>
				<p><strong>"Agency Group"</strong> means the Agency any of its affiliates and its and their respective directors, officers and employees, but shall not include the Client or its affiliates.</p>
				<p><strong>"Candidate"</strong></p> means any individual searching for employment or engagement on a permanent or temporary basis by any Client with the assistance of the Agency and includes the limited company via which an individual may choose to provide his or her services.
				<p><strong>"Conduct Regulations"</strong></p> means the Conduct of Employment Agencies and Employment Businesses Regulations 2003 (as amended).
				<p><strong>"Client"</strong></p> means any third party to whom the Agency may introduce any Candidate for the purposes of employment or engagement of that Candidate.  
				<p><strong>"Introduce"</strong></p> means any steps taken by the Agency to pass on to any Client the details whether by means of a CV letter verbally or otherwise of any Candidate to any Client and Introduction will be construed accordingly.
				<p><strong>"Site"</strong></p> means <a href="www.matchpeopleskills.com" target="_blank">www.matchpeopleskills.com</a> a site operated and maintained by Altablue.
				<p><strong>3. the site</strong></p>
				<p>3.1. The Candidate acknowledges and accepts that his / her use of the Site is governed by: (i) Terms of Use; (ii) Privacy Policy; and (iii) Cookies Policy (as published on the Site from time to time).</p>
				<p>3.2 The Candidate shall be provided with a user name and password in order to access the Site.  This must be kept private and confidential and not shared with any third party.</p>
				<p>3.3. The Candidate warrants that all information uploaded to the Site shall be complete, true and up to date.</p>
				<p><strong>4. information to be provided by a candidate</strong></p>
				<p>4.1. On a Candidate requesting the Agency to make an Introduction, the Candidate shall provide for the benefit of the Agency a full and accurate curriculum vitae and assist the Agency to complete the Candidate Information Form which is used to record the Candidate’s details and preferences.</p>
				<p><strong>5. Obligations of the Agency</strong></p>
				<p>5.1. The Agency is under no obligation to Introduce a Candidate to a Client notwithstanding that the Candidate’s skills and experiences may match or be similar to those desired by the Client.</p> 
				<p>5.2. The Agency shall seek the type of work which the Candidate has indicated, either verbally or on the Candidate Information Form that he or she is interested in.</p>
				<p>5.3. Where the Agency Introduces a Candidate to a Client with a view to the Candidate being directly engaged by the Client, the Agency shall act as an Employment Agency in terms of the Conduct Regulations.</p>
				<p>5.4. Where the Agency introduces and/or supplies a Candidate to a Client with a view to the Candidate completing a temporary assignment for the Client whilst being engaged via the Agency, the Agency shall act as an Employment Business in terms of the Conduct Regulations.</p>
				<p>5.5. The Agency shall not charge a fee to the Candidate for any activities in undertakes relating to finding the Candidate any employment or engagement.</p>
				<p><strong>6. PARTICULAR information required for an introduction </strong></p>
				<p>6.1. In the event of the Agency advising any Candidate that it is to effect an Introduction the Candidate shall at that point provide to the Agency:-</p>
				<ul>
					<li><p>6.1.1. Satisfactory evidence of the Candidate’s identity including but not limited to a certified copy of the Candidate’s passport or birth certificate and appropriate evidence of the Candidate’s home address.</p></li>
					<li><p>6.1.2. Up to date copies and/or originals of any relevant qualification and/or authorisations and/or professional qualifications required in respect of the Introduction whether or not specifically advised to the Candidate by the Agency.</p></li>
					<li><p>6.1.3. The names of two referees who shall not be relatives that the Candidate agrees the Agency may approach for the purposes of obtaining references.</p></li>
				</ul>
				<p><strong>7. offer of employment or engagement</strong></p>
				<p>7.1. The Agency is not responsible for any decision by a Client to make any offer of employment or engagement to any Candidate and the Agency does not accept any responsibility for and shall not be liable for any loss, injury or damage suffered by a Candidate in the event that any Candidate resigns from employment or engagement before or after receipt of any Client’s written offer or in the event that any Client withdraws any offer of employment and/or engagement at any time for any reason.  The Agency’s role is restricted to effecting any Introduction.</p> 
				<p>7.2. Any offer of employment and/or engagement made by any client shall be in writing and may be subject to the Client obtaining such satisfactory references and/or background checks on the Candidate as may to the Client seem appropriate.</p>
				<p><strong>8. temporary assignments</strong></p>
				<p>8.1. Where the Agency acts as an Employment Business in terms of the Conduct Regulations (supplying Candidates to Clients for temporary assignments) it shall: </p>
				<ul>
					<li><p>8.1.1. ordinarily enter into a contract for services with the limited company via which the Candidate shall work;</p></li>
					<li><p>8.1.2. pay the Candidate for any services provided regardless of whether the Agency is paid by the Client;</p></li>
					<li><p>8.1.3. ordinarily provide for one weeks’ notice to terminate the temporary assignment by either party, however, this shall be confirmed in the contract specific to the assignment.</p></li>
					<li><p>8.1.4. pay the Candidate the amount agreed in relation to the assignment on a weekly or monthly basis in arrears, however, this shall be confirmed in the contract specific to the assignment.</p></li>
					<li><p>8.1.5. not pay the Candidate holiday pay where the Candidate is engaged on a contract for services, holiday pay shall only be payable where the Candidate is engaged on a contract of services (which is not the ordinary practice of the Agency).</p></li>
				</ul>
				<p><strong>9. DATA PROTECTION ACT</strong></p>
				<p>9.1. The Candidate agrees that the Agency may process the Candidate’s personal data and/or sensitive data as defined in the Data Protection Act 1998 to the extent, necessary to facilitate the introduction of the Candidate to any Client and for the efficient management of business systems and records which may involve sharing such information within the Agency Group, and the Candidate further agrees to the Agency maintaining the Candidate’s personal data on file for the purposes of any Introduction after the date on which the Candidate had submitted their CV unless and until the Candidate indicates to the Agency by intimation in writing that the Candidate wishes their CV withdrawn.  The Candidate notes that this may include transferring data outside of the European economic area.</p>
				<p><strong>10. authority to transfer data to a client</strong></p>
				<p>10.1 The Agency shall not transfer the personal data of any Candidate to any client without the Candidate’s consent.  Such consent may be given either in writing (to include electronically) or verbally at any time.</p>
				<p><strong>11. choice of law</strong></p>
				<p>11.1 These terms and conditions shall be governed by and construed in accordance with the laws of England.</p>
			</div>

		</div>
<label for="tandc">I accept these terms and conditions</label>
{% else %}
<label for="tandc">I accept the <a href="/terms" target="_blank">terms of use</a></label>
{% endif %}
		<input id="tandc" type="checkbox" ng-model="tc.Disabled" ng-true-value="false" ng-false-value="true" />
		<a class="btn btn-success ladda-button" ng-click="tc.Accept = true" ng-class="{'disabled': tc.Disabled}" ng-model="tc.Accept" role="button" data-style="expand-right"><span class="ladda-label">Continue</span></a>

	</div>

	<div class="regWrapper" ng-show="tc.Accept">

		<div ng-bind-html="flashStatus"></div>

		{{ form('register', 'id': 'registerForm', 'onbeforesubmit': 'return false', 'onClick':'return false;', 'role':'form' ) }}

			{{ form.render('apiController', ['ng-model':'formData.apiController', 'ng-init':'formData.apiController="users"']) }}
			<input type="hidden" name="userId" ng-value="{{ userId }}" ng-model="formData.userId" ng-init="formData.userId={{ userId }}" />

		    <fieldset>

		        <div class="control-group">
		            {{ form.label('name', ['class': 'control-label']) }}
		            <div class="controls">
		                {{ form.render('name', ['class': 'form-control', 'disabled':'disabled', 'ng-disable':'disabled']) }}
		                <p class="help-block">(required)</p>
		            </div>
		        </div>

		        <div class="control-group">
		            {{ form.label('email', ['class': 'control-label']) }}
		            <div class="controls">
		                {{ form.render('email', ['class': 'form-control', 'disabled':'disabled', 'ng-disable':'disabled']) }}
		                <p class="help-block">(required)</p>
		            </div>
		        </div>

		        <div class="control-group">
		            {{ form.label('password', ['class': 'control-label']) }}
		            <div class="controls">
		                {{ form.render('password', ['class': 'form-control','ng-model':'formData.password1']) }}
		                <p class="help-block">(minimum 8 characters)</p>
		            </div>
		        </div>

		        <div class="control-group">
		            <label class="control-label" for="repeatPassword">Repeat Password</label>
		            <div class="controls">
		                {{ password_field('repeatPassword', 'class': 'form-control input-xlarge','ng-model':'formData.password2') }}
		            </div>
		        </div>

		        <div class="form-actions">
		         	<button class="btn btn-primary" ladda="loading" ng-click="submitForm()" data-style="expand-right"><span class="ladda-label">Register</span></button>
		        </div>

		    </fieldset>
		</form>
	</div>

</div>

</div>

</div>
