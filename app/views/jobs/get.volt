{{ content() }}


<div id="profiles" class="clearfix"  ng-controller="theProfiles">

    <div ng-bind-html="flashStatus"></div>

    <script type="text/ng-template" id="fillModal.html">
      <div class="modal-header">
        <h3>Contact Request:</h3>
        <button type="button" class="close" ng-click="cancel()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        
        <fieldset>
            <form>

                <input class="form-control" ng-model="fillApi.boardId" ng-init="fillApi.boardId='{{boardNumId}}'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="fillApi.job.id" ng-init="fillApi.job.id='{{jobId}}'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="fillApi.job.comp" ng-init="fillApi.job.comp='<?php echo str_replace("'", "",$jobComp); ?>'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="fillApi.candId" ng-init="fillApi.candId='{{candidateId}}'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="fillApi.job.title" ng-init="fillApi.job.title='{{title}}'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="fillApi.candComp" ng-init="fillApi.candComp='<?php echo str_replace("'", "",$boardName); ?>'" type="hidden" disabled="disabled" />

                <div class="form-group">
                    <label>Your Name</label>
                    <input class="form-control" ng-model="fillApi.name" ng-init="fillApi.name='{{name}}'" type="text" disabled="disabled" value="{{name}}" />
                </div>

                <p>Please confirm you would like to apply for the following position:</p>

                <div class="form-group clearfix">
                    <label class="col-xs-12">Position Overview:</label>
                    <br/><br/>
                    <div class="row col-xs-6">
                        <div class="left col-xs-4">
                            <figure>
                                <img style="max-width:100%" alt="User Pic" src="/imgs/avatar/job.png" class="img-circle"> 
                            </figure>
                        </div>
                        <div class="right col-xs-6">
                            <p>Job #{{ id }}</p>
                            <p>{{discipline}}</p>
                            <p>{{locationnice}}</p>
                            <p>{{availability}} hs/pw</p>
                        </div>
                    </div>
                </div>

                <div class="alert alert-warning"><strong>NOTE:</strong> Applying for a position will send your contact details to the company this advert belongs to and their assigned recruiters.</div>
            </form>
        </fieldset>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" ng-click="cancel()">Close</button>
        <button type="button" class="btn btn-primary" ng-click="ok()">Send Contact Request</button>
      </div>
    </script>

    <div class="boxTop">

        <a href="javascript:history.back()" class="btn btn-primary ladda-button btn-primary" data-style="expand-right"><span class="ladda-label"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;&nbsp;Return to Search Results</span></a>
        <br/><br/>

        <h2>view advert: Job #{{ jobId }}</h2>
        <hr/>
    </div>

    <div class="theDeets col-xs-12">
        
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" id="avatar">
            <figure>
                <img alt="User Pic" src="/imgs/avatar/job.png" class="img-circle"> 
            </figure>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="profile">
           <article class="details">
                    <?php if($userLevel >= 99): ?>
                            <p>{{company}}</p>
                            <hr/>
                        <?php endif; ?>
                <p>{{title}}</p>
                <p>{{discipline}}</p>
                <p>{{locationnice}}</p>
                <p>Required Availability: <span>{{availability}}</span></p>
                <hr/>
                <p class="bio"><?php echo nl2br($profile); ?></p>
                <hr/>
                <div class="skills">
                    {% for skill in skills %}
                        <span>{{skill}}</span>
                    {% endfor %}
                </div>
                <hr/>

                <?php if(!empty($qualifications) && is_array($qualifications)): ?>
                    <table class="table table-striped footable">
                        <thead class="hidden-lg hidden-md hidden-sm">
                            <th></th>
                            <th></th>
                            <th data-hide="phone"></th>
                            <th data-hide="phone"></th>
                            <th data-hide="phone"></th>
                        </thead>
                        <tbody>
                                {% for qual in qualifications %}
                                    <tr>
                                        <td><img src="/imgs/svgs/qual-grey.svg" /></td>
                                        <td class="hidden-lg hidden-md hidden-sm">Degree #{{loop.index}}</td>
                                        <td>{% if qual.candQualTitle is defined %}{{qual.candQualTitle}}{% endif %}</td>
                                        <td>{% if qual.candQualDegree is defined %}{{qual.candQualDegree}}{% endif %}</td>
                                        <td>{% if qual.candQualField is defined %}{{qual.candQualField}}{% endif %}</td>
                                    </tr>
                                {% endfor %}
                        </tbody>
                    </table>

                <?php else: ?>
                    <p>No qualifications specified</p>
                <?php endif; ?>

                <hr/>

                {% if candidateId is defined %}
                    <button  ng-click="assignVac()" onClick="return false;" ladda="emailLoading" data-style="expand-right" data-toggle="modal" data-target=".bs-example-modal-lg"  class="btn btn-warning ladda-button"><span class="ladda-label">apply for job</span></button>
                {% endif %}
            </article>
        </div>
    </div>
</div>
