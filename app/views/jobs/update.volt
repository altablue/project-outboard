
{{ content() }}

<div class="boxTop">
    <h2>Create Job Advert</h2>
    <hr/>
</div>
<div  ng-controller="userReg">
<section id="editSidebar" class="col-lg-4 col-md-4 col-xs-12" ng-controller="editSidebar">
    <ul class="nav nav-tabs">
      <li role="presentation" ng-click="select('progressTab')" ng-init="selected='progressTab'" ng-class="{active: isActive('progressTab')}"><a href="#'progressTab'" ng-click="select('progressTab')" ng-class="{active: isActive('progressTab')}" onClick="return false">Profile Progress</a></li>
      <li role="presentation" ng-click="select('helpTab')" ng-class="{active: isActive('helpTab')}"><a href="#'helpTab'" ng-click="select('helpTab')" ng-class="{active: isActive('helpTab')}"  onClick="return false;">Need Help?</a></li>
    </ul>

    <div id="progressTab" class="tabPanel fadeOut" ng-class="{fadeIn: isActive('progressTab')}">

        <svg style="width:100%; max-width:250px;" height="250" viewbox="0 0 250 250">
            <path id="loader" transform="translate(125, 125)"/>
            <circle id="circle" cx="125" cy="125" r="110"/>
            <g>
                <text id="tots" transform="matrix(1 0 0 1 85 120)" fill="#3d3e42" font-family="'Lato', sans-serif" font-size="52" letter-spacing="2">0</text>
            </g>
            <text transform="matrix(1 0 0 1 68 150)">
                <tspan x="18" y="0" fill="#3d3e42" font-family="'Lato', sans-serif" font-size="19.7349">profile</tspan>
                <tspan x="0" y="23.7" fill="#3d3e42" font-family="'Lato', sans-serif" font-size="19.7349">completion</tspan>
            </text>
            
            <text id="persym" transform="matrix(1 0 0 1 150 120)" fill="#3d3e42" font-family="'Lato', sans-serif" font-size="32.0636" letter-spacing="1">%</text>
        </svg>

        <hr/>

        <ul class="profileProgress" >
            <li ng-class="{true:'done', 'undefined':'undone'}[status.candTitle]" class="clearfix title" ng-switch="status.candTitle.toString()">
                <img ng-switch-when="true" src="/imgs/svgs/keyword.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/keyword.png'" />
                <img ng-switch-default src="/imgs/svgs/keyword-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/keyword-red.png'" />
                <span></span><div ng-class="{true:'icon-tick', 'undefined':'icon-cross'}[status.candTitle]" class="statusIcon icon"></div>
            </li>
            <li ng-class="{true:'done', 'undefined':'undone'}[status.candDisc]" class="clearfix discipline"  ng-switch="status.candDisc.toString()">
                <img ng-switch-default src="/imgs/svgs/discipline-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/discipline-red.png'" />
                <img ng-switch-when="true" src="/imgs/svgs/discipline.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/discipline.png'" />
                <span></span><div ng-class="{true:'icon-tick', 'undefined':'icon-cross'}[status.candDisc]" class="statusIcon icon"></div>
            </li>
            <li ng-class="{true:'done', 'undefined':'undone'}[status.niceName]" class="clearfix location"  ng-switch="status.niceName.toString()">
                <img ng-switch-default  src="/imgs/svgs/location-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/location-red.png'" />
                <img ng-switch-when="true"  src="/imgs/svgs/location.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/location-.png'" />
                <span></span><div ng-class="{true:'icon-tick', 'undefined':'icon-cross'}[status.niceName]" class="statusIcon icon"></div>
            </li>
            <li ng-class="{true:'done', 'undefined':'undone'}[status.candRoleDesc]" class="clearfix desc"  ng-switch="status.candRoleDesc.toString()">
                <img ng-switch-default  src="/imgs/svgs/desc-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/desc-red.png'" />
                <img ng-switch-when="true"  src="/imgs/svgs/desc.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/desc.png'" />
                <span></span><div ng-class="{true:'icon-tick', 'undefined':'icon-cross'}[status.candRoleDesc]" class="statusIcon icon"></div>
            </li>
            <li ng-class="{true:'done', 'undefined':'undone'}[status.candSkills]" class="clearfix"  ng-switch="status.candSkills.toString()">
                <img ng-switch-default  src="/imgs/svgs/tag-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/tag-red.png'" />
                <img ng-switch-when="true"  src="/imgs/svgs/tag.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/tag.png'" />
                <span></span><div ng-class="{true:'icon-tick', 'undefined':'icon-cross'}[status.candSkills]" class="statusIcon icon"></div>
            </li>
            <li ng-class="{true:'done', 'undefined':'undone'}[status.qual]" class="clearfix"  ng-switch="status.qual.toString()">
                <img ng-switch-default  src="/imgs/svgs/qual-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/qual-red.png'" />
                <img ng-switch-when="true"  src="/imgs/svgs/qual.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/qual.png'" />
                <span></span><div ng-class="{true:'icon-tick', 'undefined':'icon-cross'}[status.qual]" class="statusIcon icon"></div>
            </li>
            <li ng-class="{true:'done', 'undefined':'undone'}[status.candAvail]" class="clearfix avail"  ng-switch="status.candAvail.toString()">
                <img ng-switch-default  src="/imgs/svgs/avail-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/avail-red.png'" />
                <img ng-switch-when="true"  src="/imgs/svgs/avail.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/avail.png'" />
                <span></span><div ng-class="{true:'icon-tick', 'undefined':'icon-cross'}[status.candAvail]" class="statusIcon icon"></div>
            </li>
        </ul>
    </div>

    <div id="helpTab" class="tabPanel fadeOut" ng-class="{fadeIn: isActive('helpTab')}">
        <ul>
            <li><p><strong>title:</strong> some text here about what you should be entering for the title input.</p></li>
        </ul>
    </div>

</section>


<div class="fields col-lg-8 col-md-8 col-xs-12 clearfix" ng-init="numOfFields=7">

    <div ng-bind-html="flashStatus"></div>

    {{ form('#', 'id': 'UserRegisterForm', 'onClick': 'return false', 'role':'form' ) }}

        <fieldset>

            {{ form.render('apiController', ['ng-model':'formData.apiController', 'novalidate':'', 'ng-init':'formData.apiController="job"']) }}

            <input type="hidden" name="boardNum" ng-value="{{ board }}" ng-model="formData.boardNum" ng-init="formData.boardNum={{ board }}" />
            <input type="hidden" name="jobId" ng-value="{{ jobId }}" ng-model="formData.candidateId" ng-init="formData.jobId={{ jobId }}" />
            <input type="hidden" name="apiAction" ng-value="post" ng-model="formData.apiAction" ng-init="formData.apiAction='post'" />

            <div class="form-group">
                {{ form.label('jobTitle', ['class': 'control-label']) }}
                {{ form.render('jobTitle', ['class': 'form-control','required':'','data-profileStatus':'title','ng-model':'formData.candTitle', 'ng-blur':'loadProgress(formData)']) }}
            </div>    
            <div class="form-group" ng-init='formData.candDisc = "{{ discipline }}"; refreshDisciplines({{ disciplines }});'>
                {{ form.label('jobDisc', ['class': 'control-label']) }}

                <?php if($browser['browser'] == 'IE' && $browser['version'] == '8.0'): ?>
                    <select class="form-control" ng-model="formData.candDisc" placeholder="Please Select a Discipline from the list below..." ng-options="discipline for discipline in disciplines">
                       </select>
                <?php else: ?>
                    <ui-select ng-model="formData.candDisc" on-select="discUpdate($item, formData, add)" theme="bootstrap" ng-disabled="disabled">
                        <ui-select-match placeholder="Please Select a Discipline from the list below...">[[$select.selected]]</ui-select-match>
                        <ui-select-choices repeat="discipline in disciplines">
                          <div ng-bind-html="discipline"></div>
                        </ui-select-choices>
                    </ui-select>
                <?php endif; ?>
            </div>
            
            <div id="locationGroup" class="form-group">
              <div class="form-group">
                {{ form.label('jobLoc', ['class': 'control-label']) }}
                <div class="input-group">
                  {{ form.render('jobLoc', ['class': 'form-control','required':'', "ng-model":"formData.niceName",'data-profileStatus':'location', 'ng-blur':'loadProgress(formData)']) }}
                  <span class="input-group-btn">
                    <button id="geoLoc" class="btn btn-default icon icon-geo ladda-button" ladda="loading" data-spinner-color="#183043" data-spinner-size="30" ng-model="cordLoc" ng-click="getLoc()" type="button"></button>
                  </span>
                </div><!-- /input-group -->
              </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->

            <input type='hidden' name="formData.candLoc" value="[[ formData.candLoc ]]"ng-model="formData.candLoc" />

            <div class="form-group">
                {{ form.label('jobRoleDesc', ['class': 'control-label']) }}
                <textarea id="candRoleDesc" data-ck-editor class='form-control' data-profileStatus="desc" ng-model='formData.candRoleDesc' placeholder="Enter Description Here..." ng-blur='loadProgress(formData)' ng-init="formData.candRoleDesc='{{profile}}'">{{ profile }}</textarea>
            </div>

            <div class="form-group" id="skills" ng-init='setupSkills({{skills}})'>
                {{ form.label('jobSkills', ['class': 'control-label']) }}

                <?php if($browser['browser'] == 'IE' && $browser['version'] == '8.0'): ?>
                    <small>(use ',' or spaces to seperate tags)</small>
                    <input onpaste="alert('Sorry, you cannot paste into this field, please enter skills manually');return false;" type="text" class="js-example-tags form-control" ng-model="formData.candSkills" value="{{skills}}"  multiple="multiple" />
                <?php else: ?>
                    <ui-select onpaste="alert('Sorry, you cannot paste into this field, please enter skills manually');return false;" multiple tagging tagging-label="(add this skill)" on-select="skillUpdate($item, formData, add)" on-remove="skillUpdate($item, formData, delete)" ng-model="formData.candSkills" theme="bootstrap" sortable="true" ng-disabled="disabled" title="Start typing a skill (use commas to seperate skills) ..." class="form-control">
                        <ui-select-match placeholder="Start typing a skill (use commas to seperate skills) ...">[[$item]]</ui-select-match>
                        <ui-select-choices repeat="skill in skills">
                            [[skill]]
                        </ui-select-choices>
                </ui-select>
                <?php endif; ?>

            </div>

            <label>Required Qualifications</label>
            {% set countIndex = qCount-1 %}
            <input ng-model="formData.count" value="0" ng-value="0" ng-init="count={{ countIndex }}" style="display:none" />
                <script type="text/ng-template" id="myModalContent.html">
                  <div class="modal-header">
                    <button type="button" class="close" ng-click="cancel()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  </div>
                  <div class="modal-body">
                    
                    <fieldset>
                        <div class="form-group">
                            {{ form.label('jobQualTitle', ['class': 'control-label']) }}
                            {{ form.render('jobQualTitle', ['class': 'form-control','required':'','ng-model':'singleQual.candQualTitle']) }}
                        </div>

                        <div class="form-group" ng-init='refreshDegrees({{ degrees }});'>
                            {{ form.label('jobQualsDegree', ['class': 'control-label']) }}
                            <?php if($browser['browser'] == 'IE' && $browser['version'] == '8.0'): ?>
                                <select ng-model="singleQual.candQualDegree" placeholder="Please Select a Degree from the list below..." ng-options="deg for deg in degrees">
                                </select>
                            <?php else: ?>
                                    <ui-select ng-model="singleQual.candQualDegree" theme="bootstrap" ng-disabled="disabled">
                                        <ui-select-match placeholder="Please Select a Degree from the list below...">[[$select.selected]]</ui-select-match>
                                        <ui-select-choices repeat="degree in degrees">
                                          <div ng-bind-html="degree"></div>
                                        </ui-select-choices>
                                    </ui-select>
                            <?php endif; ?>
                        </div>

                        <div class="form-group">
                            {{ form.label('jobQualField', ['class': 'control-label']) }}
                            {{ form.render('jobQualField', ['class': 'form-control','required':'','ng-model':'singleQual.candQualField', 'ng-blur':'loadProgress(formData)','data-profileStatus':'quals']) }}
                        </div>
                    </fieldset>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" ng-click="cancel()">Close</button>
                    <button type="button" class="btn btn-primary" ng-click="ok()">Save changes</button>
                  </div>
                </script>
             <hr/>
             <input id="qualSaved" value='<?php echo json_encode($qualifications) ?>' style="display:none" />
            <div id="quals">
                <div id="table" class="tables" ng-init="renderTable();">

                    <?php if($browser['browser'] == 'IE' && $browser['version'] == '8.0'): ?>

                        <table class="table table-striped footable">
                            <thead>
                                <tr>
                                    <th  data-sort-ignore="true" class="hidden-sm"></th>
                                    <th  data-sort-ignore="true" data-hide="phone"></th>
                                    <th  data-sort-ignore="true" data-hide="phone"></th>
                                    <th  data-sort-ignore="true" data-hide="phone"></th>
                                    <th  data-sort-ignore="true" data-hide="phone"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="qual in tableQual track by $index" fooTableQuals>
                                    <td class="hidden-sm">Degree #[[$index+1]]</td>
                                    <td ng-bind="tableQual[$index].candQualTitle"></td>
                                    <td ng-bind="tableQual[$index].candQualDegree"></td>
                                    <td ng-bind="tableQual[$index].candQualField"></td>
                                    <td>
                                        <a class="btn btn-warning" href="#" data-toggle="modal" data-target=".bs-example-modal-lg" editThisQual ng-click="editQual($index)" class="icon icon-edit-white">edit</a><a href="#" class="btn btn-danger" ng-click="removeQual($index)" removethisqual class="icon icon-close-white">delete</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    <?php else: ?>

                        <table class="table table-striped" ng-if="desktop">
                            <tbody>
                                <tr ng-repeat="qual in tableQual track by $index" fooTableQuals>
                                    <td class="info-td" ng-bind="tableQual[$index].candQualTitle"></td>
                                    <td class="info-td" ng-bind="tableQual[$index].candQualDegree"></td>
                                    <td class="info-td" ng-bind="tableQual[$index].candQualField"></td>
                                    <td class="action-td overlay"></td>
                                    <td class="action-td overlay center">
                                        <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg" editThisQual ng-click="editQual($index)" class="icon icon-edit-white">edit</a><a href="#" ng-click="removeQual($index)" removethisqual class="icon icon-close-white">delete</a>
                                    </td>
                                    <td class="action-td overlay"></td>
                                </tr>
                            </tbody>
                        </table>

                        <table class="table table-striped footable" ng-if="mobile">
                            <thead>
                                <tr>
                                    <th  data-sort-ignore="true" class="hidden-sm"></th>
                                    <th  data-sort-ignore="true" data-hide="phone"></th>
                                    <th  data-sort-ignore="true" data-hide="phone"></th>
                                    <th  data-sort-ignore="true" data-hide="phone"></th>
                                    <th  data-sort-ignore="true" data-hide="phone"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="qual in tableQual track by $index" fooTableQuals>
                                    <td class="hidden-sm">Degree #[[$index+1]]</td>
                                    <td ng-bind="tableQual[$index].candQualTitle"></td>
                                    <td ng-bind="tableQual[$index].candQualDegree"></td>
                                    <td ng-bind="tableQual[$index].candQualField"></td>
                                    <td>
                                        <a class="btn btn-warning" href="#" data-toggle="modal" data-target=".bs-example-modal-lg" editThisQual ng-click="editQual($index)" class="icon icon-edit-white">edit</a><a href="#" class="btn btn-danger" ng-click="removeQual($index)" removethisqual class="icon icon-close-white">delete</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    <?php endif; ?>

                </div>
            </div>

            <a class="btn btn-warning" editThisQual ng-click="editQual()" data-toggle="modal" data-target=".bs-example-modal-lg"  ><span class="glyphicon glyphicon-plus"></span> Add Qualifications</a>

            <hr/>

            <div class="form-group">
                {{ form.label('jobAvail', ['class': 'control-label']) }}
                <div class="input-group">
                      {{ form.render('jobAvail', ['class': 'form-control','required':'','data-profileStatus':'avail','ng-model':'formData.candAvail', 'aria-describedby':"basic-addon2",'ng-blur':'loadProgress(formData)']) }}
                      <span class="input-group-addon" id="basic-addon2">hrs/week</span>
                </div>
            </div>

            <script>
                document.getElementById('jobAvail').onkeydown = function(e) {
                    var key = e.keyCode ? e.keyCode : e.which;
                    if ( isNaN( String.fromCharCode(key) ) && key != 8 && key != 46) return false;
                }
            </script>

            <hr/>

            <div class="form-actions">
                <button class="btn btn-primary btn-lg ladda-button"  ladda="loading" ng-click="submitForm()" data-style="expand-right"><span class="ladda-label">Save Your Profile</span></button>
            </div>

            <p ng-init="loadProgress(formData)"><span>note:</span> Once your advert is saved, will be instantly visible to other users.</p>

        </fieldset>

    </form>

</div>

</div>
