
{{ content() }}

<div class="boxTop">
	<h2>Candidates Applied for Job #{{ id }}</h2>
	<hr/>
</div>

<div class="getAllWrapper" ng-controller="recruitEdit">

	<div ng-bind-html="flashStatus"></div>
	
	<table class="table table-striped footable" ng-init="jobId = {{ id }}">

	<thead>
		<tr>
			<th data-sort-ignore="true" data-toggle="true"><strong>candidate name</strong></th>
			<th data-hide="phone"><strong>candidate email</strong></th>
			<th data-hide="phone"><strong>contact number</strong></th>
			<th data-hide="phone"><strong>candidate id</strong></th>
			<th data-hide="phone"><strong>application status</strong></th>
			<th data-hide="phone" data-sort-ignore="true" data-toggle="true"></th>
		</tr>
	</thead>

	<tbody>

	{% for cand in cands %}
		
		<tr ng-init="showList[{{ loop.index }}] = false; cand[{{ loop.index }}] = {{ cand.candidate.id }}">
			<td>{{ cand.candidate.users.name }}</td>
			<td>{{ cand.candidate.users.username }}</td>
			<td>{{ cand.candidate.phone }}</td>
			<td>{{ cand.candidate.id }}</td>
			<td>
				<div ng-hide="!showList[{{ loop.index }}]">
					<ui-select ng-model="updateStatus[{{ loop.index }}]" theme="bootstrap" on-select="CandStatusChange($item, updateStatus, add, {{ loop.index }}, 'Fill', {{ id }})"  ng-disabled="disabled">
	                    <ui-select-match placeholder="Please Select a Status...">[[$select.selected]]</ui-select-match>
	                    <ui-select-choices repeat="stat in applyStats">
	                      <div ng-bind-html="stat"></div>
	                    </ui-select-choices>
	                </ui-select>
				</div>
				{% if(cand.status == 1) %}
					{% set status = 'Applied' %}
				{% elseif(cand.status == 2) %}
					{% set status = 'Unsuccessful' %}
				{% elseif(cand.status == 3) %}
					{% set status = 'Interview Stage' %}
				{% else %}
					{% set status = 'Successful' %}
				{% endif %}
				<span ng-init="updateStatus[{{ loop.index }}]='{{ status }}'" ng-hide="showList[{{ loop.index }}]" ng-bind="updateStatus[{{ loop.index }}]"></span>
			</td>
			<td>3 days</td>
			<td>
				<a class="btn btn-primary" href="/dashboard/candidates/{{ cand.candidate.id }}">view profile</a>
				{% if(userLevel >= 99) %}<a class="btn btn-warning" ng-click="showList[{{ loop.index }}] = true" onclick="return false;" href="">update application status</a>{% endif %}
			</td>
		</tr>

	{% endfor %}

		<tfoot>
			<tr>
				<td colspan="5">
					<div class="pagination pagination-centered hide-if-no-paging pager col-xs-12"></div>
				</td>
			</tr>
		</tfoot>

	</table>

</div>
