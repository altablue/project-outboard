
{{ content() }}

<div class="boxTop">
    <h2>Create Job Advert</h2>
    <hr/>
</div>

<div ng-controller="jobReg" class="fields col-lg-12 clearfix">

    <div ng-bind-html="flashStatus"></div>

    {{ form('#', 'id': 'UserRegisterForm', 'onClick': 'return false', 'role':'form' ) }}

        <fieldset>

            {{ form.render('apiController', ['ng-model':'formData.apiController', 'ng-init':'formData.apiController="job"']) }}

            <input type="hidden" name="boardNum" ng-value="{{ boardNum }}" ng-model="formData.boardNum" ng-init="formData.boardNum={{ boardNum }}" />

            <div class="form-group">
                {{ form.label('jobTitle', ['class': 'control-label']) }}
                {{ form.render('jobTitle', ['class': 'form-control','required':'','ng-model':'formData.jobTitle']) }}
            </div>    

            <div class="form-group">
                {{ form.label('jobDisc', ['class': 'control-label']) }}
                {{ form.render('jobDisc', ['class': 'js-example-tags form-control','required':'','ng-model':'formData.jobDisc', "select-two":"", 'useEmpty':'true', 'emptyText':'Please Select a Discipline from the list below...' ])}}
            </div>
            
              <div id="locationGroup" class="form-group clearfix">
                            <div class="form-group">

                {{ form.label('jobLoc', ['class': 'control-label']) }}
                <div class="input-group">
                  {{ form.render('jobLoc', ['class': 'form-control','style':'width:95%', 'required':'', "ng-model":"formData.niceName"]) }}
                  <span class="input-group-btn">
                    <button id="geoLoc" class="btn btn-default icon icon-geo" ng-model="cordLoc" ng-click="getLoc()" type="button"></button>
                  </span>
                  </div>
                </div><!-- /input-group -->
              </div><!-- /.col-lg-6 -->

            <input type='hidden' name="formData.jobLoc" value="[[ formData.jobLoc ]]" ng-value="[[ formData.jobLoc ]]" ng-model="formData.jobLoc" />

            <div class="form-group">
                {{ form.label('jobRoleDesc', ['class': 'control-label']) }}
                {{ form.render('jobRoleDesc', ['class': 'form-control','required':'','ng-model':'formData.jobRoleDesc']) }}
            </div>

            <div class="form-group">
                {{ form.label('jobSkills', ['class': 'control-label']) }}
                {{ form.render('jobSkills', ['class': 'js-example-tags form-control','required':'','ng-model':'formData.jobSkills',  "multiple":"","select-two":"{ tags: true,tokenSeparators: [',']}", 'useEmpty':'true', 'emptyText':'Start typing a skill (use commas to seperate skills) ...' ])}}
            </div>

            <input ng-model="formData.count" value="0" ng-value="0" style="display:none" />

            <div class="modal fade bs-example-modal-lg" id="qualModel" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-body">
                    
                    <fieldset>
                        <div class="form-group">
                            {{ form.label('jobQualTitle', ['class': 'control-label']) }}
                            {{ form.render('jobQualTitle', ['class': 'form-control','required':'','ng-model':'formData.qual.jobQualTitle']) }}
                            <p class="help-block">(required)</p>
                        </div>

                        <div class="form-group">
                        [[ formData.jobQualsDegree ]]
                            {{ form.label('jobQualsDegree', ['class': 'control-label']) }}
                            {{ form.render('jobQualsDegree', ['class': 'js-example-tags form-control','required':'', "select-two":"", 'ng-model':'formData.qual.jobQualsDegree', 'useEmpty':'true', 'emptyText':'Select a Degree level ...' ])}}
                            <p class="help-block">(required)</p>
                        </div>

                        <div class="form-group">
                            {{ form.label('jobQualField', ['class': 'control-label']) }}
                            {{ form.render('jobQualField', ['class': 'form-control','required':'','ng-model':'formData.qual.jobQualField']) }}
                            <p class="help-block">(required)</p>
                        </div>
                    </fieldset>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" ng-click="addJobQualifications()">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
             
             <hr/>
            <div id="quals">
                <div id="table">
                    <table class="table table-striped" ng-init="renderTable();">
                        <tbody ng-bind-html="qual">
                        </tbody>
                    </table>
                </div>
            </div> 

            <a class="btn btn-warning" data-toggle="modal" data-target=".bs-example-modal-lg"  ><span class="glyphicon glyphicon-plus"></span> Add Qualifications</a>

            <hr/>

            <div class="form-group">
                {{ form.label('jobAvail', ['class': 'control-label']) }}
                <div class="input-group">
                    {{ form.render('jobAvail', ['class': 'form-control','required':'','ng-model':'formData.jobAvail', 'aria-describedby':"basic-addon2"]) }}
                    <span class="input-group-addon" id="basic-addon2">hrs/week</span>
                </div>
            </div>

            <div class="form-actions">
                <button class="btn btn-primary ladda-button" ng-click="submitForm()" data-style="expand-right"><span class="ladda-label">Create Job</span></button>
            </div>

        </fieldset>

    </form>

</div>