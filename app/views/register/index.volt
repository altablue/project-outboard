
{{ content() }}

<div class="boxTop">
    <h2>Company Register</h2>
    <hr/>
</div>

<div class="regWrapper" ng-controller="companyCreate">

    <div ng-bind-html="flashStatus"></div>

    {{ form('#', 'id': 'registerForm', 'onbeforesubmit': 'return false', 'onClick': 'return false;', 'role':'form' ) }}

        <fieldset>

            {{ form.render('apiController', ['ng-model':'formData.apiController', 'ng-init':'formData.apiController="company"']) }}
            {{ form.render('apiAction', ['ng-value': '','ng-model':'formData.apiAction','ng-init':'formData.apiAction="create"']) }}

            <input type="hidden" ng-model="formData.recruitId" ng-init="formData.recruitId = {{ recId }}" />

            <div class="form-group">
                {{ form.label('company_name', ['class': 'control-label']) }}
                {{ form.render('company_name', ['class': 'form-control','required':'','ng-model':'formData.company_name']) }}
                <p class="help-block">(required)</p>
            </div>

            <div class="form-group">
                <label ng-init='refreshRecruts(<?php echo json_encode($recruts); ?>);'>Assign Recruiters:</label>
                
                <div ng-repeat="assignCompany in recruters track by $index">

                    <span class="hide">[[ compAssign = 'comp['+$index+']' ]]</span>

                    <ui-select ng-model="formData.recAssign[$index]" theme="bootstrap" on-select="compUpdate($item, compAssign, 'add',$index)" on-remove="compUpdate($item, compAssign, 'delete')" name="[[ compAssign ]]" search-enabled="true" reset-search-input="true" ng-disabled="disabled"  reset-search-input="true" search-enabled="true">
                        <ui-select-match placeholder="Please Select a Recruiter...">[[$select.selected.name]]</ui-select-match>
                        <ui-select-choices ui-disable-choice="company.status == false" repeat="company in recruts | propsFilter: {name: $select.search}">
                          <div ng-bind-html="company.name"></div>
                        </ui-select-choices>
                    </ui-select>

                    <a href="#" ng-click="compAssignRemove($index);" onclick="return false;" class="btn btn-primary">remove recruiter</a>

                    <hr/>
                </div>

            </div>

            <a href="#" class="btn btn-primary" ng-click="assignAnother()" onclick="return false;">Assign Another Recruiter</a>

            <div class="form-group">
                {{ form.label('location', ['class': 'control-label']) }}
                {{ form.render('location', ['class': 'form-control','required':'','ng-model':'formData.location']) }}
                <p class="help-block">(required)</p>
            </div>

            <div class="form-group">
                {{ form.label('company_type', ['class': 'control-label']) }}
                {{ form.render('company_type', ['class': 'form-control','required':'','ng-model':'formData.company_type']) }}
                <p class="help-block">(required)</p>
            </div> 

            <div class="form-actions">
                 <button class="btn btn-primary ladda-button" ng-click="submitForm()" data-style="expand-right"><span class="ladda-label">Register Company</span></button>
            </div>

        </fieldset>

    </form>

</div>