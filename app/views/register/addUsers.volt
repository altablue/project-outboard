
{{ content() }}


<div class="boxTop">
    <h2>Register Users</h2>
    <hr/>
</div>

<div class="regWrapper" ng-controller="companyCreate">

    <div ng-bind-html="flashStatus"></div>

    {{ form('#', 'id': 'UserRegisterForm', 'onbeforesubmit': 'return false', 'onClick': 'return false', 'role':'form' ) }}

        <input type="hidden" ng-init="formData.type=2" ng-model="formData.type" />

        <fieldset>

            {{ form.render('apiController', ['ng-model':'formData.apiController', 'ng-init':'formData.apiController="users"']) }}

            <input type="hidden" name="boardNum" ng-value="{{ boardNum }}" ng-model="formData.boardNum" ng-init="formData.boardNum={{ boardNum }}" />

            <div class="form-group">
                {{ form.label('compUserName', ['class': 'control-label']) }}
                {{ form.render('compUserName', ['class': 'form-control','required':'','ng-model':'formData.compUserName']) }}
                <p class="help-block">(required)</p>
            </div>    

            <div class="form-group">
                {{ form.label('compUserEmail', ['class': 'control-label']) }}
                {{ form.render('compUserEmail', ['class': 'form-control','required':'','ng-model':'formData.compUserEmail']) }}
                <p class="help-block">(required)</p>
            </div> 

            <div class="form-group">
                {{ form.label('userLevel', ['class': 'control-label']) }}
                {{ form.render('userLevel', ['class': 'form-control','required':'','ng-model':'formData.userLevel']) }}
                <p class="help-block">(required)</p>
            </div> 

            <div class="form-actions">
                <button class="btn btn-primary ladda-button" ladda="loading" ng-click="submitForm()" data-style="expand-right"><span class="ladda-label">Register User</span></button>
                {{ submit_button('Continue', 'class': 'btn btn-default', 'ng-click': 'continue()', 'disabled':'disabled') }}

            </div>

        </fieldset>

    </form>

</div>