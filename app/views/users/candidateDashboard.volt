	<div class="boxTop">
		<h2>Your Profile's Statistics</h2>
		<hr/>
	</div>

	<div class="charts">

		<div class="completedProfiles firstGraph col-lg-4 col-md-12 col-sm-12 col-xs-12 no-gutter">
			<div class="circle">
				<p>{{ views }}><span>views</span></p>
			</div>
			<svg  version="1.1" style="width:100%; max-width:250px;" height="250" x="0px" y="0px" viewBox="0 0 250 250" xml:space="preserve">
				<g transform="translate(0, 250) scale(1, -1)">
					{% if dayView is defined %}
					<rect x="25" y="14" fill="#38D9BE" width="21" height="{{ dayView[0] }}">
						<animate attributeName="height" from="0" to="{{ dayView[0] }}" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<rect x="59.3" y="14" fill="#38D9BE" width="21" height="{{ dayView[1] }}">
						<animate attributeName="height" from="0" to="{{ dayView[1] }}" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<rect x="130" y="14" fill="#38D9BE" width="21" height="{{ dayView[2] }}">
						<animate attributeName="height" from="0" to="{{ dayView[2] }}" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<rect x="95.3" y="14" fill="#38D9BE" width="21" height="{{ dayView[3] }}">
						<animate attributeName="height" from="0" to="{{ dayView[3] }}" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<rect x="200.7" y="14" fill="#38D9BE" width="21" height="{{ dayView[4] }}">
						<animate attributeName="height" from="0" to="{{ dayView[4] }}" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<rect x="165" y="14" fill="#38D9BE" width="21" height="{{ dayView[5] }}">
						<animate attributeName="height" from="0" to="{{ dayView[5] }}" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					{% endif %}
				</g>
				<line fill="none" stroke="#E8E8E6" stroke-width="2" stroke-miterlimit="10" x1="15" y1="235" x2="235" y2="235"/>
				<text text-anchor="middle" x="125" y="80" width="250" font-family="'Lato', sans-serif" font-size="52.3664" letter-spacing="2">{% if views is defined %}{{ views }}{% endif %}</text>
				<text transform="matrix(1 0 0 1 95.8794 109.8516)" font-family="'Lato', sans-serif" font-size="19.8632">VIEWS</text>
			</svg>
		</div>

		<div class="viewedCandidates jobsDiscipline secondGraph tables col-lg-8 col-md-12 col-sm-12 col-sm-12 no-gutter clearfix">
			<h6>Latest Jobs in your Discipline <span>({% if discipline is defined %}{{ discipline }}{% endif %})</span></h6>
			<table class="table table-striped footable">
				{% if latestJobs is defined %}
					{% if(latestJobs !== false) %}
						<thead>
							<tr>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
							</tr>
						</thead>
						{% for job in latestJobs %}
							<tr>
								<td><img src="/imgs/svgs/advert.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/advert.png'" /></td>
								<td>{{ job.title }}</td>
								<td>{{ job.locationnice }}</td>
								<td><a href="/dashboard/job/{{job.ID}}" class="btn btn-green btn-success" data-style="expand-right"><span class="ladda-label">view job</span></a></td>
							</tr>
						{% endfor %}
					{% else %}
						<tr>
							<tr><td><strong><em>No data available</em></strong></td></tr>
						</tr>
					{% endif %}
				{% else %}
					<tr>
						<tr><td><strong><em>No data available</em></strong></td></tr>
					</tr>
				{% endif %}
			</table>
		</div>

		<div class="uncompletedCandidates applicationStatus thirdGraph tables row col-lg-12 col-md-12 col-sm-12 col-sm-12 no-gutter">
			<hr />
			<h6>Application Status</h6>
			<table class="table table-striped footable">
				<thead>
					<tr>
						<th data-sort-ignore="true"></th>
						<th data-sort-ignore="true"></th>
						<th data-sort-ignore="true" data-hide="phone"></th>
						<th data-sort-ignore="true" data-hide="phone"></th>
						<th data-sort-ignore="true" data-hide="phone"></th>
					</tr>
				</thead>
				{% if appliedJobs is defined %}
					{% if(appliedJobs !== false) %}
						{% for job in appliedJobs %}
							<tr>
								<td><img src="/imgs/svgs/advert.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/advert.png'" /></td>
								<td>{{ job.job.title }}</td>
								<td>{{ job.job.locationnice }}</td>
								<td>
									{% if(job.job.status == 1) %}
										<span class="applied">Applied</span>
									{% elseif(job.job.status == 2) %}
										<span class="fail">Unsuccessful</span>
									{% elseif(job.job.status == 3) %}
										<span class="interview">Interview Stage</span>
									{% else %}
										<span class="success">Successful</span>
									{% endif %}
								</td>
								<td><a href="/dashboard/job/{{job.job.ID}}" class="btn btn-success ladda-button" data-style="expand-right"><span class="ladda-label">review job</span></a></td>
							</tr>
						{% endfor %}
					{% else %}
					<tr>
						<tr><td><strong><em>No data available</em></strong></td></tr>
					</tr>
					{% endif %}
				{% else %}
					<tr>
						<tr><td><strong><em>No data available</em></strong></td></tr>
					</tr>
				{% endif %}
			</table>
		</div>

	</div>