
{{ content() }}

{% if boardType == 1 AND type == 2  %}

	{% include 'users/onboardDashboard.volt' %}

{% elseif boardType == 2 AND type == 2 %}

	{% include 'users/outboardDashboard.volt' %}

{% elseif boardType == 3 AND type == 3 %}

	{% include 'users/ABStaffDashboard.volt' %}

{% else %}

	{% include 'users/candidateDashboard.volt' %}

{% endif %}
