<div class="boxTop">
		<h2>Your Candidates' Statistics</h2>
		<hr/>
	</div>

	<div class="charts" ng-controller="theDashes">

		<div class="completedProfiles firstGraph col-lg-4 col-md-12 col-sm-12 col-xs-12  no-gutter">
			<div class="circle">
				<p>60<span>%</span><span>completed Profiles</span></p>
			</div>
			<svg width="250" height="250" viewbox="0 0 250 250" ng-init="setInterval(profileCircle({{ percent }}),100)">
					<path id="loader" transform="translate(125, 125)"/>
					<circle id="circle" cx="125" cy="125" r="110"/>
					<g>
						<text letter-spacing="2" font-size="52" font-family="'Lato', sans-serif" fill="#3d3e42" y="120" x="145" text-anchor="end" width="250" id="tots">0</text>
					</g>
					<text transform="matrix(1 0 0 1 68 150)">
						<tspan x="0" y="0" fill="#3d3e42" font-family="'Lato', sans-serif" font-size="19.7349">Completed</tspan>
						<tspan x="15.1" y="23.7" fill="#3d3e42" font-family="'Lato', sans-serif" font-size="19.7349">profiles</tspan>
					</text>
					
					<text transform="matrix(1 0 0 1 150 120)" fill="#3d3e42" font-family="'Lato', sans-serif" font-size="32.0636" letter-spacing="1">%</text>
			</svg>

		</div>

		<div class="viewedCandidates secondGraph tables col-lg-8 col-md-12 col-sm-12 col-sm-12 no-gutter clearfix">
			<h6>Most Viewed Candidates</h6>
			<table class="table table-striped  footable">
				{% if popCands is defined %}
					{% if(popCands !== false) %}
							<thead>
								<tr>
									<th data-sort-ignore="true"></th>
									<th data-sort-ignore="true"></th>
									<th data-sort-ignore="true" data-hide="phone"></th>
									<th data-sort-ignore="true" data-hide="phone"></th>
								</tr>
							</thead>
						{% for cands in popCands %}
							<tr>
								<td><img src="/imgs/svgs/candidate.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/candidate.png'" /></td>
								<td>{{ cands.users.name }}</td>
								<td>{{ cands.users.username }}</td>
								<td>{{ viewStats[cands.id] }} Views</td>
							</tr>
						{% endfor %}
					{% else %}
						<tr>
							<tr><td><strong><em>No data for any candidates available</em></strong></td></tr>
						</tr>
					{% endif %}
				{% else %}
					<tr>
						<tr><td><strong><em>No data for any candidates available</em></strong></td></tr>
					</tr>
				{% endif %}
			</table>
		</div>

		<div class="uncompletedCandidates thirdGraph tables row col-lg-12 col-md-12 col-sm-12 col-sm-12  no-gutter">
			<hr />
			<h6 class="col-lg-6 col-sm-12">Oldest Uncompleted Profiles</h6>
			<a href="/dashboard/board/uncomplete/{{ boardNum }}/" class="btn btn-primary col-sm-12 col-lg-3 right">view all uncompleted candidates</a>
			<table class="table table-striped  footable">
				{% if incompleteUsers is defined %}
					{% if(incompleteUsers !== false) %}
							<thead>
								<tr>
									<th data-sort-ignore="true"></th>
									<th data-sort-ignore="true"></th>
									<th data-sort-ignore="true" data-hide="phone"></th>
									<th data-sort-ignore="true" data-hide="phone"></th>
									<th  data-sort-ignore="true"data-hide="phone"></th>
								</tr>
							</thead>
						{% for cands in incompleteUsers %}
							<tr>
								<td><img src="/imgs/svgs/candidate-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/candidate-red.png'" /></td>
								<td>{{ cands.name }}</td>
								<td>{{ cands.username }}</td>
								<td><?php echo floor((time() - strtotime($cands->created))/(60*60*24));  ?> days</td>
								<td><button onclick="return false" ng-click='candReminder("<?php echo addslashes(json_encode($cands)); ?>",{{loop.index}})' class="btn btn-warning" ladda="remindLoading[{{loop.index}}]" data-style="expand-right"><span class="ladda-label">send reminder</span></button></td>
							</tr>
						{% endfor %}
					{% else %}
					<tr>
						<tr><td><strong><em>No data for any candidates available</em></strong></td></tr>
					</tr>
					{% endif %}
				{% else %}
					<tr>
						<tr><td><strong><em>No data for any candidates available</em></strong></td></tr>
					</tr>
				{% endif %}
			</table>
		</div>

	</div>