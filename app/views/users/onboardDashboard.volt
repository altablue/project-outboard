
	<div class="boxTop">
		<h2>Your Company Statistics</h2>
		<hr/>
	</div>

	<div class="charts"  ng-controller="theDashes">

		<script type="text/ng-template" id="dashboardMatch.html">
	      <div class="modal-header">
	      	<h3>Job Advert Fields:</h3>
	        <button type="button" class="close" ng-click="cancel()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	        
	        <fieldset>
		        <form>
		        	<input class="form-control" ng-model="emamilApi.boardId" value="{{boardNum}}" ng-init="emailApi.boardId='{{boardNum}}'" type="hidden" disabled="disabled" />

		        	<div class="form-group">
			        	<label>Your Name</label>
			        	<input class="form-control" ng-model="emailApi.name" value="{{name}}" ng-init="emailApi.name='{{name}}'" type="text" disabled="disabled"  />
		        	</div>

		        	<div class="form-group">
			        	<label>Company Name</label>
			        	<input class="form-control" ng-model="emailApi.boardName" value="{{boardName}}" ng-init="emailApi.boardName='{{boardName}}'"  type="text" disabled="disabled" />
			        </div>

			        <div class="form-group">
			        	<label>Required Discipline:</label>
			        	<input class="form-control" type="text" ng-model="emailApi.discipline" disabled="disabled"/>
			        </div>

			        <div class="form-group">
			        	<label>Required Location:</label>
			        	<input class="form-control" ng-model="emailApi.location" disabled="disabled" type="text" />
			        </div>

			        <div class="form-group tables">
			        	<label>Required Skills:</label>
			        	<table class="table table-striped">
			        	[[searchSkills]]
			        		<tr ng-repeat="skill in emailApi.searchSkills">
			        			<td ng-bind="skill"></td>
			        		</tr>
			        	</table>
			        </div>

			        <div class="form-group tables">
			        	<label>Required Qualifications:</label>
			        	<table class="table table-striped">
			        		<tr ng-repeat="qual in emailApi.qual track by $index">
			        			<td ng-bind="qual.candQualTitle"></td>
			        			<td ng-bind="qual.candQualDegree"></td>
			        			<td ng-bind="qual.candQualField"></td>
			        		</tr>
			        	</table>
			        </div>

			        <div class="form-group">
			        	<label>Required Availability:</label>
			        	<input class="form-control" disabled="disabled" ng-model="emailApi.availability" type="text" />
			        </div>
		        </form>
	        </fieldset>

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Close</button>
	        <button type="button" class="btn btn-primary" ng-click="ok()">Send for Help</button>
	      </div>
	    </script>		

		<div class="completedProfiles firstGraph col-lg-4 col-md-4 col-sm-12 col-xs-12 no-gutter">
			<div class="circle">
				<p>16<span>%</span><span>active jobs</span></p>
			</div>
			<svg version="1.1" height="250" viewbox="0 0 250 250" style="width:100%; max-width:250px;"  x="0px" y="0px">
			<line fill="none" stroke="#E8E8E6" stroke-width="2" stroke-miterlimit="10" x1="15" y1="235" x2="235" y2="235"></line>
			<g transform="translate(0, 235) scale(1, -1)">
				{% if dayJobs is defined %}
				<line fill="none" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" x1="34.1" y1="{{ dayJobs[5] }}" x2="70.2" y2="{{ dayJobs[4] }}">
					<animate attributeName="y1" from="0" to="{{ dayJobs[5] }}" begin="0s" dur="0.5s" keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
					<animate attributeName="y2" from="0" to="{{ dayJobs[4] }}" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
				</line>
				<line fill="none" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" x1="70.2" y1="{{ dayJobs[4] }}" x2="106.3" y2="{{ dayJobs[3] }}">
					<animate attributeName="y1" from="0" to="{{ dayJobs[4] }}" begin="0s" dur="0.5s" keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
					<animate attributeName="y2" from="0" to="{{ dayJobs[3] }}" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
				</line>
				<line fill="none" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" x1="106.3" y1="{{ dayJobs[3] }}" x2="142.3" y2="{{ dayJobs[2] }}">
					<animate attributeName="y1" from="0" to="{{ dayJobs[3] }}" begin="0s" dur="0.5s" keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
					<animate attributeName="y2" from="0" to="{{ dayJobs[2] }}" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
				</line>
				<line fill="none" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" x1="142.3" y1="{{ dayJobs[2] }}" x2="178.4" y2="{{ dayJobs[1] }}">
					<animate attributeName="y1" from="0" to="{{ dayJobs[2] }}" begin="0s" dur="0.5s" keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
					<animate attributeName="y2" from="0" to="{{ dayJobs[1] }}" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
				</line>
				<line fill="none" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" x1="178.4" y1="{{ dayJobs[1] }}" x2="214.5" y2="{{ dayJobs[0] }}">
					<animate attributeName="y1" from="0" to="{{ dayJobs[1] }}" begin="0s" dur="0.5s" keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
					<animate attributeName="y2" from="0" to="{{ dayJobs[0] }}" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
				</line>

				<circle fill="#FFFFFF" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" cx="34.1" cy="{{ dayJobs[5] }}" r="6.4">
					<animate attributeName="cy" from="0" to="{{ dayJobs[5] }}" begin="0s" dur="0.5s" keySplines=" 0.1 0.8 0.2 1" calcMode="spline"></animate>
				</circle>
				<circle fill="#FFFFFF" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" cx="70.2" cy="{{ dayJobs[4] }}" r="6.4">
					<animate attributeName="cy" from="0" to="{{ dayJobs[4] }}" begin="0s" dur="0.5s" keySplines=" 0.1 0.8 0.2 1" calcMode="spline"></animate>
				</circle>
				<circle fill="#FFFFFF" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" cx="142.3" cy="{{ dayJobs[3] }}" r="6.4">
					<animate attributeName="cy" from="0" to="{{ dayJobs[3] }}" begin="0s" dur="0.5s" keySplines=" 0.1 0.8 0.2 1" calcMode="spline"></animate>
				</circle>
				<circle fill="#FFFFFF" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" cx="106.3" cy="{{ dayJobs[2] }}" r="6.4">
					<animate attributeName="cy" from="0" to="{{ dayJobs[2] }}" begin="0s" dur="0.5s" keySplines=" 0.1 0.8 0.2 1" calcMode="spline"></animate>
				</circle>
				<circle fill="#FFFFFF" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" cx="178.4" cy="{{ dayJobs[1] }}" r="6.4">
					<animate attributeName="cy" from="0" to="{{ dayJobs[1] }}" begin="0s" dur="0.5s" keySplines=" 0.1 0.8 0.2 1" calcMode="spline"></animate>
				</circle>
				<circle fill="#FFFFFF" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" cx="214.5" cy="{{ dayJobs[0] }}" r="6.4">
					<animate attributeName="cy" from="0" to="{{ dayJobs[0] }}" begin="0s" dur="0.5s" keySplines=" 0.1 0.8 0.2 1" calcMode="spline"></animate>
				</circle>
				{% endif %}
			</g>

			<text transform="matrix(1 0 0 1 97.3828 84.3779)" fill="#3D3E42" font-family="'Lato', sans-serif" font-size="53.0289" letter-spacing="2">{{ numJobs }}</text>
			<text transform="matrix(1 0 0 1 63.834 108.018)" fill="#3D3E42" font-family="'Lato', sans-serif" font-size="19.1997" letter-spacing="1">ACTIVE JOBS</text>
			</svg>
		</div>

		<div class="uncompletedCandidates applicationStatus thirdGraph tables col-lg-8 col-md-8 col-sm-12 col-sm-12 no-gutter" ng-switch on="mobile">
			<h6>Most Viewed Adverts</h6>
			<div ng-switch-when="true">
				<table class="table table-striped footable toggle-small default">
					<thead>
						<tr>
							<th data-sort-ignore="true"></th>
							<th data-sort-ignore="true"></th>
							<th data-sort-ignore="true" data-hide="phone"></th>
							<th data-sort-ignore="true" data-hide="phone"></th>
							<th data-toggle="true" data-sort-ignore="true"></th>
						</tr>
					</thead>
					{% if dayJobs is defined %}
						{% if(dayJobs !== false) %}
							{% for job in popJobs %}
								<tr>
									<td><img src="/imgs/svgs/advert.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/advert.png'" /></td>
									<td>{{ job.title }}</td>
									<td>{{ job.locationnice }}</td>
									<td>{{ viewStats[job.ID] }} Views</td>
									<td></td>
								</tr>
							{% endfor %}
						{% else %}
						<tr>
							<td>No job advert data available</td>
						</tr>
						{% endif %}
					{% else %}
						<tr>
							<td>No job advert data available</td>
						</tr>
					{% endif %}
				</table>
			</div>
			<div ng-switch-default>
				<table class="table table-striped toggle-small default">
					{% if popJobs is defined %}
						{% if(popJobs !== false) %}
							{% for job in popJobs %}
								<tr>
									<td><img src="/imgs/svgs/advert.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/advert.png'" /></td>
									<td>{{ job.title }}</td>
									<td>{{ job.locationnice }}</td>
									<td>{{ viewStats[job.ID] }} Views</td>
									<td></td>
								</tr>
							{% endfor %}
						{% else %}
						<tr>
							<td>No job advert data available</td>
						</tr>
						{% endif %}
					{% else %}
						<tr>
							<td>No job advert data available</td>
						</tr>
					{% endif %}
				</table>
			</div>
		</div>

		<div class="uncompletedCandidates applicationStatus thirdGraph tables row col-lg-12 col-md-12 col-sm-12 col-sm-12 no-gutter">
			<hr />
			<h6>Oldest Unfilled Advert</h6>
			<table class="table table-striped footable toggle-small default">
				<thead>
					<tr>
						<th data-toggle="false" data-sort-ignore="true"></th>
						<th data-sort-ignore="true"></th>
						<th data-sort-ignore="true" data-hide="phone"></th>
						<th data-sort-ignore="true" data-hide="phone"></th>
						<th data-sort-ignore="true" data-hide="phone"></th>
					</tr>
				</thead>
				{% if unfilled is defined %}
					{% if(unfilled !== false) %}
						{% for index, job in unfilled %}
							<script></script>
							<tr>
								<td><img src="/imgs/svgs/advert-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/advert-red.png'" /></td>
								<td>{{ job.title }}</td>
								<td>{{ job.locationnice }}</td>
								<td><?php echo floor((time() - strtotime($job->created))/(60*60*24));  ?>  Days</td>
								<td>
									<input type="hidden" disabled="disabled" ng-init="emailApi[{{loop.index}}].discipline='{{job.discipline}}'" />
									<input type="hidden" disabled="disabled" ng-init="emailApi[{{loop.index}}].location='{{job.locationnice}}'" />
									<input type="hidden" disabled="disabled" ng-init="emailApi[{{loop.index}}].availability='{{job.availability}}'" />
									<input type="hidden" disabled="disabled" ng-init='emailApi[{{loop.index}}].searchSkills="<?php echo addslashes(json_encode(unserialize($job->skills))); ?>"' />
									<input type="hidden" disabled="disabled" ng-init='emailApi[{{loop.index}}].qual="<?php echo addslashes(json_encode(unserialize($job->qualifications))); ?>"' />
									<button ng-click="helpFill({{loop.index}})" onClick="return false;" ladda="emailLoading[{{loop.index}}]" data-style="expand-right" data-toggle="modal" data-target=".bs-example-modal-lg"  class="btn btn-success ladda-button"><span class="ladda-label">help me match</span></button>
								</td>
							</tr>
						{% endfor %}
					{% else %}
					<tr>
						<td>No job advert data available</td>
					</tr>
					{% endif %}
				{% else %}
					<tr>
						<td>No job advert data available</td>
					</tr>
				{% endif %}
			</table>
		</div>

	</div>