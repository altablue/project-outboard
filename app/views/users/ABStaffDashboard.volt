<div class="boxTop">
		<h2>Recruiter Dashboard</h2>
		<hr/>
	</div>

	<div class="charts" ng-controller="theDashes">

		<div class="viewedCandidates secondGraph tables col-lg-12 col-xs-12 no-gutter clearfix">
			<h6>Oldest Unfilled Jobs</h6>
			<table class="table table-striped footable">
				{% if unfilledJobs is defined %}
					{% if (unfilledJobs !== false) %}
						<thead>
							<tr>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
							</tr>
						</thead>
						{% for job in unfilledJobs %}
							<tr>
								<td><img src="/imgs/svgs/advert-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/advert-red.png'" /></td>
								<td>{{ job.title }}</td>
								<td>{{ job.locationnice }}</td>
								<td><?php echo floor((time() - strtotime($job->created))/(60*60*24));  ?> days</td>
								<td><a href="/dashboard/job/{{ job.ID }}" class="btn btn-warning" data-style="expand-right"><span class="ladda-label">view advert</span></a></td>
							</tr>
						{% endfor %}
					{% else %}
					<tr>
						<td>No recruiter data available</td>
					</tr>
					{% endif %}
				{% else %}
					<tr>
						<td>No recruiter data available</td>
					</tr>
				{% endif %}
			</table>
		</div>

		<div class="uncompletedCandidates thirdGraph tables row col-lg-12 col-xs-12 no-gutter">
			<hr />
			<h6>Oldest Uncompleted Profiles</h6>
			<table class="table table-striped footable">
				{% if oldCands is defined %}
					{% if (oldCands !== false) %}
						<thead>
							<tr>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
							</tr>
						</thead>
						{% for cands in oldCands %}
							<tr>
								<td><img src="/imgs/svgs/candidate-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/candidate-red.png'" /></td>
								<td>{{ cands.name }}</td>
								<td>{{ cands.username }}</td>
								<td><?php echo floor((time() - strtotime($cands->created))/(60*60*24));  ?> days</td>
								<td><button onclick="return false" ng-click='candReminder("<?php echo addslashes(json_encode($cands)); ?>",{{loop.index}})' class="btn btn-warning" ladda="remindLoading[{{loop.index}}]" data-style="expand-right"><span class="ladda-label">send reminder</span></button></td>
							</tr>
						{% endfor %}
					{% else %}
					<tr>
						<td>No recruiter data available</td>
					</tr>
					{% endif %}
				{% else %}
					<tr>
						<td>No recruiter data available</td>
					</tr>
				{% endif %}
			</table>
		</div>

	</div>
