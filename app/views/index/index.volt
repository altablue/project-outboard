    <div class="hidden alert alert-success">Thanks, we will be in touch shortly.</div>
    <div class="hidden alert alert-danger">There has been an error, please try again later</div>

<div class="container-fluid no-gutter">

    <!-- Modal -->
    <div id="contactForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-controller="MarketingController">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 id="myModalLabel">register form</h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-10 col-centered clearfix">
                        <form class="form-horizontal col-lg-12 clearfix" name="formData" novalidate>
                            <div class="form-group" ng-class="{ 'has-error' : formData.name.$invalid && !formData.name.$pristine }"><input required class="form-control" ng-model="name" type="text" name="name" placeholder="Your Full Name..." /><p ng-show="formData.name.$invalid && !formData.name.$pristine" class="help-block">Your Full Nane is Required</p></div>
                            <div class="form-group"><input class="form-control" ng-model="companyName" type="text" name="companyName" placeholder="Your Company Name..."  /></div>
                            <div class="form-group" ng-class="{ 'has-error' : formData.email.$invalid && !formData.email.$pristine }"><input required class="form-control" ng-model="email" type="email" name="email" placeholder="Contactable Email Address..." /><p ng-show="formData.email.$invalid && !formData.email.$pristine" class="help-block">A Valid Email Address is Required</p></div>
                            <div class="form-group"><input class="form-control" ng-model="phone" type="text" name="number" placeholder="Contactable Phone Number..."  /></div>
                            <div class="form-group"><textarea class="form-control" ng-model="message" name="message" placeholder="Tell us about why you are interested in signing up..."></textarea></div>
                        </form>
                    </div>
                </div><!-- End of Modal body -->
                <div class="modal-footer">
                    <div class="col-lg-10 col-centered">
                        <div class="form-group">
                        <button class="btn btn-success col-lg-6" type="submit" ladda="emailLoading" ng-click="contactSubmitForm()" data-style="expand-right" ng-disabled="formData.$invalid"><span class="ladda-label">send sign up request</span></button>
                            <span class="extra pull-right col-lg-6">we aim to respond to all contact request within 24 hours</span>
                        </div>
                    </div>
                </div>
            </div><!-- End of Modal content -->
        </div><!-- End of Modal dialog -->
    </div><!-- End of Modal -->

<div id="homepage">

    <div class="hero anyStretch" data-stretch="/imgs/homepage/hero.jpg">
        <div class="group">
            <h1>keep your people in work</h1>
            <h4>We can help ensure your people are kept in work during quieter times</h4>
            <a class="boxCTA" data-toggle="modal" href="#contactForm">get started</a>
        </div>
    </div>

    <section id="intro" class="clearfix">
        <div class="col-lg-9">
            <h3>Match People to Jobs by their Skills</h3>
            <p><span>Match People Skills</span> is a collaborative program supported by the Oil and Gas industry alongside public and private sector organisations that benefit from similar skills. This site provides a simple platform to help companies that need to outplace their people during quieter times and helps organisations who need these skills to find them quickly and easily. Our aim is to help mitigate the impact of job losses or reduced hours on the highly skilled people of the Oil and Gas Industry</p>
            <p>Companies can safeguard industry skills for the future and mitigate impact on people by working closely with public and private sector organisations to quickly fill skills gaps, especially for positions which use transferable skills and project work.</p>
        </div>
    </section>

    <section id="benefits" class="clearfix">

        <div class="padding col-lg-10">
          
            <div class="bucket box1 col-lg-4 col-sm-12 col-md-12 clearfix">
                <figure>
                    <img height="45" src="/imgs/svgs/onboard.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/onboard.png'" />
                </figure>
                <p class="userTitle">Company with skills shortage</p>
                <p>By understanding the people demand of highly skilled industries Match People Skills can help you 
engage the expertise of their workforce during times of reduced workload.</p>
            </div>

            <div class="bucket box2 col-lg-4 col-sm-12 col-md-12">
                <figure>
                    <img height="45" src="/imgs/svgs/outboard.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/outboard.png'" />
                </figure>
                <p class="userTitle">Company with Skills surplus</p>
		<p>During periods of reduced workload Match People Skills can help you reduce the impact on your highly skilled 
workforce. We 
can help you share their expertise with other industries that need their skills on a temporary orpermanent basis</p>
            </div>

            <div class="bucket box3 col-lg-4 col-sm-12 col-md-12">
                <figure>
                    <img height="45" src="/imgs/svgs/candidate-yel.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/candidate-yel.png'" />
                </figure>
                <p class="userTitle">Skilled individuals</p>
                <p>As a highly skilled individual your expertise may be able to be used across multiple industries. Match People Skills can help you transfer your skills to reduce the impact of when your chosen industries demand for people is reduced.</p>
            </div>

        </div>

    </section>

    <section id="quote" class="clearfix">
        <div class="floatbox col-lg-8 clearfix">
            <blockquote class="col-lg-6 left">
                <p>Scotland's oil and gas industry is world renowned for its exceptional skills and experience and it is vital that those skills are safeguarded to maintain our globally competitive edge. The Match People Skills initiative is one of a number of exciting and innovative projects being delivered by the Energy Jobs Taskforce that will help us achieve that by providing an effective mechanism for matching individuals seeking alternative employment opportunities with those organisations with skills gaps and recruitment vacancies.</p>
                <cite>- Lena Wilson, Chair of the Energy Jobs Taskforce</cite>
            </blockquote>
            <blockquote class="col-lg-6 right">
                <p>We are committed to working with industry to ensure that we have the skilled workforce in Scotland to strengthen our overall ambition as a major centre for energy activity.</p>
                <p>Scotland's energy sector supports thousands of jobs around the country and is vital to our future economic growth and developing our infrastructure. It is therefore crucial that we ensure that our oil, gas and renewable companies are getting the right people into the right jobs. Wood Group's Match People Skills initiative is a fantastic resource for organisations to match the skills and recruitment vacancies with those best suited for the job.</p>
                <cite>- Nicola Sturgeon, First Minister of Scotland</cite>
            </blockquote>
        </div>
    </section>

    <section id="bluebanner" class="clearfix">
        <p class="col-lg-8 col-md-12 col-sm-12">want to get started? register your interest now <a data-toggle="modal" href="#contactForm" class="cta">register now</a></p>
    </section>

    <footer>

        <div class="wrapper col-lg-8 clearfix">
        
            <p class="col-lg-5 col-md-5 col-sm-5">&copy; Copyright <?php echo date('y'); ?> MatchPeopleSkills.com.<br/>All rights reserved.</p>

            <ul class="col-lg-3 col-md-3 col-sm-3">
                <li><h4>Links</h4></li>
                <li><a href="/login">Login</a></li>
                <li><a data-toggle="modal" href="#contactForm">Contact Us</a></li>
                <li><a href="/terms">Terms &amp; Conditions</a></li>
                <li><a href="/privacy">Privacy Policy</a></li>
                <li><a href="/cookies">Cookies Policy</a></li>
            </ul>

        </div>

    </footer>

</div>
