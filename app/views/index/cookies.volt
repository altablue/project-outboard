   <div class="hidden alert alert-success">Thanks, we will be in touch shortly.</div>
    <div class="hidden alert alert-danger">There has been an error, please try again later</div>
            <nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img class="whole gapless" src="/imgs/svgs/mps.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/mps.png'" /><span>Match People Skills</span></a>
        </div>
        
        <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right collapse navbar-collapse pull-right">
                <li ng-hide="global.isSignedIn()"><a href="/login">Login</a></li>
                <li ng-show="global.isSignedIn()"><a href="/dashboard">Dashboard</a></li>
                <li ng-show="global.isSignedIn()"><a href="/logout">Logout</a></li>
            </ul>
        </nav>

    </div>
</nav>

<div class="container-fluid no-gutter">

    <!-- Modal -->
    <div id="contactForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-controller="MarketingController">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 id="myModalLabel">register form</h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-10 col-centered clearfix">
                        <form class="form-horizontal col-lg-12 clearfix">
                            <div class="form-group"><input class="form-control" ng-model="formData.name" type="text" name="name" placeholder="Your Full Name..." /></div>
                            <div class="form-group"><input class="form-control" ng-model="formData.companyName" type="text" name="companyName" placeholder="Your Company Name..."  /></div>
                            <div class="form-group"><input class="form-control" ng-model="formData.email" type="email" name="email" placeholder="Contactable Email Address..." /></div>
                            <div class="form-group"><input class="form-control" ng-model="formData.phone" type="text" name="number" placeholder="Contactable Phone Number..."  /></div>
                            <div class="form-group"><textarea class="form-control" ng-model="formData.message" name="message" placeholder="Tell us about why you are interested in signing up..."></textarea></div>
                        </form>
                    </div>
                </div><!-- End of Modal body -->
                <div class="modal-footer">
                    <div class="col-lg-10 col-centered">
                        <div class="form-group">
                            <button ng-click="submitForm()" class="btn btn-success col-lg-6" type="submit">sign me up</button>
                            <span class="extra pull-right col-lg-6">we aim to respond to all contact request within 24 hours</span>
                        </div>
                    </div>
                </div>
            </div><!-- End of Modal content -->
        </div><!-- End of Modal dialog -->
    </div><!-- End of Modal -->

<div id="homepage">

    <div class="hero anyStretch" data-stretch="/imgs/homepage/hero.jpg">
        <div class="group">
            <h1>keep your people in work</h1>
            <h4>We can help ensure your people are kept in work during quieter times</h4>
            <a class="boxCTA" data-toggle="modal" href="#contactForm">get started</a>
        </div>
    </div>

    <section id="intro" class="clearfix">
        <div class="col-lg-9 legal">
            <h3 class="whole centered col">Cookies Policy</h3>
            <p>This website uses cookies to distinguish you from other users of our website. This helps us to provide you with a good experience when you browse our website and also allows us to improve our site.</p>

            <p> cookie is a small file of letters and numbers that we store on your browser or the hard drive of your computer if you agree. Cookies contain information that is transferred to your computer's hard drive. More information on controlling cookies can be found on <a href="www.aboutcookies.org." target="_blank" >www.aboutcookies.org.</a></p>

            <p>We use the following cookies:</p>
                <ul>
                    <li>Strictly necessary cookies. These are cookies that are required for the operation of our website. They include, for example, cookies that enable you to log into secure areas of our website.</li>
                    <li>Analytical/performance cookies. They allow us to recognise and count the number of visitors and to see how visitors move around our website when they are using it. This helps us to improve the way our website works, for example, by ensuring that users are finding what they are looking for easily.</li>
                    <li>Functionality cookies. These are used to recognise you when you return to our website. This enables us to personalise our content for you, greet you by name and remember your preferences (for example, your choice of language or region).</li>
                    <li>Targeting cookies. These cookies record your visit to our website, the pages you have visited and the links you have followed. We will use this information to make our website and the advertising displayed on it more relevant to your interests. We may also share this information with third parties for this purpose.</li>
                </ul>
            <p>You can find more information about the individual cookies we use and the purposes for which we use them in the table below:</p>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <td>Type</td>
                        <td>Name</td>
                        <td>Description</td> 
                        <td>Comments</td>
                    </tr>
                </thead>
                <tbody>                 
                    <tr>
                        <td>Session Cookie</td>
                        <td>PPSN5I</td>
                        <td>Used in load balancing.</td>
                        <td>Temporary, removed at the end of session</td>
                    </tr>
                    <tr>
                        <td>Persistent Cookie</td>   
                        <td>__utma (Google Analytics)</td>   
                        <td>Tracks the first and last visit (including Days and Visit). It is a persistent cookie which remains on a computer for 2 years unless the cache is cleared.</td>
                        <td><a href="http://www.google.co.uk/intl/en-GB/policies/technologies/cookies/" target="_blank" >Google Privacy Policy</a></td>
                    </tr>
                    <tr>
                        <td>Session cookie</td>
                        <td>__utmb (Google Analytics)</td>
                        <td>Records the exact arrival time of a visit. It is a session cookie which expires when the user leaves the website or closes the browser window.</td>
                        <td><a href="http://www.google.co.uk/intl/en-GB/policies/technologies/cookies/" target="_blank" >Google Privacy Policy</a></td>
                    </tr>
                    <tr>
                        <td>Persistent cookie</td>
                        <td>__utmc (Google Analytics)</td>
                        <td>Records the exact arrival time of a visit. It is a session cookie which expires when the user leaves the website or closes the browser window.</td>
                        <td><a href="http://www.google.co.uk/intl/en-GB/policies/technologies/cookies/" target="_blank" >Google Privacy Policy</a></td>
                    </tr>
                    <tr>
                        <td>Persistent cookie</td>
                        <td>__utmc (Google Analytics)</td>
                        <td>Tracks the exit time of a visit. It remains on a computer until 30 minutes after the users last page view on the web site.</td>
                        <td><a href="http://www.google.co.uk/intl/en-GB/policies/technologies/cookies/" target="_blank" >Google Privacy Policy</a></td>
                    </tr>
                    <tr>
                        <td>Persistent cookie</td>
                        <td>__utmz</td>
                        <td>Tracks where the user entered the site from e.g. a search engine or similar website.</td>
                        <td><a href="http://www.google.co.uk/intl/en-GB/policies/technologies/cookies/" target="_blank" >Google Privacy Policy</a></td>
                    </tr>
                    <tr>
                        <td>Persistent cookie</td>
                        <td>__utmv (Google Analytics)</td>
                        <td>Used for segmentation and data experimentation The __utmv cookie works with the __utmz cookie to improve cookie targeting capability.</td>
                        <td><a href="http://www.google.co.uk/intl/en-GB/policies/technologies/cookies/" target="_blank" >Google Privacy Policy</a></td>
                    </tr>
                    <tr>
                        <td>Session Cookie</td>
                        <td>ASPNET_SessionID</td>
                        <td>A strictly necessary cookie which stores anonymous tokens and unique identifier that allows the site to identify different visitors as they move around the page.</td>
                        <td></td>
                    <tr>
                        <td>Persistnet Cookie</td>
                        <td>guest_id</td>
                        <td>This cookie is used to identify a user to twitter.<br/><br/>If you do not use or have a twitter account is as unique number to you and collects information about what you viewed which may for example appear if you later decided to create a twitter profile</td>
                        <td><a href="https://twitter.com/privacy" target="_blank" >Twitter Privacy Policy</a></td>
                    </tr>
                </tbody>
            </table>
            <br/><br/>
            <p>Please note that third parties (including, for example, advertising networks and providers of external services like web traffic analysis services) may also use cookies, over which we have no control.</p>

            <p>These cookies are likely to be analytical/performance cookies or targeting cookies. You block cookies by activating the setting on your browser that allows you to refuse the setting of all or some cookies. However, if you use your browser settings to block all cookies (including essential cookies) you may not be able to access all or parts of our site.</p>
        </div>
    </section>

    <section id="bluebanner" class="clearfix">
        <p class="col-lg-8 col-md-12 col-sm-12">want to get started? register your interest now <a data-toggle="modal" href="#contactForm" class="cta">register now</a></p>
    </section>

    <footer>

        <div class="wrapper col-lg-8 clearfix">
        
            <p class="col-lg-5 col-md-5 col-sm-5">&copy; Copyright <?php echo date('y'); ?> MatchPeopleSkills.com.<br/>All rights reserved.</p>

            <ul class="col-lg-3 col-md-3 col-sm-3">
                <li><h4>Links</h4></li>
                <li><a href="/login">Login</a></li>
                <li><a href="/tcontactus">Contact Us</a></li>
                <li><a href="/terms">Terms &amp; Conditions</a></li>
                <li><a href="/privacy">Privacy Policy</a></li>
                <li><a href="/cookies">Cookies Policy</a></li>
            </ul>

        </div>

    </footer>

</div>