​** Job Status Updated
------------------------------------------------------------

{{name}}, {{comp}}'s job #{{jobId}} ({{jobTtitle}} in {{jobLoc}}) has been updated to:

{{status}}

Login to View ({{link}})


You have recieved this email as your email address is registered on Match People Skills (https://www.matchpeopleskills.com) . If you do not believe this to be correct, please click here (info@alta-blue.com) to log a complaint.