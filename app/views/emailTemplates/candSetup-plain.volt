​
** You have been selected:
------------------------------------------------------------

{{name}}, you have been selected by {{comp}} to complete a candidate profile on Match People Skills. Please use the link below to complete your registration.
Complete Registration ({{setupUrl}})


You have recieved this email as your email address is registered on Match People Skills (https://www.matchpeopleskills.com) . If you do not believe this to be correct, please click here (mailto:info@alta-blue.com) to log a complaint.