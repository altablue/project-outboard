** Registration Request:
------------------------------------------------------------

{{name}}, Congratulations! You have been chosen to be a user for {{comp}} on Match People Skills. Please use the button below to complete your registration.
Complete Registration ({{setupUrl}})

​
You have recieved this email as your email address is registered on Match People Skills (https://www.matchpeopleskills.com) . If you do not believe this to be correct, please click here (info@alta-blue.com) to log a complaint.