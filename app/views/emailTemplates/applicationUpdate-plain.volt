​** Job Application Status Updated
------------------------------------------------------------

{{name}}, Your application status for {{jobData.title}} position in {{jobData.locationnice}} has been updated to:

{{status}}

Login to View ({{link}})



You have recieved this email as your email address is registered on Match People Skills (https://www.matchpeopleskills.com) . If you do not believe this to be correct, please click here (info@alta-blue.com) to log a complaint.