​** Request for help matching:
------------------------------------------------------------

{{recrutname}}, {{name}} from {{company}} requires helping matching with the following credentials:

field name required query
<?php foreach($params as $key => $data): ?>
  <?php echo $key; ?> - <?php echo $data; ?>
  
<?php endforeach; ?>

Login to Help ({{link}})

You have recieved this email as your email address is registered on Match People Skills (https://www.matchpeopleskills.com) . If you do not believe this to be correct, please click here (info@alta-blue.com) to log a complaint.