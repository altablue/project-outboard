** Password Reset Request:
------------------------------------------------------------

{{name}}, a request to reset your password has been logged. If you are expecting this email, please click on the link below to reset your password:

({{reset}})

If are not expecting this email, you do not been to do anything as your password has not been reset yet and you can delete this email.
​
You have recieved this email as your email address is registered on Match People Skills (https://www.matchpeopleskills.com) . If you do not believe this to be correct, please click here (info@alta-blue.com) to log a complaint.