** Reminder:
------------------------------------------------------------

{{name}}, this is a reminder to complete your candidate profile on Match People Skills. Please use the button below to login and complete your profile:
login ({{login}})

If you cannot login or have forgotten your password, please use the link below to reset your password:
Reset Password ({{reset}})

​
You have recieved this email as your email address is registered on Match People Skills (https://www.matchpeopleskills.com) . If you do not believe this to be correct, please click here (info@alta-blue.com) to log a complaint.