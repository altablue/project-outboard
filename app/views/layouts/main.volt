<nav class="navbar navbar-default navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button ng-init="navCollapsed = true" ng-click="navCollapsed = !navCollapsed" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img class="whole gapless" src="/imgs/svgs/mps.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/mps.png'" /><span>Match People Skills</span></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div collapse="navCollapsed" class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-right">

                <li ng-hide="global.isSignedIn()"><a href="/login">Login</a></li>
                <li ng-show="global.isSignedIn()"><a href="/dashboard">Dashboard</a></li>


                {% if logged %}
                    <li class="dropdown" dropdown is-open="status.isopen">
                      <a onclick="return false;" class="dropdown-toggle" dropdown-toggle ng-disabled="disabled" on-toggle="toggled(open)" data-toggle="dropdown" role="button" aria-expanded="false">I Want To &hellip; &nbsp;<span class="caret"></span></a>
                      <ul class="dropdown-menu" role="menu">
                        <li ng-if="global.isOnBoardComp() && global.isBoardUser() || global.isABStaff()"><a href="/dashboard/jobs/create">Create Job Advert</a></li>
                        <li ng-if="!global.isOnBoardComp() && global.isBoardUser() || global.isABStaff()"><a href="/dashboard/candidates/create">Add Candidate</a></li>
                        <li ng-if="global.isSignedIn() && global.isAdminUser() && !global.isABStaff() || global.isABStaff()"><a href="/register/addUsers/{{ boardNum }}">Register User</a></li>
                        <li ng-if="global.isSignedIn() && global.isABStaff()"><a href="/register/">Register Company</a></li>
                        <li ng-if="global.isSignedIn() && global.isSuperStaff()"><a href="/register/recruiter/">Register Recruiter</a></li>
                        <?php if (!empty($candidateId)): ?><li ng-if="global.isCandidateUser()"><a href="/dashboard/candidates/update/{{ candidateId }}">Edit My Profile</a></li><?php endif; ?>
                      </ul>
                    </li>

                    <li class="dropdown2" dropdown>
                        <a onclick="return false;" class="dropdown-toggle" dropdown-toggle data-toggle="dropdown2" role="button" aria-expanded="false">I Need Help &hellip; &nbsp;<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/reset-password/">Reset Password</a></li>
                            <li><a ng-click="helpMe()">Email Support</a></li>
                        </ul>
                    </li>

                {% endif %}

                <li ng-show="global.isSignedIn()"><a href="/logout">Logout</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container-fluid no-gutter">

    {% if logged %}

    {% include 'layouts/nav.volt' %}

    <script type="text/ng-template" id="helpModal.html">
      <div class="modal-header">
        <button type="button" class="close" ng-click="cancel()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        
        <fieldset>
            <p>hello world</p>
        </fieldset>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" ng-click="cancel()">Close</button>
        <button type="button" class="btn btn-primary" ng-click="ok()">Save changes</button>
      </div>
    </script>

    <section id="main_content" class="col-lg-9 col-md-10 col-sm-10 col-xs-10 no-gutter" role="main">

        <div ng-bind-html="global.flashStatus"></div>

        {{ flash.output() }}

        <div id="content_box" class="clearfix">

            {{ content() }}

        </div>

        <footer class="row no-gutter">
        </footer>
    </section>

    {% else %}

        <div ng-bind-html="global.flashStatus"></div>

        {{ flash.output() }}

        {{ content() }}

    {% endif %}
</div>
