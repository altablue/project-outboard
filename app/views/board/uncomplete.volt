
{{ content() }}


<div class="boxTop">
	<h2>{{ bName }}'s Candidates (Uncompleted Profiles)</h2>
	<hr/>
</div>

<div class="getAllWrapper" ng-controller="theDashes">

	<div ng-bind-html="flashStatus"></div>

	<table class="table table-striped footable">

	<thead>
		<tr>
			<th data-sort-ignore="true"<strong>Candidate Name</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>Candidate Email</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>Days on System</strong></th>
			<th data-sort-ignore="true" data-hide="phone"></th>
		</tr>
	</thead>
	<tbody>

	{% for cands in incompleteUsers %}

		<tr ng-init="showList[{{ loop.index }}] = false; cand[{{ loop.index }}] = {{ cands.id }}">
			<td>{{ cands.name }}</td>
			<td>{{ cands.username }}</td>
			<td><?php echo floor((time() - strtotime($cands->created))/(60*60*24));  ?> days</td>
			<td>
				<button onclick="return false" ng-click='candReminder("<?php echo addslashes(json_encode($cands)); ?>",{{loop.index}})' class="btn btn-warning" ladda="remindLoading[{{loop.index}}]" data-style="expand-right"><span class="ladda-label">send reminder</span></button>
			</td>
		</tr>

	{% endfor %}

		<tfoot>
			<tr>
				<td colspan="5">
					<div class="pagination pagination-centered hide-if-no-paging pager col-xs-12"></div>
				</td>
			</tr>
		</tfoot>

	</table>

</div>