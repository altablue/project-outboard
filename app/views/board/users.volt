
{{ content() }}


<div class="boxTop">
	<h2>{{ bName }}'s Users</h2>
	<hr/>
</div>

<div class="getAllWrapper" ng-controller="recruitEdit">

	<div ng-bind-html="flashStatus"></div>

	<table class="table table-striped footable">

	<thead>
		<tr>
			<th data-sort-ignore="true"<strong>User Name</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>User Email</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>User Level</strong></th>
		</tr>
	</thead>

	<tbody>

	{% for user in userList %}

		<tr ng-init="showList[{{ loop.index }}] = false; cand[{{ loop.index }}] = {{ user.id }}">
			<td>{{ user.name }}</td>
			<td>{{ user.username }}</td>
			<td>
				{% if(user.level == 1) %}
					{% set status = 'Candidate' %}
				{% elseif(user.level == 2) %}
					{% set status = 'User' %}
				{% elseif(user.level == 3) %}
					{% set status = 'Admin User' %}
				{% elseif(user.level >= 99) %}
					{% set status = 'Recruiter' %}
				{% else %}
					{% set status = 'User' %}
				{% endif %}
				<span ng-init="updateStatus[{{ loop.index }}]='{{ status }}'" ng-hide="showList[{{ loop.index }}]" ng-bind="updateStatus[{{ loop.index }}]"></span>
			</td>
		</tr>

	{% endfor %}

		<tfoot>
			<tr>
				<td colspan="5">
					<div class="pagination pagination-centered hide-if-no-paging pager col-xs-12"></div>
				</td>
			</tr>
		</tfoot>

	</table>

</div>