
{{ content() }}

<div class="boxTop">
	<h2>{{ bName }}'s Job Adverts</h2>
	<hr/>
</div>

<div class="getAllWrapper col-xs-12" ng-controller="recruitEdit">

	<div ng-bind-html="flashStatus"></div>

	<table class="table table-striped footable col-xs-12 toggle-small default">

		<thead>
			<tr>
				<th data-sort-ignore="true" data-toggle="true"><strong>Job title</strong></th>
				<th data-hide="phone"><strong>Job Location</strong></th>
				<th data-hide="phone"><strong>Job Discipline</strong></th>
				<th data-hide="phone"><strong>Job Status</strong></th>
				<th data-hide="phone"><strong>Days on System</strong></th>
				<th data-sort-ignore="true" data-hide="phone"></th>
			</tr>
		</thead>

		<tbody>
			{% for job in jobs %}

				<tr ng-init="showList[{{ loop.index }}] = false; cand[{{ loop.index }}] = {{ job.ID }}">
					<td>{{ job.title }}</td>
					<td>{{ job.locationnice }}</td>
					<td>{{ job.discipline }}</td>
					<td>
						<div ng-hide="!showList[{{ loop.index }}]">
							<ui-select ng-model="updateStatus[{{ loop.index }}]" theme="bootstrap" on-select="CandStatusChange($item, updateStatus, add, {{ loop.index }}, 'Job')"  ng-disabled="disabled">
			                    <ui-select-match placeholder="Please Select a Status...">[[$select.selected]]</ui-select-match>
			                    <ui-select-choices repeat="stat in adStatus">
			                      <div ng-bind-html="stat"></div>
			                    </ui-select-choices>
			                </ui-select>
						</div>
						{% if(job.status == 1) %}
							{% set status = 'Unfilled' %}
						{% else %}
							{% set status = 'Filled' %}
						{% endif %}
						<span ng-init="updateStatus[{{ loop.index }}]='{{ status }}'" ng-hide="showList[{{ loop.index }}]" ng-bind="updateStatus[{{ loop.index }}]"></span>
					</td>
					<td><?php echo floor((time() - strtotime($job->created))/(60*60*24));  ?> days</td>
					<td>
						<a class="btn btn-primary" href="/dashboard/job/{{ job.ID }}">view job advert</a>
						<a class="btn btn-success" href="/dashboard/job/{{ job.ID }}/candidates/">view applied candidates</a>
						{% if userLevel >= 99 %}<a class="btn btn-warning" ng-click="showList[{{ loop.index }}] = true" onclick="return false;" href="">edit job status</a>{% endif %}
						<a class="btn btn-info" href="/dashboard/jobs/update/{{ job.ID }}">edit job advert</a>
					</td>
				</tr>

			{% endfor %}
		</tbody>

		<tfoot>
			<tr>
				<td colspan="5">
					<div class="pagination pagination-centered hide-if-no-paging pager col-xs-12"></div>
				</td>
			</tr>
		</tfoot>

	</table>

</div>
