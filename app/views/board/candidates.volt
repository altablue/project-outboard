
{{ content() }}


<div class="boxTop">
	<h2>{{ bName }}'s Candidates</h2>
	<hr/>
</div>

<div class="getAllWrapper" ng-controller="recruitEdit">

	<div ng-bind-html="flashStatus"></div>

	<table class="table table-striped footable">

	<thead>
		<tr>
			<th data-sort-ignore="true"<strong>Candidate Name</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>Candidate Email</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>Candidate Contact Number</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>Candidate Discipline</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>Status</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>Days on System</strong></th>
			<th data-sort-ignore="true" data-hide="phone"></th>
			<th data-sort-ignore="true" data-hide="phone"></th>
		</tr>
	</thead>

	<tbody>

	{% for cands in candidates %}

		<tr ng-init="showList[{{ loop.index }}] = false; cand[{{ loop.index }}] = {{ cands.id }}">
			<td>{{ cands.users.name }}</td>
			<td>{{ cands.users.username }}</td>
			<td>{{ cands.phone }}</td>
			<td>{{ cands.discipline }}</td>
			<td>
				<div ng-hide="!showList[{{ loop.index }}]">
					<ui-select ng-model="updateStatus[{{ loop.index }}]" theme="bootstrap" on-select="CandStatusChange($item, updateStatus, add, {{ loop.index }}, 'Cand')"  ng-disabled="disabled">
	                    <ui-select-match placeholder="Please Select a Status...">[[$select.selected]]</ui-select-match>
	                    <ui-select-choices repeat="stat in candStatus">
	                      <div ng-bind="stat"></div>
	                    </ui-select-choices>
	                </ui-select>
				</div>
				{% if(cands.status == 1) %}
					{% set status = 'Active' %}
				{% else %}
					{% set status = 'Inactive' %}
				{% endif %}
				<span ng-init="updateStatus[{{ loop.index }}]='{{ status }}'" ng-hide="showList[{{ loop.index }}]" ng-bind="updateStatus[{{ loop.index }}]"></span>
			</td>
			<td><?php echo floor((time() - strtotime($cands->users->created))/(60*60*24));  ?> days</td>
			<td>
				<a class="btn btn-primary" href="/dashboard/candidates/{{ cands.id }}">view profile</a>
				<a ng-if="global.isABStaff()" class="btn btn-success" href="/dashboard/candidates/{{ cands.id }}/jobs">view applied jobs</a>
				<a href="#" ng-click="showList[{{ loop.index }}] = true" onclick="return false;" class="btn btn-warning">change status</a>
			</td>
		</tr>

	{% endfor %}

		<tfoot>
			<tr>
				<td colspan="5">
					<div class="pagination pagination-centered hide-if-no-paging pager col-xs-12"></div>
				</td>
			</tr>
		</tfoot>

	</table>

</div>