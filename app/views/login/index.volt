
{{ content() }}

<div id="main_content" class="login col-xs-12 col-md-10 col-lg-6 col-centered" ng-controller="userLogin">

<div id="content_box">

<div class="topBox">
    <h2>Login</h2>
    <hr/>
</div>

<div>

    {{ form('/dashboard', 'id': 'UserRegisterForm', 'class': 'form-horizontal',  'role':'form' ) }}
          {{ form.render('apiController', ['ng-model':'formData.apiController', 'ng-init':'formData.apiController="login"']) }}

          <div class="form-group">
            {{ form.label('username', ['class': 'col-sm-3 control-label']) }}
            <div class="col-sm-9">
              {{ form.render('username', ['class': 'form-control', 'ng-model':'formData.loginUsername']) }}
            </div>
          </div>
          <div class="form-group">
            {{ form.label('password', ['class': 'col-sm-3 control-label']) }}
            <div class="col-sm-9">
              {{ form.render('password', ['class': 'form-control','ng-model':'formData.loginPassword']) }}
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
              <div class="checkbox">
                <label>
                  <input name="formData[cookie]" value="true" ng-model="formData.cookie" type="checkbox"/> Remember me
                </label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
              <button class="btn btn-primary ladda-button" ladda="loading" ng-click="submitForm()" onClick="return false;" data-style="expand-right"><span class="ladda-label">Login</span></button>
            </div>
          </div>
    </form>

</div>

</div>

</div>
