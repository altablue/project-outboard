
{{ content() }}

<div  class="login col-xs-12 col-md-10 col-lg-10 col-centered" ng-controller="userLogin">


        <div class="topBox">
            <h2>Reset Your Password:</h2>
            <hr/>
        </div>

        <div>

            <p>Please enter the email you login with below and hit "Reset Your Password":</p>          
                
            <form class="form-horizontal" ng-model="formData.apiController" ng-init="formData.apiController='resetPassword'">
                
                <div class="form-group clearfix">
                    <div class="left col-sm-9">
                        <label>E-mail Address:</label>
                        <input class="form-control" type="email" ng-model="email" placeholder="me@me.com" />
                    </div>
                    <div class="right col-sm-3">
                        <button class="btn btn-primary" ladda="loading" ng-click="resetPass()" onClick="return false;" data-style="expand-right"><span class="ladda-label">Reset your password</span></button>
                    </div>
                </div>
            </form>

        </div>

</div>
