<?php

use Phalcon\Mvc\View;
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaData;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Session as FlashSession;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Assets\Manager;
use KeenIO\Client\KeenIOClient;
use Phalcon\Http\Response\Cookies;


use Outboard\Mail\Mail;
use Outboard\BootstrapFlash\BootstrapFlash;

//$user->persistent->remove('acl');

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

$di->set('assets', function () {
    return new Phalcon\Assets\Manager();
}, true);

/**
 * We register the events manager
 */
$di->set('dispatcher', function() use ($di) {

	$eventsManager = new EventsManager;


	/**
	 * Check if the user is allowed to access certain action using the SecurityPlugin
	 */
	$eventsManager->attach('dispatch:beforeDispatch', new SecurityPlugin);

	/**
	 * Handle exceptions and not-found exceptions using NotFoundPlugin
	 */
	$eventsManager->attach('dispatch:beforeException', new NotFoundPlugin);

	$dispatcher = new Dispatcher;
	$dispatcher->setEventsManager($eventsManager);

	return $dispatcher;
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function() use ($config){
	$url = new UrlProvider();
	$url->setBaseUri($config->application->baseUri);
	return $url;
});


$di->set('view', function() use ($config) {

	$view = new View();

	$view->setViewsDir(APP_PATH . $config->application->viewsDir);

	$view->registerEngines(array(
		".volt" => 'volt'
	));

	return $view;
});

/**
 * Setting up volt
 */
$di->set('volt', function($view, $di) {

	$volt = new VoltEngine($view, $di);

	$volt->setOptions(array(
		"compiledPath" => APP_PATH . "cache/volt/",
		'stat' => true,
        'compileAlways' => true  
	));

	$compiler = $volt->getCompiler();
	$compiler->addFunction('is_a', 'is_a');

	return $volt;
}, true);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function() use ($config) {
	$dbclass = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
	return new $dbclass(array(
		"host"     => $config->database->host,
		"username" => $config->database->username,
		"password" => $config->database->password,
		"dbname"   => $config->database->name
	));
});

$di->set('modelsCache', function() {

    //Cache data for one day by default
    $frontCache = new \Phalcon\Cache\Frontend\Data(array(
        "lifetime" => 86400
    ));

    //Memcached connection settings
    $cache = new \Phalcon\Cache\Backend\Memcache($frontCache);

    return $cache;
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function() {
	return new MetaData();
});

 $di->set('modelsManager', function() {
      return new Phalcon\Mvc\Model\Manager();
 });

/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function() {
	$session = new SessionAdapter();
	$session->start();
	return $session;
});

/**
 * Register the flash service with custom CSS classes
 */
$di->set('flash', function(){
	/*return new FlashSession(array(
		'error'   => 'alert alert-danger',
		'success' => 'alert alert-success',
		'notice'  => 'alert alert-info',
	));*/
	return new BootstrapFlash();
});

/**
 * Register a user component
 */
$di->set('elements', function(){
	return new Elements();
});

/**
 * Setting up routes
 */
$di->set('router', function(){
    require APP_PATH . 'app/config/routes.php';
    return $router;
});

/**
 * Mail service
 */
$di->set('mail', function () {
    return new Mail();
});

/**
 * Elasticsearch service
 */
$di->set('elasticsearch', function () {
    return new Elasticsearch();
});

/**
 * KeenIO service
 */
$di->set('keenio', function() use ($config){
	return KeenIOClient::factory([
	    'projectId' => $config->keenio->projectId,
	    'writeKey'  => $config->keenio->writeKey,
	    'readKey'   => $config->keenio->readKey
	]);
});

/**
*	 API Private key
**/
$di->set('apiPrivateKey', function() use ($config){
	return $config->apiHash->apiKey;
}); 

/**
*	 API Public key
**/
$di->set('apiPublicKey', function() use ($config){
	return $config->apiHash->publicKey;
}); 

$di->set('cookieHash1', function() use ($config){
	return $config->cookieHash->stayLogged;
});

$di->set('cookieHash2', function() use ($config){
	return $config->cookieHash->secondHash;
});

$di->set('publicURL', function() use ($config){
	return $config->application->publicUrl;
});

$di->set('cookies', function() {
    $cookies = new Phalcon\Http\Response\Cookies();
    $cookies->useEncryption(false);
    return $cookies;
});

/**
*	 Cookies
**/
$di->set('crypt', function() {
    $crypt = new Phalcon\Crypt();
    $crypt->setKey('aFLbkRhz&0f5zVqlbSMl'); //Use your own key!
    return $crypt;
});

/**
 * Register global vars
 *//*
$di->set('db_prefix', function() use ($config) {
	return $config->globalVars->dbPrefix;
});*/