<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

$router = new Phalcon\Mvc\Router(false);

$router->removeExtraSlashes(true);
$router->setDefaultNamespace(false);
$router->setDefaultController('index');
$router->setDefaultAction('index');


/**
 * Home page routes
 */

$router->add("/", array(
    'controller' => 'index',
    'action' => 'index',
))->via(array('GET'));

/**
 * Global Route Rules
 */

/** add Routes **/

$router->add("/:controller/:params", array(
    'controller' => 1,
    'params' => 2,
))->via(array('GET'));

$router->add("/:controller/:action/:params", array(
    'controller' => 1,
    'action' => 2,
    'params' => 3,
))->via(array('GET'));

$router->add("/terms/:params", array(
    'controller' => 'index',
    'action' => 'terms',
    'params' => 3,
))->via(array('GET'));

$router->add("/privacy/:params", array(
    'controller' => 'index',
    'action' => 'privacy',
    'params' => 3,
))->via(array('GET'));

$router->add("/cookies/:params", array(
    'controller' => 'index',
    'action' => 'cookies',
    'params' => 3,
))->via(array('GET'));

$router->add("/register/addUsers/{boardNum:[0-999999999999]+}/:params", array(
    'controller' => 'register',
    'action' => 'addUsers',
    'boardNum' => 'boardNum'
))->via(array('GET'));

$router->add("/user/register/{userId:[0-99999999999]+}/{pass}/:params", array(
    'controller' => 'signup',
    'action' => 'registerUser',
    'userId' => 'userId',
    'pass' => 'pass',
    'params' => 3,
))->via(array('GET'));

$router->add('/dashboard/:controller/:action/:params', array(
    'controller'    => 1,
    'action'        => 2,
    'params'    => 3,
))->via(array('GET'));

$router->add("/dashboard", array(
    'controller' => 'users',
    'action' => 'index'
))->via(array('GET'));

$router->add("/logout", array(
    'controller' => 'login',
    'action' => 'logout'
))->via(array('GET'));

$router->add("/dashboard/results/:params", array(
    'controller' => 'search',
    'action' => 'results',
    'params' => 1
))->via(array('GET'));

$router->add("/dashboard/candidates/{id:[0-99999999999]+}/:params", array(
    'controller' => 'candidates',
    'action' => 'get',
    'params' => 1
))->via(array('GET'));

$router->add("/dashboard/candidates/update/{id:[0-99999999999]+}/:params", array(
    'controller' => 'candidates',
    'action' => 'update',
    'params' => 1
))->via(array('GET'));

$router->add("/dashboard/job/{id:[0-99999999999]+}/:params", array(
    'controller' => 'jobs',
    'action' => 'get',
    'params' => 1
))->via(array('GET'));

$router->add("/dashboard/job/{id:[0-99999999999]+}/candidates/:params", array(
    'controller' => 'jobs',
    'action' => 'getJobCandidates',
    'params' => 1
))->via(array('GET'));

$router->add("/dashboard/board/candidates/{id:[0-99999999999]+}/:params", array(
    'controller' => 'board',
    'action' => 'candidates',
    'params' => 1
))->via(array('GET'));

$router->add("/dashboard/board/jobs/{id:[0-99999999999]+}/:params", array(
    'controller' => 'board',
    'action' => 'jobs',
    'params' => 1
))->via(array('GET'));

$router->add("/register/recruiter/:params", array(
    'controller' => 'register',
    'action' => 'addRecruiter',
    'params' => 1
))->via(array('GET'));

$router->add("/dashboard/recruiter/{id:[0-999999999999]+}/boards/:params", array(
    'controller' => 'recruiter',
    'action' => 'getCompanies',
    'params' => 1
))->via(array('GET'));

$router->add("/dashboard/candidates/{id:[0-999999999999]+}/jobs/:params", array(
    'controller' => 'candidates',
    'action' => 'getJobs',
    'params' => 1
))->via(array('GET'));

$router->add("/dashboard/board/users/{id:[0-999999999999]+}/:params", array(
    'controller' => 'board',
    'action' => 'users',
    'params' => 1
))->via(array('GET'));

$router->add("/dashboard/board/uncomplete/{id:[0-999999999999]+}/:params", array(
    'controller' => 'board',
    'action' => 'uncomplete',
    'params' => 1
))->via(array('GET'));

$router->add("/reset-password/:params", array(
    'controller' => 'login',
    'action' => 'passwordReset',
    'params' => 1
))->via(array('GET'));

/** API Routes **/

/** GET  - Check if user has valid login session **/

$router->add("/api/1.0/login/userAuth/:params", array(
    'controller' => 'api',
    'action' => 'checkAuth'
))->via(array('GET'));

$router->add("/api/1.0/candidate/{id:[0-99999999999]+}/:params", array(
    'controller' => 'api',
    'action' => 'getCandidate',
))->via(array('GET'));

$router->add("/sitemap/:params", array(
    'controller' => 'index',
    'action' => 'sitemap',
    'params' => 1
))->via(array('GET'));

/** POST - User Login **/

$router->add("/api/1.0/login/:params", array(
    'controller' => 'api',
    'action' => 'authUser',
))->via(array('POST'));

/** POST - create company **/

$router->add("/api/1.0/company/:params", array(
	'controller' => 'api',
	'action' => 'createCompany',
))->via(array('POST'));

/** POST - create users **/

$router->add("/api/1.0/users/:params", array(
	'controller' => 'api',
	'action' => 'createUser',
))->via(array('POST'));

/** POST - update users profile data **/

$router->add("/api/1.0/users/{userId:[0-99999999999]+}/:params", array(
	'controller' => 'api',
	'action' => 'updateUser',
))->via(array('PUT'));

/** POST - create candidate **/

$router->add("/api/1.0/candidate/:params", array(
    'controller' => 'api',
    'action' => 'createCandidate',
))->via(array('POST'));

/** PUT - Update candidate **/

$router->add("/api/1.0/candidate/:params", array(
    'controller' => 'api',
    'action' => 'updateCandidate',
))->via(array('PUT'));

/** GET - Search Query **/

$router->add("/api/1.0/search/:params", array(
    'controller' => 'api',
    'action' => 'search',
))->via(array('POST'));

/** GET - Search Query **/

$router->add("/api/1.0/job", array(
    'controller' => 'api',
    'action' => 'createJob',
))->via(array('POST'));


$router->add("/api/1.0/recruiterReg", array(
    'controller' => 'api',
    'action' => 'registerRecruiter',
))->via(array('POST'));

$router->add("/api/1.0/updateCandStatus/{id:[0-99999999999]+}/:params", array(
    'controller' => 'api',
    'action' => 'updateCandStatus',
))->via(array('PUT'));

$router->add("/api/1.0/updateJobStatus/{id:[0-99999999999]+}/:params", array(
    'controller' => 'api',
    'action' => 'updateJobStatus',
))->via(array('PUT'));

$router->add("/api/1.0/updateFillStatus/{id:[0-99999999999]+}/:params", array(
    'controller' => 'api',
    'action' => 'updateFillStatus',
))->via(array('PUT'));

$router->add("/api/1.0/sendMail/helpFillEmail", array(
    'controller' => 'api',
    'action' => 'helpFillEmail',
))->via(array('POST'));

$router->add("/api/1.0/sendMail/contactRequest", array(
    'controller' => 'api',
    'action' => 'contactRequest',
))->via(array('POST'));

$router->add("/api/1.0/sendMail/jobAssign", array(
    'controller' => 'api',
    'action' => 'jobApply',
))->via(array('POST'));

$router->add("/api/1.0/sendMail/contactSubmitEmail", array(
    'controller' => 'api',
    'action' => 'contactSubmitForm',
))->via(array('POST'));

$router->add("/api/1.0/sendMail/candReminder", array(
    'controller' => 'api',
    'action' => 'candReminder',
))->via(array('POST'));

$router->add("/api/1.0/addCandNote/:params", array(
    'controller' => 'api',
    'action' => 'addCandNote',
))->via(array('POST'));

$router->add("/api/1.0/resetPassword/:params", array(
    'controller' => 'api',
    'action' => 'resetPassword',
))->via(array('POST'));

//$router->handle();
//print_r($router);
