<?php
namespace Outboard\BootstrapFlash;

use Phalcon\Flash\Session;

class BootstrapFlash extends Session
    {
	public function message($type, $message)
	{
		// map the right class based on the message type
		$types = array(
			'success' => 'success',
			'notice' => 'warning',
			'error' => 'danger'
		);
		$words = array(
			'success' => 'Success',
			'notice' => 'Alert',
			'error' => 'Error'
		);

		$upper_type = $words[$type];

		$type = $types[$type];

		// pretty title
		//$upper_type = strtoupper($type);


		$message= '<div ng-hide="showAlert" role="alert" class="alert alert-'.$type.' alert-dismissible fade in"><button ng-click="showAlert = true" aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">x</span></button><strong>'.$upper_type.': </strong> '.$message.'</div>';

		parent::message($type, $message);
	}
} 