<?php
namespace Outboard\Mail;

use Phalcon\Mvc\User\Component,
	Phalcon\Mvc\View;
use Swift_Message as Message;
use Swift_Mailer as Mailer;
use Swift_SmtpTransport;

/**
 *
 * Sends e-mails based on pre-defined templates
 */
class Mail extends Component
{

	protected $_transport;

	/**
	 * Applies a template to be used in the e-mail
	 *
	 * @param string $name
	 * @param array $params
	 */
	public function getTemplate($name, $params)
	{

		$parameters = array_merge(array(
			'publicUrl' => 'https://www.matchpeopleskills.com',
		), $params);

        ob_start();

        $this->view->setRenderLevel(View::LEVEL_LAYOUT);
        $this->view->setVars($parameters);
        $this->view->render('emailTemplates', $name);

        $test = ob_get_contents(); ob_end_clean();

        return $test;

	}

	/**
	 * Sends e-mails via gmail based on predefined templates
	 *
	 * @param array $to
	 * @param string $subject
	 * @param string $name
	 * @param array $params
	 */
	public function send($to, $subject, $name, $params)
	{

        $this->view->enable();

		$template = $this->getTemplate($name, $params);

        $plain = $this->getTemplate($name.'-plain', $params);

        $this->view->disable();

        $mailSEttings = array(
            'fromName' => 'Match People Skills',
            'fromEmail' => 'noreply@matchpeopleskills.com',
            'smtp' => array(
                'server' => 'smtp.mailgun.org',
                'port' => 25,
                'security' => 'tls',
                'username' => 'noreply@matchpeopleskills.com',
                'password' => 'i8vfbxdb'
            )
        );

        // Create the message
        $message = Message::newInstance()
        ->setSubject($subject)
        ->setTo($to)
        ->setFrom(array(
                $mailSEttings['fromEmail'] => $mailSEttings['fromName']
        ))
        ->setBody($template, 'text/html')
        ->addPart($plain, 'text/plain');
        if (!$this->_transport) {
                $this->_transport = Swift_SmtpTransport::newInstance(
                        $mailSEttings['smtp']['server'],
                        $mailSEttings['smtp']['port'],
                        $mailSEttings['smtp']['security']
                )
                        ->setUsername($mailSEttings['smtp']['username'])
                        ->setPassword($mailSEttings['smtp']['password']);
        }

        // Create the Mailer using your created Transport
        $mailer = Mailer::newInstance($this->_transport);

        $sender = $mailer->send($message);

        //return $sender;
    }
}
