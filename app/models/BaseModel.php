<?php

use Phalcon\Mvc\Model;

class BaseModel extends Model
{
	public $dbPrefix;

	public function onConstruct()
    {
    	$this->dbPrefix = 'b58q9dqtr_';
    	$this->setSource($this->dbPrefix.strtolower(get_class($this)));
    }

	public function initialize()
    {
    	$this->dbPrefix = 'b58q9dqtr_';
    }

    public function beforeExecuteQuery()
    {
    	$this->setSource($this->getSource());
    }

    public function getSource()
    {
        return $this->dbPrefix.strtolower(get_class($this));
    }

    public function afterSave()
    {
        $this->refresh();
    }

}