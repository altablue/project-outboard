<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\InclusionIn;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;
use Phalcon\Mvc\Model\Query;

class Candidate extends BaseModel
{

    public $client;

    public $esParams;

    public $userid;

    public $status;

    public $id;

    public $location;

    public $skills;

    public $discipline;

    public $contact;

    public $board;
    public $availability;
    public $qualifications;
    public $title;
    public $locationnice;
    public $phone;

	public function onConstruct()
    {
    	parent::initialize();

        $this->useDynamicUpdate(true);

        //$this->skipAttributes(array('qualifications', 'availability'));

    }

    public function initialize()
    {
        $this->belongsTo("userid", "Users", "id", ['alias' => 'Users']);
        //$this->hasMany("userid", "Users", "id");
        $this->hasMany("id", "Fillstats", "cand");
        $this->hasMany("id", "Candnotes", "candid");
            
        $this->useDynamicUpdate(true);
    }

    public function getSource()
    {
        return $this->dbPrefix.strtolower(get_class($this));
    }

    public function userValidation()
    {

    }

    public function addToElastic($data)
    {
        $this->client = new Elasticsearch\Client();
        $this->esParams['index'] = 'outboard';
        $this->esParams['type'] = 'candidates-test';

        $dataArray = $data;
        $this->esParams['id'] = $dataArray['id'];
        //$this->esParams['id'] = 17;
        unset($dataArray->id);
        unset($dataArray['board']);
        unset($dataArray['status']);
        $this->esParams['body']['type'] = 1;

        foreach($dataArray as $field => $param):
            $this->esParams['body'][$field] = $param;
        endforeach;

        $ret = $this->client->index($this->esParams);
        //$this->esParams['body'] = $statement;
        //$results = $this->client->search($this->esParams);
    }

    public function removeFromElastic($data)
    {
        $this->client = new Elasticsearch\Client();
        $this->esParams['index'] = 'outboard';
        $this->esParams['type'] = 'candidates-test';   

        $this->esParams['body']['query']['bool']['must'][]['match']['id'] = $data;
        $this->esParams['body']['query']['bool']['must'][]['match']['type'] = 1; 

        $ret = $this->client->deleteByQuery($this->esParams);   
    }

    public function getPlaceCords($place)
    {
        $osm = json_decode(file_get_contents('https://nominatim.openstreetmap.org/search?q='.$place.'&featuretype=city,county&format=json&bounded=1&addressdetails=1&limit=1'));
        return $osm[0]->lat.','.$osm[0]->lon;
    }
    
}
