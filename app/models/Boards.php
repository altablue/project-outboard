<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\InclusionIn;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class Boards extends BaseModel
{

    public $id;

	public $name;

    public $type;

    public $location;

	public function initialize()
    {
    	parent::initialize();
        $this->hasMany("id", "Users", "board");
        $this->hasMany("id", "Job", "board");
        $this->hasMany("id", "Abstaff", "board");
    }

    public function companyValidation()
    {

    }

    public function userValidation()
    {
        $this->validate(new EmailValidator(array(
            'field' => 'email'
        )));
        $this->validate(new UniquenessValidator(array(
            'field' => 'email',
            'message' => 'Sorry, The email was registered by another user'
        )));
        $this->validate(new UniquenessValidator(array(
            'field' => 'username',
            'message' => 'Sorry, That username is already taken'
        )));
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }

}
