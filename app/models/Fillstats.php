<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\InclusionIn;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class Fillstats extends BaseModel
{

    public $id;

	public $jobId;

    public $cand;

    public $tstatus;

	public function initialize()
    {
    	parent::initialize();
        $this->belongsTo("cand", "Candidate", "id");
        $this->belongsTo("jobId", "Job", "ID");
    }

    public function companyValidation()
    {

    }

}
