<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\InclusionIn;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class Candnotes extends BaseModel
{

    public $id;

	public $candid;

    public $note;

    public $redid;

    public $created;

	public function initialize()
    {
    	parent::initialize();
        $this->belongsTo("candid", "Candidate", "id");
        $this->belongsTo("redid", "Users", "id");
    }

    

}
