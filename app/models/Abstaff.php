<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\InclusionIn;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class Abstaff extends BaseModel
{

    public $id;

    public $user;

    public $board;

	public function onConstruct()
    {
    	parent::initialize();

    }

    public function initialize()
    {
        $this->belongsTo("user", "Users", "id");
        $this->belongsTo("board", "Boards", "id");
    }

    public function getSource()
    {
        return $this->dbPrefix.strtolower(get_class($this));
    }

    public function registerStaff()
    {

    }
    
}
