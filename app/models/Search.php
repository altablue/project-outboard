<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\InclusionIn;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class Search extends BaseModel
{

    public $client;

    public $esParams;

	public function onConstruct()
    {
    	parent::initialize();

    }

    public function searchElastic($data, $sortBy=NULL, $term=NULL)
    {

        $this->client = new Elasticsearch\Client();
        $this->esParams['index'] = 'outboard';
        $this->esParams['type'] = 'candidates-test';

        $statement = array();


        $statement['from'] = $data['from'];
        $statement['size'] = $data['limit'];

        unset($data['from']);
        unset($data['limit']);

        if(!empty($data['sortBy'])){
            $sortBy = $data['sortBy'];
            unset($data['sortBy']);
        }else{
            $sortBy = false;
        }

        $filterOut = $data['filterOut'];
        unset($data['filterOut']);

        foreach($data as $term => $query):
            if($term == 'keyword'){
                $term == '_all';
            }       
            if(!empty($query)){
                if(is_numeric($query)){
                    $query = intval($query);
                    $statement['query']['bool']['should'][]['match'][$term] = $query;
                }else{
                    if(is_string($query)){
                        if($term == 'keyword'){
                            $statement['query']['bool']['should'][]['wildcard']['_all'] = '*'.$query.'*';
                        }else if($term != 'location'){
                            $statement['query']['bool']['should'][]['match'][$term] = $query;
                        }
                    }else{
                        if($term == 'searchSkills' || $term == 'qual'){

                            if($term == 'qual'){
                                foreach($query as $item){
                                    foreach($item as $termy => $qualy):

                                        if($termy == 'degree'){
                                            $termy = 'qualificationsDegree';
                                        }else if($termy == 'fos'){
                                            $termy = 'qualificationsField';
                                        }

                                        $statement['query']['bool']['should'][]['match'][$termy] = $qualy;

                                    endforeach;
                                }
 
                            }else if($term == 'searchSkills'){
                                $term = 'skills';
                                foreach($query as $item):
                                    $statement['query']['bool']['should'][]['match'][$term] = $item;
                                endforeach;

                            }

                        }
                    }
                }
                if($term == 'location'){
                    //print_r($data);
                    if(!$this->isPostcode($data['location'])):
                        $osm = json_decode(file_get_contents('https://nominatim.openstreetmap.org/search?q='.$data['location'].'&featuretype=city,county&format=json&bounded=1&addressdetails=1&limit=1'));
                        $loc['boundBox'] = array(
                            'left' => $osm[0]->boundingbox[2],
                            'bottom' => $osm[0]->boundingbox[0],
                            'right' => $osm[0]->boundingbox[3],
                            'top' => $osm[0]->boundingbox[1]
                        );
                        $loc['address'] = $osm[0]->lat.','.$osm[0]->lon;
                        $data['filter']['location']='boundBox';
                        //print_r($data);
                    else:
                        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($data['location'])."&sensor=false";
                        $result_string = file_get_contents($url);
                        $result = json_decode($result_string, true);
                        $loc['address'] = $result['results'][0]['geometry']['location']['lat'].','.$result['results'][0]['geometry']['location']['lng'];
                        $data['filter']['location'] = 'address';
                    endif;
                    $filter['geo_distance'] = array('distance' => '1km', 'location' => $loc['address']);
                }
            }
        endforeach;

        if($sortBy){
            $statement['track_scores'] = 'true';
            switch($sortBy){
                case 'Alphabetical - A-Z':
                    $statement['sort'][]['title'] = 'asc';
                    break;
                case 'Alphabetical - Z-A':
                    $statement['sort'][]['title'] = 'desc';
                    break;
                case 'Location - Nearest':
                    $statement['sort'][]['_geo_distance'] = array('latlng' => $loc['address'],'order' => 'asc', 'unit' => 'km');
                    break;
                case 'Location - Furthest':
                    $statement['sort'][]['_geo_distance'] = array('latlng' => $loc['address'],'order' => 'desc', 'unit' => 'km');
                    break;
            }
        }

        $filter['exists']['field'] = 'id'; 

        if(!empty($filterOut)) $statement['query']['bool']['must_not'][]['match']['type'] = $filterOut;
        if(!empty($filter)) $statement['filter'] = $filter;

        //print_r(json_encode($statement));
        //die();

        $this->esParams['body'] = $statement;
        $results = $this->client->search($this->esParams);
        $results['searchTerms'] = json_encode($statement);
        return $results;
    }

    private function isPostcode($postcode)
    {
        $postcode = strtoupper(str_replace(' ','',$postcode));
        if(preg_match("/^[A-Z]{1,2}[0-9]{2,3}[A-Z]{2}$/",$postcode) || preg_match("/^[A-Z]{1,2}[0-9]{1}[A-Z]{1}[0-9]{1}[A-Z]{2}$/",$postcode) || preg_match("/^GIR0[A-Z]{2}$/",$postcode)):
            return true;
        else:
            return false;
        endif;
    }
    
}
