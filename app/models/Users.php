<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\InclusionIn;
use Phalcon\Mvc\Model\Validator\Email as EmailValidator;
use Phalcon\Mvc\Model\Validator\Uniqueness as UniquenessValidator;

class Users extends BaseModel
{

	public $id;

    public $userTempPass;

    public $userPermPass;

    public $name;

    public $board;

    public $username;

    public $created;

    public $profileCreated;

    public $type;

    public $level;

	public function onConstruct()
    {
    	parent::initialize();

    }

    public function initialize()
    {
        $this->belongsTo("board", "Boards", "id");
        $this->hasMany("id", "Candidate", "userid", ['alias' => 'Candidate']);
        $this->hasMany("id", "Abstaff", "user");
        $this->hasMany("id", "Candnotes", "redid");
        //$this->belongsTo("parts_id", "Parts", "id");
    }

    public function getSource()
    {
        return $this->dbPrefix.strtolower(get_class($this));
    }

    public function userValidation()
    {

    }
    
}
