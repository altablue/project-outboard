 <?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Forms\Element\Hidden;

class LoginForm extends Form
{
    public function initialize($entity = null, $options = null)
    {

        // API URI Controller
        $apiController = new Hidden("apiController");
        $apiController->setDefault('users');
        $this->add($apiController);

        // Email
        $email = new Text('username');
        $email->setLabel('Email Address');
        $email->setFilters('email');
        $email->setDefault($this->view->username);
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'Email Address is required'
            )),
            new Email(array(
                'message' => 'Email Address is not valid'
            ))
        ));
        $this->add($email);

        // Password
        $password = new Password('password');
        $password->setLabel('Password');
        $password->addValidators(array(
            new PresenceOf(array(
                'message' => 'Password is required'
            ))
        ));
        $this->add($password);

         // Sign Up
        $this->add(new Submit('SignUp', array(
            'class' => 'btn btn-success'
        )));

    }
}