 <?php

use Phalcon\Forms\Form;
use Phalcon\Tag;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Forms\Element\Hidden;

class UpdateCandidate extends Form
{

    public function initialize($entity = null, $options = null)
    {

        // API URI Controller
        $apiController = new Hidden("apiController");
        $apiController->setDefault('candidate');
        $this->add($apiController);

        // Board #
        $boardNum = new Hidden("boardNum");
        $this->add($boardNum);

        // Candidate Email
        /*$candEmail = new Text('candidateEmail');
        $candEmail->setLabel('Candidate Email');
        $candEmail->setFilters('email');
        $candEmail->addValidators(array(
            new PresenceOf(array(
                'message' => 'Candidate E-mail is required'
            )),
            new Email(array(
                'message' => 'Candidate E-mail is not valid'
            ))
        ));
        $this->add($candEmail);

        // Candidate Name
        $candName = new Text('candidateName');
        $candName->setLabel('Candidate Name');
        $candName->setFilters(array('striptags', 'string'));
        $candName->addValidators(array(
            new PresenceOf(array(
                'message' => 'Candidate Name is required'
            ))
        ));
        $this->add($candName);*/

        //Skills
        $array = array();
        $selected = '';
        if(!empty($this->view->skills)):
            foreach($this->view->skills as $skill){
                $array[$skill] = $skill;
            }
        endif;
        $params = array(
            'useEmpty' => true,
            'emptyText' => 'Please Select...',
            'using' => array('name'),
            'multiple' => 'multiple',
            'value' => $array
        );
        $candSkills = new Select('candSkills', $array, $params);
        $candSkills->setLabel('Your Skills');
        $this->add($candSkills);
        $this->view->skills = json_encode($array);

        //Qualys
        $candQualsDegree = new Select('candQualsDegree', array(
            'High School' => 'High School',
            'Associates Degree' => 'Associate\'s Degree',
            'Bachelors Degree' => 'Bachelor\'s Degree',
            'Masters Degree' => 'Master\'s Degree',
            'Master of Business Administration (M.B.A.)' => 'Master of Business Administration (M.B.A.)',
            'Juris Doctor (J.D.)' => 'Juris Doctor (J.D.)',
            'Doctor of Medicine (M.D.)' => 'Doctor of Medicine (M.D.)',
            'Doctor of Philosophy (Ph.D.)' => 'Doctor of Philosophy (Ph.D.)',
            'Engineers Degree' => 'Engineer\'s Degree',
            'Other' => 'Other',
        ));
        $this->view->degrees = json_encode(array(
            'High School' => 'High School',
            'Associates Degree' => 'Associates Degree',
            'Bachelors Degree' => 'Bachelors Degree',
            'Masters Degree' => 'Masters Degree',
            'Master of Business Administration' => 'Master of Business Administration',
            'Juris Doctor' => 'Juris Doctor',
            'Doctor of Medicine' => 'Doctor of Medicine',
            'Doctor of Philosophy' => 'Doctor of Philosophy',
            'Engineers Degree' => 'Engineers Degree',
            'Other' => 'Other',
        ));
        $candQualsDegree->setLabel('Degree Level');
        $candQualsDegree->setDefault($this->view->qualifications);
        $this->add($candQualsDegree);

        //Qualification Title
        $candQualTitle = new Text('candQualTitle');
        $candQualTitle->setLabel('Qualification Title');
        $candQualTitle->setFilters(array('striptags', 'string'));
        $candQualTitle->addValidators(array(
            new PresenceOf(array(
                'message' => 'Location is required'
            ))
        ));
        $this->add($candQualTitle);

        //Qualification Field
        $candQualField = new Text('candQualField');
        $candQualField->setLabel('Qualification Field');
        $candQualField->setFilters(array('striptags', 'string'));
        $candQualField->addValidators(array(
            new PresenceOf(array(
                'message' => 'Location is required'
            ))
        ));
        $this->add($candQualField);

        //Location
        $candLoc = new Text('candLoc', array(
            'ng-init' => "formData.niceName='".$this->view->locationnice."'",
            "placeholder" => "Postcode or City Name..."
        ));
        $candLoc->setLabel('Your Location');
        $candLoc->setFilters(array('striptags', 'string'));
        $candLoc->addValidators(array(
            new PresenceOf(array(
                'message' => 'Location is required'
            ))
        ));
        $candLoc->setDefault($this->view->locationnice);

        $this->add($candLoc);

        //Role Desc
        $candRoleDesc = new Text('candRoleDesc', array(
            'ng-init' => "formData.candRoleDesc='".$this->view->profile."'",
            "placeholder" => "Enter Description Here..."
        ));
        $candRoleDesc->setLabel('Current Role Description');
        $candRoleDesc->setFilters(array('striptags', 'string'));
        $candRoleDesc->addValidators(array(
            new PresenceOf(array(
                'message' => 'Location is required'
            ))
        ));
        $candRoleDesc->setDefault($this->view->profile);
        $this->add($candRoleDesc);   

        //Discipline
        $params = array(
            'useEmpty' => true,
            'emptyText' => 'Please Select...',
            'using' => array('name'),
            'value' => $this->view->discipline,
            "placeholder" => "Discipline Your Currently Employed In..."
        );
        $this->view->disciplines = json_encode($this->getDiscList());
        $this->view->rawDisc = $this->getDiscList();
        $candDisc = new Select('candDisc', $this->getDiscList(), $params);
        $candDisc->setLabel('Your Discipline');
        $this->add($candDisc);

        //Job Title
        $candTitle = new Text('candTitle', array(
            'ng-init' => "formData.candTitle='".$this->view->title."'",
            "placeholder" => "Your Current Job Title..."
        ));
        $candTitle->setLabel('Your Current Job Title');
        $candTitle->setFilters(array('striptags', 'string'));
        $candTitle->addValidators(array(
            new PresenceOf(array(
                'message' => 'Job title is required'
            ))
        ));
        $candTitle->setDefault($this->view->title);
        $this->add($candTitle);

        //Job Title
        $candPhone = new Text('candPhone', array(
            'ng-init' => "formData.candPhone='".$this->view->phone."'",
            "placeholder" => "e.g +447123456798"
        ));
        $candPhone->setLabel('Your Contact Phone Number');
        $candPhone->setFilters(array('striptags', 'string'));
        $candPhone->addValidators(array(
            new PresenceOf(array(
                'message' => 'Job Phone is required'
            ))
        ));
        $candPhone->setDefault($this->view->Phone);
        $this->add($candPhone); 

        //Availability
        $candAvail = new Text('candAvail', array(
            'ng-value' => $this->view->availability,
            'ng-init' => "formData.candAvail='".$this->view->availability."'",
            "placeholder" => "30... 10... 20..."
        ));
        $candAvail->setLabel('Your Availability');
        $candAvail->setFilters(array('striptags', 'string'));
        $candAvail->addValidators(array(
            new PresenceOf(array(
                'message' => 'Availability is required'
            ))
        ));
        $candAvail->setDefault($this->view->availability);
        $this->add($candAvail);       
    }

    protected function getDiscList()
    {
        /*return array(
            "Accounting and Finance" => "Accounting and Finance",
            "Administration" => "Administration",
            "Applications" => "Applications",
            "Document Control" => "Document Control",
            "Finance" => "Finance",
            "IT&S" => "IT&S",
            "Process" => "Process",
            "Logistics" => "Logistics",
            "Maintenance" => "Maintenance",
            "Process" => "Process",
            "Telecomms" => "Telecomms",
            "Electrical and Instrument" => "Electrical and Instrument",
            "Labourer" => "Labourer",
            "Supply Chain " => "Supply Chain ",
            "Trades Assistant" => "Trades Assistant",
            "Industrial Cleaning" => "Industrial Cleaning"
        );*/

return array(
	'Accounting and Finance' =>'Accounting and Finance',
	'Administration' =>'Administration',
	'Applications' =>'Applications',
	'Banksman Slinger' =>'Banksman Slinger',
	'Business Acquistions' =>'Business Acquistions',
	'Business Development' =>'Business Development',
	'Business Services' =>'Business Services',
	'Business Systems' =>'Business Systems',
	'Buyer' =>'Buyer',
	'CAD' =>'CAD',
	'Chemist/Lab Tech' =>'Chemist/Lab Tech',
	'Civil/Structural' =>'Civil/Structural',
	'Close Out' =>'Close Out',
	'Commercial' =>'Commercial',
	'Commissioning' =>'Commissioning',
	'Construction' =>'Construction',
	'Consulting and Technical Support' =>'Consulting and Technical Support',
	'Contracts' =>'Contracts',
	'Control Room Operator' =>'Control Room Operator',
	'Corrosion' =>'Corrosion',
	'Cost' =>'Cost',
	'Deck Crew' =>'Deck Crew',
	'Decommissioning' =>'Decommissioning',
	'Design Applications' =>'Design Applications',
	'Document Control' =>'Document Control',
	'Drilling' =>'Drilling',
	'Electrical' =>'Electrical',
	'Engineering' =>'Engineering',
	'Equipment' =>'Equipment',
	'Estimator' =>'Estimator',
	'Fabrication' =>'Fabrication',
	'Finance' =>'Finance',
	'General Assistant' =>'General Assistant',
	'General Foreman' =>'General Foreman',
	'HAZOP' =>'HAZOP',
	'HR' =>'HR',
	'HSEQ' =>'HSEQ',
	'Hook-up/Commissioning' =>'Hook-up/Commissioning',
	'IT&S' =>'IT&S',
	'Information' =>'Information',
	'Instrumentation' =>'Instrumentation',
	'Instrument Pipefitter' =>'Instrument Pipefitter',
	'Intervention' =>'Intervention',
	'Logistics' =>'Logistics',
	'Maintenance' =>'Maintenance',
	'Marine Operator' =>'Marine Operator',
	'Material' =>'Material',
	'Mechanical' =>'Mechanical',
	'Mechanical/Piping' =>'Mechanical/Piping',
	'Miscellaneous' =>'Miscellaneous',
	'O&M' =>'O&M',
	'Offshore Construction/Onshore Construction' =>'Offshore Construction/Onshore Construction',
	'Offshore Installation' =>'Offshore Installation',
	'Offshore/Trades' =>'Offshore/Trades',
	'Operations' =>'Operations',
	'PDMS' =>'PDMS',
	'Permit' =>'Permit',
	'Pipefitter' =>'Pipefitter',
	'Pipeline' =>'Pipeline',
	'Piping' =>'Piping',
	'Planner' =>'Planner',
	'Planning' =>'Planning',
	'Plater' =>'Plater',
	'Process' =>'Process',
	'Procurement and Materials' =>'Procurement and Materials',
	'Production' =>'Production',
	'Project Engineering and Management' =>'Project Engineering and Management',
	'Project Controls' =>'Project Controls',
	'Proposals' =>'Proposals',
	'Quality' =>'Quality',
	'Quantity Surveyor' =>'Quantity Surveyor',
	'Real Estate' =>'Real Estate',
	'Strategic Resourcing' =>'Strategic Resourcing',
	'Rigger' =>'Rigger',
	'Safety' =>'Safety',
	'Security' =>'Security',
	'Storeman' =>'Storeman',
	'Structural' =>'Structural',
	'Subcontracts' =>'Subcontracts',
	'Technical Safety' =>'Technical Safety',
	'Technical writer' =>'Technical writer',
	'Telecomms' =>'Telecomms',
	'Terminal' =>'Terminal',
	'Training/Competence' =>'Training/Competence',
	'Welder' =>'Welder',
	'Welding' =>'Welding',
	'Wells' =>'Wells',
	'Workpack' =>'Workpack',
	'Pumpman' =>'Pumpman',
	'Marine' =>'Marine',
	'HVAC' =>'HVAC',
	'Designer' =>'Designer',
	'Engineer' =>'Engineer',
	'Integrity' =>'Integrity',
	'Automation' =>'Automation',
	'Electrical & Instrument' =>'Electrical & Instrument',
	'Roustabout' =>'Roustabout',
	'Labourer' =>'Labourer',
	'Drill Crew' =>'Drill Crew',
	'Inspection' =>'Inspection',
	'Auditor' =>'Auditor',
	'Flow Assurance' =>'Flow Assurance',
	'Riser Systems' =>'Riser Systems',
	'Floating Systems' =>'Floating Systems',
	'Materials and Corrosion' =>'Materials and Corrosion',
	'Supply Chain ' =>'Supply Chain ',
	'Scaffolder' =>'Scaffolder',
	'Welder' =>'Welder',
	'Boilermaker' =>'Boilermaker',
	'Trades Assistant' =>'Trades Assistant',
	'Insulation' =>'Insulation',
	'Painter' =>'Painter',
	'Industrial Cleaning' =>'Industrial Cleaning',
	'Plant Operator' =>'Plant Operator',
	'Rigger/Scaffolder' =>'Rigger/Scaffolder'
);

    }
}
