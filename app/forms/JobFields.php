 <?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Forms\Element\Hidden;

class JobFields extends Form
{

    public function initialize($entity = null, $options = null)
    {

        // API URI Controller
        $apiController = new Hidden("apiController");
        $apiController->setDefault('jobs');
        $this->add($apiController);

        // Board #
        $boardNum = new Hidden("boardNum");
        $this->add($boardNum);

        // jobidate Email
        /*$jobEmail = new Text('jobidateEmail');
        $jobEmail->setLabel('jobidate Email');
        $jobEmail->setFilters('email');
        $jobEmail->addValidators(array(
            new PresenceOf(array(
                'message' => 'jobidate E-mail is required'
            )),
            new Email(array(
                'message' => 'jobidate E-mail is not valid'
            ))
        ));
        $this->add($jobEmail);

        // jobidate Name
        $jobName = new Text('jobidateName');
        $jobName->setLabel('jobidate Name');
        $jobName->setFilters(array('striptags', 'string'));
        $jobName->addValidators(array(
            new PresenceOf(array(
                'message' => 'jobidate Name is required'
            ))
        ));
        $this->add($jobName);*/

        //Skills
        $array = array();
        $selected = '';
        if(!empty($this->view->skills)):
            foreach($this->view->skills as $skill){
                $array[$skill] = $skill;
            }
        endif;
        $params = array(
            'useEmpty' => true,
            'emptyText' => 'Please Select...',
            'using' => array('name'),
            'multiple' => 'multiple',
            'value' => $array
        );
        $candSkills = new Select('jobSkills', $array, $params);
        $candSkills->setLabel('Required Skills');
        $this->add($candSkills);
        $this->view->skills = json_encode($array);

        //Qualys
        $jobQualsDegree = new Select('jobQualsDegree', array(
            'High School' => 'High School',
            'Associates Degree' => 'Associate\'s Degree',
            'Bachelors Degree' => 'Bachelor\'s Degree',
            'Masters Degree' => 'Master\'s Degree',
            'Master of Business Administration (M.B.A.)' => 'Master of Business Administration (M.B.A.)',
            'Juris Doctor (J.D.)' => 'Juris Doctor (J.D.)',
            'Doctor of Medicine (M.D.)' => 'Doctor of Medicine (M.D.)',
            'Doctor of Philosophy (Ph.D.)' => 'Doctor of Philosophy (Ph.D.)',
            'Engineers Degree' => 'Engineer\'s Degree',
            'Other' => 'Other',
        ));
        $this->view->degrees = json_encode(array(
            'High School' => 'High School',
            'Associates Degree' => 'Associates Degree',
            'Bachelors Degree' => 'Bachelors Degree',
            'Masters Degree' => 'Masters Degree',
            'Master of Business Administration' => 'Master of Business Administration',
            'Juris Doctor' => 'Juris Doctor',
            'Doctor of Medicine' => 'Doctor of Medicine',
            'Doctor of Philosophy' => 'Doctor of Philosophy',
            'Engineers Degree' => 'Engineers Degree',
            'Other' => 'Other',
        ));
        $jobQualsDegree->setLabel('Required Qualification Level');
        $this->add($jobQualsDegree);

        //Qualification Title
        $jobQualTitle = new Text('jobQualTitle');
        $jobQualTitle->setLabel('Required Qualification Title');
        $jobQualTitle->setFilters(array('striptags', 'string'));
        $jobQualTitle->addValidators(array(
            new PresenceOf(array(
                'message' => 'Location is required'
            ))
        ));
        $this->add($jobQualTitle);

        //Qualification Field
        $jobQualField = new Text('jobQualField');
        $jobQualField->setLabel('Required Qualification Field');
        $jobQualField->setFilters(array('striptags', 'string'));
        $jobQualField->addValidators(array(
            new PresenceOf(array(
                'message' => 'Location is required'
            ))
        ));
        $this->add($jobQualField);

        //Location
        $jobLoc = new Text('jobLoc', array(
            'ng-init' => "formData.niceName='".$this->view->locationnice."'",
            "placeholder" => "Postcode or City Name..."
        ));
        $jobLoc->setLabel('Job Location');
        $jobLoc->setFilters(array('striptags', 'string'));
        $jobLoc->addValidators(array(
            new PresenceOf(array(
                'message' => 'Location is required'
            ))
        ));

        $this->add($jobLoc);

        //Role Desc
        $jobRoleDesc = new Text('jobRoleDesc');
        $jobRoleDesc->setLabel('Role Description');
        $jobRoleDesc->setFilters(array('striptags', 'string'));
        $jobRoleDesc->addValidators(array(
            new PresenceOf(array(
                'message' => 'Location is required'
            ))
        ));

        $this->add($jobRoleDesc);   

        //Discipline
        $jobDisc = new Select('jobDisc', $this->getDiscList());
        $this->view->disciplines = json_encode($this->getDiscList());
        $jobDisc->setLabel('Job Discipline');
        $this->add($jobDisc); 

        //Job Title
        $jobTitle = new Text('jobTitle', array(
            'ng-init' => "formData.candTitle='".$this->view->title."'",
        ));
        $jobTitle->setLabel('Job Title');
        $jobTitle->setFilters(array('striptags', 'string'));
        $jobTitle->addValidators(array(
            new PresenceOf(array(
                'message' => 'Job title is required'
            ))
        ));

        $this->add($jobTitle); 

        //Availability
        $jobAvail = new Text('jobAvail', array(
            'ng-value' => $this->view->availability,
            'ng-init' => "formData.candAvail='".$this->view->availability."'",
            "placeholder" => "30... 10... 20..."
        ));
        $jobAvail->setLabel('Required Availability');
        $jobAvail->setFilters(array('striptags', 'string'));
        $jobAvail->addValidators(array(
            new PresenceOf(array(
                'message' => 'Availability is required'
            ))
        ));

        $this->add($jobAvail);       
    }

    protected function getDiscList()
    {
        /*return array(
            "Accounting and Finance" => "Accounting and Finance",
            "Administration" => "Administration",
            "Applications" => "Applications",
            "Document Control" => "Document Control",
            "Finance" => "Finance",
            "IT&S" => "IT&S",
            "Logistics" => "Logistics",
            "Maintenance" => "Maintenance",
            "Process" => "Process",
            "Telecomms" => "Telecomms",
            "Electrical and Instrument" => "Electrical and Instrument",
            "Labourer" => "Labourer",
            "Supply Chain " => "Supply Chain ",
            "Trades Assistant" => "Trades Assistant",
            "Industrial Cleaning" => "Industrial Cleaning"
        );*/

return array(
	'Accounting and Finance' =>'Accounting and Finance',
	'Administration' =>'Administration',
	'Applications' =>'Applications',
	'Banksman Slinger' =>'Banksman Slinger',
	'Business Acquistions' =>'Business Acquistions',
	'Business Development' =>'Business Development',
	'Business Services' =>'Business Services',
	'Business Systems' =>'Business Systems',
	'Buyer' =>'Buyer',
	'CAD' =>'CAD',
	'Chemist/Lab Tech' =>'Chemist/Lab Tech',
	'Civil/Structural' =>'Civil/Structural',
	'Close Out' =>'Close Out',
	'Commercial' =>'Commercial',
	'Commissioning' =>'Commissioning',
	'Construction' =>'Construction',
	'Consulting and Technical Support' =>'Consulting and Technical Support',
	'Contracts' =>'Contracts',
	'Control Room Operator' =>'Control Room Operator',
	'Corrosion' =>'Corrosion',
	'Cost' =>'Cost',
	'Deck Crew' =>'Deck Crew',
	'Decommissioning' =>'Decommissioning',
	'Design Applications' =>'Design Applications',
	'Document Control' =>'Document Control',
	'Drilling' =>'Drilling',
	'Electrical' =>'Electrical',
	'Engineering' =>'Engineering',
	'Equipment' =>'Equipment',
	'Estimator' =>'Estimator',
	'Fabrication' =>'Fabrication',
	'Finance' =>'Finance',
	'General Assistant' =>'General Assistant',
	'General Foreman' =>'General Foreman',
	'HAZOP' =>'HAZOP',
	'HR' =>'HR',
	'HSEQ' =>'HSEQ',
	'Hook-up/Commissioning' =>'Hook-up/Commissioning',
	'IT&S' =>'IT&S',
	'Information' =>'Information',
	'Instrumentation' =>'Instrumentation',
	'Instrument Pipefitter' =>'Instrument Pipefitter',
	'Intervention' =>'Intervention',
	'Logistics' =>'Logistics',
	'Maintenance' =>'Maintenance',
	'Marine Operator' =>'Marine Operator',
	'Material' =>'Material',
	'Mechanical' =>'Mechanical',
	'Mechanical/Piping' =>'Mechanical/Piping',
	'Miscellaneous' =>'Miscellaneous',
	'O&M' =>'O&M',
	'Offshore Construction/Onshore Construction' =>'Offshore Construction/Onshore Construction',
	'Offshore Installation' =>'Offshore Installation',
	'Offshore/Trades' =>'Offshore/Trades',
	'Operations' =>'Operations',
	'PDMS' =>'PDMS',
	'Permit' =>'Permit',
	'Pipefitter' =>'Pipefitter',
	'Pipeline' =>'Pipeline',
	'Piping' =>'Piping',
	'Planner' =>'Planner',
	'Planning' =>'Planning',
	'Plater' =>'Plater',
	'Process' =>'Process',
	'Procurement and Materials' =>'Procurement and Materials',
	'Production' =>'Production',
	'Project Engineering and Management' =>'Project Engineering and Management',
	'Project Controls' =>'Project Controls',
	'Proposals' =>'Proposals',
	'Quality' =>'Quality',
	'Quantity Surveyor' =>'Quantity Surveyor',
	'Real Estate' =>'Real Estate',
	'Strategic Resourcing' =>'Strategic Resourcing',
	'Rigger' =>'Rigger',
	'Safety' =>'Safety',
	'Security' =>'Security',
	'Storeman' =>'Storeman',
	'Structural' =>'Structural',
	'Subcontracts' =>'Subcontracts',
	'Technical Safety' =>'Technical Safety',
	'Technical writer' =>'Technical writer',
	'Telecomms' =>'Telecomms',
	'Terminal' =>'Terminal',
	'Training/Competence' =>'Training/Competence',
	'Welder' =>'Welder',
	'Welding' =>'Welding',
	'Wells' =>'Wells',
	'Workpack' =>'Workpack',
	'Pumpman' =>'Pumpman',
	'Marine' =>'Marine',
	'HVAC' =>'HVAC',
	'Designer' =>'Designer',
	'Engineer' =>'Engineer',
	'Integrity' =>'Integrity',
	'Automation' =>'Automation',
	'Electrical & Instrument' =>'Electrical & Instrument',
	'Roustabout' =>'Roustabout',
	'Labourer' =>'Labourer',
	'Drill Crew' =>'Drill Crew',
	'Inspection' =>'Inspection',
	'Auditor' =>'Auditor',
	'Flow Assurance' =>'Flow Assurance',
	'Riser Systems' =>'Riser Systems',
	'Floating Systems' =>'Floating Systems',
	'Materials and Corrosion' =>'Materials and Corrosion',
	'Supply Chain ' =>'Supply Chain ',
	'Scaffolder' =>'Scaffolder',
	'Welder' =>'Welder',
	'Boilermaker' =>'Boilermaker',
	'Trades Assistant' =>'Trades Assistant',
	'Insulation' =>'Insulation',
	'Painter' =>'Painter',
	'Industrial Cleaning' =>'Industrial Cleaning',
	'Plant Operator' =>'Plant Operator',
	'Rigger/Scaffolder' =>'Rigger/Scaffolder'
);

    }
}
