 <?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Forms\Element\Hidden;

class RegisterForm extends Form
{

    public function initialize($entity = null, $options = null)
    {

        // API URI Controller
        $apiController = new Hidden("apiController");
        $apiController->setDefault('users');
        $this->add($apiController);

        // API URI Action
        $apiAction = new Hidden("apiAction");
        //$apiAction->setDefault('create');
        $this->add($apiAction);

        // Company Name
        $compname = new Text('company_name');
        $compname->setLabel('Company Name');
        $compname->setFilters(array('striptags', 'string'));
        $compname->addValidators(array(
            new PresenceOf(array(
                'message' => 'Company name is required'
            ))
        ));
        $this->add($compname);

        // Location
        $loc = new Text('location');
        $loc->setLabel('Location');
        $loc->setFilters(array('striptags', 'string'));
        $loc->addValidators(array(
            new PresenceOf(array(
                'message' => 'Please enter company location'
            ))
        ));
        $this->add($loc);

        // Type
        $type = new Select('company_type', array(
            '' => 'Please Select a Company Type...',
            '1' => 'Onboard Company',
            '2' => 'Outboard Company',
            '3' => 'Outboard and Onboard Company'
        ));
        $type->setLabel('Company Type');
        $this->add($type);

    }
}