 <?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Forms\Element\Hidden;

class AddCompanyUsers extends Form
{

    public function initialize($entity = null, $options = null)
    {

        // API URI Controller
        $apiController = new Hidden("apiController");
        $apiController->setDefault('company');
        $this->add($apiController);

        // API URI Action
        $apiAction = new Hidden("apiAction");
        //$apiAction->setDefault('create');
        $this->add($apiAction);

        // Board #
        $boardNum = new Hidden("boardNum");
        $this->add($boardNum);

        // Company User Email
        $userEmail = new Text('compUserEmail');
        $userEmail->setLabel('Company User Email');
        $userEmail->setFilters('email');
        $userEmail->addValidators(array(
            new PresenceOf(array(
                'message' => 'Company User E-mail is required'
            )),
            new Email(array(
                'message' => 'Company User E-mail is not valid'
            ))
        ));
        $this->add($userEmail);

        // Company User Name
        $userName = new Text('compUserName');
        $userName->setLabel('Company User Name');
        $userName->setFilters(array('striptags', 'string'));
        $userName->addValidators(array(
            new PresenceOf(array(
                'message' => 'Company User Name is required'
            ))
        ));
        $this->add($userName);

        // Company User Level
        $type = new Select('userLevel', array(
            '' => 'Please Select a user level for this user...',
            '2' => 'User (Manages Candiates)',
            '3' => 'Admin (Manages Users & Candidates)'

        ));
        $type->setLabel('User Level');
        $this->add($type);
    }
}