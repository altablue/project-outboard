 <?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Forms\Element\Hidden;

class AddRecruiter extends Form
{

    public function initialize($entity = null, $options = null)
    {

        // API URI Controller
        $apiController = new Hidden("apiController");
        $apiController->setDefault('company');
        $this->add($apiController);

        // API URI Action
        $apiAction = new Hidden("apiAction");
        //$apiAction->setDefault('create');
        $this->add($apiAction);

        // Board #
        $boardNum = new Hidden("boardNum");
        $this->add($boardNum);

        // Recruiter Email
        $userEmail = new Text('compUserEmail');
        $userEmail->setLabel('Recruiter Email');
        $userEmail->setFilters('email');
        $userEmail->addValidators(array(
            new PresenceOf(array(
                'message' => 'Recruiter E-mail is required'
            )),
            new Email(array(
                'message' => 'Recruiter E-mail is not valid'
            ))
        ));
        $this->add($userEmail);

        // Recruiter Name
        $userName = new Text('compUserName');
        $userName->setLabel('Recruiter Name');
        $userName->setFilters(array('striptags', 'string'));
        $userName->addValidators(array(
            new PresenceOf(array(
                'message' => 'Recruiter Name is required'
            ))
        ));
        $this->add($userName);

    }
}