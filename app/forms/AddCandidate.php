 <?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Forms\Element\Hidden;

class AddCandidate extends Form
{

    public function initialize($entity = null, $options = null)
    {

        // API URI Controller
        $apiController = new Hidden("apiController");
        $apiController->setDefault('candidate');
        $this->add($apiController);

        // Board #
        $boardNum = new Hidden("boardNum");
        $this->add($boardNum);

        // Candidate Email
        $candEmail = new Text('candidateEmail');
        $candEmail->setLabel('Candidate Email');
        $candEmail->setFilters('email');
        $candEmail->addValidators(array(
            new PresenceOf(array(
                'message' => 'Candidate E-mail is required'
            )),
            new Email(array(
                'message' => 'Candidate E-mail is not valid'
            ))
        ));
        $this->add($candEmail);

        // Candidate Name
        $candName = new Text('candidateName');
        $candName->setLabel('Candidate Name');
        $candName->setFilters(array('striptags', 'string'));
        $candName->addValidators(array(
            new PresenceOf(array(
                'message' => 'Candidate Name is required'
            ))
        ));
        $this->add($candName);
    }
}