<?php

class ErrorsController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Oops!');
        parent::initialize();
    }

    public function show404Action()
    {
        echo "<h1>404 - not found</h1>";
    }

    public function show401Action()
    {
        echo "<h1>401 - no access allowed</h1>";
    }

    public function show500Action()
    {
        echo "<h1>500 - access error</h1>";
    }
}
