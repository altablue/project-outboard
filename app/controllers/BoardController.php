<?php

class BoardController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Board');
        parent::initialize();

        $user = $this->session->get('auth');

        $users = new Users;

        $boardId = $this->dispatcher->getParams('id');

        if($user['userLevel'] < 99){

            $boardCheck = $users->findFirst(array(
                "id = :userId:",
                'bind' => array("userId" => $user['id'])
            ));

            if($boardCheck->boards->id != $boardId['id']){
                $test = $this->flash->outputMessage('error', $message);ob_get_contents(); ob_end_clean();
                $this->flash->error('Could not verify company info');
                $this->forward('dashboard/index');
            }
        }

        $this->view->id = $user['id'];

        $board = new Boards;

        $bName = $board->findFirst(array(
            "id = :boardId:", 'bind' => array("boardId" => $boardId['id'])
        ));

        $this->view->bName = $bName->name;
    }

    public function indexAction()
    {

    }

    public function candidatesAction()
    {

        $boardId = $this->dispatcher->getParams('id');

        $candidates = new Candidate;

        $cands = $candidates->find(array(
            "board = :boardNum:",
            'bind' => array("boardNum" => $boardId['id'])
        ));

        $this->view->candidates = $cands;

        $this->view->boardNum = $boardId['id'];

    }

    public function jobsAction()
    {

        $boardId = $this->dispatcher->getParams('id');

        $jobs = new Job;

        $jbs = $jobs->find(array(
            "board = :boardNum:",
            'bind' => array("boardNum" => $boardId['id'])
        ));

        $this->view->jobs = $jbs;

        $this->view->boardNum = $boardId['id'];
    }

    public function usersAction()
    {
        $boardId = $this->dispatcher->getParams('id');

        $users = new Users;

        $user = $users->find(array(
            "board = :boardNum:",
            'bind' => array("boardNum" => $boardId['id'])
        ));

        $this->view->userList = $user;

        $this->view->boardNum = $boardId['id'];      
    }

    public function uncompleteAction()
    {
        $users = new Users;

        $data = $users->find(array(
            "conditions" => "board = ".$this->auth['boardNum']." AND type = 1 AND profileCreated = 1", "order" => 'created ASC'
        ));

        if ($data!= false) {
            $this->view->incompleteUsers = $data;
            $this->view->date = time();
        }else{
            $this->view->incompleteUsers = false;
            $this->view->date = '';
        }

    }

}
