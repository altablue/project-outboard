<?php

class CandidatesController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Candidate');
        parent::initialize();

        $auth = $this->session->get('auth');

        $params = $this->dispatcher->getParams();

        foreach($auth as $seshKey => $val){
            $this->view->$seshKey = $val;
        }
        if($auth['userLevel'] < 99){
            if($auth['type'] != 2){
                if($auth['candidateId'] != $params['id'] && $auth['candidateId'] != 0){
                    return $this->forward('error/401');
                }
            }
        }

        if (!defined('HTMLFileLoaded')) {
            require_once APP_PATH.'app/library/Htmlpure/HTMLPurifier.auto.php';

            define('HTMLFileLoaded', 1);

            $this->htmlPConfig = HTMLPurifier_Config::createDefault();

            $this->htmlPConfig->set('HTML.AllowedElements', array('span', 'strong', 'em', 'ol', 'i', 'li', 'ul', 'h1', 'h2', 'h3', 'h4', 'h5', 'hr', 'table', 'thead','tbody','tfoot','tr','td', 'p'));

            $this->htmlPurifier = new HTMLPurifier($this->htmlPConfig);
        }

    }

    public function indexAction()
    {

    }

    public function createAction()
    {
        $form = new AddCandidate;

        $user = $this->session->get('auth');

        $this->view->boardNum = $user['boardNum'];

        $this->view->form = $form;
    }

    public function createProfileAction()
    {
        $this->loadCSS();
        $this->loadJS();

        $auth = $this->session->get('auth');


        $this->view->boardNum = $auth['boardNum'];

        $this->view->userid = $auth['id'];

        $this->view->candId = 0;

        $this->view->location = false;

        $this->view->profile = false;

        $this->view->candLoc = false;

        $this->view->qCount = 0;

        $this->view->discipline = '';

        $this->view->qualifications = false;

        foreach($auth as $seshKey => $val){
            $this->view->$seshKey = $val;
        }

        $form = new UpdateCandidate;

        $this->view->form = $form;

        $this->view->partial('candidates/update');
    }

    public function updateAction()
    {
        $this->loadCSS();
        $this->loadJS();

        $params = $this->dispatcher->getParams();

        if(!$params || $params['id'] == 0):

            $this->flash->notice('need to create candidate profile. If you have already done this, contact system admin');
            return $this->forward('candidates/createProfile');

        endif;

        $auth = $this->session->get('auth');

        $this->view->boardNum = $auth['boardNum'];

        $this->view->userid = $auth['id'];

        foreach($auth as $seshKey => $val){
            $this->view->$seshKey = $val;
        }

        $cacheData = array();

        //$this->cache->delete('candData-'.$this->auth['candidateId']);

        $candDataCache = $this->cache->get('candData-'.$this->auth['candidateId']);

        if($candDataCache === null){

            $candidate = new Candidate();

            $candidData = $candidate->findFirst(array(
                "id = :id: ",
                'bind' => array('id' => $params['id'])
            ));

            if ($candidData!= false) {
                $cacheData['candId'] = $this->view->candId = $candidData->id;
                foreach($candidData as $key => $profileField):
                    if(@unserialize($profileField)){
                        $cacheData[$key] = $this->view->$key = unserialize($profileField);
                    }else{
                        $cacheData[$key] = $this->view->$key = $profileField;
                    }
                endforeach;
            }else{
                die('not found');
            }

            $clean_html = $this->htmlPurifier->purify($this->view->profile);

            $cacheData['profile'] = $this->view->profile = str_replace("????"," ",nl2br(utf8_decode($this->cleanname($clean_html))));

            if(!empty($this->view->locationnice)){
                $cacheData['candLoc'] = $this->view->candLoc = $this->view->location;
                $cacheData['location'] = $this->view->location = $this->view->locationnice;
            }

            $cacheData['qCount'] = $this->view->qCount = count($this->view->qualifications);
            $cacheData['name'] = $this->auth['name'];
            $cacheData['board'] = $this->auth['boardNum'];
            $cacheData['boardName'] = $this->auth['boardName'];

            $this->cache->save('candData-'.$this->auth['candidateId'], $cacheData);

        }else{
            foreach($candDataCache as $key => $data){
                $this->view->$key = $data;
            }
        }

        $form = new UpdateCandidate;

        $this->view->form = $form;
    }

    public function getAction()
    {
        $candId = $this->dispatcher->getParams('id');

        if(!$candId):

            $this->flash->error('No candidate Id provided');
            return $this->forward('404');

        endif;

        $auth = $this->session->get('auth');

        $this->view->boardNum = $auth['boardNum'];


        foreach($auth as $seshKey => $val){
            $this->view->$seshKey = $val;
        }

        $candDataCache = $this->cache->get('candData-'.$candId['id']);

        if($candDataCache === null){

            $candidate = new Candidate();

            $candidData = $candidate->findFirst(array(
                "id = :id: ",
                'bind' => array('id' => $candId['id'])
            ));

            if($candidData->status >= 2){
                $this->flash->notice('This candidate is no longer available to view');
                return $this->response->redirect('dashboard'); 
            }

            if ($candidData!= false) {
                $this->view->candId = $candidData->id;
            }else{
                $this->flash->error('We could not find that candidate.');
                return $this->forward('404');
            }
            
            foreach($candidData as $key => $data):
                if(@unserialize($data)){
                    $this->view->$key = unserialize($data);
                }else{
                    $this->view->$key = $data;
                }
            endforeach;

            $this->view->profile= nl2br(str_replace('\n', '<br>', $this->view->profile));

            $clean_html = $this->htmlPurifier->purify($this->view->profile);

            $this->view->profile = str_replace("????"," ",nl2br(utf8_decode($this->cleanname($clean_html))));

        }else{
            foreach($candDataCache as $key => $data){
                $this->view->$key = $data;
            }
            $candidData = new stdClass;
            $candBoard = new stdClass;
            $candidData->users = new stdClass;
            $candidData->board = $candDataCache['board'];
            $candidData->name = $candDataCache['name'];
            $candidData->users->name = $candDataCache['name'];
            $candBoard->name = $candDataCache['boardName'];
            $candidData->id = $candDataCache['id'];
        }

        $boards = new Boards();

        $candBoard = $boards->findFirst(array(
            "id = :id: ",
            "bind" => array("id" => $candidData->board)
        ));

        $this->view->candComp = str_replace("'", "",$candBoard->name);

        if($auth['userLevel'] >= 99){
            $this->view->candName = $candidData->users->name;
            $this->view->email = $candidData->users->username;
            $this->view->phone = $candidData->phone;

            $totJobs = array();

            $job = new Job;

            $jobs = $job->find('status = 1');

            if($jobs != false){
                foreach($jobs as $j){
                    $totJobs[] = array('id' => $j->ID, 'title'=>$j->title, 'comp'=>str_replace("'", "",$j->boards->name));
                }
            }

            $this->view->jobs = $totJobs;

            $note = new Candnotes;

            $notes = $note->find(array( "conditions" => 'candid = '.$candidData->id, "order" => "created DESC"));

            if($notes != false){
                $array = array();
                foreach($notes as $candNote){
                    $array[] = array(
                        'created' => date("d/m/Y H:i:s", strtotime($candNote->created)),
                        'redid' => $candNote->users->name,
                        'note' => $candNote->note
                    );
                }
                $this->view->notes = $array;
            }else{
                $this->view->notes = false;
            }

            $this->view->recId = $this->auth['id'];
            $this->view->recName = $this->auth['name'];

        }else{
            $this->view->recId= 0;
            $this->view->recName = '';
        }

        $event = array('candidate-id' => $candidData->id, 'viewedBy' => $this->auth['boardNum'], 'candBoard' => $candidData->board);

        $this->getDI()->getKeenio()->addEvent('candidates-view', $event);

    }

    public function getJobsAction()
    {
        $candId = $this->dispatcher->getParams('id');

        if(!$candId):

            $this->flash->error('No candidate Id provided');
            return $this->forward('404');

        endif;

        $auth = $this->session->get('auth');

        $this->view->boardNum = $auth['boardNum'];

        $fillStats = new Fillstats;

        $jobs = $fillStats->find(array(
            "cand = :candId:",
            'bind' => array('candId' => $candId['id'])
        ));

        if ($jobs!= false) {
            $this->view->jobs = $jobs;
        }else{
            $this->flash->success('We could not find that job.');
            return $this->forward('404');
        }

        $this->view->candId = $candId['id'];
    }

    protected function cleanname($thename){
        $patternCounter=0;

        /*
        $patterns[$patternCounter] = '/[\x21-\x2d]/u'; // remove range of shifted characters on keyboard - !"#$%&'()*+,-
        $patternCounter++;

        $patterns[$patternCounter] = '/[\x5b-\x60]/u'; // remove range including brackets - []\^_`
        $patternCounter++;
        */

        $patterns[$patternCounter] = '/[\x7b-\xff]/u'; // remove all characters above the letter z.  This will eliminate some non-English language letters
        $patternCounter++;

        $replacement ="";

             return preg_replace($patterns, $replacement, $thename);
    }   

    protected function loadJS()
    {
        $this->assets->addJs("js/libs/geoPosition.js");
        $this->assets->addJs("js/libs/geoPositionSimulator.js");
        $this->assets->addJs("js/libs/ckeditor/ckeditor.js");
    }


    protected function loadCSS()
    {
        $this->assets->addCss("js/libs/angular/angular-ui-select/dist/select.css");
        $this->assets->addCss("css/select2.min.css");
        $this->assets->addCss("css/select2.Bootstrap.css");
        $this->assets->addCss("js/libs/ckeditor/skins/moono/editor.css");
        $this->assets->addCss("js/libs/ckeditor/skins/moono/editor_ie.css");
        $this->assets->addCss("js/libs/ckeditor/skins/moono/editor_ie8.css");
    }

}
