<?php

class SearchController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Search Results');
        parent::initialize();


        $auth = $this->session->get('auth');

        foreach($auth as $seshKey => $val){
            $this->view->$seshKey = $val;
        }

    }

    public function indexAction()
    {

    }

    public function resultsAction()
    {

        $this->loadCSS();
        $this->loadJS();

        $auth = $this->session->get('auth');

        $this->view->userType = $auth['type'];

        $this->view->auth = $auth;

        $queryArray = new stdClass();

        //print_r(http_build_query(array('qual' => array(0 => array('fos' => 'test', 'degree' => 'a degree'), 1 => array('fos' => 'the field', 'degree' => 'masters')))));

        $queryArray->keyword = '';
        $queryArray->discipline = '';
        $queryArray->location = '';
        $queryArray->skills = '';
        $queryArray->availability = '';
        $queryArray->qual = '';
        //$queryArray->fos = '';
        //$queryArray->degree = '';

        $this->view->curLimit = 0;
        $this->view->total = 0;

        $searchParams = $this->dispatcher->getParams();

        foreach($auth as $seshKey => $val){
            $this->view->$seshKey = $val;
        }

        if($this->session->has('jobSearchQuery')){
            $this->view->searchQueries = $this->session->get('jobSearchQuery');
        }else{  
            foreach($queryArray as $var => $data){
                if($var =='skills'){
                    $queryArray->$var = $this->request->getQuery($var);
                /*}else if($var == 'fos' || $var == 'degree'){
                    $queryArray->$qual = $this->request->getQuery($var);  */
                }else{
                    $queryArray->$var = $this->request->getQuery($var);
                }
            }
        }

        $this->view->searchQueries = $queryArray;

        $this->view->searchQueries->skills = json_encode($queryArray->skills); 

        $this->view->searchQueries->qual = json_encode($queryArray->qual); 

        $form = new UpdateCandidate;

        $this->view->form = $form;

    }

    protected function loadJS()
    {
        $this->assets->addJs("js/libs/geoPosition.js");
        $this->assets->addJs("js/libs/geoPositionSimulator.js");
        $this->assets->addJs("js/libs/queryString.js");
    }


    protected function loadCSS()
    {
        $this->assets->addCss("js/libs/angular/angular-ui-select/dist/select.css");
        $this->assets->addCss("css/select2.min.css");
        $this->assets->addCss("css/select2.Bootstrap.css");
    }

}