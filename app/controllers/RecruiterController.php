<?php

use Phalcon\Mvc\Controller;

class RecruiterController extends ControllerBase
{

    public function initialize()
    {
        $this->tag->setTitle('Dashboard | Recruiters');
        parent::initialize();

        $auth = $this->session->get('auth');

        foreach($auth as $seshKey => $val){
            $this->view->$seshKey = $val;
        }

    }

    public function indexAction()
    {

    }

    public function getCompaniesAction()
    {

        $user = $this->session->get('auth');

        $staff = new Abstaff;

        $results = $staff->find(array(
            "user = ".$user['id'],
        ));

        $comps = new stdClass;

        $i=0;
        foreach ($results as $comp) {
            if(!empty($comp->boards->type)):
                $comps->{$i} = new stdClass;
                if(!empty($comp->boards->name)):$comps->{$i}->name = $comp->boards->name;endif;
                if(!empty($comp->boards->id)):$comps->{$i}->id = $comp->boards->id;endif;
                if(!empty($comp->boards->location)):$comps->{$i}->location = $comp->boards->location;endif;
                if(!empty($comp->boards->type)):
                    switch($comp->boards->type){
                        case 1:
                            $comps->{$i}->type = 'Onboarding';
                            break;

                        case 2:
                            $comps->{$i}->type = 'Outboarding';
                            break;

                        case 3:
                            $comps->{$i}->type = 'Onboarding &amp; Outboarding';
                            break;
                    }
                endif;
                $i++;
            endif;
        }

        $this->view->companies = $comps;

    }

}
