<?php

use Phalcon\Mvc\Controller;

class IndexController extends ControllerBase
{

    public function initialize()
    {
        $this->tag->setTitle(' Allowing companies to transfer their people expertise to, or share them with organisations that would benefit from these skills.');
        
        parent::initialize();

        $this->view->logged = false;
    }

	public function indexAction()
	{
		$this->assets->addJs("js/libs/anyStretch.min.js");
        $this->assets->addJs("js/libs/bootstrap/modal.js");
        $this->assets->addJs("js/libs/jquery.placeholder.min.js");
	}

    public function termsAction()
    {
        $this->assets->addJs("js/libs/anyStretch.min.js");
        $this->assets->addJs("js/libs/bootstrap/modal.js");
        $this->assets->addJs("js/libs/jquery.placeholder.min.js");
    }

    public function cookiesAction()
    {
        $this->assets->addJs("js/libs/anyStretch.min.js");
        $this->assets->addJs("js/libs/bootstrap/modal.js");
        $this->assets->addJs("js/libs/jquery.placeholder.min.js");
    }

    public function privacyAction()
    {
        $this->assets->addJs("js/libs/anyStretch.min.js");
        $this->assets->addJs("js/libs/bootstrap/modal.js");
        $this->assets->addJs("js/libs/jquery.placeholder.min.js");
    }
    
    public function sitemapAction()
    {
        $this->view->disable();
        $xml = new DOMDocument();
        $xml->load('/var/www/mps/sitemap.xml');
        header('Content-type: text/xml');
        echo $xml->saveXML();
    }

}
