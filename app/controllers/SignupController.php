<?php

use Phalcon\Mvc\Controller;

class SignupController extends ControllerBase
{

    public function initialize()
    {
        $this->tag->setTitle('Sign Up/Sign In');
        parent::initialize();
    }

    public function registerUserAction()
    {
        $users = new Users();

        $users->userNum = $this->dispatcher->getParam("userId");
        $users->userTempPass = urldecode(base64_decode($this->dispatcher->getParam("pass")));

        $this->view->candidateReg = false;


        if(!empty($_GET['candidate'])){
            if($_GET['candidate'] == true){
                $this->view->candidateReg = true;
            }
        }

        $user = $users->findFirst(array(
            "id = :id: AND password = :password:",
            'bind' => array('id' => $users->userNum, 'password' => $users->userTempPass)
        ));
		//$user = $users->findFirst("id = ".$users->userNum);
        if ($user != false) {
            $this->view->name = $user->name;
            $this->view->username = $user->username;
            $this->view->userId = $user->id;
        }else{
        	$this->flash->error("Cannot find user. Please contact the system admin.");
            $this->response->redirect('login/index');  
        }

        $form = new UserRegSetup;

        $this->view->form = $form;
    }

}
