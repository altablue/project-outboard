<?php

use Phalcon\Mvc\Controller;
use Phalcon\Assets\Manager;
use Phalcon\Http\Response\Cookies;
use Browser\Browser;

use Phalcon\Cache\Frontend\Data as DataFrontend,
    Phalcon\Cache\Frontend\Output as OutputFrontend,
    Phalcon\Cache\Multiple,
    Phalcon\Cache\Backend\Apc as ApcCache,
    Phalcon\Cache\Backend\Memcache as MemcacheCache,
    Phalcon\Cache\Backend\File as FileCache;

class ControllerBase extends Controller
{

    public $auth;

    public $cache;

    protected function initialize()
    {

        $this->siteUrl = $this->publicURL;

        $this->view->candidateId = false;
        
        $this->view->logged = false;
        $this->persistent->remove('acl');
        $this->tag->prependTitle('Match People Skills | ');
        $this->view->setTemplateAfter('main');

        $this->view->currentURI =  $this->router->getRewriteUri();

        if(strpos($this->view->currentURI,'logout') !== true){
            $this->auth = $this->session->get('auth');
        }else{
            $this->auth = false;
        }

        if($this->auth){
            $this->view->logged = true;
            $this->view->type = $this->auth['type'];
            $this->view->boardType = $this->auth['boardType'];
        }else{
            if(strpos($this->view->currentURI,'dashboard') !== false){
                if(!empty($_COOKIE['signedInHash']) && !empty($_COOKIE['userId'])){

                    $hash = $_COOKIE['signedInHash'];
                    $id = $_COOKIE['userId'];

                    $handle = fopen("/tmp/".$hash, 'r');
                    $data = fread($handle,filesize("/tmp/".$hash));

                    if($data === $id){
                        $users = new Users();

                        $user = $users->findFirst(array(
                            "id = :id:",
                            'bind' => array('id' => $id)
                        ));

                        if ($user != false) {
                            $this->_registerSession($user, false);
                            $this->flash->success("You have logged in to Match People Skills successfully");
                            $this->response->redirect('dashboard');  
                        }
                    }else{
                        $this->flash->error('Your login session has expired. Please login again to continue using the website.');
                        $this->response->redirect('login/index');       
                    }

                }else{
                    $this->flash->error('Your login session has expired. Please login again to continue using the website.');
                    $this->response->redirect('login/index');   
                }

            }else{
            }
        }

        if($this->auth['type'] == 1){
            $candidate = new Candidate();

            $candidData = $candidate->findFirst(array(
                "userid = :id: ",
                'bind' => array('id' => $this->auth['id'])
            ));

            $this->view->candId = false;

            if ($candidData!= false) {
                $this->view->candidateId = $candidData->id;
            }
        }else{
            $user = new Users();

            $userIdData = $user->findFirst(array(
                "id = :id: ",
                'bind' => array('id' => $this->auth['id'])
            ));

            $this->view->boardNum = false;

            if ($userIdData!= false) {
                $this->view->boardNum = $userIdData->boards->id;
            }    
        }
        
        $this->loadJs();
        $this->loadCSS();

        $auth = $this->session->get('auth');

        if(!empty($auth)):

            foreach($auth as $seshKey => $val){
                $this->view->$seshKey = $val;
            }

        endif;

        $ultraFastFrontend = new DataFrontend(array(
            "lifetime" => 3600
        ));

        $fastFrontend = new DataFrontend(array(
            "lifetime" => 14400
        ));

        $slowFrontend = new DataFrontend(array(
            "lifetime" => 86400
        ));

        $this->cache = new Multiple(array(
            new ApcCache($ultraFastFrontend, array(
                "prefix" => 'cache',
            )),
            new MemcacheCache($fastFrontend, array(
                "prefix" => 'cache'
            )),
            new FileCache($slowFrontend, array(
                "prefix" => 'cache',
                "cacheDir" => "/tmp/"
            ))
        ));

        //$this->session->remove('browser');

        $this->view->browser = $this->session->get('browser');

        if(empty($this->view->browser)){
            $browser = new Browser;
            $this->view->browser = array('browser' => $browser->getName(), 'version' => $browser->getVersion());

            $this->session->set('browser', $this->view->browser);
        }

    }

    protected function forward($uri)
    {
        $uriParts = explode('/', $uri);
        $params = array_slice($uriParts, 2);
    	return $this->dispatcher->forward(
    		array(
    			'controller' => $uriParts[0],
    			'action' => $uriParts[1],
                'params' => $params
    		)
    	);
    }

    protected function getJSAssets($dir)
    {
        $result = array();

        try {
            $iterator = new RecursiveDirectoryIterator($dir);
            foreach (new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST) as $file) {                    
                if ($file->isFile() && $file->getFilename() != '.DS_Store') {
                    $path = explode('/', $file->getPath());
                    $result[] =  ''.$path[1].'/'.$file->getFilename();
                }
            }
        } catch (RuntimeException $e) {
        }

        usort($result,array($this,'interactionSort'));

        return $result;
    }

    protected function interactionSort($a,$b){
        return strlen($a)-strlen($b);
    }


    protected function loadJS()
    {
        $this->assets->addJs("js/libs/geoPosition.js");
        $this->assets->addJs("js/libs/geoPositionSimulator.js");
    }


    protected function loadCSS()
    {
        $this->assets->addCss("js/libs/angular/angular-ui-select/dist/select.css");
        $this->assets->addCss("js/libs/angular/ladda/dist/ladda-themeless.min.css");
        $this->assets->addCss("js/libs/angular/ladda/dist/ladda.min.css");
        $this->assets->addCss("js/libs/angular/footable/css/footable.core.min.css");
        $this->assets->addCss("css/select2.min.css");
        $this->assets->addCss("css/select2.Bootstrap.css");
    }

    private function _registerSession(Users $user, $cookie = null)
    {

        $authArray = array(
            'id' => $user->id,
            'name' => $user->name,
            'boardNum' => $user->board,
            'type' => $user->type,
            'boardType' => $user->boards->type,
            'boardName' => $user->boards->name,
            'userLevel' => $user->level
        );

        if($user->type == 1){
            if($user->type == 1){
                $candidate = new Candidate();

                $candidData = $candidate->findFirst(array(
                    "userid = :id: ",
                    'bind' => array('id' => $user->id)
                ));

                if ($candidData!= false) {
                     $authArray['candidateId'] = $candidData->id;
                }else{
                    $authArray['candidateId'] = 0;
                }
            }

        }

        $this->session->set('auth', $authArray);

        setcookie('apiPublicKey',  $this->apiPublicKey, time()+3600, '/', $this->siteUrl,false,  false);

        if($cookie){
            setcookie('userId',  $user->id, time()+86400*14, '/', $this->siteUrl, false, false);
            setcookie('signedInHash', hash_hmac('sha512', $user->username.$this->cookieHash1, $this->cookieHash2), time()+86400*14, '/', $this->siteUrl,false, false); 
            $file = fopen("/tmp/".hash_hmac('sha512', $user->username.$this->cookieHash1, $this->cookieHash2), 'w');
            fwrite($file, $user->id);
            fclose($file);
        }

        //setcookie('apiPublicKey', $thiublicKey, time(s->api)+3600); 
        //$this->cookies->set('apiPublicKey', $this->apiPublicKey, time()+3600 * 1000);
    }
}