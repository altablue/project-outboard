<?php

use Phalcon\Mvc\Controller;
use Phalcon\Filter;
use Phalcon\Http\Request;
use Phalcon\DI\InjectionAwareInterface;
use Phalcon\Mvc\Model\Query;

class UsersController extends ControllerBase
{

    public function initialize()
    {
        $this->tag->setTitle('Dashboard');
        
        parent::initialize();

        $auth = $this->session->get('auth');

        foreach($auth as $seshKey => $val){
            $this->view->$seshKey = $val;
        }

    }

    public function indexAction()
    {
        $auth = $this->session->get('auth');

        foreach($auth as $seshKey => $val){
            $this->view->$seshKey = $val;
        }

        if($auth['type'] == 1){
            $candidate = new Candidate();

            $candidData = $candidate->findFirst(array(
                "userid = :id: ",
                'bind' => array('id' => $this->view->id)
            ));

            $this->view->candId = false;

            if ($candidData!= false) {
                $this->view->candidateId = $candidData->id;
            }
        }

        switch($auth['type']){

            case 1:
                $this->view->data = $this->_getCandDashboardData();
                break;

            case 2:
                $this->view->data = $this->_getBoardDashboardData($auth['boardType']);
                break;

            case 3:
                $this->view->data = $this->_getRecruiterDashboardData();
                break;
        }
    }

    private function _getBoardDashboardData($type)
    {
        switch($type){

            case 1:

                $dashData = array();

                $dayJobsCache = $this->cache->get('onBoardDayJobs-'.$this->auth['id']);
                $keenCache = $this->cache->get('onBoardKeenIO-'.$this->auth['id']);
                $onBoardIdss = $this->cache->get('onBoardIdss-'.$this->auth['id']);

                if($keenCache === null){

                    $filters = [ [ 'property_name' => 'board', 'operator' => 'eq', 'property_value' => $this->auth['boardNum'] ] ];

                    $return = $this->getDI()->getKeenio()->count('jobs-view', ['filters' => $filters, 'target_property' => 'job-id', 'group_by' => 'job-id']);

                    $views = $return['result'];

                    $this->cache->save('onBoardKeenIO-'.$this->auth['id'], $views);

                }

                $jobs = new Job;

                if($dayJobsCache === null){

                    $dayViews = array();

                    $query = $this->modelsManager->createQuery("SELECT  DATE(job.created) Date, COUNT(DISTINCT job.ID) totalCount FROM job WHERE job.status = 1 GROUP BY DATE(job.created)");

                    $returnJobs = $query->execute();

                    $totalJobs = 0;

                    $sixDays = date('Y-m-d', strtotime('-6 days'));
                    $fiveDays = date('Y-m-d', strtotime('-5 days'));
                    $fourDays = date('Y-m-d', strtotime('-4 days'));
                    $threeDays = date('Y-m-d', strtotime('-3 days'));
                    $twoDays = date('Y-m-d', strtotime('-2 days'));
                    $oneDays = date('Y-m-d', strtotime('-1 days'));

                    $datesData = array();


                    foreach($returnJobs as $j){
                        $totalJobs += $j['totalCount'];
                        $datesData[$j['Date']] = $j['totalCount'];           
                    }

                    $dayNum = array();
                    $updatedJobs = $totalJobs;

                    if(!empty($datesData[$oneDays])){$dayNum[6] = $updatedJob = $updatedJobs - $datesData[$oneDays];}else{$dayNum[6] = $totalJobs;}
                    if(!empty($datesData[$twoDays])){$dayNum[5] = $updatedJob = $updatedJobs - $datesData[$twoDays];}else{$dayNum[5] = $dayNum[6];}
                    if(!empty($datesData[$threeDays])){$dayNum[4] = $updatedJob = $updatedJobs - $datesData[$threeDays];}else{$dayNum[4] = $dayNum[5];}
                    if(!empty($datesData[$fourDays])){$dayNum[3] = $updatedJob = $updatedJobs - $datesData[$fourDays];}else{$dayNum[3] = $dayNum[4];}
                    if(!empty($datesData[$fiveDays])){$dayNum[2] = $updatedJob = $updatedJobs - $datesData[$fiveDays];}else{$dayNum[2] = $dayNum[3];}
                    if(!empty($datesData[$sixDays])){$dayNum[1] = $updatedJob = $updatedJobs - $datesData[$sixDays];}else{$dayNum[1] = $dayNum[2];}

                    $top = 0;

                    foreach($dayNum as $day){
                        if(!empty($day)){
                            if(!empty($dayViews[0]) && $day > max($dayViews)){
                                $top = $day;
                            }elseif(empty($dayViews[0])){
                                $top = $day;
                            }
                            if($day > 0){
                                $percent = ($day/$top)*100;
                                $dayViews[] = (90 * $percent)/100;
                            }else{
                                $dayViews[] = 0;
                            }
                        }else{
                            $dayViews[] = 0;
                        }
                    }

                    $dashData['dayJobs'] = $dayViews;

                    $this->view->dayJobs = $dayViews;  

                    $this->cache->save('onBoardDayJobs-'.$this->auth['id'], $dayViews);
                }else{
                    $this->view->dayJobs = $dayJobsCache;
                }

                if(!empty($keenCache)){
                    $views = $keenCache;
                }

                if(empty($onBoardIds)){

                    usort($views, function($a, $b) {
                        return $b['result'] - $a['result'];
                    });

                    $jobViews = array_slice($views, 0, 5);

                    $dbIds = '';

                    $viewStats = array();

                    foreach($jobViews as $job){
                        if(!empty($job['job-id'])){
                            $viewStats[$job['job-id']] = $job['result'];
                            $dbIds .= ','.$job['job-id'];
                        }
                    }
                    $this->cache->save('onBoardIds-'.$this->auth['id'], $dbIds);
                }else{
                    $dbIds = $onBoardIds;
                }

                if(!empty($dbIds)){
                        $this->view->viewStats = $viewStats;

                        $returnJobs = $jobs->find(array(
                            "conditions" => "board = ".$this->auth['boardNum']." AND ID IN (".trim($dbIds,',').")", "order" => "FIELD(ID".$dbIds.")", "limit" => 5
                        ));

                        if ($returnJobs!= false) {
                            $dashData['popJobs'] = $returnJobs;
                            $this->view->popJobs= $returnJobs;
                        }else{
                            $dashData['popJobs'] = false;
                            $this->view->popJobs = false;
                        }      
                }else{
                    $dashData['popJobs'] = false;
                    $this->view->popJobs = false;
                }     

                $numJobs = $jobs->count(array( "board = ".$this->auth['boardNum']." AND status = 1" ));

                if ($numJobs!= false) {
                    $dashData['numJobs'] = $numJobs;
                    $this->view->numJobs = $numJobs;
                }else{
                    $dashData['numJobs'] = 0;
                    $this->view->numJobs = 0;
                }            

                $users = new Users;

                $unfilled = $jobs->find(array(
                    "conditions" => "board = ".$this->auth['boardNum']." AND status = 1", "order" => 'created ASC', "limit" => 5
                ));

                if ($unfilled!= false) {
                    $dashData['unfilled'] = $numJobs;
                    $dashData['date'] = time();
                    $this->view->unfilled = $unfilled;
                    $this->view->date = time();
                }else{
                    $dashData['unfilled'] = false;
                    $dashData['date'] = '';
                    $this->view->unfilled = false;
                    $this->view->date = '';
                }

                $this->cache->save('onBoardDashData-'.$this->auth['id'], $dashData);

                break;

            case 2:

                $keenCache = $this->cache->get('outBoardKeenIO-'.$this->auth['id']);

                 if($keenCache === null){

                    $filters = [ [ 'property_name' => 'candBoard', 'operator' => 'eq', 'property_value' => $this->auth['boardNum'] ] ];

                    $return = $this->getDI()->getKeenio()->count('candidates-view', ['filters' => $filters, 'target_property' => 'candidate-id', 'group_by' => 'candidate-id']);

                    $views = $return['result'];

                    usort($views, function($a, $b) {
                        return $b['result'] - $a['result'];
                    });

                    $candViews = array_slice($views, 0, 5);

                    $dbIds = '';

                    $viewStats = array();

                    foreach($candViews as $candidate){
                        $viewStats[$candidate['candidate-id']] = $candidate['result'];
                        $dbIds .= ','.$candidate['candidate-id'];
                    }

                    $this->cache->save('outBoardKeenIO-'.$this->auth['id'], $views);

                }else{
                    $views = $keenCache;
                }

                $cands = new Candidate;
                
                if(!empty($dbIds)):
                    $this->view->viewStats = $viewStats;

                    $returnCands = $cands->find(array(
                        "conditions" => "board = ".$this->auth['boardNum']." AND id IN (".trim($dbIds,',').")", "order" => "FIELD(id".rtrim($dbIds,',').")", "limit" => 5
                    ));

                    if ($returnCands!= false) {
                        $this->view->popCands= $returnCands;
                    }else{
                        $this->view->popCands = false;
                    }            
                endif;

                $users = new Users;

                $data = $users->find(array(
                    "conditions" => "board = ".$this->auth['boardNum']." AND type = 1 AND profileCreated = 1", "order" => 'created ASC', "limit" => 5
                ));

                if ($data!= false) {
                    $this->view->incompleteUsers = $data;
                    $this->view->date = time();
                }else{
                    $this->view->incompleteUsers = false;
                    $this->view->date = '';
                }

                $countComplete = $users->count(array( "board = ".$this->auth['boardNum']." AND type = 1 AND profileCreated = 2" ));
                $countTotal = $users->count(array( "board = ".$this->auth['boardNum']." AND type = 1" ));

                $this->view->percent = ($countComplete/$countTotal)*100;

                break;

        }
    }

    private function _getRecruiterDashboardData()
    {

        //$this->cache->delete('recIds');

        $dashData = array();
    
        $abStaff = new Abstaff;

        $dbIds = $this->cache->get('recIds-'.$this->auth['id']);

        if(empty($dbIds)){

            $boards = $abStaff->find(array(
                "user = ".$this->auth['id']
            ));

            $dbIds = '';

            foreach($boards as $board){
                if($board->boards->type <= 2){
                    $dbIds .= ','.$board->board;
                }
            }

            $this->cache->save('recIds-'.$this->auth['id'], trim($dbIds,','));
        }

        if(!empty($dbIds)){

            $jobs = new Job;

            $unfilledJobs = $jobs->find(array(
                'conditions' => 'status = 1 AND board in ('.trim($dbIds,',').')', "order" => 'created DESC', "limit" => 5
            ));

            $this->view->unfilledJobs = $unfilledJobs;

            $cands = new Users;

            $oldCands = $cands->find(array(
                "conditions" => "board = ".$this->auth['boardNum']." AND type = 1 AND profileCreated = 1", "order" => 'created ASC', "limit" => 5
            ));

            $this->view->oldCands = $oldCands;

        }
    }

    private function _getCandDashboardData()
    {

        $candDat = false;

        $candDashDataViews = $this->cache->get('candDashDataViews-'.$this->auth['candidateId']);
        $candDashDataDay = $this->cache->get('candDashDataDay-'.$this->auth['candidateId']);
        $keenCache = $this->cache->get('onBoardKeenIO-'.$this->auth['candidateId']);

        $dayViews = array();
        $total = 0;
        $top = 0;

        $cand = new Candidate;

        if(!empty($this->auth['candidateId']) && empty($candDashDataDay) && empty($candDashDataViews)){

            $filters = [ [ 'property_name' => 'candidate-id', 'operator' => 'eq', 'property_value' => $this->auth['candidateId'] ] ];

            $returnDays = $this->getDI()->getKeenio()->count(
                'candidates-view', 
                [
                    'filters' => $filters, 
                    'target_property' => 'candidate-id', 
                    'group_by' => 'candidate-id',
                    'timeframe' => "this_6_days",
                    'interval' => "daily"
                ]
            );

            if(!empty($returnDays['result'][0]['value'][0]['candidate-id'])):
                    foreach($returnDays['result'] as $day){
                        if(!empty($top)){
                            $top = max($dayViews);
                        }else{
                            $top = $day['value'][0]['result'];
                        }
                        if($day['value'][0]['result'] > 0){
                            $percent = ($day['value'][0]['result']/$top)*100;
                            $dayViews[] = (90 * $percent)/100;
                        }else{
                            $dayViews[] = 0;
                        }
                        $total += $day['value'][0]['result'];
                    }

                    $this->view->views = $total;

                    $this->view->dayView = $dayViews;
            endif;

            $this->cache->save('candDashDataViews-'.$this->auth['candidateId'], $total);
            $this->cache->save('candDashDataDay-'.$this->auth['candidateId'], $dayViews);

            $candDat = $cand->findFirst(array(
                "id = ".$this->auth['candidateId']
            ));
        }else if(!empty($this->auth['candidateId'])){
            if(!empty($candDashDataViews)){
                $this->view->views = $candDashDataViews;
            }
            if(!empty($candDashDataDay)){
                $this->view->dayView = $candDashDataDay;
            }
            
            $candDat = $cand->findFirst(array(
                "id = ".$this->auth['candidateId']
            ));
        }

        if($candDat != false){
            $this->view->discipline = $candDat->discipline;

            $job = new Job;

            $jobs = $job->find(array(
                "conditions" => "discipline = '".$candDat->discipline."'", "order" => "created DESC", "limit" => 5
            ));

            if($jobs != false){
                $this->view->latestJobs = $jobs;
            }else{
                $this->view->latestJobs = false;
            }

            $fillStats =  new Fillstats;

            $appliedJobs = $fillStats->find(array(
                "conditions" => "cand =".$this->auth['candidateId'], "order" => "ID DESC"
            ));

            if($appliedJobs != false){
                $this->view->appliedJobs = $appliedJobs;
            }else{
                $this->view->appliedJobs = false;
            }
        }else{
            $this->view->appliedJobs = false;
            $this->view->latestJobs = false;
            $this->view->discipline = false;
        }

    }



}
