<?php

use Phalcon\Mvc\Controller;

/**
 * SessionController
 *
 * Allows to register new users
 */
class RegisterController extends ControllerBase
{
    public function initialize()
    {

        $this->tag->setTitle('Sign Up/Sign In');
        parent::initialize();

        $auth = $this->session->get('auth');

        foreach($auth as $seshKey => $val){
            $this->view->$seshKey = $val;
        }

    }

    /**
     * Action to register a new user
     */
    public function indexAction()
    {

        $users = new Users;

        $staff = $users->find("level >= 99");

        $rec = array();

        foreach($staff as $s){
            $rec[] = array('name' => $s->name, 'id' => $s->id);
        }

        $this->view->recruts=$rec;

        $this->view->recId = $this->auth['id'];

        $form = new RegisterForm;

        $this->view->form = $form;
    }

    public function addUsersAction()
    {

        $form = new AddCompanyUsers;

        $this->view->boardNum = $this->dispatcher->getParam("boardNum");

        $this->view->form = $form;
    }

    public function addRecruiterAction()
    {
        $this->loadJs();
        $this->loadCSS();
        $this->tag->setTitle('Register Recruiter');

        $form = new AddRecruiter;

        $this->view->boardNum = 1;

        $board = new Boards;

        $boards = $board->find();

        $i=0;

        $arr = array();
        foreach($boards as $board){
            $tmp = new stdClass;
            $tmp->name = str_replace("'","",$board->name);
            $tmp->id = $board->id;
            $tmp->status = true;
            $tmp->num = $i;
            $arr[$i] = $tmp;
            $i++;
        }

        $this->view->companyList = json_encode($arr);

        $this->view->form = $form;      
    }

    protected function loadJS()
    {

    }


    protected function loadCSS()
    {
        $this->assets->addCss("js/libs/angular/angular-ui-select/dist/select.css");
        $this->assets->addCss("css/select2.min.css");
        $this->assets->addCss("css/select2.Bootstrap.css");
    }

}
