<?php

class JobsController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Jobs');
        parent::initialize();

        /*$auth = $this->session->get('auth');
        $params = $this->dispatcher->getParams();

        $job = new Job();
        $candidData = $job->findFirst(array(
            "userid = :id: ",
            'bind' => array('id' => $auth['id'])
        ));
        $this->view->candId = false;

        if ($candidData != false) {
            if($candidData->id != $params[0]){
                return $this->forward('error/401');
            }
        }else{
            return $this->forward('error/401');
        }*/


        $auth = $this->session->get('auth');

        foreach($auth as $seshKey => $val){
            $this->view->$seshKey = $val;
        }

        if (!defined('HTMLFileLoaded')) {
            require_once APP_PATH.'app/library/Htmlpure/HTMLPurifier.auto.php';

            define('HTMLFileLoaded', 1);

            $this->htmlPConfig = HTMLPurifier_Config::createDefault();

            $this->htmlPConfig->set('HTML.AllowedElements', array('span', 'strong', 'em', 'ol', 'i', 'li', 'ul', 'h1', 'h2', 'h3', 'h4', 'h5', 'hr', 'table', 'thead','tbody','tfoot','tr','td', 'p'));

            $this->htmlPurifier = new HTMLPurifier($this->htmlPConfig);
        }

    }

    public function indexAction()
    {

    }

    public function createAction()
    {
        $this->loadCSS();
        $this->loadJS();

        $auth = $this->session->get('auth');

        $this->view->boardNum = $auth['boardNum'];


        foreach($auth as $seshKey => $val){
            $this->view->$seshKey = $val;
        }

        $jobForm = new JobFields;

        $this->view->form = $jobForm;
    }


    public function updateAction()
    {
        
        $this->loadCSS();
        $this->loadJS();

        $params = $this->dispatcher->getParams();

        $auth = $this->session->get('auth');

        $this->view->boardNum = $auth['boardNum'];
        $this->view->userid = $auth['id'];

        foreach($auth as $seshKey => $val){
            $this->view->$seshKey = $val;
        }

        $cacheData = array();

        //$this->cache->delete('jobData-'.$params[0]);

        $candDataCache = $this->cache->get('jobData-'.$params[0]);

        if($candDataCache === null){

            $jobs = new Job();

            $jobData = $jobs->findFirst(array(
                "ID = :id: ",
                'bind' => array('id' => $params[0])
            ));

            if ($jobData!= false) {
                $this->view->jobId = $jobData->ID;
                foreach($jobData as $key => $profileField):
                    if(@unserialize($profileField)){
                        $cacheData[$key] = $this->view->$key = unserialize($profileField);
                    }else{
                        $cacheData[$key] = $this->view->$key = $profileField;
                    }
                endforeach;
            }else{
                die('not found');
            }

            if(!empty($this->view->locationnice)){
                $cacheData['candLoc'] = $this->view->candLoc = $this->view->location;
                $cacheData['location'] = $this->view->location = $this->view->locationnice;
            }

            $cacheData['qCount'] = $this->view->qCount = count($this->view->qualifications);

            $this->view->profile = nl2br(preg_replace("/<br\W*?\/>/",'\n', $this->view->profile));

            $clean_html = $this->htmlPurifier->purify($this->view->profile);

            $cacheData['profile'] =  $this->view->profile = str_replace("????"," ",utf8_decode($this->cleanname($clean_html)));

            $this->view->jobId = $this->view->ID;

            $cacheData['name'] = $this->auth['name'];
            $cacheData['jobId'] = $this->view->ID;
            $cacheData['board'] = $cacheData['boardNum'] = $cacheData['boardNumId'] = $this->auth['boardNum'];
            $cacheData['jobComp'] = $jobData->boards->name;
            $cacheData['boardName'] = $this->auth['boardName'];

            $this->cache->save('jobData-'.$params[0], $cacheData);

        }else{

            foreach($candDataCache as $key => $data){
                $this->view->$key = $data;
            }

        }

        $form = new JobFields;
        $this->view->form = $form;

    }

    public function getAction()
    {
        $jobId = $this->dispatcher->getParams('id');

        if(!$jobId):

            $this->flash->error('No Job Id provided');
            return $this->forward('404');

        endif;

        $auth = $this->session->get('auth');

        $this->view->boardNum = $auth['boardNum'];

        foreach($auth as $seshKey => $val){
            $this->view->$seshKey = $val;
        }

        $jobDataCache = $this->cache->get('jobData-'.$jobId['id']);

        if($jobDataCache === null){

            $job = new Job();

            $jobidData = $job->findFirst(array(
                "ID = :id: ",
                'bind' => array('id' => $jobId['id'])
            ));

            if($jobidData->status >= 2){
                $this->flash->notice('This Job is no longer available to view');
                return $this->response->redirect('dashboard'); 
            }

            $boards = new Boards();

            $jobBoard = $boards->findFirst(array(
                "id = :id: ",
                "bind" => array("id" => $jobidData->board)
            ));

            $this->view->jobComp = str_replace("'", "",$jobBoard->name);
            $this->view->boardNumId = $jobBoard->id;

            if ($jobidData!= false) {
                $this->view->jobId = $jobidData->ID;
                $this->view->boardId = $jobidData->boards->id;
                $this->view->company = str_replace("'", "",$jobidData->boards->name);
            }else{
                $this->flash->success('We could not find that job.');
                return $this->forward('404');
            }
            
            foreach($jobidData as $key => $data):
                if(@unserialize($data)){
                    $this->view->$key = unserialize($data);
                }else{
                    if($key == 'qualifications'){
                        $this->view->$key = false;
                    }else{
                        $this->view->$key = $data;
                    }
                }
            endforeach;

            $clean_html = $this->htmlPurifier->purify($this->view->profile);

            $this->view->profile = str_replace("????"," ",utf8_decode($this->cleanname($clean_html)));

        }else{
            foreach($jobDataCache as $key => $data){
                $this->view->$key = $data;
            }
            $jobidData = new stdClass;
            $jobBoard = new stdClass;
            $jobidData->users = new stdClass;
            $jobidData->boards = new stdClass;
            $jobidData->boards->id = $jobDataCache['boardNum'];
            $jobidData->board = $jobDataCache['board'];
            $jobidData->name = $jobDataCache['name'];
            $jobidData->users->name = $jobDataCache['name'];
            $jobBoard->name = $jobDataCache['boardName'];
            $jobidData->ID = $jobDataCache['id'];

        }

        $event = array('job-id' => $jobidData->ID, 'viewedBy' => $this->auth['boardNum'], 'board' => $jobidData->boards->id);

        $this->getDI()->getKeenio()->addEvent('jobs-view', $event);

    }

    public function getJobCandidatesAction()
    {
        $jobId = $this->dispatcher->getParams('id');

        if(!$jobId):

            $this->flash->error('No Job Id provided');
            return $this->forward('404');

        endif;

        $auth = $this->session->get('auth');

        $this->view->boardNum = $auth['boardNum'];

        $fillStats = new Fillstats;

        $cands = $fillStats->find(array(
            "jobId = :jobId:",
            'bind' => array('jobId' => $jobId['id'])
        ));

        if ($cands!= false) {
            $this->view->cands = $cands;
        }else{
            $this->flash->success('We could not find that job.');
            return $this->forward('404');
        }

        $this->view->id = $jobId['id'];
    }

    protected function cleanname($thename){
        $patternCounter=0;

        /*
        $patterns[$patternCounter] = '/[\x21-\x2d]/u'; // remove range of shifted characters on keyboard - !"#$%&'()*+,-
        $patternCounter++;

        $patterns[$patternCounter] = '/[\x5b-\x60]/u'; // remove range including brackets - []\^_`
        $patternCounter++;
        */

        $patterns[$patternCounter] = '/[\x7b-\xff]/u'; // remove all characters above the letter z.  This will eliminate some non-English language letters
        $patternCounter++;

        $replacement ="";

             return preg_replace($patterns, $replacement, $thename);
    }

    protected function loadJS()
    {
        $this->assets->addJs("js/libs/geoPosition.js");
        $this->assets->addJs("js/libs/geoPositionSimulator.js");
        $this->assets->addJs("js/libs/ckeditor/ckeditor.js");
    }


    protected function loadCSS()
    {
        $this->assets->addCss("js/libs/angular/angular-ui-select/dist/select.css");
        $this->assets->addCss("css/select2.min.css");
        $this->assets->addCss("css/select2.Bootstrap.css");
        $this->assets->addCss("js/libs/ckeditor/skins/moono/editor.css");
        $this->assets->addCss("js/libs/ckeditor/skins/moono/editor_ie.css");
        $this->assets->addCss("js/libs/ckeditor/skins/moono/editor_ie8.css");
    }

}
