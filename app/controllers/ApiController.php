<?php

use Phalcon\Mvc\Controller;
use Phalcon\Filter;
use Phalcon\Http\Request;
use Phalcon\DI\InjectionAwareInterface;
use Phalcon\Http\Response\Cookies;

use Phalcon\Cache\Frontend\Data as DataFrontend,
    Phalcon\Cache\Frontend\Output as OutputFrontend,
    Phalcon\Cache\Multiple,
    Phalcon\Cache\Backend\Apc as ApcCache,
    Phalcon\Cache\Backend\Memcache as MemcacheCache,
    Phalcon\Cache\Backend\File as FileCache;

class ApiController extends Controller
{

    public $submitData;

    private $siteUrl;

    public $htmlPConfig;

    public $htmlPurifier;

    public $cache;

    public function initialize()
    {
        $request = new Phalcon\Http\Request();

        $this->submitData = $request->getJsonRawBody();

        if(empty($this->submitData->render) || !isset($this->submitData->render)){
            $this->view->disable();
        }

        $this->siteUrl = $this->publicURL;      

        $ultraFastFrontend = new DataFrontend(array(
            "lifetime" => 3600
        ));

        $fastFrontend = new DataFrontend(array(
            "lifetime" => 14400
        ));

        $slowFrontend = new DataFrontend(array(
            "lifetime" => 86400
        ));

        $this->cache = new Multiple(array(
            new ApcCache($ultraFastFrontend, array(
                "prefix" => 'cache',
            )),
            new MemcacheCache($fastFrontend, array(
                "prefix" => 'cache'
            )),
            new FileCache($slowFrontend, array(
                "prefix" => 'cache',
                "cacheDir" => "/tmp/"
            ))
        ));

    }

    /**
     * Action to register a new user
     */
    public function indexAction()
    {

    }

    public function createCompanyAction()
    {
 
        if(!$this->_requestAuth()){
            echo json_encode(array('status' => 'error')); 
            die();
        } 

        $board = new Boards();

        $response_array = array();

        $checkBoard = $board->findFirst(array(
            "name = :name: ",
            'bind' => array('name' => $this->submitData->company_name)
        ));

        if(!$checkBoard && !empty($this->submitData->company_name)){

            if ($board->create(
                array(
                    "name" => $this->submitData->company_name,
                    "type" => $this->submitData->company_type,
                    "location" => $this->submitData->location
                )
            ) == false) {
                foreach ($board->getMessages() as $message) {
                    $this->flash->error($message);
                }
                if(!empty($this->submitData->returnFlash)){
                    $this->flash->output();
                    $response_array['flash'] = ob_get_contents(); ob_end_clean();
                }
                $response_array['status'] = 'error';
            }else{
                
                foreach($this->submitData->recAssign as $recrut):

                    $staff= new Abstaff();

                    $staff->save(array("board" => $board->id, "user" => $recrut->id));

                endforeach;

                $this->flash->success('Company has been registered successfully, please being to add users for this company');
                $response_array['url'] = 'register/addUsers/'.$board->id;
                $response_array['status'] = 'success';
            }
        }else{
            $this->flash->error('Company has already been registered, please try another company to register');
            $this->flash->output();
            $response_array['flash'] = ob_get_contents(); ob_end_clean();
            $response_array['status'] = 'error';          
        }

        header('Content-type: application/json');
        echo json_encode($response_array);
    }

    public function createUserAction($email=null, $name=null, $board=null,$level=null, $type=null, $recruiter=null)
    { 

        if(!$this->_requestAuth()){
            echo json_encode(array('status' => 'error')); 
            die();
        }

        if(!$email && !empty($this->submitData->compUserEmail)) $email = $this->submitData->compUserEmail;
        if(!$name && !empty($this->submitData->compUserName)) $name = $this->submitData->compUserName;
        if(!$board && !empty($this->submitData->boardNum)) $board = $this->submitData->boardNum;
        if(!$level && !empty($this->submitData->userLevel)) $level = $this->submitData->userLevel;
        if(!$type) $type = 2;
        
        $user = new Users();

        $response_array = array();

        $checkUser = $user->findFirst(array(
            "username = :email: ",
            'bind' => array('email' => $email)
        ));

        if(!$checkUser && !empty($email)):

            $pass = $this->security->hash(rtrim(base64_encode(md5(microtime())),"="));

            if ($user->create(
                array(
                    "username" => $email,
                    "level" => $level,
                    "password" => $pass,
                    "board" => $board,
                    "type" => $type,
                    "name" => $name,
                    "created" => date("Y-m-d H:i:s"),
                    "profileCreated" => 3
                )
            ) == false) {
                foreach ($user->getMessages() as $message) {
                    $this->flash->error($message);
                }
                if(!empty($this->submitData->returnFlash)){
                    $this->flash->output();
                    $response_array['flash'] = ob_get_contents(); ob_end_clean();
                }
                $response_array['status'] = 'error';
            }else{
                $this->flash->success('Password saved. Please sign in using the fields below.');
                $response_array['url'] = '/dashboard';
                $response_array['status'] = 'success';

                if($recruiter){

                    $response_array['id'] = $user->id;
                    $this->getDI()
                        ->getMail()
                        ->send(array(
                        $email => $name
                    ), "You have setup as a recruiter on Match People Skills", 'recSetup', array(
                        'setupUrl' => 'https://www'.$this->siteUrl.'/user/register/'.$user->id.'/'.urlencode(base64_encode($pass)), 'name' => $name
                    ));

                }else{

                    $response_array['id'] = $user->id;
                    $this->getDI()
                        ->getMail()
                        ->send(array(
                        $email => $name
                    ), "You have setup as a user on Match People Skills", 'setup', array(
                        'setupUrl' => 'https://www'.$this->siteUrl.'/user/register/'.$user->id.'/'.urlencode(base64_encode($pass)), 'name' => $name, 'comp' => $user->boards->name
                    ));

                }
            }

        else:
            $this->flash->error('Email address has already been registered, please try a different email address');
            $this->flash->output();
            $response_array['flash'] = ob_get_contents(); ob_end_clean();
            $response_array['status'] = 'error';          
        endif;

        if(!$recruiter){
            header('Content-type: application/json');
            echo json_encode($response_array);
        }else{
            return $response_array;
        }    
    }

    public function updateUserAction()
    {

        $response_array = array();

        if ($this->submitData->password1 != $this->submitData->password2) {
            $this->flash->error('Passwords do not match.');
            if(!empty($this->submitData->returnFlash)){
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
                $response_array['status'] = 'error';

            }else{
                $response_array['status'] = 'error';
                $response_array['errorFeedback'] = 'Passwords do not match';
            }
            header('Content-type: application/json');
            echo json_encode($response_array);
            die();
        }
        
        $pass = $this->security->hash($this->submitData->password1);
        $users = new Users();

        $user = $users->findFirst(array(
            "id = :id:",
            'bind' => array('id' => $this->submitData->userId)
        ));

        if($user == false){
            $this->flash->error('User cannot be found');
            if(!empty($this->submitData->returnFlash)){
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
            }
            $response_array['status'] = 'error';
            header('Content-type: application/json');
            echo json_encode($response_array);
            die();
        }

        if ($user->update(
            array(
                "password" => $pass
            )
        ) == false) {
            foreach ($user->getMessages() as $message) {
                $this->flash->error($message);
            }
            if(!empty($this->submitData->returnFlash)){
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
            }
            $response_array['status'] = 'error';
        } else {
            $this->flash->error('User has been registered. An email has been sent to the user.');
            $this->flash->output();
            $response_array['flash'] = ob_get_contents(); ob_end_clean();
            $response_array['url'] = '/login';
            $response_array['status'] = 'success';
        }

        header('Content-type: application/json');
        echo json_encode($response_array); 
    }

    public function authUserAction()
    {
        $cookie = false;

        $response_array = array();       

        $username = $this->submitData->loginUsername;
        $password = $this->submitData->loginPassword;

        $users = new Users();

        $user = $users->findFirst(array(
            "username = :username:",
            'bind' => array('username' => $username)
        ));

        if ($user != false) {
            if ($this->security->checkHash($password, $user->password)) {
                $this->flash->success("You have logged in to Match People Skills successfully");
                if(!empty($this->submitData->cookie)){
                    $cookie = true;
                }
                $this->_registerSession($user, $cookie);
                $response_array['url'] = '/dashboard';
                $response_array['status'] = 'success';
                header('Content-type: application/json');
                echo json_encode($response_array);
                die();
            }else{
                $this->flash->error('Cannot login as either username or password is incorrect (DEV: PASSWORD)');
                if(!empty($this->submitData->returnFlash)){
                    $this->flash->output();
                    $response_array['flash'] = ob_get_contents(); ob_end_clean();
                }
                $response_array['status'] = 'error';
                header('Content-type: application/json');
                echo json_encode($response_array);
                die();                
            }
        }else{
            $this->flash->error('Cannot login as either username or password is incorrect (DEV: USER)');
            if(!empty($this->submitData->returnFlash)){
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
            }
            $response_array['status'] = 'error';
            header('Content-type: application/json');
            echo json_encode($response_array);
            die();
        }

        //$this->flash->error('Wrong email/password');

        //return $this->forward('session/index');

    }

    public function checkAuthAction()
    {

        $response_array = array();     

        $auth = $this->session->get('auth');

        if(!$auth){

            if(!empty($_COOKIE['signedInHash']) && !empty($_COOKIE['userId'])){

                $hash = $this->cookies->get('signedInHash');
                $id = $this->cookies->get('userId');

                $handle = fopen("/tmp/".$hash, 'r');
                $data = fread($handle,filesize("/tmp/".$hash));

                if($data == $id){
                    $users = new Users();

                    $user = $users->findFirst(array(
                        "id = :id:",
                        'bind' => array('id' => $id)
                    ));

                    if ($user != false) {
                        $this->flash->success("You have logged in to Match People Skills successfully");
                        $this->_registerSession($user, false);
                         $this->response->redirect('users/index'); 
                    }
                }else{
                    if(strpos($this->view->currentURI,'dashboard') !== false){
                        $this->flash->error('Your session has expired, please login again.');
                        if(!empty($this->submitData->returnFlash)){
                            $this->flash->output();
                            $response_array['flash'] = ob_get_contents(); ob_end_clean();
                        }
                        $response_array['status'] = 'error';

                        header('Content-type: application/json');
                        echo json_encode($response_array);
                    }      
                }

            }else{
                if(!empty($this->submitData->returnFlash)){
                    if (isset($_SERVER['HTTP_COOKIE'])) {
                        $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
                        foreach($cookies as $cookie) {
                            $parts = explode('=', $cookie);
                            $name = trim($parts[0]);
                            setcookie($name, '', time()-1000);
                            setcookie($name, '', time()-1000, '/');
                        }
                    }
                    $this->flash->output();
                    $response_array['flash'] = ob_get_contents(); ob_end_clean();
                }
                $response_array['status'] = 'error';

                header('Content-type: application/json');
                echo json_encode($response_array);
            }

        }else{
            $response_array['data']['userType'] = $auth['type'];
            $response_array['data']['boardType'] = $auth['boardType'];
            $response_array['data']['userLevel'] = $auth['userLevel'];
            $response_array['status'] = 'success';

            //setcookie('apiPublicKey', $thiublicKey, time(s->api)+3600); 

            $this->cookies->set('apiPublicKey',  $this->apiPublicKey, time()+3600, '/', false, $this->siteUrl, false);

            header('Content-type: application/json');
            echo json_encode($response_array);
        }

    }

    public function createCandidateAction()
    {     

        if(!$this->_requestAuth()){
            echo json_encode(array('status' => 'error api auth')); 
            die();
        } 

        $user = new Users();

        $response_array = array();

        $pass = $this->security->hash(rtrim(base64_encode(md5(microtime())),"="));

        $userCheck = $user->findFirst(array(
            "username = :username:",
            'bind' => array('username' => $this->submitData->candidateEmail)
        )); 

        if(!$userCheck){
            if ($user->create(
                array(
                    "username" => $this->submitData->candidateEmail,
                    "level" => 1,
                    "password" => $pass,
                    "board" => $this->submitData->boardNum,
                    "type" => 1,
                    "name" => $this->submitData->candidateName,
                    "created" => date("Y-m-d H:i:s"),
                    "profileCreated" => 1
                )
            ) == false) {
                foreach ($user->getMessages() as $message) {
                    $this->flash->error($message);
                }
                if(!empty($this->submitData->returnFlash)){
                    $this->flash->output();
                    $response_array['flash'] = ob_get_contents(); ob_end_clean();
                }
                $response_array['status'] = 'error';
            }else{
                $this->flash->error('Cadidate has been created successfully. An email has been sent to the candidate.');
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
                $response_array['url'] = '/dashboard';
                $response_array['status'] = 'success';
            }

            $this->getDI()
                ->getMail()
                ->send(array(
                $this->submitData->candidateEmail => $this->submitData->candidateName
            ), "You have setup as a user on Match People Skills", 'candSetup', array(
                'setupUrl' => 'https://www.'.$this->siteUrl.'/user/register/'.$user->id.'/'.urlencode(base64_encode($pass)).'?candidate=true', 'name' => $this->submitData->candidateName, 'comp' => $user->boards->name
            ));
        }else{
            $this->flash->error('Email address already registered');
            $this->flash->output();
            $response_array['flash'] = ob_get_contents(); ob_end_clean();
            $response_array['status'] = 'error';
        }

        header('Content-type: application/json');
        echo json_encode($response_array);       
    }

    public function updateCandidateAction()
    {       

        if(!$this->_requestAuth()){
            echo json_encode(array('status' => 'error')); 
            die();
        } 

        if (!defined('HTMLFileLoaded')) {
            require_once APP_PATH.'app/library/Htmlpure/HTMLPurifier.auto.php';

            define('HTMLFileLoaded', 1);

            $this->htmlPConfig = HTMLPurifier_Config::createDefault();

            $this->htmlPConfig->set('HTML.AllowedElements', array('span', 'strong', 'em', 'ol', 'i', 'li', 'ul', 'h1', 'h2', 'h3', 'h4', 'h5', 'hr', 'table', 'thead','tbody','tfoot','tr','td', 'p'));

            $this->htmlPurifier = new HTMLPurifier($this->htmlPConfig);
        }

        $candidate = new Candidate();

        $response_array = array();

        if(!empty($this->submitData->niceName) && empty($this->submitData->candLoc)){
           $this->submitData->candLoc = $candidate->getPlaceCords($this->submitData->niceName);
        }

        $this->submitData->candRoleDesc = nl2br($this->submitData->candRoleDesc);

        if(!empty($this->submitData->qual)){
            $qual = serialize($this->submitData->qual);
        }else{
            $qual = new \Phalcon\Db\RawValue('""');
        }
        
        if(!is_object($this->submitData->candSkills) && !is_array($this->submitData->candSkills)){
            $skills = explode(',',$this->submitData->candSkills);
        }else{
            $skills = $this->submitData->candSkills; 
        }

        $clean_html = $this->htmlPurifier->purify($this->submitData->candRoleDesc);

        $profile = str_replace("????"," ",utf8_decode($this->cleanname($clean_html)));

        $dataArray = array(
            "location" =>  $this->submitData->candLoc,//$this->submitData->location,
            "locationnice" =>  $this->submitData->niceName,
            "profile" => serialize($profile),//serialize(),
            "skills" => serialize($skills),
            "discipline" => $this->submitData->candDisc,
            "qualifications" => $qual,
            "availability" => $this->submitData->candAvail,
            "title" => $this->submitData->candTitle,
            "phone" => $this->submitData->candPhone,
            "contact" => 'temp',//serialize(),
            "status" => 1,
            "board" => $this->submitData->boardNum,
            "userid" => $this->submitData->userId
        );

        $esQualsField = $esQualsDeg = $esSkills = false;

        if(!empty($this->submitData->qual)){
            foreach($this->submitData->qual as $qualifications):
                if(is_object($qualifications)){
                    if(!empty($qualifications->candQualField)): $esQualsField .= ','.$qualifications->candQualField; endif;
                    if(!empty($qualifications->candQualDegree)): $esQualsDeg .= ','.$qualifications->candQualDegree; endif;
                }
            endforeach;
        }else{
            $esQualsField = ',';
            $esQualsDeg = ',';
        }

        foreach($skills as $skill):
            $esSkills .= ','.$skill;
        endforeach;

        $esDataArray = array(
            "locationnice" =>  $this->submitData->niceName,
            "profile" => $this->submitData->candRoleDesc,
            "skills" => trim($esSkills,','),
            "discipline" => $this->submitData->candDisc,
            "availability" => $this->submitData->candAvail,
            "title" => $this->submitData->candTitle,
        );

        if(!empty(trim($this->submitData->candLoc,','))){
            $esDataArray['location'] = $this->submitData->candLoc;
            unset($esDataArray['locationnice']);
        }

        if(!empty(trim($esQualsField,','))){
            $esDataArray['qualificationsField'] = trim($esQualsField,',');
        }

        if(!empty(trim($esQualsDeg,','))){
            $esDataArray['qualificationsDegree'] = trim($esQualsDeg,',');
        }

        if(!empty($this->submitData->candidateId)){
            $dataArray['id'] = $this->submitData->candidateId;
            $update = $candidate->update($dataArray);
        }else{
            $this->cache->delete('candData-0');
            $update = $candidate->save($dataArray);
        }

        if ($update == false) {
            foreach ($candidate->getMessages() as $message) {
                $this->flash->error($message);
            }
            if(!empty($this->submitData->returnFlash)){
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
            }
            $response_array['status'] = 'error';
        }else{

            $this->cache->delete('candData-'.$this->submitData->userId);

            $user = new Users;

            $users = $user->findFirst(array(
                "id = :id:",
                'bind' => array('id' => $this->submitData->userId)
            )); 

            $users->update(array('profileCreated' => 2));

            $esDataArray['id'] = $candidate->id;
            $candidate->addToElastic($esDataArray);
            $this->flash->success('Your profile has updated successfully');
            $response_array['url'] = '/dashboard';
            $response_array['status'] = 'success';

            $users = new Users();

            $user = $users->findFirst(array(
                "id = :userid:",
                'bind' => array('userid' => $this->submitData->userId)
            ));

            $this->_registerSession($user);
        }

        header('Content-type: application/json');
        echo json_encode($response_array);       
    }

    public function searchAction()
    {

        if(!$this->_requestAuth()){
            echo json_encode(array('status' => 'error')); 
            die();
        } 

        $search = new Search();

        $response_array = array();

        $queries = $this->submitData;
        unset($queries->returnFlash);
        $esFiltered = array();

        foreach($queries as $key => $query):
            switch($key){

                case 'keyword': 
                    $esFiltered['keyword'] = $query;
                    break;

                case 'discipline': 
                    $esFiltered['discipline'] = $query;
                    break;

                case 'filterOut': 
                    $esFiltered['filterOut'] = $query;
                    break;

                case 'qual': 
                    $esFiltered['qual'] = $query;
                    break;

                case 'location': 
                    $esFiltered['location'] = $query;
                    break;

                case 'availability': 
                    $esFiltered['availability'] = $query;
                    break;

                case 'searchSkills': 
                    $esFiltered['searchSkills'] = $query;
                    break;

                case 'from': 
                    $esFiltered['from'] = $query;
                    break;

                case 'limit': 
                    $esFiltered['limit'] = $query;
                    break;

                case 'sortBy': 
                    $esFiltered['sortBy'] = $query;
                    break;

            }

        endforeach;

        $results = $search->searchElastic($esFiltered);

        if(!$results){
            foreach ($candidate->getMessages() as $message) {
                $this->flash->error($message);
            }
            if(!empty($this->submitData->returnFlash)){
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
            }
            $response_array['status'] = 'error';
        }else{
            $response_array['results'] = $results;
            $response_array['status'] = 'success';
        }

        header('Content-type: application/json');
        echo json_encode($response_array);   
    }

    public function createJobAction()
    {

        if(!$this->_requestAuth()){
            echo json_encode(array('status' => 'error')); 
            die();
        } 
 
        if (!defined('HTMLFileLoaded')) {
            require_once APP_PATH.'app/library/Htmlpure/HTMLPurifier.auto.php';

            define('HTMLFileLoaded', 1);

            $this->htmlPConfig = HTMLPurifier_Config::createDefault();

            $this->htmlPConfig->set('HTML.AllowedElements', array('span', 'strong', 'em', 'ol', 'i', 'li', 'ul', 'h1', 'h2', 'h3', 'h4', 'h5', 'hr', 'table', 'thead','tbody','tfoot','tr','td', 'p'));

            $this->htmlPurifier = new HTMLPurifier($this->htmlPConfig);
        }
        
        $job = new Job();

        $response_array = array();

        if(!empty($this->submitData->niceName) && empty($this->submitData->candLoc)){
           $this->submitData->candLoc = $job->getPlaceCords($this->submitData->niceName);
        }

        if(empty($this->submitData->candSkills)){
            $this->submitData->skills = array();
        }

        if(empty($this->submitData->qual)){
            $this->submitData->quals = array();
        }

        //print_r($this->submitData);
        //die();

        $clean_html = $this->htmlPurifier->purify($this->submitData->candRoleDesc);

        $profile = str_replace("????"," ",utf8_decode($this->cleanname($clean_html)));

        $dataArray = array(
            "location" =>  $this->submitData->candLoc,//$this->submitData->location,
            "locationnice" =>  $this->submitData->niceName,
            "profile" => serialize($profile),//serialize(),
            "skills" => serialize($this->submitData->candSkills),
            "discipline" => $this->submitData->candDisc,
            "qualifications" => serialize($this->submitData->qual),
            "availability" => $this->submitData->candAvail,
            "title" => $this->submitData->candTitle,
            "created" => date("Y-m-d H:i:s"),
            "status" => 1,
            "board" => $this->submitData->boardNum
        );

        $esQualsField = $esQualsDeg = $esSkills = '';

        foreach($this->submitData->qual as $qualifications):
            $esQualsField .= ','.$qualifications->candQualField;
            $esQualsDeg .= ','.$qualifications->candQualDegree;
        endforeach;

        $skills = $this->submitData->candSkills;
        foreach($skills as $skill):
            $esSkills .= ','.$skill;
        endforeach;

    	if(!empty($this->submitData->jobId)){
    	    $dataArray['ID'] = $this->submitData->jobId;
    	    $update = $job->update($dataArray);
    	}else{
            $this->cache->delete('jobData-0');
            $update = $job->save($dataArray);
	    }

        if ($update == false) {
            foreach ($job->getMessages() as $message) {
                $this->flash->error($message);
            }
            if(!empty($this->submitData->returnFlash)){
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
            }
            $response_array['status'] = 'error';
        }else{

            $this->cache->delete('jobData-'.$job->ID);

            $event = array('job-id' => $job->ID, 'board' => $this->submitData->boardNum);

            $this->getDI()->getKeenio()->addEvent('job-created', $event);

            $esDataArray = array(
                "location" =>  $this->submitData->candLoc,
                "locationnice" =>  $this->submitData->niceName,
                "profile" => $this->submitData->candRoleDesc,
                "skills" => trim($esSkills,','),
                "discipline" => $this->submitData->candDisc,
                "qualificationsField" => trim($esQualsField,','),
                "qualificationsDegree" => trim($esQualsDeg,','),
                "availability" => $this->submitData->candAvail,
                "title" => $this->submitData->candTitle,
            );
            $esDataArray['id'] = $job->ID;
            $job->addToElastic($esDataArray);
            $this->flash->success('Job Ad Created Successfully. Here is a search for candidates based on your job advert.');

            $searchArray = $dataArray;
            $searchArray['skills'] = unserialize($dataArray['skills']);
            $searchArray['location'] = $dataArray['locationnice'];

            foreach($this->submitData->qual as $qual){
                $searchArray['qual'][] = array('fos' => $qual->candQualField, 'degName' => $qual->candQualDegree);
            }

            unset($searchArray['profile']);
            unset($searchArray['title']);
            unset($searchArray['status']);
            unset($searchArray['board']);
            unset($searchArray['locationnice']);
            unset($searchArray['qualifications']);

            $url = http_build_query($searchArray);

            $response_array['url'] = '/dashboard/results?'.trim($url,'&');
            $response_array['status'] = 'success';
        }

        header('Content-type: application/json');
        echo json_encode($response_array);         
    }



    public function registerRecruiterAction()
    {

        $response_array = array();
        
        if(!$this->_requestAuth()){
            echo json_encode(array('status' => 'error')); 
            die();
        }

        $id = $this->createUserAction($this->submitData->compUserEmail, $this->submitData->compUserName, 1,99, 3, true);

        if(!empty($id['id']) && $id['status'] == 'success'){
            foreach($this->submitData->compAssign as $key => $board){
                $dbData = array(
                    "board" => $board->id,
                    "user" => $id['id'],
                );

                $staff = new Abstaff();

                if ($staff->create($dbData) == false) {   
                    foreach ($staff->getMessages() as $message) {
                        $this->flash->error($message);
                    }
                    if(!empty($this->submitData->returnFlash)){
                        $this->flash->output();
                        $response_array['flash'] = ob_get_contents(); ob_end_clean();
                    }
                    $response_array['status'] = 'error';
                }  
            }

            if(empty($response_array['status'])){
                $this->flash->success('Recruiter created successfully');
                $response_array['url'] = '/dashboard';
                $response_array['status'] = 'success';
            }

            header('Content-type: application/json');
            echo json_encode($response_array);

        }else{
            header('Content-type: application/json');
            echo json_encode($id);   
        }
    }

    public function updateCandStatusAction()
    {
        if(!$this->_requestAuth()){
            echo json_encode(array('status' => 'error')); 
            die();
        }

        $dataArray = array();

        $id = $this->submitData->userId;

        $cands = new Candidate();

        $candData = $cands->findFirst(array(
            "id = :id: ",
            'bind' => array('id' => $id)
        ));

        if($this->submitData->status == 'Active'){

            $dataArray['status'] = 1;

            if ($candData != false) {

                $esDataArray = array();
            
                foreach($candData as $key => $data):
                    if(@unserialize($data)){
                        $esDataArray[$key] = unserialize($data);
                    }else{
                        $esDataArray[$key] = $data;
                    }
                endforeach;

                $esQualsField = $esQualsDeg = $esSkills = '';
        
                foreach($esDataArray['qualifications'] as $qualifications):
                    $esQualsField .= ','.$qualifications->candQualField;
                    $esQualsDeg .= ','.$qualifications->candQualDegree;
                endforeach;

                foreach($esDataArray['skills'] as $skill):
                    $esSkills .= ','.$skill;
                endforeach;

                $esDataArray["qualificationsField"] = trim($esQualsField,',');
                $esDataArray["qualificationsDegree"] = trim($esQualsDeg,',');
                $esDataArray["skills"] = trim($esSkills,',');

                unset($esDataArray['esParams']);
                unset($esDataArray['client']);
                unset($esDataArray['qualifications']);
                unset($esDataArray['dbPrefix']);

                $cands->addToElastic($esDataArray);

            }else{

                $this->flash->error('We could not find that candidate.');
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
                $response_array['status'] = 'error';
            }


        }else{
            $dataArray['status'] = 2;
            $cands->removeFromElastic($id);
        }

        $candUpdate = $cands->findFirst("id = '".$id."'");

        $candUpdate->status = $dataArray['status'];

        if ($candUpdate->save() == false) {
            foreach ($candUpdate->getMessages() as $message) {
                $this->flash->error($message);
            }
            if(!empty($this->submitData->returnFlash)){
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
            }
            $response_array['status'] = 'error';
        }else{

            $email = $this->getDI()
                ->getMail()
                ->send(array(
                $candData->users->username => $candData->users->name
            ), 'Match People Skills - '.$candData->users->name.' candidate status has been updated.', 'statsUpdate', array(
                'status' => $this->submitData->status, 'name' => $candData->users->name
            ));

            $this->flash->success('Candidate Status Updated Successfully');
            $this->flash->output();
            $response_array['flash'] = ob_get_contents(); ob_end_clean();
            $response_array['status'] = 'success';
        }

        header('Content-type: application/json');
        echo json_encode($response_array);
    }

    public function updateJobStatusAction()
    {
        if(!$this->_requestAuth()){
            echo json_encode(array('status' => 'error')); 
            die();
        }

        $dataArray = array();

        $dataArray['id'] = $this->submitData->userId;

        $jobs = new Job();

        $jobData = $jobUpdate = $jobs->findFirst("ID = ".$dataArray['id']);

        $esQualsField = $esQualsDeg = $esSkills = '';

        if($this->submitData->status === 'Unfilled'){

            $dataArray['status'] = 1;

            if ($jobData != false) {

                $esDataArray = array();
            
                foreach($jobData as $key => $data):
                    if(@unserialize($data)){
                        $esDataArray[$key] = unserialize($data);
                    }else{
                        $esDataArray[$key] = $data;
                    }
                endforeach;

                $esQualsField = $esQualsDeg = $esSkills = '';
        
                foreach($esDataArray['qualifications'] as $qualifications):
                    $esQualsField .= ','.$qualifications->candQualField;
                    $esQualsDeg .= ','.$qualifications->candQualDegree;
                endforeach;

                foreach($esDataArray['skills'] as $skill):
                    $esSkills .= ','.$skill;
                endforeach;

                $esDataArray["qualificationsField"] = trim($esQualsField,',');
                $esDataArray["qualificationsDegree"] = trim($esQualsDeg,',');
                $esDataArray["skills"] = trim($esSkills,',');

                unset($esDataArray['esParams']);
                unset($esDataArray['client']);
                unset($esDataArray['qualifications']);
                unset($esDataArray['dbPrefix']);

                $jobs->addToElastic($esDataArray);

            }else{

                $this->flash->error('We could not find that job.');

            }


        }else{
            $dataArray['status'] = 2;
            $jobs->removeFromElastic($dataArray);
        }

        $jobUpdate->status = $dataArray['status'];

        if ($jobUpdate->save() == false) {
            foreach ($jobUpdate->getMessages() as $message) {
                $this->flash->error($message);
            }
            if(!empty($this->submitData->returnFlash)){
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
            }
            $response_array['status'] = 'error';
        }else{

            $abStaff = new Abstaff;

            $users = new Users;

            $recrut = $abStaff->find(array(
                "board = :boardId: ",
                'bind' => array('boardId' => $jobUpdate->board)
            ));

            $user = $users->find(array(
                "board = :boardId: ",
                'bind' => array('boardId' => $jobUpdate->board)            
            ));

            $recruiters = $userEmail = array();

            foreach($recrut as $r){
                $recruiters[] = array('email' => $r->users->username, 'name' => $r->users->name);
            }

            foreach($user as $u){
                $userEmail[] = array('email' => $u->username, 'name' => $u->name);
            }

            foreach($recruiters as $recruiterEmail){

                $email = $this->getDI()
                    ->getMail()
                    ->send(array(
                    $recruiterEmail['email'] => $recruiterEmail['name']
                ), 'Match People Skills - Job '.$dataArray['title'].' (#'.$dataArray['id'].') has been updated', 'jobUpdate', array(
                    'status' => $this->submitData->status, 'name'=>$recruiterEmail['name'], 'jobTtitle' => $jobData->title, 'comp' => $jobData->boards->name , 'jobLoc'=>$jobData->locationnice, 'jobId' => $dataArray['id']
                ));

            }

            foreach($userEmail as $email){

                $email = $this->getDI()
                    ->getMail()
                    ->send(array(
                    $email['email'] => $email['name']
                ), 'Match People Skills - Job #'.$dataArray['id'].' has been updated', 'jobUpdate', array(
                    'status' => $this->submitData->status, 'name'=>$email['name'], 'JobTtitle' => $jobData->title, 'comp' => $jobData->boards->name , 'jobLoc'=>$jobData->locationnice, 'jobId' => $dataArray['id']
                ));

            }


            $this->flash->success('Candidate Status Updated Successfully');
            $this->flash->output();
            $response_array['flash'] = ob_get_contents(); ob_end_clean();
            $response_array['url'] = '/dashboard';
            $response_array['status'] = 'success';

            header('Content-type: application/json');
            echo json_encode($response_array);
        }
    }

    public function updateFillStatusAction()
    {

        switch($this->submitData->status){

            case 'Interview Stage':
                $status = 3;
                break;

            case 'Successful':
                $status = 4;
                break;

            case 'Unsuccessful':
                $status = 2;
                break;

            default:
                $status = 1;
                break;
        }

        $fill = new Fillstats();

        $fillUpdate = $fill->findFirst(array("jobId = ".$this->submitData->jobId." AND cand = ".$this->submitData->userId));

        $fillUpdate->status = $status;
   
        if ($fillUpdate->save() == false) {
            foreach ($fillUpdate->getMessages() as $message) {
                $this->flash->error($message);
            }
            if(!empty($this->submitData->returnFlash)){
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
            }
            $response_array['status'] = 'error';
        }else{

            $cands = new Candidate;

            $cand = $cands->findFirst(array(
                "id = :id:",
                'bind' => array('id' => $this->submitData->userId)
            ));

            $email = $this->getDI()
                ->getMail()
                ->send(array(
                $cand->users->username => $cand->users->name
            ), 'Match People Skills - '.$cand->users->name.' your application status for job #'.$this->submitData->jobId.' has been updated', 'applicationUpdate', array(
                'jobData' => $fillUpdate->job, 'status' => $this->submitData->status, 'link' => 'https://'.$this->siteUrl.'/dashboard','name' => $cand->users->name
            ));

            $this->flash->success('Candidate Application Status Updated Successfully');
            $this->flash->output();
            $response_array['flash'] = ob_get_contents(); ob_end_clean();
            $response_array['url'] = '/dashboard';
            $response_array['status'] = 'success';

            header('Content-type: application/json');
            echo json_encode($response_array);
        }
    }

    public function analyticsPush($params){
        
    }

    public function helpFillEmailAction(){

        if(!$this->_requestAuth()){
            echo json_encode(array('status' => 'error')); 
            die();
        }

        $abStaff = new Abstaff;

        $recrut = $abStaff->find(array(
            "board = :boardId:",
            'bind' => array('boardId' => $this->submitData->data->boardId)
        ));

        $recruiters = array();

        foreach($recrut as $r){
            $recruiters[] = array('email' => $r->users->username, 'name' => $r->users->name);
        }

        $name = $this->submitData->data->name;
        $company = $this->submitData->data->boardName;

        unset($this->submitData->data->limit);
        unset($this->submitData->data->from);
        unset($this->submitData->data->sortBy);
        unset($this->submitData->data->returnFlash);
        unset($this->submitData->data->apiAction);
        unset($this->submitData->data->filterOut);
        unset($this->submitData->data->hash);
        unset($this->submitData->data->microtime);
        unset($this->submitData->data->apiController);
        unset($this->submitData->data->boardId);
        unset($this->submitData->data->name);
        unset($this->submitData->data->boardName);

        foreach($recruiters as $recruiterEmail){

            $email = $this->getDI()
                ->getMail()
                ->send(array(
                $recruiterEmail['email'] => $recruiterEmail['name']
            ), 'Match People Skills - '.$name.' from '.$company.' needs help filling a position', 'fillHelp', array(
                'params' => $this->submitData->data, 'name' => $name, 'company' => $company, 'recrutname' => $recruiterEmail['name']
            ));
        }

        $this->flash->success('Your request for help has been sent to your assigned recruiters.');
        $this->flash->output();
        $response_array['flash'] = ob_get_contents(); ob_end_clean();
        $response_array['status'] = 'success';

        header('Content-type: application/json');
        echo json_encode($response_array);
    }

    public function contactRequestAction(){

        if(!$this->_requestAuth()){
            echo json_encode(array('status' => 'error')); 
            die();
        }

        $abStaff = new Abstaff;

        $recrut = $abStaff->find(array(
            "board = :boardId: ",
            'bind' => array('boardId' => $this->submitData->data->boardId)
        ));

        $recruiters = array();

        foreach($recrut as $r){
            $recruiters[] = array('email' => $r->users->username, 'name' => $r->users->name);
        }

        $name = $this->submitData->data->name;

        if(!empty($this->submitData->data->boardName)){
            $company = $this->submitData->data->boardName;
            $subject = 'Match People Skills - '.$name.' from '.$company.' has a contact request';
        }else{
            $subject = 'Match People Skills - '.$name.' has a contact request';
        $company = false;
        }


        unset($this->submitData->returnFlash);
        unset($this->submitData->apiAction);
        unset($this->submitData->filterOut);
        unset($this->submitData->hash);
        unset($this->submitData->microtime);
        unset($this->submitData->data->apiController);
        unset($this->submitData->data->boardId);
        unset($this->submitData->data->name);
        unset($this->submitData->data->boardName);

    $comp = false;

        if(!empty($this->submitData->data->candComp)){
            $comp = $this->submitData->data->candComp;
        }

        if(!empty($this->submitData->data->jobComp)){
            $comp = $this->submitData->data->jobComp;
        }

        if(!empty($this->submitData->data->type) && $this->submitData->data->type == 'cand'){
            $link = 'https://'.$this->siteUrl.'/dashboard/candidates/'.$this->submitData->data->candId;
        }else{
            $link = 'https://'.$this->siteUrl.'/dashboard/jobs/'.$this->submitData->data->candId;
        }

        foreach($recruiters as $recruiterEmail){

            $email = $this->getDI()
                ->getMail()
                ->send(array(
                $recruiterEmail['email'] => $recruiterEmail['name']
            ), $subject, 'contactReq', array(
                'params' => $this->submitData->data,'comp' => $comp, 'link' => $link, 'name' => $name, 'company' => $company
            ));

        }

        $this->flash->success('Your contact request has been sent to your assigned recruiters.');
        $this->flash->output();
        $response_array['flash'] = ob_get_contents(); ob_end_clean();
        $response_array['status'] = 'success';

        header('Content-type: application/json');
        echo json_encode($response_array);
    }

    public function jobApplyAction()
    {
        if(!$this->_requestAuth()){
            echo json_encode(array('status' => 'error')); 
            die();
        }


        $fill = new Fillstats;

        $addFill = $fill->save(array(
            "jobId" => $this->submitData->data->job->id, 
            "cand" => $this->submitData->data->candId,
            "status" => 1
        ));

        if($addFill == false){

            foreach ($fillUpdate->getMessages() as $message) {
                $this->flash->error($message);
            }
            if(!empty($this->submitData->returnFlash)){
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
            }
            $response_array['status'] = 'error';

        }else{
        
            $abStaff = new Abstaff;

            $recrut = $abStaff->find(array(
                "board = :boardId: ",
                'bind' => array('boardId' => $this->submitData->data->boardId)
            ));

            $recruiters = array();

            foreach($recrut as $r){
                $recruiters[] = array('email' => $r->users->username, 'name' => $r->users->name);
            }

            if(!empty($this->submitData->data->candComp)){
                $comp = $this->submitData->data->candComp;
            }

            if(!empty($this->submitData->data->jobComp)){
                $comp = $this->submitData->data->jobComp;
            }

            $recLink = "https://".$this->siteUrl.'/dashboard/job/'.$this->submitData->data->job->id.'/candidates/';
            $candLink = "https://".$this->siteUrl.'/dashboard/job/'.$this->submitData->data->job->id;

            $cands = new Candidate;

            $cand = $cands->findFirst("id = ".$this->submitData->data->candId);

            if(!empty($comp)){
                $subject = 'Match People Skills - '.$cand->users->name.' from '.$comp.' has a applied for a position';
            }else{
                $subject = 'Match People Skills - '.$cand->users->name.' has a applied for a position';
            }  

            foreach($recruiters as $recruiterEmail){

                $email = $this->getDI()
                    ->getMail()
                    ->send(array(
                    $recruiterEmail['email'] => $recruiterEmail['name']
                ), $subject, 'jobApplyRec', array(
                    'params' => $this->submitData->data,'comp' => $comp, 'link' => $recLink, 'name' => $cand->users->name, 'company' => $this->submitData->data->job->comp
                ));

            }


            $subject = 'Match People Skills - '.$cand->users->name.', you have applied for a position';


            $email = $this->getDI()
                ->getMail()
                ->send(array(
                $cand->users->username => $cand->users->name
            ), $subject, 'jobApplyRec', array(
                'params' => $this->submitData->data,'comp' => $comp, 'link' => $candLink, 'name' => $cand->users->name, 'company' => $this->submitData->data->job->comp
            ));

            $this->flash->success('Application has been submitted successfully');
            $this->flash->output();
            $response_array['flash'] = ob_get_contents(); ob_end_clean();
            $response_array['status'] = 'success';
        }

        header('Content-type: application/json');
        echo json_encode($response_array);

    }

    public function contactSubmitFormAction()
    {


	   $di = $this->getDI();

        $email = $di->getMail()->send(array(
            'brenda.fraser@alta-blue.com' => 'brenda fraser'
        ), 'Contact Submission on Match People Skills Marketing Page', 'contactSubmit', array(
            'params' => $this->submitData->data
        ));

        $email = $di->getMail()->send(array(
            'lauren.allen@alta-blue.com' => 'lauren allen'
        ), 'Contact Submission on Match People Skills Marketing Page', 'contactSubmit', array(
            'params' => $this->submitData->data
        ));

        $email = $di->getMail()->send(array(
            'morag.reglinski-gill@alta-blue.com' => 'morag reglinski-gill'
        ), 'Contact Submission on Match People Skills Marketing Page', 'contactSubmit', array(
            'params' => $this->submitData->data
        ));


    }

    public function candReminderAction()
    {
        if(!$this->_requestAuth()){
            echo json_encode(array('status' => 'error')); 
            die();
        }

        $di = $this->getDI();

        $email = $di->getMail()->send(array(
            $this->submitData->data->username => $this->submitData->data->name
        ), 'Reminder to complete your profile on Match People Skills', 'reminder', array(
            'name'=>$this->submitData->data->name, 'login' => 'https://www.'.$this->siteUrl.'/login', 'reset' => 'https://www.'.$this->siteUrl.'/user/register/'.$this->submitData->data->id.'/'.urlencode(base64_encode($this->submitData->data->password)).'?candidate=true',
        ));

        if(empty($email)){
            $this->flash->success('Reminder email sent to candidate');
            $this->flash->output();
            $response_array['flash'] = ob_get_contents(); ob_end_clean();
            $response_array['status'] = 'success'; 
        }else{
            $this->flash->error('Could not send a reminder email');
            $response_array['flash'] = ob_get_contents(); ob_end_clean();
            $response_array['status'] = 'error';
        }

        header('Content-type: application/json');
        echo json_encode($response_array);

    }

    public function resetPasswordAction()
    {

        $di = $this->getDI();

        $user = new Users;

        $users = $user->findFirst('username = "'.$this->submitData->email.'"');

        if($users == false){
            $this->flash->error('Could not find a user with the submitted email address');
            $this->flash->output();
            $response_array['flash'] = ob_get_contents(); ob_end_clean();
            $response_array['status'] = 'error'; 
        }else{

            $email = $di->getMail()->send(array(
                $this->submitData->email => $users->name
            ), 'Request to Reset Your Match People Skills Password', 'passReset', array(
                'name'=>$users->name, 'login' => 'https://www.'.$this->siteUrl.'/login', 'reset' => 'https://www.'.$this->siteUrl.'/user/register/'.$users->id.'/'.urlencode(base64_encode($users->password)).'?candidate=true',
            ));

            if(empty($email)){
                $this->flash->success('Password reset email sent');
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
                $response_array['status'] = 'success'; 
            }else{
                $this->flash->error('Could not send a password reset email');
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
                $response_array['status'] = 'error';
            }

        }

        header('Content-type: application/json');
        echo json_encode($response_array);

    }

    public function addCandNoteAction()
    {
        if(!$this->_requestAuth()){
            echo json_encode(array('status' => 'error')); 
            die();
        }

        $note = new Candnotes;

        $dataArray = array(
            'candid' => $this->submitData->data->candid,
            'note' => $this->submitData->data->note,
            'redid' => $this->submitData->data->redid,
            'created' => date("Y-m-d H:i:s")
        ); 

        if ($note->create($dataArray) == false) {
            foreach ($job->getMessages() as $message) {
                $this->flash->error($message);
            }
            if(!empty($this->submitData->returnFlash)){
                $this->flash->output();
                $response_array['flash'] = ob_get_contents(); ob_end_clean();
            }
            $response_array['status'] = 'error';
        }else{

            $this->flash->success('Note added successfully');
            $this->flash->output();
            $response_array['flash'] = ob_get_contents(); ob_end_clean();
            $response_array['status'] = 'success';

        }

        header('Content-type: application/json');
        echo json_encode($response_array);

    }

    private function _registerCandId(Candidate $candidate)
    {
        $this->session->set('candid', array(
            'id' => $candidate->id
        ));
    }

    private function _registerSession(Users $user, $cookie = null)
    {

        $authArray = array(
            'id' => $user->id,
            'name' => $user->name,
            'boardNum' => $user->board,
            'type' => $user->type,
            'boardType' => $user->boards->type,
            'boardName' => $user->boards->name,
            'userLevel' => $user->level
        );

        if($user->type == 1){
            if($user->type == 1){
                $candidate = new Candidate();

                $candidData = $candidate->findFirst(array(
                    "userid = :id: ",
                    'bind' => array('id' => $user->id)
                ));

                if ($candidData!= false) {
                     $authArray['candidateId'] = $candidData->id;
                }else{
                    $authArray['candidateId'] = 0;
                }
            }

        }

        $this->session->set('auth', $authArray);

        setcookie('apiPublicKey',  $this->apiPublicKey, time()+3600, '/', $this->siteUrl,false,  false);

        if($cookie){
            setcookie('userId',  $user->id, time()+86400*14, '/', $this->siteUrl, false, false);
            setcookie('signedInHash', hash_hmac('sha512', $user->username.$this->cookieHash1, $this->cookieHash2), time()+86400*14, '/', $this->siteUrl,false, false); 
            $file = fopen("/tmp/".hash_hmac('sha512', $user->username.$this->cookieHash1, $this->cookieHash2), 'w');
            fwrite($file, $user->id);
            fclose($file);
        }

        //setcookie('apiPublicKey', $thiublicKey, time(s->api)+3600); 
        //$this->cookies->set('apiPublicKey', $this->apiPublicKey, time()+3600 * 1000);
    }

    private function _requestAuth()
    {
        if ($this->cookies->has('apiPublicKey')) {

            $cookieData = $this->cookies->get('apiPublicKey');

            $publicKey = $cookieData->getValue();

            $serverMicrotime = microtime(true);
            $timeDiff = $serverMicrotime - $this->submitData->microtime;
         
            if($timeDiff > 10) die(false);
            
            $serverToken = hash_hmac('sha512',$this->apiPublicKey.$this->submitData->microtime,$this->apiPrivateKey);

            $requestToken = $this->submitData->hash;

            if($serverToken === $requestToken){
                return true;
            }else{
                return false;
            }

        }else{
            return false;
        }
    }

    protected function cleanname($thename){
        $patternCounter=0;

        /*
        $patterns[$patternCounter] = '/[\x21-\x2d]/u'; // remove range of shifted characters on keyboard - !"#$%&'()*+,-
        $patternCounter++;

        $patterns[$patternCounter] = '/[\x5b-\x60]/u'; // remove range including brackets - []\^_`
        $patternCounter++;
        */

        $patterns[$patternCounter] = '/[\x7b-\xff]/u'; // remove all characters above the letter z.  This will eliminate some non-English language letters
        $patternCounter++;

        $replacement ="";

             return preg_replace($patterns, $replacement, $thename);
    } 

    /*protected function forward($uri)
    {
        $uriParts = explode('/', $uri);
        $params = array_slice($uriParts, 2);
        return $this->dispatcher->forward(
            array(
                'controller' => $uriParts[0],
                'action' => $uriParts[1],
                'params' => $params
            )
        );
    }*/
}
