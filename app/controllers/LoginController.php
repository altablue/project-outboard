<?php

use Phalcon\Mvc\Controller;

class LoginController extends ControllerBase
{

    public function initialize()
    {
        $this->tag->setTitle('Login');
        parent::initialize();
    }

    public function indexAction()
    {

    	$form = new LoginForm;

        $this->view->logged = false;

        $this->view->form = $form;
        //setcookie('apiPublicKey', $this->apiPublicKey, time()+3600);

    }

    public function logoutAction()
    {

        $this->session->remove('auth');

        unset($_SESSION['auth']);

        $this->session->destroy();

        $this->cookies->delete('signedInHas');
        $this->cookies->delete('userId');

        unset($_COOKIE['signedInHash']);
        unset($_COOKIE['userId']);
        unset($_COOKIE['apiPublicKey']);
        unset($_COOKIE['PHPSESSID']);

        setcookie('signedInHash',  false, time() - 60*100000, '/', $this->siteUrl,false,  false);
        setcookie('userId',  false, time() - 60*100000, '/', $this->siteUrl,false,  false);
        setcookie('apiPublicKey',  false, time() - 60*100000, '/', $this->siteUrl,false,  false);

        $this->view->logged = false;

        $this->auth = false;

        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time()-1000);
                setcookie($name, '', time()-1000, '/');
            }
        }

        $this->flash->success('You have successfully logged out.');
        return $this->response->redirect('login/index');
    }

    public function passwordResetAction()
    {
        $this->tag->setTitle('Reset Password');
    }

	private function _registerSession(Users $user)
    {
        $this->session->set('auth', array(
            'id' => $user->id,
            'name' => $user->name
        ));
    }

}
