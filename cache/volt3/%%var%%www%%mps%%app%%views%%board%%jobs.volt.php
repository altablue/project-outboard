
<?php echo $this->getContent(); ?>

<div class="boxTop">
	<h2><?php echo $bName; ?>'s Job Adverts</h2>
	<hr/>
</div>

<div class="getAllWrapper col-xs-12" ng-controller="recruitEdit">

	<div ng-bind-html="flashStatus"></div>

	<table class="table table-striped footable col-xs-12 toggle-small default">

		<thead>
			<tr>
				<th  data-sort-ignore="true" data-toggle="true"><strong>Job title</strong></th>
				<th  data-sort-ignore="true" data-hide="phone,tablet"><strong>Job Location</strong></th>
				<th  data-sort-ignore="true" data-hide="phone,tablet"><strong>Job Discipline</strong></th>
				<th  data-sort-ignore="true" data-hide="phone,tablet"><strong>Job Status</strong></th>
				<th  data-sort-ignore="true" data-hide="phone,tablet"><strong>Days on System</strong></th>
				<th  data-sort-ignore="true" data-hide="phone,tablet"></th>
			</tr>
		</thead>

		<tbody>
			<?php $v21635878953204041321iterator = $jobs; $v21635878953204041321incr = 0; $v21635878953204041321loop = new stdClass(); $v21635878953204041321loop->length = count($v21635878953204041321iterator); $v21635878953204041321loop->index = 1; $v21635878953204041321loop->index0 = 1; $v21635878953204041321loop->revindex = $v21635878953204041321loop->length; $v21635878953204041321loop->revindex0 = $v21635878953204041321loop->length - 1; ?><?php foreach ($v21635878953204041321iterator as $job) { ?><?php $v21635878953204041321loop->first = ($v21635878953204041321incr == 0); $v21635878953204041321loop->index = $v21635878953204041321incr + 1; $v21635878953204041321loop->index0 = $v21635878953204041321incr; $v21635878953204041321loop->revindex = $v21635878953204041321loop->length - $v21635878953204041321incr; $v21635878953204041321loop->revindex0 = $v21635878953204041321loop->length - ($v21635878953204041321incr + 1); $v21635878953204041321loop->last = ($v21635878953204041321incr == ($v21635878953204041321loop->length - 1)); ?>

				<tr ng-init="showList[<?php echo $v21635878953204041321loop->index; ?>] = false; cand[<?php echo $v21635878953204041321loop->index; ?>] = <?php echo $job->ID; ?>">
					<td><?php echo $job->title; ?></td>
					<td><?php echo $job->locationnice; ?></td>
					<td><?php echo $job->discipline; ?></td>
					<td>
						<div ng-hide="!showList[<?php echo $v21635878953204041321loop->index; ?>]">
							<ui-select ng-model="updateStatus[<?php echo $v21635878953204041321loop->index; ?>]" theme="bootstrap" on-select="CandStatusChange($item, updateStatus, add, <?php echo $v21635878953204041321loop->index; ?>, 'Job')"  ng-disabled="disabled">
			                    <ui-select-match placeholder="Please Select a Status...">[[$select.selected]]</ui-select-match>
			                    <ui-select-choices repeat="stat in adStatus">
			                      <div ng-bind-html="stat"></div>
			                    </ui-select-choices>
			                </ui-select>
						</div>
						<?php if (($job->status == 1)) { ?>
							<?php $status = 'Unfilled'; ?>
						<?php } else { ?>
							<?php $status = 'Filled'; ?>
						<?php } ?>
						<span ng-init="updateStatus[<?php echo $v21635878953204041321loop->index; ?>]='<?php echo $status; ?>'" ng-hide="showList[<?php echo $v21635878953204041321loop->index; ?>]" ng-bind="updateStatus[<?php echo $v21635878953204041321loop->index; ?>]"></span>
					</td>
					<td>3 days</td>
					<td>
						<a class="btn btn-primary" href="/dashboard/job/<?php echo $job->ID; ?>">view job advert</a>
						<a class="btn btn-success" href="/dashboard/job/<?php echo $job->ID; ?>/candidates/">view applied candidates</a>
						<a class="btn btn-warning" ng-click="showList[<?php echo $v21635878953204041321loop->index; ?>] = true" onclick="return false;" href="">edit job status</a>
						<a class="btn btn-info" href="/dashboard/jobs/update/<?php echo $job->ID; ?>">edit job advert</a>
					</td>
				</tr>

			<?php $v21635878953204041321incr++; } ?>
		</tbody>

	</table>

</div>