
<?php echo $this->getContent(); ?>

<div id="main_content" class="login col-xs-12 col-md-10 col-lg-6 col-centered" ng-controller="userLogin">

<div id="content_box">

<div class="topBox">
    <h2>Login</h2>
    <hr/>
</div>

<div>

    <?php echo $this->tag->form(array('/dashboard', 'id' => 'UserRegisterForm', 'class' => 'form-horizontal', 'role' => 'form')); ?>
          <?php echo $form->render('apiController', array('ng-model' => 'formData.apiController', 'ng-init' => 'formData.apiController="login"')); ?>

          <div class="form-group">
            <?php echo $form->label('username', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
              <?php echo $form->render('username', array('class' => 'form-control', 'ng-model' => 'formData.loginUsername')); ?>
            </div>
          </div>
          <div class="form-group">
            <?php echo $form->label('password', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-9">
              <?php echo $form->render('password', array('class' => 'form-control', 'ng-model' => 'formData.loginPassword')); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
              <div class="checkbox">
                <label>
                  <input name="formData[cookie]" value="true" ng-model="formData.cookie" type="checkbox"/> Remember me
                </label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
              <button class="btn btn-primary ladda-button" ladda="loading" ng-click="submitForm()" onClick="return false;" data-style="expand-right"><span class="ladda-label">Login</span></button>
            </div>
          </div>
    </form>

</div>

</div>

</div>
