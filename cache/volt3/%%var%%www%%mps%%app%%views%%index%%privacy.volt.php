   <div class="hidden alert alert-success">Thanks, we will be in touch shortly.</div>
    <div class="hidden alert alert-danger">There has been an error, please try again later</div>
            <nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img class="whole gapless" src="/imgs/svgs/mps.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/mps.png'" /><span>Match People Skills</span></a>
        </div>
        
        <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right collapse navbar-collapse pull-right">
                <li ng-hide="global.isSignedIn()"><a href="/login">Login</a></li>
                <li ng-show="global.isSignedIn()"><a href="/dashboard">Dashboard</a></li>
                <li ng-show="global.isSignedIn()"><a href="/logout">Logout</a></li>
            </ul>
        </nav>

    </div>
</nav>

<div class="container-fluid no-gutter">

    <!-- Modal -->
    <div id="contactForm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-controller="MarketingController">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 id="myModalLabel">register form</h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-10 col-centered clearfix">
                        <form class="form-horizontal col-lg-12 clearfix">
                            <div class="form-group"><input class="form-control" ng-model="formData.name" type="text" name="name" placeholder="Your Full Name..." /></div>
                            <div class="form-group"><input class="form-control" ng-model="formData.companyName" type="text" name="companyName" placeholder="Your Company Name..."  /></div>
                            <div class="form-group"><input class="form-control" ng-model="formData.email" type="email" name="email" placeholder="Contactable Email Address..." /></div>
                            <div class="form-group"><input class="form-control" ng-model="formData.phone" type="text" name="number" placeholder="Contactable Phone Number..."  /></div>
                            <div class="form-group"><textarea class="form-control" ng-model="formData.message" name="message" placeholder="Tell us about why you are interested in signing up..."></textarea></div>
                        </form>
                    </div>
                </div><!-- End of Modal body -->
                <div class="modal-footer">
                    <div class="col-lg-10 col-centered">
                        <div class="form-group">
                            <button ng-click="submitForm()" class="btn btn-success col-lg-6" type="submit">sign me up</button>
                            <span class="extra pull-right col-lg-6">we aim to respond to all contact request within 24 hours</span>
                        </div>
                    </div>
                </div>
            </div><!-- End of Modal content -->
        </div><!-- End of Modal dialog -->
    </div><!-- End of Modal -->

<div id="homepage">

    <div class="hero anyStretch" data-stretch="/imgs/homepage/hero.jpg">
        <div class="group">
            <h1>keep your people in work</h1>
            <h4>We can help ensure your people are kept in work during quieter times</h4>
            <a class="boxCTA" data-toggle="modal" href="#contactForm">get started</a>
        </div>
    </div>

    <section id="intro" class="clearfix">
        <div class="col-lg-9 legal">
            <h3 class="whole centered col">Privacy Policy</h3>
            <p>Altablue Limited ("We") are committed to protecting and respecting your privacy.</p>
            <p><strong>What does this policy cover?</strong></p>

            <p>This policy (together with our terms of use and any other documents referred to on it) sets out the basis on which any personal data we collect from you, or that you provide to us, on the behalf of another person, will be processed by to use. To the extent that you provide personal data on behalf of another person, you warrant that you have obtained that person's express prior to contsent to submitting their personal information and provided them with a copy of these terms. References in these terms to phrases suych as "your information", or "personal information", refer not only to information relating to you but also includes information that you submit in relation to another person.<br/><br/>By submitting your personal information to us, or by using this website, you agree to the terms of this privacy policy, our Cookies Statement and our Terms of Use. Please ensure you read these documents before using our website.</p>
            
            <p><strong>What if I disagree with all or part of the terms of this policy?</strong></p>

            <p>We ask that you do not use jobs.alta-blue.com (our site) or submit any personal information unless you agree with the terms of this privacy policy. If you have any questions or wish to explore the possibility of submitting information to us other than via our site please email <a href="mailto:data.privacy@alta-blue.com">data.privacy@alta-blue.com</a> with the subject heading "Privacy Policy Query".</p>
            <p><strong> Who is the data controller?</strong></p>

            <p>For the purpose of the Data Protection Act 1998 (the Act), the data controller is Altablue Limited a private company limited by shares registered in Jersey by the Jersey Financial Services Commission Companies Registry having its head office at La Motte Chambers, St Helier, Jersey with company registration No. 15271 and an establishment registered in Scotland under No. BR016918.</p>
            
            <p><strong>What information do you collect?</strong></p>

            <p>Information you submit: We will collect and process personal data (such as your name and contact details) and sensitive data (defined below) that you submit to us (whether submitted via our site, by email or telephone correspondence, or otherwise).<br/><br/>

            You should only provide sensitive data (information about your racial/ethnic origin, political opinions, religious beliefs, trade union memberships, health, sexual life and the commission of offences and related proceedings) if you agree to our processing of this information in accordance with the terms of this privacy policy.<br/><br/>

            Information we collect about you: Each time you visit our site we may automatically collect the following information:</p>
            <ul>
                <li>technical information, including the Internet protocol (IP) address used to connect your computer to the Internet, your login information, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform;</li>
                <li>information about your visit, including the full Uniform Resource Locators (URL) clickstream to, through and from our site (including date and time); vacancies you viewed or searched for; page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), and methods used to browse away from the page and any phone number used to call us.</li>
            </ul>
            <p><em>Accuracy of information you provide us</em></p>

            <p>You warrant that all personal data and information submitted by you to us (whether submitted via our site, by email or telephone correspondence or otherwise) is true, accurate, complete and not misleading.</p>
            <p><strong>How will your information be used?</strong></p>

            <p>Information you give to us: The personal data and sensitive data you provide will be stored, processed and disclosed by us to:</p>
            <ul>
                <li>provide jobs matching services to you and to facilitate the recruitment process.</li>
                <li>assess your suitability for any vacancy which you have applied for and contact you about such vacancy.</li>
                <li>consider and contact you in relation to vacancies which you have not applied for but which we consider you may be suitable for, this may include vacancies outside the country in which you are based.</li>
                <li>permit our clients to assess your suitability for vacancies.</li>
                <li>enable us to send you job alerts and marketing information about our products and services.</li>
                <li>carry out our obligations arising from any contracts entered into between you and us or us and our client.</li>
                <li>answer your questions and enquiries.</li>
                <li>permit third parties to provide services to you or our clients as are necessary to facilitate the recruitment process, including medicals, skills testing and mandatory training courses.</li>
                <li>permit third parties to provide services to us or carry out functions on our behalf such as professional advisors and IT service providers.</li>
                <li>notify you about changes to our services.</li>
            </ul>
            <p><em>Information we collect about you. We will use this information:</em></p>
            <ul>
                <li>to administer our site and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes.</li>
                <li>to improve our site to ensure that content is presented in the most effective manner for you and for your computer.</li>
                <li>to allow you to participate in interactive features of our service, when you choose to do so.</li>
                <li>as part of our efforts to keep our site safe and secure.</li>
                <li>to measure or understand the effectiveness of advertising we serve to you and others, and to deliver relevant advertising to you.</li>
                <li>to make suggestions and recommendations to you and other users of our site about goods or services that may interest you or them.</li>
            </ul>
            <p>We may share your personal information with any member of our group, which means our subsidiaries, our ultimate holding company and its subsidiaries, as defined in section 1159 of the UK Companies Act 2006.</p>

            <p>We may share your information with selected third parties including:</p>
            <ul>
                <li>Business partners, suppliers and sub-contractors for the performance of any contract we enter into with them or you.</li>
                <li>Analytics and search engine providers that assist us in the improvement and optimisation of our site.</li> We may disclose your personal information to third parties:
                <li>In the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets.</li>
                <li>If Altablue Limited or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets.</li>
                <li>If we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce or apply our Terms of Use [or terms and conditions of business and other agreements; or to protect the rights, property, or safety of Altablue Limited, our clients, or others.</li> This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.</li>
            </ul>
            <p><strong>How long will your information be stored?</strong></p>

            <p>We will hold your information for as long as is necessary to comply with our legal and contractual obligations and to in accordance with our legitimate interests as data controller.</p>
            <p><strong>Cookies</strong></p>

            <p>A cookie is a small file of letters and numbers that we store on your browser or the hard drive of your computer if you agree. Our website uses cookies to distinguish you from other users of our website. This helps us to provide you with a good experience when you browse our website and also allows us to improve our site. For detailed information on the cookies we use and the purposes for which we use them see our Cookie Statement.</p>
            <p><strong>Where do we store your personal data?</strong></p>

            <p>The data that we collect from you may be transferred to, and stored at, a destination outside the European Economic Area ("EEA"). It may also be processed by staff operating outside the EEA who work for us or one of our group companies or service providers. Such staff maybe engaged in, among other things, the assessment of your suitability for a vacancy, processing of your information in connection with facilitating the recruitment process and the provision of support services. By submitting your personal information, you agree to this transfer, storing or processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.</p>
            <p><strong>Security</strong></p>

            <p>We have measures in place to keep your information secure. This information may be stored by us either manually or automatically. We take reasonable steps to ensure that the collection and use of your information is in accordance with the Act.<br/><br/>

            Where we have given you (or where you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.</p>
            <p><strong>Sending information over the Internet</strong></p>

            <p>Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.</p>
            <p><strong>Your rights</strong></p>

            <p>You have the right to ask us not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by contacting us at <a href="mailto:info@alta-blue.com">info@alta-blue.com</a>.<br/><br/>

            You have the right to review and amend any personal information held about you by us, for example, if you believe your details may be out of date or incorrect. You also have the right to withdraw your consent to the use of your personal information.</p>
            <p><strong>Access to Information</strong></p>

            <p>The Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. Any access request may be subject to a fee of &pound;10 to meet our costs in providing you with details of the information we hold about you.</p>
            <p><strong>Links to other websites</strong></p>

            <p>Our site may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies. Please check these policies before you submit any personal data to these websites.</p>
            <p><strong>Changes to our privacy policy</strong></p>

            <p>Any changes we may make to our privacy policy in the future will be posted on this page and such change shall be highlighted on our site. Please check back frequently to see any updates or changes to our privacy policy.</p>
            <p><strong>Severability</strong></p>

            <p>In the event that any or part of these terms of use shall be determined by any competent authority to be invalid, unlawful or unenforceable to any extent, such terms shall to that extent be severed from the remaining terms which shall continue to be valid and enforceable to the fullest extent permitted by law.</p>
            <p><strong>Applicable Law</strong></p>

            <p>This privacy policy and your use of this website are subject to and shall be construed in accordance with the laws of England and Wales, although we retain the right to bring proceedings against you for breach of these terms in your country of residence or any other relevant country.</p>
            <p><strong>Contact</strong></p>

            <p>Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to <a href="mailto:info@alta-blue.com">info@alta-blue.com</a></p>
        </div>
    </section>

    <section id="bluebanner" class="clearfix">
        <p class="col-lg-8 col-md-12 col-sm-12">want to get started? register your interest now <a data-toggle="modal" href="#contactForm" class="cta">register now</a></p>
    </section>

    <footer>

        <div class="wrapper col-lg-8 clearfix">
        
            <p class="col-lg-5 col-md-5 col-sm-5">&copy; Copyright <?php echo date('y'); ?> MatchPeopleSkills.com.<br/>All rights reserved.</p>

            <ul class="col-lg-3 col-md-3 col-sm-3">
                <li><h4>Links</h4></li>
                <li><a href="/login">Login</a></li>
                <li><a href="/tcontactus">Contact Us</a></li>
                <li><a href="/terms">Terms &amp; Conditions</a></li>
                <li><a href="/privacy">Privacy Policy</a></li>
                <li><a href="/cookies">Cookies Policy</a></li>
            </ul>

        </div>

    </footer>

</div>