
<?php echo $this->getContent(); ?>


<div class="boxTop">
    <h2>Create Candidate</h2>
    <hr/>
</div>

<div class="regWrapper" ng-controller="candCreate">

    <div ng-bind-html="flashStatus"></div>

    <?php echo $this->tag->form(array('#', 'id' => 'UserRegisterForm', 'onbeforesubmit' => 'return false', 'onClick' => 'return false;', 'role' => 'form')); ?>

        <fieldset>

            <?php echo $form->render('apiController', array('ng-model' => 'formData.apiController', 'ng-init' => 'formData.apiController="candidate"')); ?>

            <input type="hidden" name="boardNum" ng-value="<?php echo $boardNum; ?>" ng-model="formData.boardNum" ng-init="formData.boardNum=<?php echo $boardNum; ?>" />

            <div class="form-group">
                <?php echo $form->label('candidateName', array('class' => 'control-label')); ?>
                <?php echo $form->render('candidateName', array('class' => 'form-control', 'required' => '', 'ng-model' => 'formData.candidateName')); ?>
                <p class="help-block">(required)</p>
            </div>    

            <div class="form-group">
                <?php echo $form->label('candidateEmail', array('class' => 'control-label')); ?>
                <?php echo $form->render('candidateEmail', array('class' => 'form-control', 'required' => '', 'ng-model' => 'formData.candidateEmail')); ?>
                <p class="help-block">(required)</p>

            <div class="form-actions">
            <button class="btn btn-primary ladda-button" ladda="loading" ng-click="submitForm()" data-style="expand-right"><span class="ladda-label">Create Candidate</span></button>

            </div>

        </fieldset>

    </form>

    </form>

</div>