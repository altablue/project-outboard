
<!DOCTYPE>
<!--[if lt IE 7 ]> <html class="ie6" id="root" lang="en"> <![endif]-->
<!--[if IE 7 ]> <html class="ie7" id="root" lang="en"> <![endif]-->
<!--[if IE 8 ]> <html class="ie8" id="root" lang="en"> <![endif]-->
<!--[if gt IE 8 ]> <html class="ie9" id="root" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html id="root" lang="en"> <!--<![endif]-->
    <head>
        <base href="https://www.matchpeopleskills.com/">
        <?php echo $this->tag->getTitle(); ?>
        <?php echo $this->tag->stylesheetLink('css/main.min.css'); ?>
                <meta name="description" content="Match People Skills | Allowing companies to transfer their people expertise to, or share them with organisations that would benefit from these skills.">

    <meta itemprop="name" content="Match People Skills">
    <meta itemprop="description" content="Match People Skills | Allowing companies to transfer their people expertise to, or share them with organisations that would benefit from these skills.">
    <meta itemprop="image" content="https://www.matchpeopleskills.com/_assets/imgs/social/site-prev.jpg">

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Match People Skills | Allowing companies to transfer their people expertise to, or share them with organisations that would benefit from these skills.">
    <meta name="twitter:description" content="Match People Skills | Allowing companies to transfer their people expertise to, or share them with organisations that would benefit from these skills.">
    <meta name="twitter:image:src" content="https://www.matchpeopleskills.com/_assets/imgs/social/site-prev.jpg">

    <meta property="og:title" content="Match People Skills | Allowing companies to transfer their people expertise to, or share them with organisations that would benefit from these skills." />
    <meta property="og:type" content="company" />
    <meta property="og:url" content="https://www.matchpeopleskills.com" />
    <meta property="og:image" content="https://www.matchpeopleskills.com/_assets/imgs/social/site-prev.jpg" />
    <meta property="og:description" content="Match People Skills | Allowing companies to transfer their people expertise to, or share them with organisations that would benefit from these skills." />
    <meta property="og:site_name" content="Match People Skills" />

    <meta name="author" content="Altablue"><meta name='robots' content='index,follow'>    <meta name='copyright' content='Altablue'><meta name='language' content='EN'><meta name='coverage' content='Worldwide'><meta name='distribution' content='Global'><meta name='HandheldFriendly' content='True'><meta http-equiv='Expires' content='0'><meta http-equiv='Pragma' content='no-cache'><meta http-equiv='Cache-Control' content='no-cache'><meta http-equiv='imagetoolbar' content='no'><meta http-equiv='x-dns-prefetch-control' content='off'>            
        <?php echo $this->assets->outputCss(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/es5-shim/4.0.5/es5-shim.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <style>
                .ng-hide {
                    display: none !important;
                }
            </style>
        <![endif]-->
        <style>
            .animate-fadein
            {
                -webkit-transition:all ease-in 0.4s;
                -moz-transition:all ease-in 0.4s;
                -ms-transition:all ease-in 0.4s;
                -o-transition:all ease-in 0.4s;
                transition:all ease-in 0.4s;
                opacity:1;
                display:block;
            }
            div.animate-fadein.hidden
            { 
                opacity:0;
                display:block !important;
                visibility: visible !important;
            }
            span.select2-container{
                width:100% !important;
            }
            .alert{
                margin-bottom:0;
            }
            span.select2-container{
                width:100% !important;
            }
            .cc_logo{
                display:none !important;
            }
        </style>

        <script type="text/javascript" src="/js/libs/browser-detect.js"></script>

        <script type="text/javascript" src="/js/libs/modernizr.min.js"></script>
        <meta name="google-site-verification" content="JbX6LEpaZrccwOBJrUXWQKhoZAFQ_RPkOH7YjL1FzvE" />

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52411705-5', 'auto');
  ga('set', 'forceSSL', true);
  ga('send', 'pageview');

</script>

    </head>
    <body id="ng-app">
        <div remove-class="hidden" class="hidden animate-fadein" ng-controller="accessControl">
            <?php echo $this->getContent(); ?>
        </div>

        <?php 
            $browser = get_browser(null, true);
            if($browser['browser'] == 'IE' && $browser['version'] == '8.0'):?>

            <!-- IE 8 only JS files! -->

            <script>
                document.createElement('ng-include');
                document.createElement('ng-pluralize');
                document.createElement('ng-view');
                // Optionally these for CSS
                document.createElement('ng:include');
                document.createElement('ng:pluralize');
                document.createElement('ng:view');

                  document.createElement('ui-select');
                  document.createElement('ui-select-match');
                  document.createElement('ui-select-choices');

            </script>

            <script src="/js/libs/jquery/jquery.min.js"></script>
            <script src="/js/libs/hmac-sha512.js"></script>
            <script src="https://code.angularjs.org/1.2.27/angular.js"></script>
                <script src="//cdnjs.cloudflare.com/ajax/libs/es5-shim/4.0.5/es5-shim.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/json2/20140204/json2.min.js"></script>
            <script src="https://code.angularjs.org/1.2.27/angular-animate.js"></script>
        
            <script src="/js/dist/ie8.all.js"></script>

            <script src="/js/plugins.js"></script>
            <script src="https://rawgithub.com/javascript/augment/master/augment.js"></script>
            <script src="/js/libs/select2.min.js"></script>   

        <?php else: ?>

            <?php echo $this->tag->javascriptInclude('js/dist/all.js'); ?>

        <?php endif; ?>

        <?php echo $this->tag->javascriptInclude('js/libs/anyStretch.min.js'); ?>
        <?php echo $this->tag->javascriptInclude('js/libs/hmac-sha512.js'); ?>

        <script type="text/javascript" id="cookiebanner" data-moreinfo="/cookies" data-bg="#183043" src="/js/libs/jquery.cookieBanner.js"></script>
        
        <script> 
        var $buoop = {vs:{i:8,f:15,o:12.1,s:5.1},c:2}; 
        function $buo_f(){ 
         var e = document.createElement("script"); 
         e.src = "//browser-update.org/update.js"; 
         document.body.appendChild(e);
        };
        try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
        catch(e){window.attachEvent("onload", $buo_f)}
        </script> 

    </body>
</html>
