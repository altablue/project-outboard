
<?php echo $this->getContent(); ?>

<input type="hidden" name="userType" ng-model="userType" ng-init="userType=<?php echo $userType; ?>" />

<div ng-controller="searchResults" id="searchResults">

	<script type="text/ng-template" id="myModalContent2.html">
      <div class="modal-header">
      	<h3>Details of Your Search:</h3>
        <button type="button" class="close" ng-click="cancel()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        
        <fieldset>
	        <form>

	        	<input class="form-control" ng-model="emamilApi.boardId" ng-init="emailApi.boardId='<?php echo $boardNum; ?>'" type="hidden" disabled="disabled" />

	        	<div class="form-group">
		        	<label>Your Name</label>
		        	<input class="form-control" ng-model="emailApi.name" ng-init="emailApi.name='<?php echo $name; ?>'" type="text" disabled="disabled" value="<?php echo $name; ?>" />
	        	</div>

	        	<div class="form-group">
		        	<label>Company Name</label>
		        	<input class="form-control" ng-model="emailApi.boardName"  ng-init="emailApi.boardName='<?php echo $boardName; ?>'"  type="text" disabled="disabled" value="<?php echo $boardName; ?>" />
		        </div>

		        <div class="form-group">
		        	<label>Required Discipline:</label>
		        	<input class="form-control" type="text" ng-model="emailApi.discipline" disabled="disabled"/>
		        </div>

		        <div class="form-group">
		        	<label>Required Location:</label>
		        	<input class="form-control" ng-model="emailApi.location" disabled="disabled" type="text" />
		        </div>

		        <div class="form-group tables">
		        	<label>Required Skills:</label>
		        	<table class="table table-striped">
		        		<tr ng-repeat="skill in emailApi.searchSkills">
		        			<td ng-bind="skill"></td>
		        		</tr>
		        	</table>
		        </div>

		        <div class="form-group tables">
		        	<label>Required Qualifications:</label>
		        	<table class="table table-striped">
		        		<tr ng-repeat="qual in emailApi.qual track by $index">
		        			<td ng-bind="qual.fos"></td>
		        			<td ng-bind="qual.degName"></td>
		        		</tr>
		        	</table>
		        </div>

		        <div class="form-group">
		        	<label>Required Availability:</label>
		        	<input class="form-control" disabled="disabled" ng-model="emailApi.availability" type="text" />
		        </div>
	        </form>
        </fieldset>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" ng-click="cancel()">Close</button>
        <button type="button" class="btn btn-primary" ng-click="ok()">Send for Help</button>
      </div>
    </script>

	<div>

		<div class="boxTop">
			<h2>Search Results</h2>
			<hr/>
		</div>

		<div class="row clearfix" id="results">

			<div class="col-md-8 col-xs-12 right">

				<div ng-bind-html="flashStatus"></div>

				<div id="sort" class="clearfix row">
					
					<div class="col-lg-5">
						<p>showing <span ng-bind="curHitsRange"><?php echo $curLimit; ?></span> of <span ng-bind="totalHits"><?php echo $total; ?></span> results</p>
					</div>

					<div class="col-lg-3 sortSelect">
						<ui-select name="skills" ng-model="sortBySelect" on-select="sortUpdate($item, sortBySelect, add)" theme="bootstrap" sortable="true" title="Sort By...">
		                    <ui-select-match placeholder="Sort By... ">[[$select.selected.name]]</ui-select-match>
		                    <ui-select-choices ui-disable-choice="sorts.status == false" repeat="sorts in sortBy">
		                        [[sorts.name]]
		                    </ui-select-choices>
		                </ui-select>
					</div>

				</div>

				<div class="spinner" ng-show="loading">
					<center>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="100" height="100" fill="white">
						  <circle fill="#e7e7e7" transform="translate(8 0)" cx="0" cy="16" r="0"> 
						    <animate attributeName="r" values="0; 4; 0; 0" dur="1.2s" repeatCount="indefinite" begin="0"
						      keytimes="0;0.2;0.7;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.6 0.4 0.8" calcMode="spline" />
						  </circle>
						  <circle fill="#e7e7e7" transform="translate(16 0)" cx="0" cy="16" r="0"> 
						    <animate attributeName="r" values="0; 4; 0; 0" dur="1.2s" repeatCount="indefinite" begin="0.3"
						      keytimes="0;0.2;0.7;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.6 0.4 0.8" calcMode="spline" />
						  </circle>
						  <circle fill="#e7e7e7" transform="translate(24 0)" cx="0" cy="16" r="0"> 
						    <animate attributeName="r" values="0; 4; 0; 0" dur="1.2s" repeatCount="indefinite" begin="0.6"
						      keytimes="0;0.2;0.7;1" keySplines="0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.6 0.4 0.8" calcMode="spline" />
						  </circle>
						</svg>	
					</center>				
				</div>
				<div ng-hide="loading || !noResults"><div class="alert alert-warning" role="alert">No results were found for your search term. <a href="#" ng-click="helpFill()" onClick="return false;" ladda="emailLoading" data-style="expand-right" data-toggle="modal" data-target=".bs-example-modal-lg"  class="btn btn-success ladda-button">help me find results</a></div></div>

				<div ng-hide="loading" ng-repeat="candidate in searchResults.hits.hits" class="resultHit">

					<div class="row" ng-if="candidate._source.type == 1">
						<img src="/imgs/avatar/default.png" class="col-md-2 img-circle" />
						<div class="col-md-5 details">
							<p>[[ candidate._source.title ]]</p>
							<p>[[ candidate._source.discipline ]]</p>
							<p>[[ candidate._source.locationnice ]]</p>
						</div>
						<div class="col-md-2"><img ng-if="candidate._source.availability >= 35" src="/imgs/svgs/fulltime.svg" /><img ng-if="candidate._source.availability < 35" src="/imgs/svgs/parttime.svg" /></div>
						<div class="col-md-3">
							<a href="/dashboard/candidates/[[ candidate._source.id ]]" class="btn btn-primary ladda-button" data-style="expand-right"><span class="ladda-label">View Profile</span></a>
						</div>
					</div>
					<div class="row" ng-if="candidate._source.type == 2">
						<img src="/imgs/avatar/job.png" class="col-md-2 img-circle" />
						<div class="col-md-5 details">
							<p>[[ candidate._source.title ]]</p>
							<p>[[ candidate._source.discipline ]]</p>
							<p>[[ candidate._source.locationnice ]]</p>
						</div>
						<div class="col-md-2 avail"><img ng-if="candidate._source.availability >= 35" src="/imgs/svgs/fulltime.svg" /><img ng-if="candidate._source.availability < 35" src="/imgs/svgs/parttime.svg" /></div>
						<div class="col-md-3">
							<a href="/dashboard/job/[[ candidate._source.id ]]" class="btn btn-primary ladda-button" data-style="expand-right"><span class="ladda-label">View Job</span></a>
						</div>
					</div>
					<hr/>
				</div>
 
				<center><pagination total-items="bigTotalItems" ng-model="currentPage" rotate="false" ng-change="submitForm()" max-size="maxSize" num-pages="numPages" onclick="return false;" class="pagination-sm" boundary-links="true"></pagination></center>

			</div>

			<div class="col-md-4 col-xs-12 left filters">
				<form method="GET">

					<h2>Filters</h2>
					<hr/>
					
					<input type="hidden" name="searchParams.apiController" ng-model="searchParams.apiController" ng-init="searchParams.apiController='search'" value="search" />

					<div class="form-group">
						<label for="exampleInputEmail1">Keyword</label>
						<input type="text" class="form-control" ng-init='searchParams.keyword="<?php echo $searchQueries->keyword; ?>"' ng-model="searchParams.keyword" name="keyword" id="exampleInputEmail1" placeholder="Keyword">
					</div>

					<div class="form-group" ng-init='refreshDisciplines(<?php echo $disciplines; ?>); searchParams.discipline="<?php echo $searchQueries->discipline; ?>"'>
						<label for="exampleInputPassword1">Discipline</label>
						<ui-select ng-if="!ieFallback" name="discipline" ng-model="searchParams.discipline" theme="bootstrap" on-select="discUpdate($item, formData, add)" on-remove="discUpdate($item, formData, delete)" ng-disabled="disabled">
		                    <ui-select-match placeholder="Please Select a Discipline from the list below...">[[$select.selected]]</ui-select-match>
		                    <ui-select-choices repeat="discipline in disciplines">
		                      <div ng-bind-html="discipline"></div>
		                    </ui-select-choices>
		                </ui-select>
		                <select class="selectTwoNorm"  ng-if="ieFallback" ng-model="searchParams.discipline" style="width:100%;"> 
		                	<?php foreach ($rawDisc as $disc) { ?>
								<option value="<?php echo $disc; ?>"><?php echo $disc; ?></option>
		                	<?php } ?>
		                </select>
	                </div>

					<div class="form-group">
						<label for="exampleInputPassword1">Location</label>
						<input type="text" class="form-control" name="location" ng-change="checkLoc()" ng-init='searchParams.location="<?php echo $searchQueries->location; ?>"; checkLoc();' ng-model="searchParams.location" id="exampleInputPassword1" placeholder="Location">
					</div>

					<div class="form-group" id="skills" ng-init='setupSkills(<?php echo $searchQueries->skills; ?>)'>
						<label for="exampleInputPassword1">Skills</label>
						<ui-select ng-if="!ieFallback" multiple tagging name="skills" tagging-label="(search for this skill)" on-select="skillUpdate($item, searchParams, add)" on-remove="skillUpdate($item, searchParams, delete)" ng-model="searchParams.searchSkills" theme="bootstrap" sortable="true" ng-disabled="disabled" title="Start typing a skill (use commas to seperate skills) ..." class="form-control">
		                    <ui-select-match placeholder="Start typing a skill (use commas to seperate skills) ...">[[$item]]</ui-select-match>
		                    <ui-select-choices repeat="skill in searchParams.searchSkills">
		                        [[skill]]
		                    </ui-select-choices>
		                </ui-select>

		                <input type="text"  ng-if="ieFallback" class="tagging selectTwo" ng-model="searchParams.searchSkills" multiple="multiple" select-two="{ tags: true,tokenSeparators: [',']}" style="width:100%;" />

					</div>
					<?php if (($searchQueries->qual == 'null')) { ?>
						<?php $quals = '[]'; ?>
					<?php } else { ?>
						<?php $quals = $searchQueries->qual; ?>
					<?php } ?>
					<div  id="quals" class="form-group" ng-init='searchParams.qual=<?php echo $quals; ?>'>
						<label ng-init='refreshDegrees(<?php echo $degrees; ?>);' for="exampleInputPassword1">Qualifications</label>
						<div class="singleQual" ng-repeat="qual in searchParams.qual track by $index">
							<span class="hide">[[ fosName = 'qual['+$index+'][fos]' ]][[ degName = 'qual['+$index+'][fos]' ]]</span>
							<input type="text" class="form-control qual" name="[[ fosName ]]"  ng-model="qual.fos" placeholder="Field of Study">
							
							<ui-select ng-if="!ieFallback" ng-model="qual.degName" theme="bootstrap" name="[[ degName ]]" on-select="discUpdate($item, qual, add)" on-remove="discUpdate($item, qual, delete)" ng-disabled="disabled">
			                    <ui-select-match placeholder="Please Select a Degree from the list below...">[[$select.selected]]</ui-select-match>
			                    <ui-select-choices repeat="degree in degrees">
			                      <div ng-bind-html="degree"></div>
			                    </ui-select-choices>
			                </ui-select>

			                <select class="selectTwoNorm" ng-if="ieFallback" ng-model="qual.degName" style="width:100%;">
				               <?php if (isset($qualifications)) { ?><?php foreach ($qualifications as $qual) { ?>
									<option value="<?php echo $qual; ?>"><?php echo $qual; ?></option>
			                	<?php } ?><?php } ?>
			                </select>

			                <a href="#" ng-click="removeQual($index);" onclick="return false;" class="btn btn-primary">remove qualification</a>

			                <hr/>
		                </div>
		                <a href="#" ng-click="addQual();" onclick="return false;" class="btn btn-success">add qualification</a>
					</div>

					<div class="form-group" ng-init='searchParams.availability="<?php echo $searchQueries->availability; ?>"'>
						<label for="exampleInputPassword1">Availability</label>
						<div class="input-group">
					  		<input type="text" id="exampleInputPassword1" ng-model="searchParams.availability" class="form-control" name="availability" placeholder="Availability" aria-describedby="basic-addon2">
					  		<span class="input-group-addon" id="basic-addon2">hrs/week</span>
						</div>
					</div>

					<hr  ng-init="submitForm()"/>

					<button class="btn btn-primary ladda-button" ng-click="submitForm(searchParams)" rotate="false" id="filterSubmit" data-style="expand-right"><span class="ladda-label">Update Results</span></button>
				</form>
			</div>

		</div>

	</div>

</div>
