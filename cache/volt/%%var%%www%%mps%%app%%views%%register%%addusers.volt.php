
<?php echo $this->getContent(); ?>


<div class="boxTop">
    <h2>Register Users</h2>
    <hr/>
</div>

<div class="regWrapper" ng-controller="companyCreate">

    <div ng-bind-html="flashStatus"></div>

    <?php echo $this->tag->form(array('#', 'id' => 'UserRegisterForm', 'onbeforesubmit' => 'return false', 'onClick' => 'return false', 'role' => 'form')); ?>

        <input type="hidden" ng-init="formData.type=2" ng-model="formData.type" />

        <fieldset>

            <?php echo $form->render('apiController', array('ng-model' => 'formData.apiController', 'ng-init' => 'formData.apiController="users"')); ?>

            <input type="hidden" name="boardNum" ng-value="<?php echo $boardNum; ?>" ng-model="formData.boardNum" ng-init="formData.boardNum=<?php echo $boardNum; ?>" />

            <div class="form-group">
                <?php echo $form->label('compUserName', array('class' => 'control-label')); ?>
                <?php echo $form->render('compUserName', array('class' => 'form-control', 'required' => '', 'ng-model' => 'formData.compUserName')); ?>
                <p class="help-block">(required)</p>
            </div>    

            <div class="form-group">
                <?php echo $form->label('compUserEmail', array('class' => 'control-label')); ?>
                <?php echo $form->render('compUserEmail', array('class' => 'form-control', 'required' => '', 'ng-model' => 'formData.compUserEmail')); ?>
                <p class="help-block">(required)</p>
            </div> 

            <div class="form-group">
                <?php echo $form->label('userLevel', array('class' => 'control-label')); ?>
                <?php echo $form->render('userLevel', array('class' => 'form-control', 'required' => '', 'ng-model' => 'formData.userLevel')); ?>
                <p class="help-block">(required)</p>
            </div> 

            <div class="form-actions">
                <button class="btn btn-primary ladda-button" ladda="loading" ng-click="submitForm()" data-style="expand-right"><span class="ladda-label">Register User</span></button>
                <?php echo $this->tag->submitButton(array('Continue', 'class' => 'btn btn-default', 'ng-click' => 'continue()', 'disabled' => 'disabled')); ?>

            </div>

        </fieldset>

    </form>

</div>