<nav id="sideNav" ng-class="{'slide': showFullmenu == true}" class="col-lg-3 col-md-2 col-sm-2 col-xs-2 no-gutter" ng-init="showFullmenu = false;">
	<ul>
		<li><a href="/dashboard" <?php if ($currentURI == '/dashboard') { ?> class="active" <?php } ?>><span class="glyphicon glyphicon-dashboard"></span><span>Dashboard</span></a></li>
		<li ng-if="!global.isOnBoardComp() && global.isBoardUser() || global.isABStaff()"><a href="/dashboard/candidates/create" <?php if ($currentURI == '/dashboard/candidates/create') { ?> class="active" <?php } ?> ><span class="fa fa-user-plus"></span><span>Create Candidate</span></a></li>
		<li ng-if="!global.isOnBoardComp() && global.isBoardUser()"><a href="/dashboard/board/candidates/<?php echo $boardNum; ?>" <?php if ($currentURI == '/dashboard/board/candidates/{{ boardNum }}') { ?> class="active" <?php } ?>><span class="fa fa-users"></span><span>Show our Candidates</span></a></li>

		<li ng-if="global.isOnBoardComp() && global.isBoardUser() || global.isABStaff()"><a href="/dashboard/jobs/create" <?php if ($currentURI == '/dashboard/jobs/create') { ?> class="active" <?php } ?>><span class="fa fa-plus-square"></span><span>Create Job Advert</span></a></li>
		<li ng-if="global.isOnBoardComp() && global.isBoardUser()"><a href="/dashboard/board/jobs/<?php echo $boardNum; ?>" <?php if ($currentURI == '/dashboard/board/jobs/{{ boardNum }}') { ?> class="active" <?php } ?>><span class="fa fa-newspaper-o"></span><span>Show Our Active Jobs</span></a></li>

		<li ng-if="global.isCandidateUser()"><a href="/dashboard/candidates/update/<?php echo $candidateId; ?>" <?php if ($currentURI == '/dashboard/candidates/update/21') { ?> class="active" <?php } ?>><span class="fa fa-pencil-square-o"></span><span>Edit My Profile</span></a></li>
		<li ng-if="global.isCandidateUser()"><a href="/dashboard/candidates/<?php echo $candidateId; ?>" <?php if ($currentURI == '/dashboard/candidates/{{ candId }}') { ?> class="active" <?php } ?>><span class="fa fa-eye"></span><span>View My Profile</span></a></li>


		<li ng-if="global.isSignedIn() && global.isABStaff()"><a href="/dashboard/recruiter/<?php echo $id; ?>/boards/"><span class="fa fa-building-o"></span><span>view your companies</span></a></li>

		<?php if ($logged) { ?>
	
			<li><a href="/dashboard/search/results"><span class="fa fa-search"></span><span>Search</span></a></li>

		<?php } ?>

		<li ng-hide="showFullmenu" class="hidden-lg hidden-md hidden-sm"><a href="#" onclick="return false;" ng-click="showFullmenu=true"><span class="fa fa-angle-double-right"></span></a></li>
		<li ng-hide="!showFullmenu" class="hidden-lg hidden-md hidden-sm"><a href="#" onclick="return false;" ng-click="showFullmenu=false"><span class="close fa fa-angle-double-left"></span></a></li>
	</ul>
</nav>
