
<?php echo $this->getContent(); ?>


<div class="boxTop">
	<h2><?php echo $bName; ?>'s Users</h2>
	<hr/>
</div>

<div class="getAllWrapper" ng-controller="recruitEdit">

	<div ng-bind-html="flashStatus"></div>

	<table class="table table-striped footable">

	<thead>
		<tr>
			<th data-sort-ignore="true"<strong>User Name</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>User Email</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>User Level</strong></th>
		</tr>
	</thead>

	<?php $v160752056021295559441iterator = $userList; $v160752056021295559441incr = 0; $v160752056021295559441loop = new stdClass(); $v160752056021295559441loop->length = count($v160752056021295559441iterator); $v160752056021295559441loop->index = 1; $v160752056021295559441loop->index0 = 1; $v160752056021295559441loop->revindex = $v160752056021295559441loop->length; $v160752056021295559441loop->revindex0 = $v160752056021295559441loop->length - 1; ?><?php foreach ($v160752056021295559441iterator as $user) { ?><?php $v160752056021295559441loop->first = ($v160752056021295559441incr == 0); $v160752056021295559441loop->index = $v160752056021295559441incr + 1; $v160752056021295559441loop->index0 = $v160752056021295559441incr; $v160752056021295559441loop->revindex = $v160752056021295559441loop->length - $v160752056021295559441incr; $v160752056021295559441loop->revindex0 = $v160752056021295559441loop->length - ($v160752056021295559441incr + 1); $v160752056021295559441loop->last = ($v160752056021295559441incr == ($v160752056021295559441loop->length - 1)); ?>

		<tr ng-init="showList[<?php echo $v160752056021295559441loop->index; ?>] = false; cand[<?php echo $v160752056021295559441loop->index; ?>] = <?php echo $cands->id; ?>">
			<td><?php echo $user->name; ?></td>
			<td><?php echo $user->username; ?></td>
			<td>
				<?php if (($user->level == 1)) { ?>
					<?php $status = 'User'; ?>
				<?php } else { ?>
					<?php $status = 'Admin User'; ?>
				<?php } ?>
				<span ng-init="updateStatus[<?php echo $v160752056021295559441loop->index; ?>]='<?php echo $status; ?>'" ng-hide="showList[<?php echo $v160752056021295559441loop->index; ?>]" ng-bind="updateStatus[<?php echo $v160752056021295559441loop->index; ?>]"></span>
			</td>
		</tr>

	<?php $v160752056021295559441incr++; } ?>

	</table>

</div>