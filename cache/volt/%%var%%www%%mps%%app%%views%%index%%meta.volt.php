    <meta name="description" content="Match People Skills | Allowing companies to transfer their people expertise to, or share them with organisations that would benefit from these skills.">

    <meta itemprop="name" content="Match People Skills">
    <meta itemprop="description" content="Match People Skills | Allowing companies to transfer their people expertise to, or share them with organisations that would benefit from these skills.">
    <meta itemprop="image" content="https://www.matchpeopleskills.com/_assets/imgs/social/site-prev.jpg">

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Match People Skills | Allowing companies to transfer their people expertise to, or share them with organisations that would benefit from these skills.">
    <meta name="twitter:description" content="Match People Skills | Allowing companies to transfer their people expertise to, or share them with organisations that would benefit from these skills.">
    <meta name="twitter:image:src" content="https://www.matchpeopleskills.com/_assets/imgs/social/site-prev.jpg">

    <meta property="og:title" content="Match People Skills | Allowing companies to transfer their people expertise to, or share them with organisations that would benefit from these skills." />
    <meta property="og:type" content="company" />
    <meta property="og:url" content="https://www.matchpeopleskills.com" />
    <meta property="og:image" content="https://www.matchpeopleskills.com/_assets/imgs/social/site-prev.jpg" />
    <meta property="og:description" content="Match People Skills | Allowing companies to transfer their people expertise to, or share them with organisations that would benefit from these skills." />
    <meta property="og:site_name" content="Match People Skills" />

    <meta name="author" content="Altablue"><meta name='robots' content='index,follow'>    <meta name='copyright' content='Altablue'><meta name='language' content='EN'><meta name='coverage' content='Worldwide'><meta name='distribution' content='Global'><meta name='HandheldFriendly' content='True'><meta http-equiv='Expires' content='0'><meta http-equiv='Pragma' content='no-cache'><meta http-equiv='Cache-Control' content='no-cache'><meta http-equiv='imagetoolbar' content='no'><meta http-equiv='x-dns-prefetch-control' content='off'>