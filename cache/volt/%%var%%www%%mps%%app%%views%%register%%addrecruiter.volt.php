
<?php echo $this->getContent(); ?>

<div class="boxTop">
	<h2>Register Recruiter</h2>
	<hr/>
</div>

<div class="regWrapper" ng-controller="recruiterCreate">

    <div ng-bind-html="flashStatus"></div>

    <?php echo $this->tag->form(array('#', 'id' => 'UserRegisterForm', 'onbeforesubmit' => 'return false', 'onClick' => 'return false', 'role' => 'form')); ?>

        <fieldset>

            <?php echo $form->render('apiController', array('ng-model' => 'formData.apiController', 'ng-init' => 'formData.apiController="recruiterReg"')); ?>

            <input type="hidden" name="boardNum" ng-value="<?php echo $boardNum; ?>" ng-model="formData.boardNum" ng-init="formData.boardNum=<?php echo $boardNum; ?>" />

            <div class="form-group">
                <?php echo $form->label('compUserName', array('class' => 'control-label')); ?>
                <?php echo $form->render('compUserName', array('class' => 'form-control', 'required' => '', 'ng-model' => 'formData.compUserName')); ?>
            </div>    

            <div class="form-group">
                <?php echo $form->label('compUserEmail', array('class' => 'control-label')); ?>
                <?php echo $form->render('compUserEmail', array('class' => 'form-control', 'required' => '', 'ng-model' => 'formData.compUserEmail')); ?>
            </div> 

            <div class="form-group">
                <label ng-init='refreshComps(<?php echo $companyList; ?>)'>Recruiter for:</label>
                
                <div ng-repeat="assignCompany in assignedCompanies track by $index">

                    <span class="hide">[[ compAssign = 'comp['+$index+']' ]]</span>

                    <ui-select ng-model="formData.compAssign[$index]" theme="bootstrap" on-select="compUpdate($item, compAssign, 'add',$index)" on-remove="compUpdate($item, compAssign, 'delete')" name="[[ compAssign ]]" search-enabled="true" reset-search-input="true" ng-disabled="disabled" reset-search-input="true" search-enabled="true">
                        <ui-select-match placeholder="Please Select a Company...">[[$select.selected.name]]</ui-select-match>
                        <ui-select-choices ui-disable-choice="company.status == false" repeat="company in companies | propsFilter: {name: $select.search}">
                          <div ng-bind-html="company.name"></div>
                        </ui-select-choices>
                    </ui-select>

                    <a href="#" ng-click="compAssignRemove($index);" onclick="return false;" class="btn btn-primary">remove company</a>

                    <hr/>
                </div>

            </div>

            <a href="#" class="btn btn-primary" ng-click="assignAnother()" onclick="return false;">Assign Another Company</a>

            <div class="form-actions">
                <button class="btn btn-primary ladda-button" ladda="loading" ng-click="submitForm()" data-style="expand-right"><span class="ladda-label">Register Recruiter</span></button>
            </div>

        </fieldset>

    </form>

</div>
