<nav class="navbar navbar-default navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button ng-init="navCollapsed = true" ng-click="navCollapsed = !navCollapsed" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img class="whole gapless" src="/imgs/svgs/mps.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/mps.png'" /><span>Match People Skills</span></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div collapse="navCollapsed" class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-right">

                <?php if ($logged) { ?>
                    <li id="searchBar" class="hidden-md hidden-lg hidden-sm">
                        <form action="/dashboard/results/" role="search" method="GET" ng-if="global.isSignedIn()">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                                        </span>
                                        <input type="text" class="form-control" name="keyword" placeholder="Search for...">
                                    </div><!-- /input-group -->
                                </div><!-- /.col-lg-6 -->
                            </div><!-- /.row -->
                        </form>
                    </li>
                <?php } ?>

                <li ng-hide="global.isSignedIn()"><a href="/login">Login</a></li>
                <li ng-show="global.isSignedIn()"><a href="/dashboard">Dashboard</a></li>

                <?php if ($logged) { ?>
                    <!--<li ng-show="global.isSignedIn()"><a href="#">Your Account</a></li>-->
                    <li ng-if="global.isOnBoardComp() && global.isBoardUser() || global.isABStaff()"><a href="/dashboard/jobs/create">Create Job Advert</a></li>
                    <li ng-if="!global.isOnBoardComp() && global.isBoardUser() || global.isABStaff()"><a href="/dashboard/candidates/create">Add Candidate</a></li>
                    <li ng-if="global.isSignedIn() && global.isAdminUser() && !global.isABStaff() || global.isABStaff()"><a href="/register/addUsers/<?php echo $boardNum; ?>">Register User</a></li>
                    <li ng-if="global.isSignedIn() && global.isABStaff()"><a href="/register/">Register Company</a></li>
                    <li ng-if="global.isSignedIn() && global.isSuperStaff()"><a href="/register/recruiter/">Register Recruiter</a></li>
                    <li ng-if="global.isCandidateUser()"><a href="/dashboard/candidates/update/<?php echo $candidateId; ?>">Edit My Profile</a></li>
                <?php } ?>

                <li ng-show="global.isSignedIn()"><a href="/logout">Logout</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container-fluid no-gutter">

    <?php if ($logged) { ?>

    <nav id="sideNav" ng-class="{'slide': showFullmenu == true}" class="col-lg-3 col-md-2 col-sm-2 col-xs-2 no-gutter" ng-init="showFullmenu = false;">
	<ul>
		<li><a href="/dashboard" <?php if ($currentURI == '/dashboard') { ?> class="active" <?php } ?>><span class="glyphicon glyphicon-dashboard"></span><span>Dashboard</span></a></li>
		<li ng-if="!global.isOnBoardComp() && global.isBoardUser() || global.isABStaff()"><a href="/dashboard/candidates/create" <?php if ($currentURI == '/dashboard/candidates/create') { ?> class="active" <?php } ?> ><span class="fa fa-user-plus"></span><span>Create Candidate</span></a></li>
		<li ng-if="!global.isOnBoardComp() && global.isBoardUser()"><a href="/dashboard/board/candidates/<?php echo $boardNum; ?>" <?php if ($currentURI == '/dashboard/board/candidates/{{ boardNum }}') { ?> class="active" <?php } ?>><span class="fa fa-users"></span><span>Show our Candidates</span></a></li>

		<li ng-if="global.isOnBoardComp() && global.isBoardUser() || global.isABStaff()"><a href="/dashboard/jobs/create" <?php if ($currentURI == '/dashboard/jobs/create') { ?> class="active" <?php } ?>><span class="fa fa-plus-square"></span><span>Create Job Advert</span></a></li>
		<li ng-if="global.isOnBoardComp() && global.isBoardUser()"><a href="/dashboard/board/jobs/<?php echo $boardNum; ?>" <?php if ($currentURI == '/dashboard/board/jobs/{{ boardNum }}') { ?> class="active" <?php } ?>><span class="fa fa-newspaper-o"></span><span>Show Our Active Jobs</span></a></li>

		<li ng-if="global.isCandidateUser()"><a href="/dashboard/candidates/update/<?php echo $candidateId; ?>" <?php if ($currentURI == '/dashboard/candidates/update/21') { ?> class="active" <?php } ?>><span class="fa fa-pencil-square-o"></span><span>Edit My Profile</span></a></li>
		<li ng-if="global.isCandidateUser()"><a href="/dashboard/candidates/<?php echo $candidateId; ?>" <?php if ($currentURI == '/dashboard/candidates/{{ candId }}') { ?> class="active" <?php } ?>><span class="fa fa-eye"></span><span>View My Profile</span></a></li>


		<li ng-if="global.isSignedIn() && global.isABStaff()"><a href="/dashboard/recruiter/<?php echo $id; ?>/boards/"><span class="fa fa-building-o"></span><span>view your companies</span></a></li>

		<?php if ($logged) { ?>
	
			<li><a href="/dashboard/search/results"><span class="fa fa-search"></span><span>Search</span></a></li>

		<?php } ?>

		<li ng-hide="showFullmenu" class="hidden-lg hidden-md hidden-sm"><a href="#" onclick="return false;" ng-click="showFullmenu=true"><span class="fa fa-angle-double-right"></span></a></li>
		<li ng-hide="!showFullmenu" class="hidden-lg hidden-md hidden-sm"><a href="#" onclick="return false;" ng-click="showFullmenu=false"><span class="close fa fa-angle-double-left"></span></a></li>
	</ul>
</nav>


    <section id="main_content" class="col-lg-9 col-md-10 col-sm-10 col-xs-10 no-gutter" role="main">

        <div ng-bind-html="global.flashStatus"></div>

        <?php echo $this->flash->output(); ?>

        <div id="content_box" class="clearfix">

            <?php echo $this->getContent(); ?>

        </div>

        <footer class="row no-gutter">
        </footer>
    </section>

    <?php } else { ?>

        <div ng-bind-html="global.flashStatus"></div>

        <?php echo $this->flash->output(); ?>

        <?php echo $this->getContent(); ?>

    <?php } ?>
</div>
