	<div class="boxTop">
		<h2>Your Profile's Statistics</h2>
		<hr/>
	</div>

	<div class="charts">

		<div class="completedProfiles firstGraph col-lg-4 col-md-12 col-sm-12 col-xs-12 no-gutter">
			<div class="circle">
				<p><?php echo $views; ?>><span>views</span></p>
			</div>
			<svg  version="1.1" style="width:100%; max-width:250px;" height="250" x="0px" y="0px" viewBox="0 0 250 250" xml:space="preserve">
				<g transform="translate(0, 250) scale(1, -1)">
					<?php if (isset($dayView)) { ?>
					<rect x="25" y="14" fill="#38D9BE" width="21" height="<?php echo $dayView[0]; ?>">
						<animate attributeName="height" from="0" to="<?php echo $dayView[0]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<rect x="59.3" y="14" fill="#38D9BE" width="21" height="<?php echo $dayView[1]; ?>">
						<animate attributeName="height" from="0" to="<?php echo $dayView[1]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<rect x="130" y="14" fill="#38D9BE" width="21" height="<?php echo $dayView[2]; ?>">
						<animate attributeName="height" from="0" to="<?php echo $dayView[2]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<rect x="95.3" y="14" fill="#38D9BE" width="21" height="<?php echo $dayView[3]; ?>">
						<animate attributeName="height" from="0" to="<?php echo $dayView[3]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<rect x="200.7" y="14" fill="#38D9BE" width="21" height="<?php echo $dayView[4]; ?>">
						<animate attributeName="height" from="0" to="<?php echo $dayView[4]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<rect x="165" y="14" fill="#38D9BE" width="21" height="<?php echo $dayView[5]; ?>">
						<animate attributeName="height" from="0" to="<?php echo $dayView[5]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<?php } ?>
				</g>
				<line fill="none" stroke="#E8E8E6" stroke-width="2" stroke-miterlimit="10" x1="15" y1="235" x2="235" y2="235"/>
				<text text-anchor="middle" x="125" y="80" width="250" font-family="'Lato', sans-serif" font-size="52.3664" letter-spacing="2"><?php if (isset($views)) { ?><?php echo $views; ?><?php } ?></text>
				<text transform="matrix(1 0 0 1 95.8794 109.8516)" font-family="'Lato', sans-serif" font-size="19.8632">VIEWS</text>
			</svg>
		</div>

		<div class="viewedCandidates jobsDiscipline secondGraph tables col-lg-8 col-md-12 col-sm-12 col-sm-12 no-gutter clearfix">
			<h6>Latest Jobs in your Discipline <span>(<?php if (isset($discipline)) { ?><?php echo $discipline; ?><?php } ?>)</span></h6>
			<table class="table table-striped footable">
				<?php if (isset($latestJobs)) { ?>
					<?php if (($latestJobs !== false)) { ?>
						<thead>
							<tr>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
							</tr>
						</thead>
						<?php foreach ($latestJobs as $job) { ?>
							<tr>
								<td><img src="/imgs/svgs/advert.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/advert.png'" /></td>
								<td><?php echo $job->title; ?></td>
								<td><?php echo $job->locationnice; ?></td>
								<td><a href="/dashboard/job/<?php echo $job->ID; ?>" class="btn btn-green btn-success" data-style="expand-right"><span class="ladda-label">view job</span></a></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<tr><td><strong><em>No data available</em></strong></td></tr>
						</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<tr><td><strong><em>No data available</em></strong></td></tr>
					</tr>
				<?php } ?>
			</table>
		</div>

		<div class="uncompletedCandidates applicationStatus thirdGraph tables row col-lg-12 col-md-12 col-sm-12 col-sm-12 no-gutter">
			<hr />
			<h6>Application Status</h6>
			<table class="table table-striped footable">
				<thead>
					<tr>
						<th data-sort-ignore="true"></th>
						<th data-sort-ignore="true"></th>
						<th data-sort-ignore="true" data-hide="phone"></th>
						<th data-sort-ignore="true" data-hide="phone"></th>
						<th data-sort-ignore="true" data-hide="phone"></th>
					</tr>
				</thead>
				<?php if (isset($appliedJobs)) { ?>
					<?php if (($appliedJobs !== false)) { ?>
						<?php foreach ($appliedJobs as $job) { ?>
							<tr>
								<td><img src="/imgs/svgs/advert.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/advert.png'" /></td>
								<td><?php echo $job->job->title; ?></td>
								<td><?php echo $job->job->locationnice; ?></td>
								<td>
									<?php if (($job->job->status == 1)) { ?>
										<span class="applied">Applied</span>
									<?php } elseif (($job->job->status == 2)) { ?>
										<span class="fail">Unsuccessful</span>
									<?php } elseif (($job->job->status == 3)) { ?>
										<span class="interview">Interview Stage</span>
									<?php } else { ?>
										<span class="success">Successful</span>
									<?php } ?>
								</td>
								<td><a href="/dashboard/job/<?php echo $job->job->ID; ?>" class="btn btn-success ladda-button" data-style="expand-right"><span class="ladda-label">review job</span></a></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
					<tr>
						<tr><td><strong><em>No data available</em></strong></td></tr>
					</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<tr><td><strong><em>No data available</em></strong></td></tr>
					</tr>
				<?php } ?>
			</table>
		</div>

	</div>