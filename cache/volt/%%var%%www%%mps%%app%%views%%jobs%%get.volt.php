<?php echo $this->getContent(); ?>


<div id="profiles" class="clearfix"  ng-controller="theProfiles">

    <div ng-bind-html="flashStatus"></div>

    <script type="text/ng-template" id="fillModal.html">
      <div class="modal-header">
        <h3>Contact Request:</h3>
        <button type="button" class="close" ng-click="cancel()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        
        <fieldset>
            <form>

                <input class="form-control" ng-model="fillApi.boardId" ng-init="fillApi.boardId='<?php echo $boardNumId; ?>'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="fillApi.job.id" ng-init="fillApi.job.id='<?php echo $jobId; ?>'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="fillApi.job.comp" ng-init="fillApi.job.comp='<?php echo str_replace("'", "",$jobComp); ?>'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="fillApi.candId" ng-init="fillApi.candId='<?php echo $candidateId; ?>'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="fillApi.job.title" ng-init="fillApi.job.title='<?php echo $title; ?>'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="fillApi.candComp" ng-init="fillApi.candComp='<?php echo str_replace("'", "",$boardName); ?>'" type="hidden" disabled="disabled" />

                <div class="form-group">
                    <label>Your Name</label>
                    <input class="form-control" ng-model="fillApi.name" ng-init="fillApi.name='<?php echo $name; ?>'" type="text" disabled="disabled" value="<?php echo $name; ?>" />
                </div>

                <p>Please confirm you would like to apply for the following position:</p>

                <div class="form-group clearfix">
                    <label class="col-xs-12">Position Overview:</label>
                    <br/><br/>
                    <div class="row col-xs-6">
                        <div class="left col-xs-4">
                            <figure>
                                <img style="max-width:100%" alt="User Pic" src="/imgs/avatar/job.png" class="img-circle"> 
                            </figure>
                        </div>
                        <div class="right col-xs-6">
                            <p>Job #<?php echo $id; ?></p>
                            <p><?php echo $discipline; ?></p>
                            <p><?php echo $locationnice; ?></p>
                            <p><?php echo $availability; ?> hs/pw</p>
                        </div>
                    </div>
                </div>

                <div class="alert alert-warning"><strong>NOTE:</strong> Applying for a position will send your contact details to the company this advert belongs to and their assigned recruiters.</div>
            </form>
        </fieldset>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" ng-click="cancel()">Close</button>
        <button type="button" class="btn btn-primary" ng-click="ok()">Send Contact Request</button>
      </div>
    </script>

    <div class="boxTop">

        <a href="javascript:history.back()" class="btn btn-primary ladda-button btn-primary" data-style="expand-right"><span class="ladda-label"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;&nbsp;Return to Search Results</span></a>
        <br/><br/>

        <h2>view advert: Job #<?php echo $jobId; ?></h2>
        <hr/>
    </div>

    <div class="theDeets col-xs-12">
        
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" id="avatar">
            <figure>
                <img alt="User Pic" src="/imgs/avatar/job.png" class="img-circle"> 
            </figure>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="profile">
           <article class="details">
                    <?php if($userLevel >= 99): ?>
                            <p><?php echo $company; ?></p>
                            <hr/>
                        <?php endif; ?>
                <p><?php echo $title; ?></p>
                <p><?php echo $discipline; ?></p>
                <p><?php echo $locationnice; ?></p>
                <p>Required Availability: <span><?php echo $availability; ?></span></p>
                <hr/>
                <p class="bio"><?php echo nl2br($profile); ?></p>
                <hr/>
                <div class="skills">
                    <?php foreach ($skills as $skill) { ?>
                        <span><?php echo $skill; ?></span>
                    <?php } ?>
                </div>
                <hr/>
                <table class="table table-striped footable">
                    <thead class="hidden-lg hidden-md hidden-sm">
                        <th></th>
                        <th></th>
                        <th data-hide="phone"></th>
                        <th data-hide="phone"></th>
                        <th data-hide="phone"></th>
                    </thead>
                    <tbody>
                        <?php if ($this->length($qualifications) > 1) { ?>
                            <?php $v173020777290175765081iterator = $qualifications; $v173020777290175765081incr = 0; $v173020777290175765081loop = new stdClass(); $v173020777290175765081loop->length = count($v173020777290175765081iterator); $v173020777290175765081loop->index = 1; $v173020777290175765081loop->index0 = 1; $v173020777290175765081loop->revindex = $v173020777290175765081loop->length; $v173020777290175765081loop->revindex0 = $v173020777290175765081loop->length - 1; ?><?php foreach ($v173020777290175765081iterator as $qual) { ?><?php $v173020777290175765081loop->first = ($v173020777290175765081incr == 0); $v173020777290175765081loop->index = $v173020777290175765081incr + 1; $v173020777290175765081loop->index0 = $v173020777290175765081incr; $v173020777290175765081loop->revindex = $v173020777290175765081loop->length - $v173020777290175765081incr; $v173020777290175765081loop->revindex0 = $v173020777290175765081loop->length - ($v173020777290175765081incr + 1); $v173020777290175765081loop->last = ($v173020777290175765081incr == ($v173020777290175765081loop->length - 1)); ?>
                                <tr>
                                    <td><img src="/imgs/svgs/qual-grey.svg" /></td>
                                    <td class="hidden-lg hidden-md hidden-sm">Degree #<?php echo $v173020777290175765081loop->index; ?></td>
                                    <td><?php echo $qual->candQualTitle; ?></td>
                                    <td><?php echo $qual->candQualDegree; ?></td>
                                    <td><?php echo $qual->candQualField; ?></td>
                                </tr>
                            <?php $v173020777290175765081incr++; } ?>
                        <?php } ?>
                    </tbody>
                </table>
                <hr/>
                 <button  ng-click="assignVac()" onClick="return false;" ladda="emailLoading" data-style="expand-right" data-toggle="modal" data-target=".bs-example-modal-lg"  class="btn btn-warning ladda-button"><span class="ladda-label">apply for job</span></button>
            </article>
        </div>
    </div>
</div>
