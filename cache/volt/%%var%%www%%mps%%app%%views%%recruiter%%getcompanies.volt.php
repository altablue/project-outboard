
<?php echo $this->getContent(); ?>


<div class="boxTop row">
	<div class="row">
		<h2 class="col-lg-6">Your Recruiter Companies</h2>
		<form class="col-lg-4 offset-col-lg-2">
	        <div class="row">
	          <div class="col-lg-12">
	            <div class="input-group">
	              <span class="input-group-btn">
	                <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
	              </span>
	              <input type="text" class="form-control" name="keyword" placeholder="Search for...">
	            </div><!-- /input-group -->
	          </div><!-- /.col-lg-6 -->
	        </div><!-- /.row -->
		</form>
	</div>
	<hr/>
</div>

<div class="getAllWrapper">

	<table class="table table-striped footable">

	<thead>
		<tr>
			<th data-sort-ignore="true"><strong>Company Name</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>Company Location</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>Company Type</strong></th>
			<th data-sort-ignore="true" data-hide="phone"></th>
			<th data-sort-ignore="true" data-hide="phone"></th>
		</tr>
	</thead>

	<?php foreach ($companies as $comps) { ?>

		<tr>
			<td><?php echo $comps->name; ?></td>
			<td><?php echo $comps->location; ?></td>
			<td><?php echo $comps->type; ?></td>
			<td><a href="/dashboard/board/users/<?php echo $comps->id; ?>/" class="btn btn-primary">view company users</a></td>
			<td>
				<a href="/dashboard/board/candidates/<?php echo $comps->id; ?>/" class="btn btn-primary">view company candidates</a><br/><br/>
				<a href="/dashboard/board/jobs/<?php echo $comps->id; ?>/" class="btn btn-primary">view company job adverts</a>
			</td>
		</tr>

	<?php } ?>

	</table>

</div>