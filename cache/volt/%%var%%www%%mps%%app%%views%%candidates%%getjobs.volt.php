
<?php echo $this->getContent(); ?>

<div class="boxTop">
	<h2>Jobs Candidate #<?php echo $candId; ?> Applied for</h2>
	<hr/>
</div>

<div class="getAllWrapper" ng-controller="recruitEdit">

	<div ng-bind-html="flashStatus"></div>
	
	<table class="table table-striped">

	<thead>
		<tr>
			<td><strong>job id</strong></td>
			<td><strong>job title</strong></td>
			<td><strong>company</strong></td>
			<td><strong>location</strong></td>
			<td><strong>application status</strong></td>
			<td></td>
		</tr>
	</thead>

	<?php $v59445257243306457401iterator = $jobs; $v59445257243306457401incr = 0; $v59445257243306457401loop = new stdClass(); $v59445257243306457401loop->length = count($v59445257243306457401iterator); $v59445257243306457401loop->index = 1; $v59445257243306457401loop->index0 = 1; $v59445257243306457401loop->revindex = $v59445257243306457401loop->length; $v59445257243306457401loop->revindex0 = $v59445257243306457401loop->length - 1; ?><?php foreach ($v59445257243306457401iterator as $appJob) { ?><?php $v59445257243306457401loop->first = ($v59445257243306457401incr == 0); $v59445257243306457401loop->index = $v59445257243306457401incr + 1; $v59445257243306457401loop->index0 = $v59445257243306457401incr; $v59445257243306457401loop->revindex = $v59445257243306457401loop->length - $v59445257243306457401incr; $v59445257243306457401loop->revindex0 = $v59445257243306457401loop->length - ($v59445257243306457401incr + 1); $v59445257243306457401loop->last = ($v59445257243306457401incr == ($v59445257243306457401loop->length - 1)); ?>
		
		<tr ng-init="showList[<?php echo $v59445257243306457401loop->index; ?>] = false; cand[<?php echo $v59445257243306457401loop->index; ?>] = <?php echo $candId; ?>">
			<td><?php echo $appJob->job->ID; ?></td>
			<td><?php echo $appJob->job->title; ?></td>
			<td><?php echo $appJob->job->boards->name; ?></td>
			<td><?php echo $appJob->job->locationnice; ?></td>
			<td>
				<div ng-hide="!showList[<?php echo $v59445257243306457401loop->index; ?>]">
					<ui-select ng-model="updateStatus[<?php echo $v59445257243306457401loop->index; ?>]" theme="bootstrap" on-select="CandStatusChange($item, updateStatus, add, <?php echo $v59445257243306457401loop->index; ?>, 'Fill', <?php echo $appJob->job->ID; ?>)"  ng-disabled="disabled">
	                    <ui-select-match placeholder="Please Select a Status...">[[$select.selected]]</ui-select-match>
	                    <ui-select-choices repeat="stat in applyStats">
	                      <div ng-bind-html="stat"></div>
	                    </ui-select-choices>
	                </ui-select>
				</div>
				<?php if (($appJob->status == 1)) { ?>
					<?php $status = 'Applied'; ?>
				<?php } elseif (($appJob->status == 2)) { ?>
					<?php $status = 'Unsuccessful'; ?>
				<?php } elseif (($appJob->status == 3)) { ?>
					<?php $status = 'Interview Stage'; ?>
				<?php } else { ?>
					<?php $status = 'Successful'; ?>
				<?php } ?>
				<span ng-init="updateStatus[<?php echo $v59445257243306457401loop->index; ?>]='<?php echo $status; ?>'" ng-hide="showList[<?php echo $v59445257243306457401loop->index; ?>]" ng-bind="updateStatus[<?php echo $v59445257243306457401loop->index; ?>]"></span>
			</td>
			<td>
				<a class="btn btn-primary" href="/dashboard/jobs/<?php echo $appJob->job->ID; ?>">view advert</a>
				<a class="btn btn-warning" ng-click="showList[<?php echo $v59445257243306457401loop->index; ?>] = true" onclick="return false;" href="">update application status</a>
			</td>
		</tr>

	<?php $v59445257243306457401incr++; } ?>

	</table>

</div>