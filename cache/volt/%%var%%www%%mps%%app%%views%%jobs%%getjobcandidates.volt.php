
<?php echo $this->getContent(); ?>

<div class="boxTop">
	<h2>Candidates Applied for Job #<?php echo $id; ?></h2>
	<hr/>
</div>

<div class="getAllWrapper" ng-controller="recruitEdit">

	<div ng-bind-html="flashStatus"></div>
	
	<table class="table table-striped" ng-init="jobId = <?php echo $id; ?>">

	<thead>
		<tr>
			<td><strong>candidate name</strong></td>
			<td><strong>candidate email</strong></td>
			<td><strong>contact number</strong></td>
			<td><strong>candidate id</strong></td>
			<td><strong>application status</strong></td>
			<td></td>
		</tr>
	</thead>

	<?php $v11895827464639722311iterator = $cands; $v11895827464639722311incr = 0; $v11895827464639722311loop = new stdClass(); $v11895827464639722311loop->length = count($v11895827464639722311iterator); $v11895827464639722311loop->index = 1; $v11895827464639722311loop->index0 = 1; $v11895827464639722311loop->revindex = $v11895827464639722311loop->length; $v11895827464639722311loop->revindex0 = $v11895827464639722311loop->length - 1; ?><?php foreach ($v11895827464639722311iterator as $cand) { ?><?php $v11895827464639722311loop->first = ($v11895827464639722311incr == 0); $v11895827464639722311loop->index = $v11895827464639722311incr + 1; $v11895827464639722311loop->index0 = $v11895827464639722311incr; $v11895827464639722311loop->revindex = $v11895827464639722311loop->length - $v11895827464639722311incr; $v11895827464639722311loop->revindex0 = $v11895827464639722311loop->length - ($v11895827464639722311incr + 1); $v11895827464639722311loop->last = ($v11895827464639722311incr == ($v11895827464639722311loop->length - 1)); ?>
		
		<tr ng-init="showList[<?php echo $v11895827464639722311loop->index; ?>] = false; cand[<?php echo $v11895827464639722311loop->index; ?>] = <?php echo $cand->candidate->id; ?>">
			<td><?php echo $cand->candidate->users->name; ?></td>
			<td><?php echo $cand->candidate->users->username; ?></td>
			<td><?php echo $cand->candidate->phone; ?></td>
			<td><?php echo $cand->candidate->id; ?></td>
			<td>
				<div ng-hide="!showList[<?php echo $v11895827464639722311loop->index; ?>]">
					<ui-select ng-model="updateStatus[<?php echo $v11895827464639722311loop->index; ?>]" theme="bootstrap" on-select="CandStatusChange($item, updateStatus, add, <?php echo $v11895827464639722311loop->index; ?>, 'Fill', <?php echo $id; ?>)"  ng-disabled="disabled">
	                    <ui-select-match placeholder="Please Select a Status...">[[$select.selected]]</ui-select-match>
	                    <ui-select-choices repeat="stat in applyStats">
	                      <div ng-bind-html="stat"></div>
	                    </ui-select-choices>
	                </ui-select>
				</div>
				<?php if (($cand->status == 1)) { ?>
					<?php $status = 'Applied'; ?>
				<?php } elseif (($cand->status == 2)) { ?>
					<?php $status = 'Unsuccessful'; ?>
				<?php } elseif (($cand->status == 3)) { ?>
					<?php $status = 'Interview Stage'; ?>
				<?php } else { ?>
					<?php $status = 'Successful'; ?>
				<?php } ?>
				<span ng-init="updateStatus[<?php echo $v11895827464639722311loop->index; ?>]='<?php echo $status; ?>'" ng-hide="showList[<?php echo $v11895827464639722311loop->index; ?>]" ng-bind="updateStatus[<?php echo $v11895827464639722311loop->index; ?>]"></span>
			</td>
			<td>3 days</td>
			<td>
				<a class="btn btn-primary" href="/dashboard/candidates/<?php echo $cand->candidate->id; ?>">view profile</a>
				<?php if (($userLevel >= 99)) { ?><a class="btn btn-warning" ng-click="showList[<?php echo $v11895827464639722311loop->index; ?>] = true" onclick="return false;" href="">update application status</a><?php } ?>
			</td>
		</tr>

	<?php $v11895827464639722311incr++; } ?>

	</table>

</div>
