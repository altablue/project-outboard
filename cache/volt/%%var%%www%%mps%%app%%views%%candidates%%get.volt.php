<?php echo $this->getContent(); ?>


<div id="profiles" class="clearfix" ng-controller="theProfiles">

    <div ng-bind-html="flashStatus"></div>

    <script type="text/ng-template" id="contactModal.html">
      <div class="modal-header">
        <h3>Contact Request:</h3>
        <button type="button" class="close" ng-click="cancel()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        
        <fieldset>
            <form>

                <input class="form-control" ng-model="emailApi.boardId" ng-init="emailApi.boardId='<?php echo $board; ?>'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="emailApi.candId" ng-init="emailApi.candId='<?php echo $id; ?>'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="emailApi.candComp" ng-init="emailApi.candComp='<?php echo $candComp; ?>'" type="hidden" disabled="disabled" />
                <input class="form-control" ng-model="emailApi.type" ng-init="emailApi.type='cand'" type="hidden" disabled="disabled" />

                <div class="form-group">
                    <label>Your Name</label>
                    <input class="form-control" ng-model="emailApi.name" ng-init="emailApi.name='<?php echo $name; ?>'" type="text" disabled="disabled" value="<?php echo $name; ?>" />
                </div>

                <div class="form-group">
                    <label>Company Name</label>
                    <input class="form-control" ng-model="emailApi.boardName"  ng-init="emailApi.boardName='<?php echo $boardName; ?>'"  type="text" disabled="disabled" value="<?php echo $boardName; ?>" />
                </div>

                <p>Please confirm you would like to register your intention of contacting the following candidate:</p>

                <div class="form-group">
                    <label class="col-xs-12">Candidate Overview:</label>
                    <br/><br/>
                    <div class="row col-xs-6">
                        <div class="left col-xs-4">
                            <figure>
                                <img style="max-width:100%" alt="User Pic" src="/imgs/avatar/default.png" class="img-circle"> 
                            </figure>
                        </div>
                        <div class="right col-xs-6">
                            <p>Candidate #<?php echo $id; ?></p>
                            <p><?php echo $discipline; ?></p>
                            <p><?php echo $locationnice; ?></p>
                            <p><?php echo $availability; ?> hrs/pw</p>
                        </div>
                    </div>
                </div>
            </form>
        </fieldset>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" ng-click="cancel()">Close</button>
        <button type="button" class="btn btn-primary" ng-click="ok()">Send Contact Request</button>
      </div>
    </script>

    <?php if($userLevel >= 99): ?>
        <script type="text/ng-template" id="fillModal.html">
          <div class="modal-header">
            <h3>Contact Request:</h3>
            <button type="button" class="close" ng-click="cancel()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            
            <fieldset>
                <form>

                    <input class="form-control" ng-model="fillApi.boardId" ng-init="fillApi.boardId='<?php echo $board; ?>'" type="hidden" disabled="disabled" />
                    <input class="form-control" ng-model="fillApi.candId" ng-init="fillApi.candId='<?php echo $id; ?>'" type="hidden" disabled="disabled" />
                    <input class="form-control" ng-model="fillApi.candComp" ng-init="fillApi.candComp='<?php echo $candComp; ?>'" type="hidden" disabled="disabled" />
                    <input class="form-control" ng-model="fillApi.type" ng-init="fillApi.type='cand'" type="hidden" disabled="disabled" />
                    <div class="form-group" ng-init='refreshJobs(<?php echo json_encode($jobs); ?>);'>
                        <label>Assign to job:</label>
                        <div ng-if="browserDect.browser != 'Explorer' && browserDect.version > 8">
                            <ui-select ng-model="fillApi.job" theme="bootstrap" reset-search-input="true" search-enabled="true">
                                <ui-select-match placeholder="Please Select a Job from the list below...">[[$select.selected.title]]</ui-select-match>
                                <ui-select-choices repeat="j in job | propsFilter: {title: $select.search, comp: $select.search}">
                                  [[j.title]] - <em>([[j.comp]])</em>
                                </ui-select-choices>
                            </ui-select>
                        </div>
                    </div>

                    <p>Please confirm you would like to assign the following candidate to the vancy selected above:</p>

                    <div class="form-group">
                        <label class="col-xs-12">Candidate Overview:</label>
                        <br/><br/>
                        <div class="row col-xs-6">
                            <div class="left col-xs-4">
                                <figure>
                                    <img style="max-width:100%" alt="User Pic" src="/imgs/avatar/default.png" class="img-circle"> 
                                </figure>
                            </div>
                            <div class="right col-xs-6">
                                <p>Candidate #<?php echo $id; ?></p>
                                <p><?php echo $discipline; ?></p>
                                <p><?php echo $locationnice; ?></p>
                                <p><?php echo $availability; ?> hrs/pw</p>
                            </div>
                        </div>
                    </div>
                </form>
            </fieldset>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" ng-click="cancel()">Close</button>
            <button type="button" class="btn btn-primary" ng-click="ok()">Send Contact Request</button>
          </div>
        </script>
    <?php endif; ?>

    <div class="boxTop">

        <a href="javascript:history.back()" class="btn btn-primary ladda-button btn-primary" data-style="expand-right"><span class="ladda-label"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;&nbsp;Return to Search Results</span></a>
        <br/><br/>

        <h2>view profile: candidate #<?php echo $id; ?></h2>
        <hr/>
    </div>

    <div class="theDeets col-xs-12">
        
        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" id="avatar">
            <figure>
                <img alt="User Pic" src="/imgs/avatar/default.png" class="img-circle"> 
            </figure>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" id="profile">
           <article class="details">
                <?php if($userLevel >= 99): ?>
                    <p><strong>name: </strong> <?php echo $candName; ?></p>
                    <p><strong>email: </strong> <?php echo $email; ?></p>
                    <p><strong>phone #:</strong> <?php echo $phone; ?></p>
                    <hr/>
                <?php endif; ?>
                <p>
                <p><?php echo $title; ?></p>
                <p><?php echo $discipline; ?></p>
                <p><?php echo $locationnice; ?></p>
                <p>Available for <span><?php echo $availability; ?></span><em> hrs/pw</em></p>
                <hr/>
                <p class="bio"><?php echo $profile; ?></p>
                <hr/>
                <div class="skills">
                    <?php foreach ($skills as $skill) { ?>
                        <span><?php echo $skill; ?></span>
                    <?php } ?>
                </div>
                <hr/>
                <table class="table table-striped footable">
                    <thead class="hidden-lg hidden-md hidden-sm">
                        <th></th>
                        <th></th>
                        <th data-hide="phone"></th>
                        <th data-hide="phone"></th>
                        <th data-hide="phone"></th>
                    </thead>
                    <tbody>
                        <?php $v105914902292703599661iterator = $qualifications; $v105914902292703599661incr = 0; $v105914902292703599661loop = new stdClass(); $v105914902292703599661loop->length = count($v105914902292703599661iterator); $v105914902292703599661loop->index = 1; $v105914902292703599661loop->index0 = 1; $v105914902292703599661loop->revindex = $v105914902292703599661loop->length; $v105914902292703599661loop->revindex0 = $v105914902292703599661loop->length - 1; ?><?php foreach ($v105914902292703599661iterator as $qual) { ?><?php $v105914902292703599661loop->first = ($v105914902292703599661incr == 0); $v105914902292703599661loop->index = $v105914902292703599661incr + 1; $v105914902292703599661loop->index0 = $v105914902292703599661incr; $v105914902292703599661loop->revindex = $v105914902292703599661loop->length - $v105914902292703599661incr; $v105914902292703599661loop->revindex0 = $v105914902292703599661loop->length - ($v105914902292703599661incr + 1); $v105914902292703599661loop->last = ($v105914902292703599661incr == ($v105914902292703599661loop->length - 1)); ?>
                            <tr>
                                <td><img src="/imgs/svgs/qual-grey.svg" /></td>
                                <td class="hidden-lg hidden-md hidden-sm">Degree #<?php echo $v105914902292703599661loop->index; ?></td>
                                <td><?php echo $qual->candQualTitle; ?></td>
                                <td><?php echo $qual->candQualDegree; ?></td>
                                <td><?php echo $qual->candQualField; ?></td>
                            </tr>
                        <?php $v105914902292703599661incr++; } ?>
                    </tbody>
                </table>
                <hr/>
                <button  ng-if="global.isOnBoardComp() && global.isBoardUser()"  ng-click="contactThis()" onClick="return false;" ladda="emailLoading" data-style="expand-right" data-toggle="modal" data-target=".bs-example-modal-lg"  class="btn btn-warning ladda-button"><span class="ladda-label">contact candidate</span></button>

                 <?php if($userLevel >= 99): ?>
                    <button  ng-if="global.isABStaff()"  ng-click="assignVac()" onClick="return false;" ladda="vacLoading" data-style="expand-right" data-toggle="modal" data-target=".bs-example-modal-lg"  class="btn btn-warning ladda-button"><span class="ladda-label">assign to vancancy</span></button>
                <?php endif; ?>

            </article>
        </div>
    </div>
</div>