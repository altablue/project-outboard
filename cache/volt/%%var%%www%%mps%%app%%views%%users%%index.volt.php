
<?php echo $this->getContent(); ?>

<?php if ($boardType == 1 && $type == 2) { ?>

	
	<div class="boxTop">
		<h2>Your Company Statistics</h2>
		<hr/>
	</div>

	<div class="charts"  ng-controller="theDashes">

		<script type="text/ng-template" id="dashboardMatch.html">
	      <div class="modal-header">
	      	<h3>Job Advert Fields:</h3>
	        <button type="button" class="close" ng-click="cancel()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	        
	        <fieldset>
		        <form>
		        	<input class="form-control" ng-model="emamilApi.boardId" value="<?php echo $boardNum; ?>" ng-init="emailApi.boardId='<?php echo $boardNum; ?>'" type="hidden" disabled="disabled" />

		        	<div class="form-group">
			        	<label>Your Name</label>
			        	<input class="form-control" ng-model="emailApi.name" value="<?php echo $name; ?>" ng-init="emailApi.name='<?php echo $name; ?>'" type="text" disabled="disabled"  />
		        	</div>

		        	<div class="form-group">
			        	<label>Company Name</label>
			        	<input class="form-control" ng-model="emailApi.boardName" value="<?php echo $boardName; ?>" ng-init="emailApi.boardName='<?php echo $boardName; ?>'"  type="text" disabled="disabled" />
			        </div>

			        <div class="form-group">
			        	<label>Required Discipline:</label>
			        	<input class="form-control" type="text" ng-model="emailApi.discipline" disabled="disabled"/>
			        </div>

			        <div class="form-group">
			        	<label>Required Location:</label>
			        	<input class="form-control" ng-model="emailApi.location" disabled="disabled" type="text" />
			        </div>

			        <div class="form-group tables">
			        	<label>Required Skills:</label>
			        	<table class="table table-striped">
			        	[[searchSkills]]
			        		<tr ng-repeat="skill in emailApi.searchSkills">
			        			<td ng-bind="skill"></td>
			        		</tr>
			        	</table>
			        </div>

			        <div class="form-group tables">
			        	<label>Required Qualifications:</label>
			        	<table class="table table-striped">
			        		<tr ng-repeat="qual in emailApi.qual track by $index">
			        			<td ng-bind="qual.candQualTitle"></td>
			        			<td ng-bind="qual.candQualDegree"></td>
			        			<td ng-bind="qual.candQualField"></td>
			        		</tr>
			        	</table>
			        </div>

			        <div class="form-group">
			        	<label>Required Availability:</label>
			        	<input class="form-control" disabled="disabled" ng-model="emailApi.availability" type="text" />
			        </div>
		        </form>
	        </fieldset>

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" ng-click="cancel()">Close</button>
	        <button type="button" class="btn btn-primary" ng-click="ok()">Send for Help</button>
	      </div>
	    </script>		

		<div class="completedProfiles firstGraph col-lg-4 col-md-4 col-sm-12 col-xs-12 no-gutter">
			<div class="circle">
				<p>16<span>%</span><span>active jobs</span></p>
			</div>
			<svg version="1.1" height="250" viewbox="0 0 250 250" style="width:100%; max-width:250px;"  x="0px" y="0px">
			<line fill="none" stroke="#E8E8E6" stroke-width="2" stroke-miterlimit="10" x1="15" y1="235" x2="235" y2="235"></line>
			<g transform="translate(0, 235) scale(1, -1)">
				<?php if (isset($dayJobs)) { ?>
				<line fill="none" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" x1="34.1" y1="<?php echo $dayJobs[5]; ?>" x2="70.2" y2="<?php echo $dayJobs[4]; ?>">
					<animate attributeName="y1" from="0" to="<?php echo $dayJobs[5]; ?>" begin="0s" dur="0.5s" keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
					<animate attributeName="y2" from="0" to="<?php echo $dayJobs[4]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
				</line>
				<line fill="none" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" x1="70.2" y1="<?php echo $dayJobs[4]; ?>" x2="106.3" y2="<?php echo $dayJobs[3]; ?>">
					<animate attributeName="y1" from="0" to="<?php echo $dayJobs[4]; ?>" begin="0s" dur="0.5s" keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
					<animate attributeName="y2" from="0" to="<?php echo $dayJobs[3]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
				</line>
				<line fill="none" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" x1="106.3" y1="<?php echo $dayJobs[3]; ?>" x2="142.3" y2="<?php echo $dayJobs[2]; ?>">
					<animate attributeName="y1" from="0" to="<?php echo $dayJobs[3]; ?>" begin="0s" dur="0.5s" keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
					<animate attributeName="y2" from="0" to="<?php echo $dayJobs[2]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
				</line>
				<line fill="none" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" x1="142.3" y1="<?php echo $dayJobs[2]; ?>" x2="178.4" y2="<?php echo $dayJobs[1]; ?>">
					<animate attributeName="y1" from="0" to="<?php echo $dayJobs[2]; ?>" begin="0s" dur="0.5s" keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
					<animate attributeName="y2" from="0" to="<?php echo $dayJobs[1]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
				</line>
				<line fill="none" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" x1="178.4" y1="<?php echo $dayJobs[1]; ?>" x2="214.5" y2="<?php echo $dayJobs[0]; ?>">
					<animate attributeName="y1" from="0" to="<?php echo $dayJobs[1]; ?>" begin="0s" dur="0.5s" keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
					<animate attributeName="y2" from="0" to="<?php echo $dayJobs[0]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline"></animate>
				</line>

				<circle fill="#FFFFFF" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" cx="34.1" cy="<?php echo $dayJobs[5]; ?>" r="6.4">
					<animate attributeName="cy" from="0" to="<?php echo $dayJobs[5]; ?>" begin="0s" dur="0.5s" keySplines=" 0.1 0.8 0.2 1" calcMode="spline"></animate>
				</circle>
				<circle fill="#FFFFFF" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" cx="70.2" cy="<?php echo $dayJobs[4]; ?>" r="6.4">
					<animate attributeName="cy" from="0" to="<?php echo $dayJobs[4]; ?>" begin="0s" dur="0.5s" keySplines=" 0.1 0.8 0.2 1" calcMode="spline"></animate>
				</circle>
				<circle fill="#FFFFFF" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" cx="142.3" cy="<?php echo $dayJobs[3]; ?>" r="6.4">
					<animate attributeName="cy" from="0" to="<?php echo $dayJobs[3]; ?>" begin="0s" dur="0.5s" keySplines=" 0.1 0.8 0.2 1" calcMode="spline"></animate>
				</circle>
				<circle fill="#FFFFFF" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" cx="106.3" cy="<?php echo $dayJobs[2]; ?>" r="6.4">
					<animate attributeName="cy" from="0" to="<?php echo $dayJobs[2]; ?>" begin="0s" dur="0.5s" keySplines=" 0.1 0.8 0.2 1" calcMode="spline"></animate>
				</circle>
				<circle fill="#FFFFFF" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" cx="178.4" cy="<?php echo $dayJobs[1]; ?>" r="6.4">
					<animate attributeName="cy" from="0" to="<?php echo $dayJobs[1]; ?>" begin="0s" dur="0.5s" keySplines=" 0.1 0.8 0.2 1" calcMode="spline"></animate>
				</circle>
				<circle fill="#FFFFFF" stroke="#38D9BE" stroke-width="4" stroke-miterlimit="10" cx="214.5" cy="<?php echo $dayJobs[0]; ?>" r="6.4">
					<animate attributeName="cy" from="0" to="<?php echo $dayJobs[0]; ?>" begin="0s" dur="0.5s" keySplines=" 0.1 0.8 0.2 1" calcMode="spline"></animate>
				</circle>
				<?php } ?>
			</g>

			<text transform="matrix(1 0 0 1 97.3828 84.3779)" fill="#3D3E42" font-family="'Lato', sans-serif" font-size="53.0289" letter-spacing="2"><?php echo $numJobs; ?></text>
			<text transform="matrix(1 0 0 1 63.834 108.018)" fill="#3D3E42" font-family="'Lato', sans-serif" font-size="19.1997" letter-spacing="1">ACTIVE JOBS</text>
			</svg>
		</div>

		<div class="uncompletedCandidates applicationStatus thirdGraph tables col-lg-8 col-md-8 col-sm-12 col-sm-12 no-gutter" ng-switch on="mobile">
			<h6>Most Viewed Adverts</h6>
			<div ng-switch-when="true">
				<table class="table table-striped footable toggle-small default">
					<thead>
						<tr>
							<th data-sort-ignore="true"></th>
							<th data-sort-ignore="true"></th>
							<th data-sort-ignore="true" data-hide="phone"></th>
							<th data-sort-ignore="true" data-hide="phone"></th>
							<th data-toggle="true" data-sort-ignore="true"></th>
						</tr>
					</thead>
					<?php if (isset($dayJobs)) { ?>
						<?php if (($dayJobs !== false)) { ?>
							<?php foreach ($popJobs as $job) { ?>
								<tr>
									<td><img src="/imgs/svgs/advert.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/advert.png'" /></td>
									<td><?php echo $job->title; ?></td>
									<td><?php echo $job->locationnice; ?></td>
									<td><?php echo $viewStats[$job->ID]; ?> Views</td>
									<td></td>
								</tr>
							<?php } ?>
						<?php } else { ?>
						<tr>
							<td>No job advert data available</td>
						</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<td>No job advert data available</td>
						</tr>
					<?php } ?>
				</table>
			</div>
			<div ng-switch-default>
				<table class="table table-striped toggle-small default">
					<?php if (isset($popJobs)) { ?>
						<?php if (($popJobs !== false)) { ?>
							<?php foreach ($popJobs as $job) { ?>
								<tr>
									<td><img src="/imgs/svgs/advert.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/advert.png'" /></td>
									<td><?php echo $job->title; ?></td>
									<td><?php echo $job->locationnice; ?></td>
									<td><?php echo $viewStats[$job->ID]; ?> Views</td>
									<td></td>
								</tr>
							<?php } ?>
						<?php } else { ?>
						<tr>
							<td>No job advert data available</td>
						</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<td>No job advert data available</td>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>

		<div class="uncompletedCandidates applicationStatus thirdGraph tables row col-lg-12 col-md-12 col-sm-12 col-sm-12 no-gutter">
			<hr />
			<h6>Oldest Unfilled Advert</h6>
			<table class="table table-striped footable toggle-small default">
				<thead>
					<tr>
						<th data-toggle="false" data-sort-ignore="true"></th>
						<th data-sort-ignore="true"></th>
						<th data-sort-ignore="true" data-hide="phone"></th>
						<th data-sort-ignore="true" data-hide="phone"></th>
						<th data-sort-ignore="true" data-hide="phone"></th>
					</tr>
				</thead>
				<?php if (isset($unfilled)) { ?>
					<?php if (($unfilled !== false)) { ?>
						<?php $v111344721537286682371iterator = $unfilled; $v111344721537286682371incr = 0; $v111344721537286682371loop = new stdClass(); $v111344721537286682371loop->length = count($v111344721537286682371iterator); $v111344721537286682371loop->index = 1; $v111344721537286682371loop->index0 = 1; $v111344721537286682371loop->revindex = $v111344721537286682371loop->length; $v111344721537286682371loop->revindex0 = $v111344721537286682371loop->length - 1; ?><?php foreach ($v111344721537286682371iterator as $index => $job) { ?><?php $v111344721537286682371loop->first = ($v111344721537286682371incr == 0); $v111344721537286682371loop->index = $v111344721537286682371incr + 1; $v111344721537286682371loop->index0 = $v111344721537286682371incr; $v111344721537286682371loop->revindex = $v111344721537286682371loop->length - $v111344721537286682371incr; $v111344721537286682371loop->revindex0 = $v111344721537286682371loop->length - ($v111344721537286682371incr + 1); $v111344721537286682371loop->last = ($v111344721537286682371incr == ($v111344721537286682371loop->length - 1)); ?>
							<script></script>
							<tr>
								<td><img src="/imgs/svgs/advert-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/advert-red.png'" /></td>
								<td><?php echo $job->title; ?></td>
								<td><?php echo $job->locationnice; ?></td>
								<td><?php echo floor((time() - strtotime($job->created))/(60*60*24));  ?>  Days</td>
								<td>
									<input type="hidden" disabled="disabled" ng-init="emailApi[<?php echo $v111344721537286682371loop->index; ?>].discipline='<?php echo $job->discipline; ?>'" />
									<input type="hidden" disabled="disabled" ng-init="emailApi[<?php echo $v111344721537286682371loop->index; ?>].location='<?php echo $job->locationnice; ?>'" />
									<input type="hidden" disabled="disabled" ng-init="emailApi[<?php echo $v111344721537286682371loop->index; ?>].availability='<?php echo $job->availability; ?>'" />
									<input type="hidden" disabled="disabled" ng-init='emailApi[<?php echo $v111344721537286682371loop->index; ?>].searchSkills="<?php echo addslashes(json_encode(unserialize($job->skills))); ?>"' />
									<input type="hidden" disabled="disabled" ng-init='emailApi[<?php echo $v111344721537286682371loop->index; ?>].qual="<?php echo addslashes(json_encode(unserialize($job->qualifications))); ?>"' />
									<button ng-click="helpFill(<?php echo $v111344721537286682371loop->index; ?>)" onClick="return false;" ladda="emailLoading[<?php echo $v111344721537286682371loop->index; ?>]" data-style="expand-right" data-toggle="modal" data-target=".bs-example-modal-lg"  class="btn btn-success ladda-button"><span class="ladda-label">help me match</span></button>
								</td>
							</tr>
						<?php $v111344721537286682371incr++; } ?>
					<?php } else { ?>
					<tr>
						<td>No job advert data available</td>
					</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td>No job advert data available</td>
					</tr>
				<?php } ?>
			</table>
		</div>

	</div>

<?php } elseif ($boardType == 2 && $type == 2) { ?>

	<div class="boxTop">
		<h2>Your Candidates' Statistics</h2>
		<hr/>
	</div>

	<div class="charts" ng-controller="theDashes">

		<div class="completedProfiles firstGraph col-lg-4 col-md-12 col-sm-12 col-xs-12  no-gutter">
			<div class="circle">
				<p>60<span>%</span><span>completed Profiles</span></p>
			</div>
			<svg width="250" height="250" viewbox="0 0 250 250" ng-init="setInterval(profileCircle(<?php echo $percent; ?>),100)">
					<path id="loader" transform="translate(125, 125)"/>
					<circle id="circle" cx="125" cy="125" r="110"/>
					<g>
						<text letter-spacing="2" font-size="52" font-family="'Lato', sans-serif" fill="#3d3e42" y="120" x="145" text-anchor="end" width="250" id="tots">0</text>
					</g>
					<text transform="matrix(1 0 0 1 68 150)">
						<tspan x="0" y="0" fill="#3d3e42" font-family="'Lato', sans-serif" font-size="19.7349">Completed</tspan>
						<tspan x="15.1" y="23.7" fill="#3d3e42" font-family="'Lato', sans-serif" font-size="19.7349">profiles</tspan>
					</text>
					
					<text transform="matrix(1 0 0 1 150 120)" fill="#3d3e42" font-family="'Lato', sans-serif" font-size="32.0636" letter-spacing="1">%</text>
			</svg>

		</div>

		<div class="viewedCandidates secondGraph tables col-lg-8 col-md-12 col-sm-12 col-sm-12 no-gutter clearfix">
			<h6>Most Viewed Candidates</h6>
			<table class="table table-striped  footable">
				<?php if (isset($popCands)) { ?>
					<?php if (($popCands !== false)) { ?>
							<thead>
								<tr>
									<th data-sort-ignore="true"></th>
									<th data-sort-ignore="true"></th>
									<th data-sort-ignore="true" data-hide="phone"></th>
									<th data-sort-ignore="true" data-hide="phone"></th>
								</tr>
							</thead>
						<?php foreach ($popCands as $cands) { ?>
							<tr>
								<td><img src="/imgs/svgs/candidate.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/candidate.png'" /></td>
								<td><?php echo $cands->users->name; ?></td>
								<td><?php echo $cands->users->username; ?></td>
								<td><?php echo $viewStats[$cands->id]; ?> Views</td>
							</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<tr><td><strong><em>No data for any candidates available</em></strong></td></tr>
						</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<tr><td><strong><em>No data for any candidates available</em></strong></td></tr>
					</tr>
				<?php } ?>
			</table>
		</div>

		<div class="uncompletedCandidates thirdGraph tables row col-lg-12 col-md-12 col-sm-12 col-sm-12  no-gutter">
			<hr />
			<h6>Oldest Uncompleted Profiles</h6>
			<table class="table table-striped  footable">
				<?php if (isset($incompleteUsers)) { ?>
					<?php if (($incompleteUsers !== false)) { ?>
							<thead>
								<tr>
									<th data-sort-ignore="true"></th>
									<th data-sort-ignore="true"></th>
									<th data-sort-ignore="true" data-hide="phone"></th>
									<th data-sort-ignore="true" data-hide="phone"></th>
									<th  data-sort-ignore="true"data-hide="phone"></th>
								</tr>
							</thead>
						<?php foreach ($incompleteUsers as $cands) { ?>
							<tr>
								<td><img src="/imgs/svgs/candidate-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/candidate-red.png'" /></td>
								<td><?php echo $cands->name; ?></td>
								<td><?php echo $cands->username; ?></td>
								<td><?php echo floor((time() - strtotime($cands->created))/(60*60*24));  ?> days</td>
								<td><button onClick="return false;" class="btn btn-warning ladda-button" data-style="expand-right"><span class="ladda-label">send reminder</span></button></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
					<tr>
						<tr><td><strong><em>No data for any candidates available</em></strong></td></tr>
					</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<tr><td><strong><em>No data for any candidates available</em></strong></td></tr>
					</tr>
				<?php } ?>
			</table>
		</div>

	</div>

<?php } elseif ($boardType == 3 && $type == 3) { ?>

	<div class="boxTop">
		<h2>Recruiter Dashboard</h2>
		<hr/>
	</div>

	<div class="charts">

		<div class="viewedCandidates secondGraph tables col-lg-12 col-xs-12 no-gutter clearfix">
			<h6>Oldest Unfilled Jobs</h6>
			<table class="table table-striped footable">
				<?php if (isset($unfilledJobs)) { ?>
					<?php if (($unfilledJobs !== false)) { ?>
						<thead>
							<tr>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
							</tr>
						</thead>
						<?php foreach ($unfilledJobs as $job) { ?>
							<tr>
								<td><img src="/imgs/svgs/advert-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/advert-red.png'" /></td>
								<td><?php echo $job->title; ?></td>
								<td><?php echo $job->locationnice; ?></td>
								<td><?php echo floor((time() - strtotime($job->created))/(60*60*24));  ?> days</td>
								<td><a href="/dashboard/job/<?php echo $job->ID; ?>" class="btn btn-warning" data-style="expand-right"><span class="ladda-label">view advert</span></a></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
					<tr>
						<td>No recruiter data available</td>
					</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td>No recruiter data available</td>
					</tr>
				<?php } ?>
			</table>
		</div>

		<div class="uncompletedCandidates thirdGraph tables row col-lg-12 col-xs-12 no-gutter">
			<hr />
			<h6>Oldest Uncompleted Profiles</h6>
			<table class="table table-striped footable">
				<?php if (isset($oldCands)) { ?>
					<?php if (($oldCands !== false)) { ?>
						<thead>
							<tr>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
							</tr>
						</thead>
						<?php foreach ($oldCands as $cands) { ?>
							<tr>
								<td><img src="/imgs/svgs/candidate-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/candidate-red.png'" /></td>
								<td><?php echo $cands->name; ?></td>
								<td><?php echo $cands->username; ?></td>
								<td><?php echo floor((time() - strtotime($cands->created))/(60*60*24));  ?> days</td>
								<td><a href="#"  class="btn btn-warning" data-style="expand-right"><span class="ladda-label">send reminder</span></a></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
					<tr>
						<td>No recruiter data available</td>
					</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td>No recruiter data available</td>
					</tr>
				<?php } ?>
			</table>
		</div>

	</div>


<?php } else { ?>

		<div class="boxTop">
		<h2>Your Profile's Statistics</h2>
		<hr/>
	</div>

	<div class="charts">

		<div class="completedProfiles firstGraph col-lg-4 col-md-12 col-sm-12 col-xs-12 no-gutter">
			<div class="circle">
				<p><?php echo $views; ?>><span>views</span></p>
			</div>
			<svg  version="1.1" style="width:100%; max-width:250px;" height="250" x="0px" y="0px" viewBox="0 0 250 250" xml:space="preserve">
				<g transform="translate(0, 250) scale(1, -1)">
					<?php if (isset($dayView)) { ?>
					<rect x="25" y="14" fill="#38D9BE" width="21" height="<?php echo $dayView[0]; ?>">
						<animate attributeName="height" from="0" to="<?php echo $dayView[0]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<rect x="59.3" y="14" fill="#38D9BE" width="21" height="<?php echo $dayView[1]; ?>">
						<animate attributeName="height" from="0" to="<?php echo $dayView[1]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<rect x="130" y="14" fill="#38D9BE" width="21" height="<?php echo $dayView[2]; ?>">
						<animate attributeName="height" from="0" to="<?php echo $dayView[2]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<rect x="95.3" y="14" fill="#38D9BE" width="21" height="<?php echo $dayView[3]; ?>">
						<animate attributeName="height" from="0" to="<?php echo $dayView[3]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<rect x="200.7" y="14" fill="#38D9BE" width="21" height="<?php echo $dayView[4]; ?>">
						<animate attributeName="height" from="0" to="<?php echo $dayView[4]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<rect x="165" y="14" fill="#38D9BE" width="21" height="<?php echo $dayView[5]; ?>">
						<animate attributeName="height" from="0" to="<?php echo $dayView[5]; ?>" begin="0s" dur="0.5s"  keySplines="0.1 0.8 0.2 1;" calcMode="spline" />
					</rect>
					<?php } ?>
				</g>
				<line fill="none" stroke="#E8E8E6" stroke-width="2" stroke-miterlimit="10" x1="15" y1="235" x2="235" y2="235"/>
				<text text-anchor="middle" x="125" y="80" width="250" font-family="'Lato', sans-serif" font-size="52.3664" letter-spacing="2"><?php if (isset($views)) { ?><?php echo $views; ?><?php } ?></text>
				<text transform="matrix(1 0 0 1 95.8794 109.8516)" font-family="'Lato', sans-serif" font-size="19.8632">VIEWS</text>
			</svg>
		</div>

		<div class="viewedCandidates jobsDiscipline secondGraph tables col-lg-8 col-md-12 col-sm-12 col-sm-12 no-gutter clearfix">
			<h6>Latest Jobs in your Discipline <span>(<?php if (isset($discipline)) { ?><?php echo $discipline; ?><?php } ?>)</span></h6>
			<table class="table table-striped footable">
				<?php if (isset($latestJobs)) { ?>
					<?php if (($latestJobs !== false)) { ?>
						<thead>
							<tr>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
							</tr>
						</thead>
						<?php foreach ($latestJobs as $job) { ?>
							<tr>
								<td><img src="/imgs/svgs/advert.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/advert.png'" /></td>
								<td><?php echo $job->title; ?></td>
								<td><?php echo $job->locationnice; ?></td>
								<td><a href="/dashboard/job/<?php echo $job->ID; ?>" class="btn btn-green btn-success" data-style="expand-right"><span class="ladda-label">view job</span></a></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<tr><td><strong><em>No data available</em></strong></td></tr>
						</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<tr><td><strong><em>No data available</em></strong></td></tr>
					</tr>
				<?php } ?>
			</table>
		</div>

		<div class="uncompletedCandidates applicationStatus thirdGraph tables row col-lg-12 col-md-12 col-sm-12 col-sm-12 no-gutter">
			<hr />
			<h6>Application Status</h6>
			<table class="table table-striped footable">
				<thead>
					<tr>
						<th data-sort-ignore="true"></th>
						<th data-sort-ignore="true"></th>
						<th data-sort-ignore="true" data-hide="phone"></th>
						<th data-sort-ignore="true" data-hide="phone"></th>
						<th data-sort-ignore="true" data-hide="phone"></th>
					</tr>
				</thead>
				<?php if (isset($appliedJobs)) { ?>
					<?php if (($appliedJobs !== false)) { ?>
						<?php foreach ($appliedJobs as $job) { ?>
							<tr>
								<td><img src="/imgs/svgs/advert.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/advert.png'" /></td>
								<td><?php echo $job->job->title; ?></td>
								<td><?php echo $job->job->locationnice; ?></td>
								<td>
									<?php if (($job->job->status == 1)) { ?>
										<span class="applied">Applied</span>
									<?php } elseif (($job->job->status == 2)) { ?>
										<span class="fail">Unsuccessful</span>
									<?php } elseif (($job->job->status == 3)) { ?>
										<span class="interview">Interview Stage</span>
									<?php } else { ?>
										<span class="success">Successful</span>
									<?php } ?>
								</td>
								<td><a href="/dashboard/job/<?php echo $job->job->ID; ?>" class="btn btn-success ladda-button" data-style="expand-right"><span class="ladda-label">review job</span></a></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
					<tr>
						<tr><td><strong><em>No data available</em></strong></td></tr>
					</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<tr><td><strong><em>No data available</em></strong></td></tr>
					</tr>
				<?php } ?>
			</table>
		</div>

	</div>

<?php } ?>
