<div class="boxTop">
		<h2>Recruiter Dashboard</h2>
		<hr/>
	</div>

	<div class="charts">

		<div class="viewedCandidates secondGraph tables col-lg-12 col-xs-12 no-gutter clearfix">
			<h6>Oldest Unfilled Jobs</h6>
			<table class="table table-striped footable">
				<?php if (isset($unfilledJobs)) { ?>
					<?php if (($unfilledJobs !== false)) { ?>
						<thead>
							<tr>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
							</tr>
						</thead>
						<?php foreach ($unfilledJobs as $job) { ?>
							<tr>
								<td><img src="/imgs/svgs/advert-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/advert-red.png'" /></td>
								<td><?php echo $job->title; ?></td>
								<td><?php echo $job->locationnice; ?></td>
								<td><?php echo floor((time() - strtotime($job->created))/(60*60*24));  ?> days</td>
								<td><a href="/dashboard/job/<?php echo $job->ID; ?>" class="btn btn-warning" data-style="expand-right"><span class="ladda-label">view advert</span></a></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
					<tr>
						<td>No recruiter data available</td>
					</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td>No recruiter data available</td>
					</tr>
				<?php } ?>
			</table>
		</div>

		<div class="uncompletedCandidates thirdGraph tables row col-lg-12 col-xs-12 no-gutter">
			<hr />
			<h6>Oldest Uncompleted Profiles</h6>
			<table class="table table-striped footable">
				<?php if (isset($oldCands)) { ?>
					<?php if (($oldCands !== false)) { ?>
						<thead>
							<tr>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
								<th data-sort-ignore="true" data-hide="phone"></th>
							</tr>
						</thead>
						<?php foreach ($oldCands as $cands) { ?>
							<tr>
								<td><img src="/imgs/svgs/candidate-red.svg" onerror="this.onerror=null; this.src='/imgs/svgs-pngs/candidate-red.png'" /></td>
								<td><?php echo $cands->name; ?></td>
								<td><?php echo $cands->username; ?></td>
								<td><?php echo floor((time() - strtotime($cands->created))/(60*60*24));  ?> days</td>
								<td><a href="#"  class="btn btn-warning" data-style="expand-right"><span class="ladda-label">send reminder</span></a></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
					<tr>
						<td>No recruiter data available</td>
					</tr>
					<?php } ?>
				<?php } else { ?>
					<tr>
						<td>No recruiter data available</td>
					</tr>
				<?php } ?>
			</table>
		</div>

	</div>
