
<?php echo $this->getContent(); ?>


<div class="boxTop">
	<h2><?php echo $bName; ?>'s Candidates</h2>
	<hr/>
</div>

<div class="getAllWrapper" ng-controller="recruitEdit">

	<div ng-bind-html="flashStatus"></div>

	<table class="table table-striped footable">

	<thead>
		<tr>
			<th data-sort-ignore="true"<strong>Candidate Name</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>Candidate Email</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>Candidate Contact Number</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>Status</strong></th>
			<th data-sort-ignore="true" data-hide="phone"><strong>Days on System</strong></th>
			<th data-sort-ignore="true" data-hide="phone"></th>
			<th data-sort-ignore="true" data-hide="phone"></th>
		</tr>
	</thead>

	<?php $v111404855655907267261iterator = $candidates; $v111404855655907267261incr = 0; $v111404855655907267261loop = new stdClass(); $v111404855655907267261loop->length = count($v111404855655907267261iterator); $v111404855655907267261loop->index = 1; $v111404855655907267261loop->index0 = 1; $v111404855655907267261loop->revindex = $v111404855655907267261loop->length; $v111404855655907267261loop->revindex0 = $v111404855655907267261loop->length - 1; ?><?php foreach ($v111404855655907267261iterator as $cands) { ?><?php $v111404855655907267261loop->first = ($v111404855655907267261incr == 0); $v111404855655907267261loop->index = $v111404855655907267261incr + 1; $v111404855655907267261loop->index0 = $v111404855655907267261incr; $v111404855655907267261loop->revindex = $v111404855655907267261loop->length - $v111404855655907267261incr; $v111404855655907267261loop->revindex0 = $v111404855655907267261loop->length - ($v111404855655907267261incr + 1); $v111404855655907267261loop->last = ($v111404855655907267261incr == ($v111404855655907267261loop->length - 1)); ?>

		<tr ng-init="showList[<?php echo $v111404855655907267261loop->index; ?>] = false; cand[<?php echo $v111404855655907267261loop->index; ?>] = <?php echo $cands->id; ?>">
			<td><?php echo $cands->users->name; ?></td>
			<td><?php echo $cands->users->username; ?></td>
			<td><?php echo $cands->phone; ?></td>
			<td>
				<div ng-hide="!showList[<?php echo $v111404855655907267261loop->index; ?>]">
					<ui-select ng-model="updateStatus[<?php echo $v111404855655907267261loop->index; ?>]" theme="bootstrap" on-select="CandStatusChange($item, updateStatus, add, <?php echo $v111404855655907267261loop->index; ?>, 'Cand')"  ng-disabled="disabled">
	                    <ui-select-match placeholder="Please Select a Status...">[[$select.selected]]</ui-select-match>
	                    <ui-select-choices repeat="stat in candStatus">
	                      <div ng-bind="stat"></div>
	                    </ui-select-choices>
	                </ui-select>
				</div>
				<?php if (($cands->status == 1)) { ?>
					<?php $status = 'Active'; ?>
				<?php } else { ?>
					<?php $status = 'Inactive'; ?>
				<?php } ?>
				<span ng-init="updateStatus[<?php echo $v111404855655907267261loop->index; ?>]='<?php echo $status; ?>'" ng-hide="showList[<?php echo $v111404855655907267261loop->index; ?>]" ng-bind="updateStatus[<?php echo $v111404855655907267261loop->index; ?>]"></span>
			</td>
			<td><?php echo floor((time() - strtotime($cands->users->created))/(60*60*24));  ?> days</td>
			<td>
				<a class="btn btn-primary" href="/dashboard/candidates/<?php echo $cands->id; ?>">view profile</a>
				<a ng-if="global.isABStaff()" class="btn btn-success" href="/dashboard/candidates/<?php echo $cands->id; ?>/jobs">view applied jobs</a>
				<a href="#" ng-click="showList[<?php echo $v111404855655907267261loop->index; ?>] = true" onclick="return false;" class="btn btn-warning">change status</a>
			</td>
		</tr>

	<?php $v111404855655907267261incr++; } ?>

	</table>

</div>