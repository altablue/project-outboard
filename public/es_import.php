<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once '../vendor/autoload.php';

$client = new Elasticsearch\Client();


require_once('../import/b58q9dqtr_candidate.php');

foreach($b58q9dqtr_candidate as $candidate):

	$elasticSearchReady = array();
	$dbReady = array();
	$esParams = array();
	$sql = "INSERT INTO ".$table;
	$values = 'VALUES (';
	$cols = '(';
	$quals = array();
	$qualTemp = $qualTemp2 = array();

	foreach($candidate as $key => $profileField):

		switch($key){

			case 'profile':
				$esParams['body']['profile'] = unserialize($profileField);				
				break;

			case 'skills':
				$cols .= $key.',';
				$tmp = '';
				$unser = unserialize($profileField);
				foreach($unser as $ex){
					$tmp .= ','.$ex;
				}
				$esParams['body']['skills'] = trim($tmp,',');				
				break;

			case 'qualifications':
				$tmp = unserialize($profileField);
				$deg = '';
				$field = '';
				foreach($tmp as $key => $qual):
					if($key == 'candQualDegree'){
						$deg .= ','.$qual['candQualDegree'];
					}else if($key == 'candQualField'){
						$field .= ','.$qual['candQualField'];
					}	
				endforeach;
				$esParams['body']['qualificationsField'] = trim($field,',');
				$esParams['body']['qualificationsDegree'] = trim($deg,',');
				break;

			default:
				$esParams['body'][$key] = $profileField;
				break;

		}

	endforeach;

	/*foreach($qualTemp as $key => $qual):

		$quals[$key]['candQualTitle'] = 'Degree Title';
		$quals[$key]['candQualDegree'] = $qualTemp2[$key]['candQualsDegree'];
		$quals[$key]['candQualField'] = $qualTemp[$key]['candQualField'];

	endforeach;*/

	$esParams['body']['type'] = 1;

	$esParams['body']['id'] = $candidate['id'];

	$esParams['index'] = 'outboard';
    $esParams['type'] = 'candidates-test';

	$client->index($esParams);

	//sleep(2);

endforeach;