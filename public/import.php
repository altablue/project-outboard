<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once '../vendor/autoload.php';

$client = new Elasticsearch\Client();

$table = 'b58q9dqtr_candidate';
$username = 'root';
$password = 'callum';

$db = new mysqli('localhost', $username, $password, 'project_outboard');
if($db->connect_errno > 0){
    die('Unable to connect to database [' . $db->connect_error . ']');
}

require_once('../import/dummyData.php');

foreach($data as $candidate):

	$elasticSearchReady = array();
	$dbReady = array();
	$esParams = array();
	$sql = "INSERT INTO ".$table;
	$values = 'VALUES (';
	$cols = '(';
	$quals = array();
	$qualTemp = $qualTemp2 = array();

	foreach($candidate as $key => $profileField):

		switch($key){

			case 'profile':
				$cols .= $key.','; 
				$values .= "'".serialize($profileField)."',";
				$esParams['body'][$key] = $profileField;				
				break;

			case 'skills':
				$cols .= $key.',';
				$array = explode(', ', $profileField);
				$tmp = array();
				if(!empty($array[0])){
					foreach($array as $arr){
						if(!empty($arr)){
							$tmp[]= $arr;
						}
					}
				}else{
					$tmp = $profileField;
				}
				$values .= "'".serialize($tmp)."',";
				$esParams['body'][$key] = $profileField;				
				break;

			case 'nicename-city':
				$cols .= 'locationnice,'; 
				$values .= "'".htmlspecialchars($profileField,ENT_QUOTES)."',";
				$esParams['body']['locationnice'] = $profileField;		

				$osm = json_decode(file_get_contents('https://nominatim.openstreetmap.org/search?q='.urlencode($profileField).'&countrycodes=gb&featuretype=city,county&format=json&bounded=1&addressdetails=1&limit=1'));
				$loc8ton = $osm[0]->lat.','.$osm[0]->lon;

				if(!empty($loc8ton) && strlen($loc8ton) > 1){
					$esParams['body']['location'] = $loc8ton;
					$cols .= 'location,'; 
					$values .= "'".str_replace(' ', '',$loc8ton)."',";
				}

				break;

			case 'qualificationsField':
				$array = explode(', ', $profileField);
				if(!empty($array[0])){
					foreach($array as $arr){
						$qualTemp[]['candQualField'] = $arr;
					}
				}else{
					$qualTemp[]['candQualField'] = $profileField;
				}
				$esParams['body'][$key] = $profileField;
				break;

			case 'location':
				break;

			case 'qualificationsDegree':
				$array = explode(', ', $profileField);
				if(!empty($array[0])){
					foreach($array as $arr){
						$qualTemp2[]['candQualsDegree'] = $arr;
					}
				}else{
					$qualTemp2[]['candQualsDegree'] = $profileField;
				}
				$esParams['body'][$key] = $profileField;
				break;

			case 'nicename-country':
				break;



			default:
				$cols .= $key.',';
				$values .= "'".$profileField."',";
				$esParams['body'][$key] = $profileField;
				break;

		}

	endforeach;

	foreach($qualTemp as $key => $qual):

		$quals[$key]['candQualTitle'] = 'Degree Title';
		$quals[$key]['candQualDegree'] = $qualTemp2[$key]['candQualsDegree'];
		$quals[$key]['candQualField'] = $qualTemp[$key]['candQualField'];

	endforeach;

	$esParams['body']['type'] = 1;

	$values .= "'".serialize($quals)."'";

	$cols .= "qualifications)";

	$sql .= $cols.$values.')';

	if($db->query($sql) === false) {
	  die('Wrong SQL: ' . $sql . ' Error: ' . $db->error);
	} else {
	  $affected_rows = $db->affected_rows;
	}

	$esParams['body']['id'] = $db->ID;

	unset($esParams['body']['board']);
    unset($esParams['body']['status']);

	$esParams['index'] = 'outboard';
    $esParams['type'] = 'candidates-test';

	$client->index($esParams);

	sleep(2);

endforeach;