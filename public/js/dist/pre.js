'use strict';

var siteBaseUrl = "http://local.project-outboard.com";

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = 'matchpeopleskills';
	var applicationModuleVendorDependencies = ['ngResource', 'ngRoute', 'ngCookies',  'ngAnimate', 'ui.select', 'ngTouch',  'ngSanitize',  'ui.router', 'ui.bootstrap', 'ui.utils', 'angular-ladda', 'ui.footable', 'matchMedia', 'html5.placeholder'];

	// Add a new vertical module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();
'use strict';
//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		//$locationProvider.html5Mode(true).hashPrefix('!');
	}
]);

angular.module(ApplicationConfiguration.applicationModuleName).config(['$interpolateProvider','$routeProvider','$locationProvider',
	function($interpolateProvider,$routeProvider,$locationProvider) {
  		$interpolateProvider.startSymbol('[[');
 	 	$interpolateProvider.endSymbol(']]');
 	 }
]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('dashboard');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('marketing');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('register');
'use strict';

angular.module('core').directive('removeClass', function($timeout){
    return {
        restrict: 'A',
        link: function($scope,element, attrs, controller){
            $timeout(function(){
                element.removeClass(attrs.removeClass);
            });
        }
    }
});
'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('core').factory('Global', ['$http', '$cookies',

function ($http, $cookies) {
    
    var current_user = false;
    var user_type =  0;
    var board_type = 1;
    var user_level = 1;

    return {
        setSignedIn: function(data){
            current_user = data.signedIn;
            user_type = data.userType;
            board_type = data.boardType;
            user_level = data.userLevel;

        },
        currentUser: function() { 
            return current_user;
        },
        isSignedIn: function() {
            return !!current_user; 
        },
        getUserType: function(){
            return user_type;
        },
        getUserLevel: function(){
            return user_level;
        },
        isAdminUser: function(){
            if(!!current_user && user_level > 2){
                return true;
            }else{
                return false;
            }
        },
        isBoardUser: function() {
            if(!!current_user && user_type > 1 && user_level < 99){
                return true;
            }else{
                return false;
            }
        },
        isCandidateUser: function() {
            if(!!current_user && user_type == 1){
                return true;
            }else{
                return false;
            }
        },
        isOnBoardComp: function (){
            if(!!current_user && user_type > 1 && board_type == 1){
                return true;
            }else{
                return false;
            } 
        },
        isABStaff: function (){
            if(user_level >= 99 && board_type == 3){
                return true;
            }else{
                return false;
            }
        },
        isSuperStaff: function (){
            if(user_level > 99 && board_type == 3){
                return true;
            }else{
                return false;
            }
        },

        emailApi: function (passedData) {

            var timestamp = getMicrotime(true).toString();
            passedData.hash = getHMAC($cookies.apiPublicKey, timestamp);
            passedData.microtime = timestamp;

            return $http.post(siteBaseUrl+'/api/1.0/sendMail/'+passedData.apiController, JSON.stringify(passedData))
                .then(function(data){
                    return data;
                }, function(data){
                    return 'error';
                });
        }
    };
}
])
;

var getHMAC = function(key, timestamp) {
    var key2 = "A5woV0GpCX5NuNnWMoaOUZSiq4dC2RgK6rLm0OtpzY2Y7RVmplsEwEyuTgk7On2xekoYiBdSHItMei9N8Os6c9jfqGvrpGlULOadHH6iH4StZDEeSw7TBebhAGAi8wKFuBFOBctei78s6m8GWbq6HL84FM5BpdaFhTKUuMvuVEy57mtWYya7nJ08uSNohexsYsFggF0LILZKMwmk2OBJiltnTk39duJA2DQ";
    var hash = CryptoJS.HmacSHA512(key+timestamp,key2);
    return hash.toString();
};

var getMicrotime = function (get_as_float) {

  var now = new Date().getTime() / 1000;
  var s = parseInt(now, 10);

  return (get_as_float) ? now : (Math.round((now - s) * 1000) / 1000) + ' ' + s;
}; 
'use strict';

angular.module('dashboard').controller('candCreate', ['$scope', '$http', '$sce', 'Global', 'inputInteract', '$cookies', '$modal',

    function ($scope, $http, $sce, Global, inputInteract, $cookies, $modal) {

        $scope.loading = false;

        $scope.submitForm = function() {
            $scope.formData.returnFlash = true;
            $scope.loading = true;
            inputInteract.apiSubmit($scope.formData)
                .then(function(data){
                    $scope.loading = false;
                    if(data.data.status === "success"){
                        window.location.href = data.data.url;
                    }else{
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }
                }, function(data){
                    $scope.loading = false;
                    $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
        };

    }
])
;

'use strict';

angular.module('dashboard').controller('userLogin', ['$scope', '$http', '$sce', 'Global', '$cookies', 'ladda',

    function ($scope, $http, $sce, Global, $cookies, ladda) {

        $scope.submitForm = function() {
            $scope.formData.returnFlash = true;
            $scope.loading = true;
            $http.post(siteBaseUrl+'/api/1.0/'+$scope.formData.apiController, JSON.stringify($scope.formData))
                .success(function(data, status){
                    $scope.loading = false;
                    if(data.status != 'success'){
                        if(typeof $data == 'undefined'){
                            $scope.global.flashStatus = $sce.trustAsHtml(data.flash);
                        }else{
                            $scope.global.flashStatus = $sce.trustAsHtml(data.flash);
                        }
                    }else{
                        window.location.href = data.url;
                    }
                })
                .error(function(){
                    $scope.loading = false;
                    $scope.global.flashStatus = $sce.trustAsHtml('<div class="alert alert-danger">There has been a system error. Please try again in a few minutes.</div>');
                })
            ; 
        }

    }
])
;

'use strict';

angular.module('dashboard').controller('theDashes', ['$scope', '$http', '$sce', 'Global', 'inputInteract', '$cookies', '$modal',

    function ($scope, $http, $sce, Global, inputInteract, $cookies, $modal) {

    	$scope.emailLoading = [];

    	$scope.job = [];

        $scope.profileCircle = function (percent)
        {
            inputInteract.circleProgress(percent);
        }

		$scope.helpFill = function(num){

            $scope.emailLoading[num] = true;

            var items = $scope.emailApi[num];

            items.searchSkills = JSON.parse(items.searchSkills.replace(/\"/g,'"'));
            items.qual = JSON.parse(items.qual.replace(/\"/g,'"'));

            var apiParams = {};

            var modalInstance = $modal.open({
              templateUrl: 'dashboardMatch.html',
              controller: 'dashboardMatch1',
              size: 'lg',
              resolve: {
                items: function () {
                  return items;
                }
              }
            });

            modalInstance.result.then(function (returnData) {
                apiParams.apiController = 'helpFillEmail';
                apiParams.data = returnData;
                apiParams.returnFlash = true;
                Global.emailApi(apiParams)
                    .then(function(data){
                        $scope.emailLoading[num] = false;
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    });
            }, function(results){
                $scope.emailLoading[num] = false;
            });

        }

        console.log($scope.job);

    }
])

.controller('dashboardMatch1',

    function ($scope, $modalInstance, items, Global) {

        $scope.browserDect = BrowserDetect;

        $scope.emailApi = items;

        $scope.ok = function () {
            $modalInstance.close($scope.emailApi);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }
)
;

function stripslashes (str) {

  return (str + '').replace(/\\(.?)/g, function (s, n1) {
    switch (n1) {
    case '\\':
      return '\\';
    case '0':
      return '\u0000';
    case '':
      return '';
    case '\"':
    	return '"';
    default:
      return n1;
    }
  });
}

'use strict';

angular.module('dashboard').controller('userReg', ['$scope', '$http', '$sce', 'Global', 'inputInteract', '$cookies', '$modal', 'screenSize',

    function ($scope, $http, $sce, Global, inputInteract, $cookies, $modal, screenSize) {

        $scope.tc = {};
        $scope.formData = {};
        $scope.tableQual = [];

        $scope.inputInteract = inputInteract;

        $scope.tc.Accept = false;

        $scope.formData.count = 0;
        $scope.formData.qual = [];

        $scope.tc.Disabled = true;

        $scope.count = 0;

        var profileFields = 7;

        $scope.test = 1;

        $scope.skills = [];

        $scope.disciplines = [];

        $scope.modalInstance = '';
        
        $scope.qualFields = "";
        $scope.qual = "";

        $scope.ajaxReq= '';


        $scope.submitForm = function() {
            $scope.loading = true;
            $scope.formData.returnFlash = true;
            inputInteract.apiSubmit($scope.formData)
                .then(function(data){
                    $scope.loading = false;
                    if(data.data.status === "success"){
                        window.location.href = data.data.url;
                    }else{
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }
                }, function(data){
                    $scope.loading = false;
                    $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
        };

        $scope.setupSkills = function(skills){
            $scope.formData.candSkills = [];
            for (var property in skills) {
                if (skills.hasOwnProperty(property)) {
                    $scope.formData.candSkills.push(property);
                    $scope.skills.push(property);
                }
            }   
        }

        $scope.skillUpdate = function (item, data, action) {
            $scope.status = inputInteract.getProgress(data);
            if(action == 'delete'){
                $scope.formData.candSkills.splice(item,1);
            }else if(action == 'add'){
                $scope.formData.candSkills.push(item);
            }
            circleProgressCheck(profileFields);
        };

        $scope.discUpdate = function (item, data, action) {
            $scope.status = inputInteract.getProgress(data);
            if(action == 'delete'){
                $scope.formData.candSkills.splice(item,1);
            }else if(action == 'add'){
                $scope.formData.candSkills.push(item);
            }
            circleProgressCheck(profileFields);
        };

        $scope.setUpQuals = function(quals){
            $scope.formData.qual = quals;
            $scope.tableQual = quals;
        }

        $scope.refreshDisciplines = function(disciplines) {
            for (var property in disciplines) {
                if (disciplines.hasOwnProperty(property)) {
                    $scope.disciplines.push(property);
                }
            }
        };

        $scope.renderTable = function()
        {
            if($('#qualSaved').val() != '' && typeof $('#qualSaved').val() != 'undefined'){
                $scope.formData.qual = eval('{'+$('#qualSaved').val()+ '}');
                $scope.tableQual = eval('{'+$('#qualSaved').val()+ '}');
            }
        }

        $scope.removeQual = function(num)
        {
            var index = num;
            $scope.formData.qual.splice(index,1);
            $scope.tableQual.splice(index,1);
            $scope.loadProgress($scope.formData);
            $scope.count--;
        }

        $scope.editQual = function(num){
        
            if(typeof num !== 'undefined'){
                var index = num;
                var items = $scope.formData.qual[index];
            }else{
                var index = $scope.count+1;
                var items = { candQualTitle:"", candQualDegree:"", candQualField:""};
            }

            var modalInstance = $modal.open({
              templateUrl: 'myModalContent.html',
              controller: 'modalControl',
              size: 'lg',
              resolve: {
                items: function () {
                    console.log(JSON.stringify(items));
                  return items;
                }
              }
            });

            modalInstance.result.then(function (returnData) {
                if(typeof num == 'undefined'){$scope.count++;}
                if($scope.tableQual !== false){
                    $scope.tableQual[index] = returnData;
                }else{
                    $scope.tableQual = [];
                    $scope.tableQual[0] = returnData;
                }
                if($scope.formData.qual !== false){
                    $scope.formData.qual[index] = returnData;   
                }else{
                    $scope.formData.qual = [];
                    $scope.formData.qual[0] = returnData;
                }
                $scope.loadProgress($scope.formData);
            });

        }

        $scope.getLoc = function(){
            $scope.loading = true;
            if(geoPosition.init()){
                geoPosition.getCurrentPosition(success_callback,error_callback,{enableHighAccuracy:true});
            }else{
               $scope.geoStatus = $sce.trustAsHtml('<span class="error">Functionality not available</span>');
            }
            function success_callback(p)
            {
                var latitude = p.coords.latitude
                var longitude = p.coords.longitude
                $scope.$apply(function()
                {
                    $http.get("https://nominatim.openstreetmap.org/reverse?lat="+latitude+"&lon="+longitude+"&format=json&bounded=1&addressdetails=1&limit=1").success(function(data){
                        $scope.loading = false;
                        if(typeof data.address != 'undefined'){
                            $scope.formData.niceName = data.address.city+','+data.address.state;
                            $scope.formData.candLoc = latitude + ','+ longitude;
                        }else{
                            $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error">Unable to get Location successfully</div>');
                        }
                    })
                    .error(function(){
                        $scope.loading = false;
                        $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error">Unable to get Location successfully</div>');
                    })            
                });
            }
            function error_callback(p)
            {
                $scope.loading = false;
                $scope.geoStatus = $sce.trustAsHtml('<span class="error">' + p.message + '</span>');
            }
        }

        $scope.loadProgress = function(data) {
            if(Global.browserDect != "Explorer" && Global.browserDect.version > 8){
                $scope.status = inputInteract.getProgress(data);
                circleProgressCheck(profileFields);
            }
        }

        function circleProgressCheck(profileFields){
            var changeTest = ($scope.status.fieldsComplete/profileFields)*100;
            if($scope.circlePercent != changeTest){
                $scope.circlePercent = ($scope.status.fieldsComplete/profileFields)*100;
                setInterval(inputInteract.circleProgress($scope.circlePercent),100)
            }
        }

    }
])

.controller('editSidebar', function ($scope, $http, $sce, Global, inputInteract, $cookies, $modal, screenSize) {

    $scope.selectedTab = 'progressTab';

    $scope.select= function(item) {
        $scope.selectedTab = item;

     };

     $scope.isActive = function(item) {
        return $scope.selectedTab === item;
    };

})

.controller('modalControl',

    function ($scope, $modalInstance, items, Global) {


        $scope.browserDect = BrowserDetect;

        $scope.degrees = [];

        $scope.singleQual = items;

        $scope.refreshDegrees = function(degrees) {
            for (var property in degrees) {
                if (degrees.hasOwnProperty(property)) {
                    $scope.degrees.push(property);
                }
            }
        };

        $scope.ok = function () {
            $modalInstance.close($scope.singleQual);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }
);
'use strict';

angular.module('dashboard').controller('theProfiles', ['$scope', '$http', '$sce', 'Global', 'inputInteract', '$cookies', '$modal',

    function ($scope, $http, $sce, Global, inputInteract, $cookies, $modal) {

    	$scope.emailLoading = false

    	$scope.job = [];

		$scope.contactThis = function(){

            $scope.emailLoading = true;

            var items = {};

            var apiParams = {};

            var modalInstance = $modal.open({
              templateUrl: 'contactModal.html',
              controller: 'contactModal',
              size: 'lg',
              resolve: {
                items: function () {
                  return items;
                }
              }
            });

            modalInstance.result.then(function (returnData) {
                apiParams.apiController = 'contactRequest';
                apiParams.data = returnData;
                apiParams.returnFlash = true;
                Global.emailApi(apiParams)
                    .then(function(data){
                        $scope.emailLoading = false;
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }, function(data){
                        $scope.emailLoading = false;
                    });
            }, function(results){
                $scope.emailLoading = false;
            });

        }

        console.log($scope.job);

    }
])

.controller('contactModal',

    function ($scope, $modalInstance, items, Global) {

        $scope.browserDect = BrowserDetect;

        $scope.emailApi = {};

        $scope.ok = function () {
            $modalInstance.close($scope.emailApi);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }
)
;

function stripslashes (str) {

  return (str + '').replace(/\\(.?)/g, function (s, n1) {
    switch (n1) {
    case '\\':
      return '\\';
    case '0':
      return '\u0000';
    case '':
      return '';
    case '\"':
    	return '"';
    default:
      return n1;
    }
  });
}

'use strict';

angular.module('dashboard').controller('recruitEdit', ['$scope', '$http', '$sce', 'Global', 'inputInteract', '$cookies', '$modal',

    function ($scope, $http, $sce, Global, inputInteract, $cookies, $modal) {
        $scope.showList = [];

        $scope.data = {};

        $scope.cand = [];

        $scope.candStatus = ['Active', 'Inactive'];

        $scope.adStatus = ['Unfilled', 'Filled'];

        $scope.applyStats = ['Applied', 'Interview Stage', 'Unsuccessful', 'Successful']; 

        $scope.updateStatus = [];

        $scope.jobId = [];


        $scope.CandStatusChange = function(item, data, action, loopIndex, type, jobId)
        {
        	$scope.updateStatus[loopIndex] = item;
        	$scope.showList[loopIndex] = false;
        	$scope.submitForm(item, loopIndex, type, jobId);
        }

        $scope.submitForm = function(status, num, type, jobId) {

            $scope.data.returnFlash = true;
            $scope.data.userId = $scope.cand[num];
            $scope.data.status = status;
            if(typeof jobId != 'undefined'){
                $scope.data.jobId = jobId;
            }
            $scope.data.controller = "api";
            $scope.data.apiController = "update"+type+"Status";
            $scope.data.apiAction = "put";

            inputInteract.apiSubmit($scope.data)
                .then(function(data){
                    if(data.status === "success"){
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }else{
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }
                }, function(data){
                    $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
        };

    }
])
;

'use strict';

angular.module('dashboard').controller('searchResults', ['$scope', '$http', '$sce', 'Global', 'inputInteract', '$cookies', '$modal', 'screenSize',

    function ($scope, $http, $sce, Global, inputInteract, $cookies, $modal, screenSize) {

        $scope.searchParams = {};
        $scope.searchResults ={};
        $scope.disciplines = [];
        $scope.searchParams.discipline = []
        $scope.maxSize = 5;
        $scope.degrees = [];
        $scope.resultsPerView = 10;
        $scope.bigTotalItems = 1;
        $scope.currentPage = 1;
        $scope.numPages = 1;
        $scope.searchParams.from = 0;
        $scope.searchParams.limit = 10;
        $scope.sortBy = [{name:'Relevance', status:true},{name:'Alphabetical - A-Z', status:true},{name:'Alphabetical - Z-A', status:true},{name:'Location - Nearest', status:false},{name:'Location - Furthest',status:false}];
        $scope.sortBySelect = {name:'Relevance'};
        $scope.searchParams.sortBy = 'Relevance';
        $scope.searchParams.qual = [{}];
        $scope.searchParams.skills = {};
        $scope.noResults = false;

        //$scope.searchParams.keyword = $location.search().keyword;

        $scope.getUrlParams = function(){
            var q= new QueryString();
            var params = {}
            var keys= q.keys().sort();
            for (var i= 0; i < keys.length; i++)
            {
                var vall= q.values(keys[i]).join(',');
                if (typeof vall !== 'undefined' && !vall.trim()){
                }else{
                    params[keys[i]] = vall;
                }
            }
            if (keys.length == 0)
                params = false;

            return params;
        }

        $scope.submitForm = function() {
            $scope.noResults = true;
            $scope.loading = true;
            $scope.searchParams.from = $scope.currentPage-1;
            $scope.searchParams.returnFlash = true;
            $scope.searchParams.apiAction = 'post';
            $scope.searchParams.filterOut = $scope.userType;
            console.log($scope.searchParams);
            inputInteract.apiSubmit($scope.searchParams)
                .then(function(data){
                    $scope.loading = false;
                    if(data.data.status === "success"){
                        if(data.data.results.hits.total !== 0){
                            $scope.noResults = false;
                            $scope.searchResults = angular.fromJson(data.data.results);
                            $scope.bigTotalItems = $scope.totalHits = data.data.results.hits.total;
                            $scope.numPages = data.data.results.hits.total/$scope.resultsPerView;
                            var from = $scope.searchParams.limit*$scope.searchParams.from;
                            if(from <= 0) from = 1;
                            $scope.curHitsRange = from+' - '+(($scope.searchParams.limit*$scope.searchParams.from)+$scope.searchParams.limit);
                        }else{
                            $scope.noResults = true;
                        }
                    }else{
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }
                }, function(data){
                    $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
        };


        $scope.refreshDisciplines = function(disciplines) {
            for (var property in disciplines) {
                if (disciplines.hasOwnProperty(property)) {
                    $scope.disciplines.push(property);
                }
            }
        };

        $scope.setupSkills = function(skills){
            $scope.searchParams.searchSkills = [];
            console.log(JSON.stringify(skills));
            if(toType(skills) !== 'null' && skills != 'null'){
                var c = skills.length;
                for(var i=0; i < c; i++){
                    $scope.searchParams.searchSkills.push(skills[i]);
                }   
            }
        }

        $scope.sortUpdate = function(item, data, action){
            $scope.sortBySelect = data.name;
            $scope.searchParams.sortBy = data.name;
            $scope.submitForm();
        }

        $scope.skillUpdate = function (item, data, action) {
            if(action == 'delete'){
                $scope.searchParams.searchSkills.splice(item,1);
            }else if(action == 'add'){
                $scope.searchParams.searchSkills.push(item);
            }
        };

        $scope.checkLoc = function(){
            if($scope.searchParams.location.length > 0){
                $scope.sortBy[4].status = true;
                $scope.sortBy[3].status = true;
            }else{
                $scope.sortBy[4].status = false;
                $scope.sortBy[3].status = false;
            }
        }

        $scope.removeQual = function(num){
            var index = num;
            $scope.searchParams.qual.splice(index,1);
        };

        $scope.addQual = function(){
            $scope.searchParams.qual.push({});
        };

        $scope.refreshDegrees = function(degrees) {
            for (var property in degrees) {
                if (degrees.hasOwnProperty(property)) {
                    $scope.degrees.push(property);
                }
            }
        };

       $scope.helpFill = function(){

            $scope.emailLoading = true;

            var items = $scope.searchParams;

            var apiParams = {};

            var modalInstance = $modal.open({
              templateUrl: 'myModalContent2.html',
              controller: 'modalControl2',
              size: 'lg',
              resolve: {
                items: function () {
                  return items;
                }
              }
            });

            modalInstance.result.then(function (returnData) {
                apiParams.apiController = 'helpFillEmail';
                apiParams.data = returnData;
                apiParams.returnFlash = true;
                Global.emailApi(apiParams)
                    .then(function(data){
                        $scope.emailLoading = false;
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    });
            }, function(results){
                $scope.emailLoading = false;
            });

        }


        $('.tagging').on("change", function(e){
            if (e.removed) {
                skillUpdate(e,'data','delete');
            }
            if(e.added){
                skillUpdate(e,'data','add')
            }
        });

    }

])

.controller('modalControl2',

    function ($scope, $modalInstance, items, Global) {

        $scope.browserDect = BrowserDetect;

        $scope.emailApi = items;

        $scope.ok = function () {
            $modalInstance.close($scope.emailApi);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }
);

var toType = function(obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}
'use strict';

angular.module('dashboard')

/*.directive('selectTwo', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            $(element).select2(scope.$eval(attrs.selectTwo));
        }
    };
})

.directive('selectTwoNorm', function() {
    return {
        restrict: 'C',
        link: function(scope, element, attrs) {
            $(element).select2();
        }
    };
})*/

.directive('removethisqual',function() {
      return {
      	restrict: 'A',
        link : function(scope, element, attrs) {
            element.bind('click', function() {
            	element.parent().parent().remove()
            });
       }
   };
})

.directive('contenteditable', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      // view -> model
      elm.on('blur', function() {
        scope.$apply(function() {
          ctrl.$setViewValue(elm.html());
        });
      });

      // model -> view
      ctrl.$render = function() {
        elm.html(ctrl.$viewValue);
      };

      // load init value from DOM
      ctrl.$setViewValue(elm.html());
    }
  };
})

.directive('footablequals', function(){
  return function(scope, element){
    var footableTable = element.parents('table');


    if( !scope.$last ) {
        return false;
    }

    scope.$evalAsync(function(){

        if (! footableTable.hasClass('footable-loaded')) {
            footableTable.footable();
        }

        footableTable.trigger('footable_initialized');
        footableTable.trigger('footable_resize');
        footableTable.data('footable').redraw();

    });
  };
})
;
'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('dashboard').factory('inputInteract', ['$http', '$cookies',

    function ($http, $cookies) {

        var aStart = 0;
        var nStart = 0;

        var inputs = ['candAvail', 'niceName', 'candRoleDesc', 'candDisc', 'candTitle', 'qual', 'candSkills'];

        return {

            circleProgress: function(percent){

                if (typeof(percent)==='undefined') percent = 0;
                
                var loader = document.getElementById('loader')
                    , number = document.getElementById('tots')
                    , persym = document.getElementById('persym')
                    , a = 0//$scope.aStart
                    , p = Math.PI
                    , n = 0//$scope.nStart
                    , per = percent //40//$scope.completedRate
                    , l = (360/100)*per
                    , t = 1;
                    (function num(){
                        if(n < per && n < 100){
                            n++;
                            if(n == 98){
                                n = 100;
                                number.textContent = 100;
                                per = 100;
                                nStart = 100;
                                number.style.transform = 'matrix(1, 0, 0, 1, 70, 120)';
                                persym.style.transform = 'matrix(1, 0, 0, 1, 162, 120)';
                            }else{
                                number.textContent = n;
                                nStart = n;
                                setTimeout(num, 25); // Redraw
                            }
                        }else if(n >= 100){
                            number.textContent = 100;
                            per = 100;
                            nStart = 100;
                            number.style.transform = 'matrix(1, 0, 0, 1, 70, 120)';
                            persym.style.transform = 'matrix(1, 0, 0, 1, 162, 120)';
                        }
                })();

                (function draw() {
                if(a < l && a < 359.5 ){
                    if(a >= 350){
                        a = 359.5
                    }else{
                        a++;
                    }
                    if(a == 359.5){
                        a = 359.5;
                    }else{
                        a %= 359.5;
                    }
                    var r = ( a * p / 180 )
                    , x = Math.sin( r ) * 125
                    , y = Math.cos( r ) * - 125
                    , mid = ( a > 180 ) ? 1 : 0
                    , anim = 'M 0 0 v -125 A 125 125 1 ' + mid + ' 1 ' +  x  + ' ' +  y  + ' z';

                    loader.setAttribute( 'd', anim );

                    aStart = a;

                    setTimeout(draw, t); // Redraw
                }
                })();
            },

            getProgress: function (formObj) {
                var fieldStatus = {};
                var fieldsComplete = 0;
                var length = inputs.length;
                for (var i = 0; i < length; i++) {
                    if(typeof formObj[inputs[i]] != 'undefined' && !isEmpty(formObj[inputs[i]])){
                        fieldsComplete++;
                        fieldStatus[inputs[i]] = true;
                    }
                }
                fieldStatus.fieldsComplete = fieldsComplete;
                return fieldStatus;
            },

            apiSubmit: function (passedData) {

                var timestamp = getMicrotime(true).toString();
                passedData.hash = getHMAC($cookies.apiPublicKey, timestamp);
                passedData.microtime = timestamp;

                if(passedData.apiAction == 'put'){
                    return $http.put(siteBaseUrl+'/api/1.0/'+passedData.apiController+'/'+passedData.userId+'/', JSON.stringify(passedData))
                        .then(function(data){
                            return data;
                        }, function(data){
                            return 'error';
                        });
                }else if(passedData.apiAction == 'get'){
                    
                }else{
                    return $http.post(siteBaseUrl+'/api/1.0/'+passedData.apiController, JSON.stringify(passedData))
                        .then(function(data){
                            return data;
                        }, function(data){
                            return 'error';
                        });                    
                }
            }
        }

    }
]);

var getHMAC = function(key, timestamp) {
    var key2 = "A5woV0GpCX5NuNnWMoaOUZSiq4dC2RgK6rLm0OtpzY2Y7RVmplsEwEyuTgk7On2xekoYiBdSHItMei9N8Os6c9jfqGvrpGlULOadHH6iH4StZDEeSw7TBebhAGAi8wKFuBFOBctei78s6m8GWbq6HL84FM5BpdaFhTKUuMvuVEy57mtWYya7nJ08uSNohexsYsFggF0LILZKMwmk2OBJiltnTk39duJA2DQ";
    var hash = CryptoJS.HmacSHA512(key+timestamp,key2);
    return hash.toString();
};

var getMicrotime = function (get_as_float) {

  var now = new Date().getTime() / 1000;
  var s = parseInt(now, 10);

  return (get_as_float) ? now : (Math.round((now - s) * 1000) / 1000) + ' ' + s;
}; 

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
'use strict';

angular.module('core').run(function ($rootScope) {
    $rootScope.Modernizr = Modernizr;    
});

angular.module('core').controller('accessControl', ['$scope', '$http', '$sce', 'Global', '$cookies', 'screenSize',

	function ($scope, $http, $sce, Global, $cookies, screenSize) {

		Global.browserDect = BrowserDetect;

		if(Global.browserDect.browser != 'Explorer' && Global.browserDect.version != 8){

	        $scope.desktop = screenSize.is('md, lg');
	        $scope.mobile = screenSize.is('xs, sm');

	        $scope.desktop = screenSize.on('md, lg', function(match){
	            $scope.desktop = match;
	        });
	        $scope.mobile = screenSize.on('xs, sm', function(match){
	            $scope.mobile = match;
	        });

	    }else{

	    	$scope.ieFallback = true;
	    }

	    $scope.contentLoaded = true;

	    $scope.loggedInData = {};

	    $scope.global = Global;


		$http.get(siteBaseUrl+'/api/1.0/login/userAuth')
	    .success(function(data, status){
	        if(data.status != 'success'){
	            $scope.loggedInData.signedIn = false;
	            Global.setSignedIn($scope.loggedInData);
	        }else{
	            $scope.loggedInData.signedIn = true;
	            $scope.loggedInData.userType = data.data.userType;
	            $scope.loggedInData.boardType = data.data.boardType;
	            $scope.loggedInData.userLevel = data.data.userLevel;
	            Global.setSignedIn($scope.loggedInData);
	            //window.location.href = data.url;
	        }
	    })
	    
	    .error(function(){
	        alert('fail');
	    });

	}

])

.controller('AlertDemoCtrl',  ['$scope', '$http', '$sce', 'Global', '$cookies',

	function  ($scope, $http, $sce, Global, $cookies, $bootstrap) {

		$scope.showAlert = false;

	}

])
;
'use strict';

// Configuring the Articles module
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		/*$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		});*/
	}
]);
'use strict';

// Setting up route
angular.module('core').config(['$stateProvider',
	function($stateProvider) {

	}
]);
'use strict';

// Configuring the Articles module
angular.module('marketing').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		/*$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		});*/
	}
]);
'use strict';

// Setting up route
angular.module('marketing').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		
	}
]);
'use strict';

angular.module('marketing').controller('MarketingController', ['$scope', '$stateParams', '$location',
	function($scope, $stateParams, $location) {
		
	}
]);
'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('marketing').directive('anyStretch', function($timeout){
    return {
        restrict: 'C',
        link: function($scope,element, attrs, controller){
            $timeout(function(){
            	console.log(element);
            	$('.anyStretch').anystretch();
            });
        }
    };
});
'use strict';

// Configuring the Articles module
angular.module('register').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		/*$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		});*/
	}
]);
'use strict';

// Setting up route
angular.module('register').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		
	}
]);
'use strict';

angular.module('register').controller('recruiterCreate', ['$scope', '$http', '$sce', 'Global', '$cookies', '$modal', 'dataPassage',

    function ($scope, $http, $sce, Global, $cookies, $modal, dataPassage) {

        $scope.loading = false;

    	$scope.companies = [];
    	$scope.assignedCompanies = [{'':''}];
    	var prevSelect = [0];
    	var prevOption = 0;
	
    	$scope.submitForm = function()
    	{
            $scope.loading = true;
    		$scope.formData.apiAction = 'post';
    		$scope.formData.returnFlash = true;
            dataPassage.apiSubmit($scope.formData)
                .then(function(data){
                    $scope.loading = false;
                    if(data.data.status === "success"){
                        window.location.href = data.data.url;
                    }else{
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }
                }, function(data){
                    $scope.loading = false;
                    $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
    	}

    	$scope.assignAnother = function()
    	{
            $scope.assignedCompanies.push({'':''});
        };

        $scope.compUpdate = function(item, index, action, option)
        {
        	if(typeof prevSelect[option] != 'undefined' && prevSelect[option] != item.num){
        		var num = prevSelect[option];
        		$scope.companies[num].status = true;
        	}
        	item.status = false;
        	prevSelect[option] = item.num;
        }

        $scope.compAssignRemove = function(num)
        {
            var index = num;
            $scope.assignedCompanies.splice(index,1);
        };

	}

])

.controller('companyCreate', ['$scope', '$http', '$sce', 'Global', '$cookies', '$modal', 'dataPassage',

    function ($scope, $http, $sce, Global, $cookies, $modal, dataPassage) {

        $scope.loading = false;

        $scope.submitForm = function()
        {
            $scope.loading = true;
            $scope.formData.apiAction = 'post';
            $scope.formData.returnFlash = true;
            dataPassage.apiSubmit($scope.formData)
                .then(function(data){
                    $scope.loading = false;
                    if(data.data.status === "success"){
                        window.location.href = data.data.url;
                    }else{
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }
                }, function(data){
                    $scope.loading = false;
                    $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
        } 
              
    }


])

.controller('signUp', ['$scope', '$http', '$sce', 'Global', '$cookies', '$modal', 'dataPassage',

    function ($scope, $http, $sce, Global, $cookies, $modal, dataPassage) {

        $scope.loading = false;

        $scope.submitForm = function()
        {
            $scope.loading = true;
            $scope.formData.apiAction = 'put';
            $scope.formData.returnFlash = true;
            dataPassage.apiSubmit($scope.formData)
                .then(function(data){
                    $scope.loading = false;
                    if(data.data.status === "success"){
                        window.location.href = data.data.url;
                    }else{
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }
                }, function(data){
                    $scope.loading = false;
                    $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
        }


    }


])

;
'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('register').factory('dataPassage', ['$http', '$cookies',

    function ($http, $cookies) {

        return {

            apiSubmit: function (passedData) {

                var timestamp = getMicrotime(true).toString();
                passedData.hash = getHMAC($cookies.apiPublicKey, timestamp);
                passedData.microtime = timestamp;

                if(passedData.apiAction == 'put'){
                    return $http.put(siteBaseUrl+'/api/1.0/'+passedData.apiController+'/'+passedData.userId, JSON.stringify(passedData))
                        .then(function(data){
                            return data;
                        }, function(data){
                            return 'error';
                        });
                }else if(passedData.apiAction == 'get'){
                    
                }else{
                    return $http.post(siteBaseUrl+'/api/1.0/'+passedData.apiController, JSON.stringify(passedData))
                        .then(function(data){
                            return data;
                        }, function(data){
                            return 'error';
                        });                    
                }
            }
        }

    }
]);

var getHMAC = function(key, timestamp) {
    var key2 = "A5woV0GpCX5NuNnWMoaOUZSiq4dC2RgK6rLm0OtpzY2Y7RVmplsEwEyuTgk7On2xekoYiBdSHItMei9N8Os6c9jfqGvrpGlULOadHH6iH4StZDEeSw7TBebhAGAi8wKFuBFOBctei78s6m8GWbq6HL84FM5BpdaFhTKUuMvuVEy57mtWYya7nJ08uSNohexsYsFggF0LILZKMwmk2OBJiltnTk39duJA2DQ";
    var hash = CryptoJS.HmacSHA512(key+timestamp,key2);
    return hash.toString();
};

var getMicrotime = function (get_as_float) {

  var now = new Date().getTime() / 1000;
  var s = parseInt(now, 10);

  return (get_as_float) ? now : (Math.round((now - s) * 1000) / 1000) + ' ' + s;
}; 
