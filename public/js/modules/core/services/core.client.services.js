'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('core').factory('Global', ['$http', '$cookies',

function ($http, $cookies) {
    
    var current_user = false;
    var user_type =  0;
    var board_type = 1;
    var user_level = 1;

    return {
        setSignedIn: function(data){
            current_user = data.signedIn;
            user_type = data.userType;
            board_type = data.boardType;
            user_level = data.userLevel;

        },
        currentUser: function() { 
            return current_user;
        },
        isSignedIn: function() {
            return !!current_user; 
        },
        getUserType: function(){
            return user_type;
        },
        getUserLevel: function(){
            return user_level;
        },
        isAdminUser: function(){
            if(!!current_user && user_level > 2){
                return true;
            }else{
                return false;
            }
        },
        isBoardUser: function() {
            if(!!current_user && user_type > 1 && user_level < 99){
                return true;
            }else{
                return false;
            }
        },
        isCandidateUser: function() {
            if(!!current_user && user_type == 1){
                return true;
            }else{
                return false;
            }
        },
        isOnBoardComp: function (){
            if(!!current_user && user_type > 1 && board_type == 1){
                return true;
            }else{
                return false;
            } 
        },
        isABStaff: function (){
            if(user_level >= 99 && board_type == 3){
                return true;
            }else{
                return false;
            }
        },
        isSuperStaff: function (){
            if(user_level > 99 && board_type == 3){
                return true;
            }else{
                return false;
            }
        },

        emailApi: function (passedData) {

            var timestamp = getMicrotime(true).toString();
            passedData.hash = getHMAC($cookies.apiPublicKey, timestamp);
            passedData.microtime = timestamp;

            return $http.post(siteBaseUrl+'/api/1.0/sendMail/'+passedData.apiController, JSON.stringify(passedData))
                .then(function(data){
                    return data;
                }, function(data){
                    return 'error';
                });
        }
    };
}
])
;

var getHMAC = function(key, timestamp) {
    var key2 = "A5woV0GpCX5NuNnWMoaOUZSiq4dC2RgK6rLm0OtpzY2Y7RVmplsEwEyuTgk7On2xekoYiBdSHItMei9N8Os6c9jfqGvrpGlULOadHH6iH4StZDEeSw7TBebhAGAi8wKFuBFOBctei78s6m8GWbq6HL84FM5BpdaFhTKUuMvuVEy57mtWYya7nJ08uSNohexsYsFggF0LILZKMwmk2OBJiltnTk39duJA2DQ";
    var hash = CryptoJS.HmacSHA512(key+timestamp,key2);
    return hash.toString();
};

var getMicrotime = function (get_as_float) {

  var now = new Date().getTime() / 1000;
  var s = parseInt(now, 10);

  return (get_as_float) ? now : (Math.round((now - s) * 1000) / 1000) + ' ' + s;
}; 