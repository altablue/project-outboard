'use strict';

angular.module('core').directive('removeClass', function($timeout){
    return {
        restrict: 'A',
        link: function($scope,element, attrs, controller){
            $timeout(function(){
                element.removeClass(attrs.removeClass);
            });
        }
    }
});