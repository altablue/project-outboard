'use strict';

angular.module('core').run(function ($rootScope) {
    $rootScope.Modernizr = Modernizr;    
});

angular.module('core').controller('accessControl', ['$scope', '$http', '$sce', 'Global', '$cookies', 'screenSize',

	function ($scope, $http, $sce, Global, $cookies, screenSize) {

		Global.browserDect = BrowserDetect;

		if(Global.browserDect.browser != 'Explorer' && Global.browserDect.version != 8){

	        $scope.desktop = screenSize.is('md, lg');
	        $scope.mobile = screenSize.is('xs, sm');

	        $scope.desktop = screenSize.on('md, lg', function(match){
	            $scope.desktop = match;
	        });
	        $scope.mobile = screenSize.on('xs, sm', function(match){
	            $scope.mobile = match;
	        });

	    }else{

	    	$scope.ieFallback = true;
	    }

	    $scope.contentLoaded = true;

	    $scope.loggedInData = {};

	    $scope.global = Global;


		$http.get(siteBaseUrl+'/api/1.0/login/userAuth')
	    .success(function(data, status){
	        if(data.status != 'success'){
	            $scope.loggedInData.signedIn = false;
	            Global.setSignedIn($scope.loggedInData);
	        }else{
	            $scope.loggedInData.signedIn = true;
	            $scope.loggedInData.userType = data.data.userType;
	            $scope.loggedInData.boardType = data.data.boardType;
	            $scope.loggedInData.userLevel = data.data.userLevel;
	            Global.setSignedIn($scope.loggedInData);
	            //window.location.href = data.url;
	        }
	    })
	    
	    .error(function(){
	        alert('fail');
	    });

	}

])

.controller('AlertDemoCtrl',  ['$scope', '$http', '$sce', 'Global', '$cookies',

	function  ($scope, $http, $sce, Global, $cookies, $bootstrap) {

		$scope.showAlert = false;

	}

])
;