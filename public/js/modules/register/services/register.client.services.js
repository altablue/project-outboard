'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('register').factory('dataPassage', ['$http', '$cookies',

    function ($http, $cookies) {

        return {

            apiSubmit: function (passedData) {

                var timestamp = getMicrotime(true).toString();
                passedData.hash = getHMAC($cookies.apiPublicKey, timestamp);
                passedData.microtime = timestamp;

                if(passedData.apiAction == 'put'){
                    return $http.put(siteBaseUrl+'/api/1.0/'+passedData.apiController+'/'+passedData.userId, JSON.stringify(passedData))
                        .then(function(data){
                            return data;
                        }, function(data){
                            return 'error';
                        });
                }else if(passedData.apiAction == 'get'){
                    
                }else{
                    return $http.post(siteBaseUrl+'/api/1.0/'+passedData.apiController, JSON.stringify(passedData))
                        .then(function(data){
                            return data;
                        }, function(data){
                            return 'error';
                        });                    
                }
            }
        }

    }
]);

var getHMAC = function(key, timestamp) {
    var key2 = "A5woV0GpCX5NuNnWMoaOUZSiq4dC2RgK6rLm0OtpzY2Y7RVmplsEwEyuTgk7On2xekoYiBdSHItMei9N8Os6c9jfqGvrpGlULOadHH6iH4StZDEeSw7TBebhAGAi8wKFuBFOBctei78s6m8GWbq6HL84FM5BpdaFhTKUuMvuVEy57mtWYya7nJ08uSNohexsYsFggF0LILZKMwmk2OBJiltnTk39duJA2DQ";
    var hash = CryptoJS.HmacSHA512(key+timestamp,key2);
    return hash.toString();
};

var getMicrotime = function (get_as_float) {

  var now = new Date().getTime() / 1000;
  var s = parseInt(now, 10);

  return (get_as_float) ? now : (Math.round((now - s) * 1000) / 1000) + ' ' + s;
}; 
