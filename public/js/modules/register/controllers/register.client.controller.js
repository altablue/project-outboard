'use strict';

angular.module('register').controller('recruiterCreate', ['$scope', '$http', '$sce', 'Global', '$cookies', '$modal', 'dataPassage',

    function ($scope, $http, $sce, Global, $cookies, $modal, dataPassage) {

        $scope.loading = false;

    	$scope.companies = [];
    	$scope.assignedCompanies = [{'':''}];
    	var prevSelect = [0];
    	var prevOption = 0;
	
    	$scope.submitForm = function()
    	{
            $scope.loading = true;
    		$scope.formData.apiAction = 'post';
    		$scope.formData.returnFlash = true;
            dataPassage.apiSubmit($scope.formData)
                .then(function(data){
                    $scope.loading = false;
                    if(data.data.status === "success"){
                        window.location.href = data.data.url;
                    }else{
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }
                }, function(data){
                    $scope.loading = false;
                    $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
    	}

    	$scope.assignAnother = function()
    	{
            $scope.assignedCompanies.push({'':''});
        };

        $scope.compUpdate = function(item, index, action, option)
        {
        	if(typeof prevSelect[option] != 'undefined' && prevSelect[option] != item.num){
        		var num = prevSelect[option];
        		$scope.companies[num].status = true;
        	}
        	item.status = false;
        	prevSelect[option] = item.num;
        }

        $scope.compAssignRemove = function(num)
        {
            var index = num;
            $scope.assignedCompanies.splice(index,1);
        };

	}

])

.controller('companyCreate', ['$scope', '$http', '$sce', 'Global', '$cookies', '$modal', 'dataPassage',

    function ($scope, $http, $sce, Global, $cookies, $modal, dataPassage) {

        $scope.loading = false;

        $scope.recruts = [{}];

        $scope.submitForm = function()
        {
            $scope.loading = true;
            $scope.formData.apiAction = 'post';
            $scope.formData.returnFlash = true;
            dataPassage.apiSubmit($scope.formData)
                .then(function(data){
                    $scope.loading = false;
                    if(data.data.status === "success"){
                        window.location.href = data.data.url;
                    }else{
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }
                }, function(data){
                    $scope.loading = false;
                    $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
        } 
              
    }


])

.controller('signUp', ['$scope', '$http', '$sce', 'Global', '$cookies', '$modal', 'dataPassage',

    function ($scope, $http, $sce, Global, $cookies, $modal, dataPassage) {

        $scope.loading = false;

        $scope.submitForm = function()
        {
            $scope.loading = true;
            $scope.formData.apiAction = 'put';
            $scope.formData.returnFlash = true;
            dataPassage.apiSubmit($scope.formData)
                .then(function(data){
                    $scope.loading = false;
                    if(data.data.status === "success"){
                        window.location.href = data.data.url;
                    }else{
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }
                }, function(data){
                    $scope.loading = false;
                    $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
        }


    }


])

;