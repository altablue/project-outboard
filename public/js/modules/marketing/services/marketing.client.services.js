'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('marketing').directive('anyStretch', function($timeout){
    return {
        restrict: 'C',
        link: function($scope,element, attrs, controller){
            $timeout(function(){
            	console.log(element);
            	$('.anyStretch').anystretch();
            });
        }
    };
});