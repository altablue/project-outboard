'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('dashboard').factory('inputInteract', ['$http', '$cookies',

    function ($http, $cookies) {

        var aStart = 0;
        var nStart = 0;

        var inputs = ['candAvail', 'niceName', 'candRoleDesc', 'candDisc', 'candTitle', 'qual', 'candSkills'];

        return {

            circleProgress: function(percent){

                if (typeof(percent)==='undefined') percent = 0;
                
                var loader = document.getElementById('loader')
                    , number = document.getElementById('tots')
                    , persym = document.getElementById('persym')
                    , a = 0//$scope.aStart
                    , p = Math.PI
                    , n = 0//$scope.nStart
                    , per = percent //40//$scope.completedRate
                    , l = (360/100)*per
                    , t = 1;
                    (function num(){
                        if(n < per && n < 100){
                            n++;
                            if(n == 98){
                                n = 100;
                                number.textContent = 100;
                                per = 100;
                                nStart = 100;
                                number.style.transform = 'matrix(1, 0, 0, 1, 70, 120)';
                                persym.style.transform = 'matrix(1, 0, 0, 1, 162, 120)';
                            }else{
                                number.textContent = n;
                                nStart = n;
                                setTimeout(num, 25); // Redraw
                            }
                        }else if(n >= 100){
                            number.textContent = 100;
                            per = 100;
                            nStart = 100;
                            number.style.transform = 'matrix(1, 0, 0, 1, 70, 120)';
                            persym.style.transform = 'matrix(1, 0, 0, 1, 162, 120)';
                        }
                })();

                (function draw() {
                if(a < l && a < 359.5 ){
                    if(a >= 350){
                        a = 359.5
                    }else{
                        a++;
                    }
                    if(a == 359.5){
                        a = 359.5;
                    }else{
                        a %= 359.5;
                    }
                    var r = ( a * p / 180 )
                    , x = Math.sin( r ) * 125
                    , y = Math.cos( r ) * - 125
                    , mid = ( a > 180 ) ? 1 : 0
                    , anim = 'M 0 0 v -125 A 125 125 1 ' + mid + ' 1 ' +  x  + ' ' +  y  + ' z';

                    loader.setAttribute( 'd', anim );

                    aStart = a;

                    setTimeout(draw, t); // Redraw
                }
                })();
            },

            getProgress: function (formObj) {
                var fieldStatus = {};
                var fieldsComplete = 0;
                var length = inputs.length;
                for (var i = 0; i < length; i++) {
                    if(typeof formObj[inputs[i]] != 'undefined' && !isEmpty(formObj[inputs[i]])){
                        fieldsComplete++;
                        fieldStatus[inputs[i]] = true;
                    }
                }
                fieldStatus.fieldsComplete = fieldsComplete;
                return fieldStatus;
            },

            apiSubmit: function (passedData) {

                var timestamp = getMicrotime(true).toString();
                passedData.hash = getHMAC($cookies.apiPublicKey, timestamp);
                passedData.microtime = timestamp;

                if(passedData.apiAction == 'put'){
                    return $http.put(siteBaseUrl+'/api/1.0/'+passedData.apiController+'/'+passedData.userId+'/', JSON.stringify(passedData))
                        .then(function(data){
                            return data;
                        }, function(data){
                            return 'error';
                        });
                }else if(passedData.apiAction == 'get'){
                    
                }else{
                    return $http.post(siteBaseUrl+'/api/1.0/'+passedData.apiController, JSON.stringify(passedData))
                        .then(function(data){
                            return data;
                        }, function(data){
                            return 'error';
                        });                    
                }
            }
        }

    }
]);

var getHMAC = function(key, timestamp) {
    var key2 = "A5woV0GpCX5NuNnWMoaOUZSiq4dC2RgK6rLm0OtpzY2Y7RVmplsEwEyuTgk7On2xekoYiBdSHItMei9N8Os6c9jfqGvrpGlULOadHH6iH4StZDEeSw7TBebhAGAi8wKFuBFOBctei78s6m8GWbq6HL84FM5BpdaFhTKUuMvuVEy57mtWYya7nJ08uSNohexsYsFggF0LILZKMwmk2OBJiltnTk39duJA2DQ";
    var hash = CryptoJS.HmacSHA512(key+timestamp,key2);
    return hash.toString();
};

var getMicrotime = function (get_as_float) {

  var now = new Date().getTime() / 1000;
  var s = parseInt(now, 10);

  return (get_as_float) ? now : (Math.round((now - s) * 1000) / 1000) + ' ' + s;
}; 

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}