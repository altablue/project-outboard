'use strict';

angular.module('dashboard')

/*.directive('selectTwo', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            $(element).select2(scope.$eval(attrs.selectTwo));
        }
    };
})

.directive('selectTwoNorm', function() {
    return {
        restrict: 'C',
        link: function(scope, element, attrs) {
            $(element).select2();
        }
    };
})*/

.directive('removethisqual',function() {
      return {
      	restrict: 'A',
        link : function(scope, element, attrs) {
            element.bind('click', function() {
            	element.parent().parent().remove()
            });
       }
   };
})

.directive('contenteditable', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      // view -> model
      elm.on('blur', function() {
        scope.$apply(function() {
          ctrl.$setViewValue(elm.html());
        });
      });

      // model -> view
      ctrl.$render = function() {
        elm.html(ctrl.$viewValue);
      };

      // load init value from DOM
      ctrl.$setViewValue(elm.html());
    }
  };
})

.directive('footablequals', function(){
  return function(scope, element){
    var footableTable = element.parents('table');


    if( !scope.$last ) {
        return false;
    }

    scope.$evalAsync(function(){

        if (! footableTable.hasClass('footable-loaded')) {
            footableTable.footable();
        }

        footableTable.trigger('footable_initialized');
        footableTable.trigger('footable_resize');
        footableTable.data('footable').redraw();

    });
  };
})
;