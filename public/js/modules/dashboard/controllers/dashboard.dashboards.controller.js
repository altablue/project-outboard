'use strict';

angular.module('dashboard').controller('theDashes', ['$scope', '$http', '$sce', 'Global', 'inputInteract', '$cookies', '$modal',

    function ($scope, $http, $sce, Global, inputInteract, $cookies, $modal) {

    	$scope.emailLoading = [];

    	$scope.job = [];

        $scope.profileCircle = function (percent)
        {
            inputInteract.circleProgress(percent);
        }

		$scope.helpFill = function(num){

            $scope.emailLoading[num] = true;

            var items = $scope.emailApi[num];

            items.searchSkills = JSON.parse(items.searchSkills.replace(/\"/g,'"'));
            items.qual = JSON.parse(items.qual.replace(/\"/g,'"'));

            var apiParams = {};

            var modalInstance = $modal.open({
              templateUrl: 'dashboardMatch.html',
              controller: 'dashboardMatch1',
              size: 'lg',
              resolve: {
                items: function () {
                  return items;
                }
              }
            });

            modalInstance.result.then(function (returnData) {
                apiParams.apiController = 'helpFillEmail';
                apiParams.data = returnData;
                apiParams.returnFlash = true;
                Global.emailApi(apiParams)
                    .then(function(data){
                        $scope.emailLoading[num] = false;
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    });
            }, function(results){
                $scope.emailLoading[num] = false;
            });

        }

        console.log($scope.job);

    }
])

.controller('dashboardMatch1',

    function ($scope, $modalInstance, items, Global) {

        $scope.browserDect = BrowserDetect;

        $scope.emailApi = items;

        $scope.ok = function () {
            $modalInstance.close($scope.emailApi);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }
)
;

function stripslashes (str) {

  return (str + '').replace(/\\(.?)/g, function (s, n1) {
    switch (n1) {
    case '\\':
      return '\\';
    case '0':
      return '\u0000';
    case '':
      return '';
    case '\"':
    	return '"';
    default:
      return n1;
    }
  });
}
