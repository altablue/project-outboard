'use strict';

angular.module('dashboard').controller('recruitEdit', ['$scope', '$http', '$sce', 'Global', 'inputInteract', '$cookies', '$modal',

    function ($scope, $http, $sce, Global, inputInteract, $cookies, $modal) {
        $scope.showList = [];

        $scope.data = {};

        $scope.cand = [];

        $scope.candStatus = ['Active', 'Inactive'];

        $scope.adStatus = ['Unfilled', 'Filled'];

        $scope.applyStats = ['Applied', 'Interview Stage', 'Unsuccessful', 'Successful']; 

        $scope.updateStatus = [];

        $scope.jobId = [];


        $scope.CandStatusChange = function(item, data, action, loopIndex, type, jobId)
        {
        	$scope.updateStatus[loopIndex] = item;
        	$scope.showList[loopIndex] = false;
        	$scope.submitForm(item, loopIndex, type, jobId);
        }

        $scope.submitForm = function(status, num, type, jobId) {

            $scope.data.returnFlash = true;
            $scope.data.userId = $scope.cand[num];
            $scope.data.status = status;
            if(typeof jobId != 'undefined'){
                $scope.data.jobId = jobId;
            }
            $scope.data.controller = "api";
            $scope.data.apiController = "update"+type+"Status";
            $scope.data.apiAction = "put";

            inputInteract.apiSubmit($scope.data)
                .then(function(data){
                    if(data.status === "success"){
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }else{
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }
                }, function(data){
                    $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
        };

    }
])
;
