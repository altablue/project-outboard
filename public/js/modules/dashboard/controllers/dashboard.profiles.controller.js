'use strict';

angular.module('dashboard').controller('theProfiles', ['$scope', '$http', '$sce', 'Global', 'inputInteract', '$cookies', '$modal',

    function ($scope, $http, $sce, Global, inputInteract, $cookies, $modal) {

    	$scope.emailLoading = false;

      $scope.vacLoading = false;

    	$scope.job = [];

  		$scope.contactThis = function(){

              $scope.emailLoading = true;

              var items = {};

              var apiParams = {};

              var modalInstance = $modal.open({
                templateUrl: 'fillModal.html',
                controller: 'fillModal',
                size: 'lg',
                resolve: {
                  items: function () {
                    return items;
                  }
                }
              });

              modalInstance.result.then(function (returnData) {
                  apiParams.apiController = 'contactRequest';
                  apiParams.data = returnData;
                  apiParams.returnFlash = true;
                  Global.emailApi(apiParams)
                      .then(function(data){
                          $scope.emailLoading = false;
                          $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                      }, function(data){
                          $scope.emailLoading = false;
                      });
              }, function(results){
                  $scope.emailLoading = false;
              });

          }

      }]

      $scope.assignVac = function(){

              $scope.emailLoading = true;

              var items = {};

              var apiParams = {};

              var modalInstance = $modal.open({
                templateUrl: 'contactModal.html',
                controller: 'contactModal',
                size: 'lg',
                resolve: {
                  items: function () {
                    return items;
                  }
                }
              });

              modalInstance.result.then(function (returnData) {
                  apiParams.apiController = 'contactRequest';
                  apiParams.data = returnData;
                  apiParams.returnFlash = true;
                  Global.emailApi(apiParams)
                      .then(function(data){
                          $scope.emailLoading = false;
                          $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                      }, function(data){
                          $scope.emailLoading = false;
                      });
              }, function(results){
                  $scope.emailLoading = false;
              });

          }

      }
])

.controller('contactModal',

    function ($scope, $modalInstance, items, Global) {

        $scope.browserDect = BrowserDetect;

        $scope.emailApi = {};

        $scope.ok = function () {
            $modalInstance.close($scope.emailApi);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }
)

.controller('fillModal',

    function ($scope, $modalInstance, items, Global) {

        $scope.browserDect = BrowserDetect;

        $scope.fillApi = {};

        $scope.ok = function () {
            $modalInstance.close($scope.fillApi);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }
)
;

function stripslashes (str) {

  return (str + '').replace(/\\(.?)/g, function (s, n1) {
    switch (n1) {
    case '\\':
      return '\\';
    case '0':
      return '\u0000';
    case '':
      return '';
    case '\"':
    	return '"';
    default:
      return n1;
    }
  });
}
