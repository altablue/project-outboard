'use strict';

angular.module('dashboard').controller('userLogin', ['$scope', '$http', '$sce', 'Global', '$cookies', 'ladda',

    function ($scope, $http, $sce, Global, $cookies, ladda) {

        $scope.submitForm = function() {
            $scope.formData.returnFlash = true;
            $scope.loading = true;
            $http.post(siteBaseUrl+'/api/1.0/'+$scope.formData.apiController, JSON.stringify($scope.formData))
                .success(function(data, status){
                    $scope.loading = false;
                    if(data.status != 'success'){
                        if(typeof $data == 'undefined'){
                            $scope.global.flashStatus = $sce.trustAsHtml(data.flash);
                        }else{
                            $scope.global.flashStatus = $sce.trustAsHtml(data.flash);
                        }
                    }else{
                        window.location.href = data.url;
                    }
                })
                .error(function(){
                    $scope.loading = false;
                    $scope.global.flashStatus = $sce.trustAsHtml('<div class="alert alert-danger">There has been a system error. Please try again in a few minutes.</div>');
                })
            ; 
        }

    }
])
;
