'use strict';

angular.module('dashboard').controller('userReg', ['$scope', '$http', '$sce', 'Global', 'inputInteract', '$cookies', '$modal', 'screenSize',

    function ($scope, $http, $sce, Global, inputInteract, $cookies, $modal, screenSize) {

        $scope.tc = {};
        $scope.formData = {};
        $scope.tableQual = [];

        $scope.inputInteract = inputInteract;

        $scope.tc.Accept = false;

        $scope.formData.count = 0;
        $scope.formData.qual = [];

        $scope.tc.Disabled = true;

        $scope.count = 0;

        var profileFields = 7;

        $scope.test = 1;

        $scope.skills = [];

        $scope.disciplines = [];

        $scope.modalInstance = '';
        
        $scope.qualFields = "";
        $scope.qual = "";

        $scope.ajaxReq= '';


        $scope.submitForm = function() {
            $scope.loading = true;
            $scope.formData.returnFlash = true;
            inputInteract.apiSubmit($scope.formData)
                .then(function(data){
                    $scope.loading = false;
                    if(data.data.status === "success"){
                        window.location.href = data.data.url;
                    }else{
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }
                }, function(data){
                    $scope.loading = false;
                    $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
        };

        $scope.setupSkills = function(skills){
            $scope.formData.candSkills = [];
            for (var property in skills) {
                if (skills.hasOwnProperty(property)) {
                    $scope.formData.candSkills.push(property);
                    $scope.skills.push(property);
                }
            }   
        }

        $scope.skillUpdate = function (item, data, action) {
            $scope.status = inputInteract.getProgress(data);
            if(action == 'delete'){
                $scope.formData.candSkills.splice(item,1);
            }else if(action == 'add'){
                $scope.formData.candSkills.push(item);
            }
            circleProgressCheck(profileFields);
        };

        $scope.discUpdate = function (item, data, action) {
            $scope.status = inputInteract.getProgress(data);
            if(action == 'delete'){
                $scope.formData.candSkills.splice(item,1);
            }else if(action == 'add'){
                $scope.formData.candSkills.push(item);
            }
            circleProgressCheck(profileFields);
        };

        $scope.setUpQuals = function(quals){
            $scope.formData.qual = quals;
            $scope.tableQual = quals;
        }

        $scope.refreshDisciplines = function(disciplines) {
            for (var property in disciplines) {
                if (disciplines.hasOwnProperty(property)) {
                    $scope.disciplines.push(property);
                }
            }
        };

        $scope.renderTable = function()
        {
            if($('#qualSaved').val() != '' && typeof $('#qualSaved').val() != 'undefined'){
                $scope.formData.qual = eval('{'+$('#qualSaved').val()+ '}');
                $scope.tableQual = eval('{'+$('#qualSaved').val()+ '}');
            }
        }

        $scope.removeQual = function(num)
        {
            var index = num;
            $scope.formData.qual.splice(index,1);
            $scope.tableQual.splice(index,1);
            $scope.loadProgress($scope.formData);
            $scope.count--;
        }

        $scope.editQual = function(num){
        
            if(typeof num !== 'undefined'){
                var index = num;
                var items = $scope.formData.qual[index];
            }else{
                var index = $scope.count+1;
                var items = { candQualTitle:"", candQualDegree:"", candQualField:""};
            }

            var modalInstance = $modal.open({
              templateUrl: 'myModalContent.html',
              controller: 'modalControl',
              size: 'lg',
              resolve: {
                items: function () {
                    console.log(JSON.stringify(items));
                  return items;
                }
              }
            });

            modalInstance.result.then(function (returnData) {
                if(typeof num == 'undefined'){$scope.count++;}
                if($scope.tableQual !== false){
                    $scope.tableQual[index] = returnData;
                }else{
                    $scope.tableQual = [];
                    $scope.tableQual[0] = returnData;
                }
                if($scope.formData.qual !== false){
                    $scope.formData.qual[index] = returnData;   
                }else{
                    $scope.formData.qual = [];
                    $scope.formData.qual[0] = returnData;
                }
                $scope.loadProgress($scope.formData);
            });

        }

        $scope.getLoc = function(){
            $scope.loading = true;
            if(geoPosition.init()){
                geoPosition.getCurrentPosition(success_callback,error_callback,{enableHighAccuracy:true});
            }else{
               $scope.geoStatus = $sce.trustAsHtml('<span class="error">Functionality not available</span>');
            }
            function success_callback(p)
            {
                var latitude = p.coords.latitude
                var longitude = p.coords.longitude
                $scope.$apply(function()
                {
                    $http.get("https://nominatim.openstreetmap.org/reverse?lat="+latitude+"&lon="+longitude+"&format=json&bounded=1&addressdetails=1&limit=1").success(function(data){
                        $scope.loading = false;
                        if(typeof data.address != 'undefined'){
                            $scope.formData.niceName = data.address.city+','+data.address.state;
                            $scope.formData.candLoc = latitude + ','+ longitude;
                        }else{
                            $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error">Unable to get Location successfully</div>');
                        }
                    })
                    .error(function(){
                        $scope.loading = false;
                        $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error">Unable to get Location successfully</div>');
                    })            
                });
            }
            function error_callback(p)
            {
                $scope.loading = false;
                $scope.geoStatus = $sce.trustAsHtml('<span class="error">' + p.message + '</span>');
            }
        }

        $scope.loadProgress = function(data) {
            if(Global.browserDect != "Explorer" && Global.browserDect.version > 8){
                $scope.status = inputInteract.getProgress(data);
                circleProgressCheck(profileFields);
            }
        }

        function circleProgressCheck(profileFields){
            var changeTest = ($scope.status.fieldsComplete/profileFields)*100;
            if($scope.circlePercent != changeTest){
                $scope.circlePercent = ($scope.status.fieldsComplete/profileFields)*100;
                setInterval(inputInteract.circleProgress($scope.circlePercent),100)
            }
        }

    }
])

.controller('editSidebar', function ($scope, $http, $sce, Global, inputInteract, $cookies, $modal, screenSize) {

    $scope.selectedTab = 'progressTab';

    $scope.select= function(item) {
        $scope.selectedTab = item;

     };

     $scope.isActive = function(item) {
        return $scope.selectedTab === item;
    };

})

.controller('modalControl',

    function ($scope, $modalInstance, items, Global) {


        $scope.browserDect = BrowserDetect;

        $scope.degrees = [];

        $scope.singleQual = items;

        $scope.refreshDegrees = function(degrees) {
            for (var property in degrees) {
                if (degrees.hasOwnProperty(property)) {
                    $scope.degrees.push(property);
                }
            }
        };

        $scope.ok = function () {
            $modalInstance.close($scope.singleQual);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }
);