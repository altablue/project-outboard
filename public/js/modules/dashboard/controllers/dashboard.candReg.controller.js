'use strict';

angular.module('dashboard').controller('candCreate', ['$scope', '$http', '$sce', 'Global', 'inputInteract', '$cookies', '$modal',

    function ($scope, $http, $sce, Global, inputInteract, $cookies, $modal) {

        $scope.loading = false;

        $scope.submitForm = function() {
            $scope.formData.returnFlash = true;
            $scope.loading = true;
            inputInteract.apiSubmit($scope.formData)
                .then(function(data){
                    $scope.loading = false;
                    if(data.data.status === "success"){
                        window.location.href = data.data.url;
                    }else{
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }
                }, function(data){
                    $scope.loading = false;
                    $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
        };

    }
])
;
