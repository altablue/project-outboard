'use strict';

angular.module('dashboard').controller('searchResults', ['$scope', '$http', '$sce', 'Global', 'inputInteract', '$cookies', '$modal', 'screenSize',

    function ($scope, $http, $sce, Global, inputInteract, $cookies, $modal, screenSize) {

        $scope.searchParams = {};
        $scope.searchResults ={};
        $scope.disciplines = [];
        $scope.searchParams.discipline = []
        $scope.maxSize = 5;
        $scope.degrees = [];
        $scope.resultsPerView = 10;
        $scope.bigTotalItems = 1;
        $scope.currentPage = 1;
        $scope.numPages = 1;
        $scope.searchParams.from = 0;
        $scope.searchParams.limit = 10;
        $scope.sortBy = [{name:'Relevance', status:true},{name:'Alphabetical - A-Z', status:true},{name:'Alphabetical - Z-A', status:true},{name:'Location - Nearest', status:false},{name:'Location - Furthest',status:false}];
        $scope.sortBySelect = {name:'Relevance'};
        $scope.searchParams.sortBy = 'Relevance';
        $scope.searchParams.qual = [{}];
        $scope.searchParams.skills = {};
        $scope.noResults = false;

        //$scope.searchParams.keyword = $location.search().keyword;

        $scope.getUrlParams = function(){
            var q= new QueryString();
            var params = {}
            var keys= q.keys().sort();
            for (var i= 0; i < keys.length; i++)
            {
                var vall= q.values(keys[i]).join(',');
                if (typeof vall !== 'undefined' && !vall.trim()){
                }else{
                    params[keys[i]] = vall;
                }
            }
            if (keys.length == 0)
                params = false;

            return params;
        }

        $scope.submitForm = function() {
            $scope.noResults = true;
            $scope.loading = true;
            $scope.searchParams.from = $scope.currentPage-1;
            $scope.searchParams.returnFlash = true;
            $scope.searchParams.apiAction = 'post';
            $scope.searchParams.filterOut = $scope.userType;
            console.log($scope.searchParams);
            inputInteract.apiSubmit($scope.searchParams)
                .then(function(data){
                    $scope.loading = false;
                    if(data.data.status === "success"){
                        if(data.data.results.hits.total !== 0){
                            $scope.noResults = false;
                            $scope.searchResults = angular.fromJson(data.data.results);
                            $scope.bigTotalItems = $scope.totalHits = data.data.results.hits.total;
                            $scope.numPages = data.data.results.hits.total/$scope.resultsPerView;
                            var from = $scope.searchParams.limit*$scope.searchParams.from;
                            if(from <= 0) from = 1;
                            $scope.curHitsRange = from+' - '+(($scope.searchParams.limit*$scope.searchParams.from)+$scope.searchParams.limit);
                        }else{
                            $scope.noResults = true;
                        }
                    }else{
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    }
                }, function(data){
                    $scope.flashStatus = $sce.trustAsHtml('<div class="alert alert-error>There has been an error. Please try your request again later.</div>')
                });
        };


        $scope.refreshDisciplines = function(disciplines) {
            for (var property in disciplines) {
                if (disciplines.hasOwnProperty(property)) {
                    $scope.disciplines.push(property);
                }
            }
        };

        $scope.setupSkills = function(skills){
            $scope.searchParams.searchSkills = [];
            console.log(JSON.stringify(skills));
            if(toType(skills) !== 'null' && skills != 'null'){
                var c = skills.length;
                for(var i=0; i < c; i++){
                    $scope.searchParams.searchSkills.push(skills[i]);
                }   
            }
        }

        $scope.sortUpdate = function(item, data, action){
            $scope.sortBySelect = data.name;
            $scope.searchParams.sortBy = data.name;
            $scope.submitForm();
        }

        $scope.skillUpdate = function (item, data, action) {
            if(action == 'delete'){
                $scope.searchParams.searchSkills.splice(item,1);
            }else if(action == 'add'){
                $scope.searchParams.searchSkills.push(item);
            }
        };

        $scope.checkLoc = function(){
            if($scope.searchParams.location.length > 0){
                $scope.sortBy[4].status = true;
                $scope.sortBy[3].status = true;
            }else{
                $scope.sortBy[4].status = false;
                $scope.sortBy[3].status = false;
            }
        }

        $scope.removeQual = function(num){
            var index = num;
            $scope.searchParams.qual.splice(index,1);
        };

        $scope.addQual = function(){
            $scope.searchParams.qual.push({});
        };

        $scope.refreshDegrees = function(degrees) {
            for (var property in degrees) {
                if (degrees.hasOwnProperty(property)) {
                    $scope.degrees.push(property);
                }
            }
        };

       $scope.helpFill = function(){

            $scope.emailLoading = true;

            var items = $scope.searchParams;

            var apiParams = {};

            var modalInstance = $modal.open({
              templateUrl: 'myModalContent2.html',
              controller: 'modalControl2',
              size: 'lg',
              resolve: {
                items: function () {
                  return items;
                }
              }
            });

            modalInstance.result.then(function (returnData) {
                apiParams.apiController = 'helpFillEmail';
                apiParams.data = returnData;
                apiParams.returnFlash = true;
                Global.emailApi(apiParams)
                    .then(function(data){
                        $scope.emailLoading = false;
                        $scope.flashStatus = $sce.trustAsHtml(data.data.flash);
                    });
            }, function(results){
                $scope.emailLoading = false;
            });

        }


        $('.tagging').on("change", function(e){
            if (e.removed) {
                skillUpdate(e,'data','delete');
            }
            if(e.added){
                skillUpdate(e,'data','add')
            }
        });

    }

])

.controller('modalControl2',

    function ($scope, $modalInstance, items, Global) {

        $scope.browserDect = BrowserDetect;

        $scope.emailApi = items;

        $scope.ok = function () {
            $modalInstance.close($scope.emailApi);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

    }
);

var toType = function(obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}