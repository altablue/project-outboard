-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 24, 2015 at 08:41 AM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `project_outboard`
--

-- --------------------------------------------------------

--
-- Table structure for table `b58q9dqtr_abstaff`
--

CREATE TABLE IF NOT EXISTS `b58q9dqtr_abstaff` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `board` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

-- --------------------------------------------------------

--
-- Table structure for table `b58q9dqtr_boards`
--

CREATE TABLE IF NOT EXISTS `b58q9dqtr_boards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(2) NOT NULL,
  `location` longtext NOT NULL,
  `name` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

-- --------------------------------------------------------

--
-- Table structure for table `b58q9dqtr_candidate`
--

CREATE TABLE IF NOT EXISTS `b58q9dqtr_candidate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` longtext NOT NULL,
  `profile` longtext NOT NULL,
  `skills` longtext NOT NULL,
  `discipline` varchar(250) NOT NULL,
  `contact` mediumtext NOT NULL,
  `status` int(2) NOT NULL,
  `board` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `availability` int(4) NOT NULL,
  `qualifications` longtext NOT NULL,
  `title` varchar(800) NOT NULL,
  `locationnice` varchar(500) NOT NULL,
  `phone` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1006 ;

-- --------------------------------------------------------

--
-- Table structure for table `b58q9dqtr_changeLog`
--

CREATE TABLE IF NOT EXISTS `b58q9dqtr_changeLog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `changelog` longtext NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b58q9dqtr_fillstats`
--

CREATE TABLE IF NOT EXISTS `b58q9dqtr_fillstats` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `jobId` int(11) NOT NULL,
  `cand` int(11) NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `b58q9dqtr_job`
--

CREATE TABLE IF NOT EXISTS `b58q9dqtr_job` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `location` longtext NOT NULL,
  `discipline` longtext NOT NULL,
  `profile` longtext NOT NULL,
  `board` int(11) NOT NULL,
  `status` int(2) NOT NULL,
  `skills` longtext NOT NULL,
  `qualifications` longtext NOT NULL,
  `locationnice` varchar(500) NOT NULL,
  `availability` varchar(250) NOT NULL,
  `title` varchar(500) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

-- --------------------------------------------------------

--
-- Table structure for table `b58q9dqtr_users`
--

CREATE TABLE IF NOT EXISTS `b58q9dqtr_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(300) NOT NULL,
  `password` varchar(500) NOT NULL,
  `type` int(2) NOT NULL,
  `level` int(11) NOT NULL,
  `board` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `profileCreated` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=101 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
