// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var autoprefixer= require('gulp-autoprefixer');
var rename = require('gulp-rename');
var notify = require('gulp-notify');
var cache = require('gulp-cache');
var del = require('del');
var minifycss = require('gulp-minify-css');
var iconify = require('gulp-iconify');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');

// Lint Task
gulp.task('lint', function() {
    return gulp.src('public/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('public/scss/*.scss')
        .pipe(sass(
            { 
                style: 'expanded',
                errLogToConsole: false,
                onError: function(err) {
                    return notify().write(err);
                }
            }
        ))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(gulp.dest('public/css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss({ compatibility: 'ie8', noAdvanced: true }))
        .pipe(gulp.dest('public/css'))
        .pipe(notify({ message: 'Styles task complete' }));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src([
        'public/js/libs/jquery/jquery.min.js',
        'public/js/libs/angular/angular/angular.min.js',
        'public/js/libs/angular/angular-resource/angular-resource.min.js',
        'public/js/libs/angular/angular-cookies/angular-cookies.min.js',
        'public/js/libs/angular/angular-animate/angular-animate.min.js',
        'public/js/libs/angular/angular-touch/angular-touch.min.js',
        'public/js/libs/angular/angular-route/angular-route.min.js',
        'public/js/libs/angular/angular-sanitize/angular-sanitize.min.js',
        'public/js/libs/angular/angular-ui-router/release/angular-ui-router.min.js',
        'public/js/libs/angular/angular-ui-utils/ui-utils.min.js',
        'public/js/libs/angular/angular-bootstrap/ui-bootstrap-tpls.min.js',
        'public/js/libs/angular/angular-ui-select/dist/select.min.js',
        'public/js/libs/angular/ladda/dist/spin.min.js',
        'public/js/libs/angular/angularjs-placeholder/src/angularjs-placeholder.js',
        'public/js/libs/angular/ladda/js/ladda.js',
        'public/js/libs/angular/angular-ladda/dist/angular-ladda.min.js',
        'public/js/libs/angular/footable/dist/footable.all.min.js',
        'public/js/libs/angular/angular-footable/dist/angular-footable.min.js',
        'public/js/libs/angular/angular-media-queries/match-media.js',
        'public/js/config.js',
        'public/js/application.js',
        'public/js/modules/**/*.js'])
        .pipe(concat('all.js'))
        .pipe(gulp.dest('public/js/dist'))
});

gulp.task('ie8scripts', function() {
    return gulp.src([
        'public/js/libs/angular/angular-resource/angular-resource.min.js',
        'public/js/libs/angular/angular-cookies/angular-cookies.min.js',
        'public/js/libs/angular/angular-animate/angular-animate.min.js',
        'public/js/libs/angular/angular-touch/angular-touch.min.js',
        'public/js/libs/angular/angular-route/angular-route.min.js',
        'public/js/libs/angular/angular-sanitize/angular-sanitize.min.js',
        'public/js/libs/angular/angular-ui-router/release/angular-ui-router.min.js',
        'public/js/libs/angular/angular-ui-utils/ui-utils.min.js',
        'public/js/libs/angular/angular-bootstrap/ui-bootstrap-tpls.js',
        'public/js/libs/angular/angular-ui-select/dist/select.ie8.js',
        'public/js/libs/angular/ladda/dist/spin.min.js',
        'public/js/libs/angular/angularjs-placeholder/src/angularjs-placeholder.js',
        'public/js/libs/angular/ladda/js/ladda.js',
        'public/js/libs/angular/angular-ladda/dist/angular-ladda.min.js',
        'public/js/libs/angular/footable/dist/footable.all.min.js',
        'public/js/libs/angular/angular-footable/dist/angular-footable.min.js',
        'public/js/libs/angular/angular-media-queries/match-media.js',
        'public/js/config.js',
        'public/js/application.js',
        'public/js/modules/**/*.js',
        'public/js/ie8/plugins.js',])
        .pipe(concat('ie8.all.js'))
        .pipe(gulp.dest('public/js/dist'))
        .pipe(rename('ie8.all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js/dist'));
});

gulp.task('images', function() {
  return gulp.src('/public/imgs')
    .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
    .pipe(gulp.dest('/public/imgs'))
    .pipe(notify({ message: 'Images task complete' }));
});

gulp.task('svgs', function() {
    return iconify({
        errLogToConsole: false,
        onError: function(err) {
            return notify().write(err);
        },
        src: 'public/imgs/svgs/*.svg',
        pngOutput: 'public/imgs/svgs-pngs',
        scssOutput: 'public/scss/svgs',
        cssOutput:  'public/css/svgs'
    })
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('public/js/*.js', ['lint', 'scripts']);
    gulp.watch('public/scss/*.scss', ['sass']);
    gulp.watch('public/imgs/svgs/*.svg', ['svgs']);
    gulp.watch('public/imgs/*', ['images']);
});

// Default Task
gulp.task('default', ['lint', 'sass', 'svgs', 'images', 'watch']);